( function( window, $ ) {

// CONSIDER NEXT:
// - Save in DB for each user instead of localstorage?
// - Shared clips across users? Or allow to import from global list of favorites?
// - Frontend editor
// - VC History: Allow to Redo/Undo changes


function clips() {
  var self = this;

  if ( ! self.hasLocalStorage() ) {
    window.console && console.log && console.log( 'localStorage not supported' );
    return false;
  }

  $( function onDomReady() {

    // vc is a global variable
    if ( window.vc ) {
      // Fires once when VC inits for the first time
      vc.events.once( 'app.addAll', self.init.bind( self ) );
      vc.events.once( 'shortcodeView:ready', self.init.bind( self ) );
    }
  });
}

clips.prototype.translate = function clipsI18n( text ) {
  return this.i18n[ text ] ? this.i18n[ text ] : text;
};

clips.prototype.init = function clipsInit() {
  if ( this.initialized ) return;
  this.initialized = true;

  var self = this;
  var jqVisualComposerContent = $( '#visual_composer_content' );

  this.maxRecentItems = window.cptClips ? cptClips.recent_clips_limit  : 50;
  this.i18n = window.cptClips ? cptClips.i18n : {};

  this.jqClipRowBtn = $( '<a href="#" class="vc_control cpt-clip-btn cpt-clip-btn-paste" title="' + this.translate( 'Paste from clipboard' ) + '"><i class="vc-composer-icon dashicons-before dashicons-clipboard"></i></a><a href="#" class="vc_control cpt-clip-btn cpt-clip-btn-copy" title="' + this.translate( 'Copy to clipboard' ) + '"><i class="vc-composer-icon dashicons-before dashicons-pressthis cpt-clip-pin"></i><span class="cpt-clip-copied">' + this.translate( 'Copied' ) + '</span></a>' );
  this.jqClipContainerBtn = $( '<a href="#" class="vc_control cpt-clip-btn cpt-clip-btn-copy" title="' + this.translate( 'Copy to clipboard' ) + '"><i class="vc-composer-icon dashicons-before dashicons-pressthis cpt-clip-pin"></i><span class="cpt-clip-copied">' + this.translate( 'Copied' ) + '</span></a><a href="#" class="vc_control cpt-clip-btn cpt-clip-btn-paste" title="' + this.translate( 'Paste from clipboard' ) + '"><i class="vc-composer-icon dashicons-before dashicons-clipboard"></i></a>' );
  this.jqClipElementBtn = $( '<a href="#" class="vc_control-btn cpt-clip-btn cpt-clip-btn-copy" title="' + this.translate( 'Copy to clipboard' ) + '"><span class="vc_btn-content"><i class="dashicons-before dashicons-pressthis cpt-clip-pin"></i></span><span class="cpt-clip-copied">' + this.translate( 'Copied' ) + '</span></a><a href="#" class="vc_control-btn cpt-clip-btn cpt-clip-btn-paste" title="' + this.translate( 'Paste from clipboard' ) + '"><span class="vc_btn-content"><i class="dashicons-before dashicons-clipboard"></i></span></a>' );
  this.jqClipNonEmptyContainerBtn = $( '<a href="#" class="vc_add-element-not-empty-button cpt-clip-btn cpt-clip-btn-paste" title="' + this.translate( 'Paste from clipboard' ) + '"><span class="vc_btn-content"><i class="dashicons-before dashicons-clipboard"></i></span></a>' );
  this.jqClipEmptyContainerBtn = $( '<a class="vc_general vc_ui-button vc_ui-button-shape-rounded vc_ui-button-info cpt-clip-btn cpt-clip-btn-paste" href="#"><i class="dashicons-before dashicons-clipboard"></i><span>' + this.translate( 'Paste from clipboard' ) + '</span></a>' );
  this.jqClipDialog = $( '<div class="cpt-clip-dialog"><div class="cpt-clip-dialog-header"><span class="cpt-clip-dialog-header-title">' + this.translate( 'Clips' ) + '</span><i class="dashicons-before dashicons-no cpt-clip-dialog-btn-close"></i></div><div class="cpt-clip-dialog-notifications"></div><div class="cpt-clip-dialog-body"><div class="cpt-clip-dialog-panel active" rel="recent"></div><div class="cpt-clip-dialog-panel" rel="favorites"></div><div class="cpt-clip-dialog-panel" rel="import"><div class="cpt-clip-dialog-import-section-start"><div class="cpt-clip-dialog-import-info">' + this.translate( 'Paste exported clip code here' ) + ':</div><input type="text" class="cpt-clip-dialog-import-input" /><span class="cpt-clip-dialog-import-start-btn">' + this.translate( 'Start import' ) + '</span></div><div class="cpt-clip-dialog-import-section-finish"><div class="cpt-clip-dialog-import-details"></div><span class="cpt-clip-dialog-import-finish-btn">' + this.translate( 'Import' ) + '</span></div></div></div><div class="cpt-clip-dialog-tools"><i class="dashicons-before dashicons-backup cpt-clip-dialog-tools-btn active" rel="recent" title="' + this.translate( 'Recent' ) + '"></i><i class="dashicons-before dashicons-heart cpt-clip-dialog-tools-btn" rel="favorites" title="' + this.translate( 'Favorites' ) + '"></i><i class="dashicons-before dashicons-download cpt-clip-dialog-tools-btn" rel="import" title="' + this.translate( 'Import' ) + '"></i></div>' );
  this.jqClipDialogOverlay = $( '<div class="cpt-clip-dialog-overlay"></div>' );
  this.jqNotification = $( '<div class="cpt-clip-dialog-notification"></div>' );
  this.jqNotifications = this.jqClipDialog.find( '.cpt-clip-dialog-notifications' );

  this.exportImportCodePrefix = 'cpt_clip:';

  this.dialogItemTemplates = {
    recent: '<div class="cpt-clip-dialog-item" rel="{uid}">{icon}<i class="dashicons-before dashicons-no cpt-clip-dialog-item-btn" rel="remove" title="' + this.translate( 'Remove' ) + '"></i><i class="dashicons-before dashicons-share-alt2 cpt-clip-dialog-item-btn" rel="export" title="' + this.translate( 'Export' ) + '"></i><i class="dashicons-before dashicons-heart cpt-clip-dialog-item-btn" rel="favorites" title="' + this.translate( 'Add to favorites' ) + '"></i><div class="cpt-clip-dialog-item-title-wrapper" title="{title}"><span class="cpt-clip-dialog-item-title">{title}</span></div></div>',
    favorites: '<div class="cpt-clip-dialog-item" rel="{uid}">{icon}<i class="dashicons-before dashicons-no cpt-clip-dialog-item-btn" rel="remove" title="' + this.translate( 'Remove' ) + '"></i><i class="dashicons-before dashicons-share-alt2 cpt-clip-dialog-item-btn" rel="export" title="' + this.translate( 'Export' ) + '"></i><i class="dashicons-before dashicons-edit cpt-clip-dialog-item-btn" rel="edit" title="' + this.translate( 'Rename' ) + '"></i><i class="dashicons-before dashicons-yes cpt-clip-dialog-item-btn" rel="update" title="' + this.translate( 'Update' ) + '"></i><div class="cpt-clip-dialog-item-title-wrapper" title="{title}"><input type="text" class="cpt-clip-dialog-item-title" value="{title}"></div></div>'
  };
  this.jqClipDialog.draggable( { handle: '.cpt-clip-dialog-header' } ).css( 'position', 'absolute' );

  // Add paste buttons to empty and non-empty containers
  $( '#vc_not-empty-add-element' ).after( this.jqClipNonEmptyContainerBtn );
  $( '#vc_templates-more-layouts' ).after( this.jqClipEmptyContainerBtn );

  // Setup events
  vc.events.on( 'shortcodes:add', function onAddShortcode( model ) {
    self.addControls( model.view.$el.find( '.vc_controls' ).first() );
  });
  vc.events.on( 'shortcodes:update', function onUpdateShortcode( model) {
    self.addControls( model.view.$el.find( '.vc_controls' ).first() );
  });

  jqVisualComposerContent.on( 'click', '.cpt-clip-btn-copy', function onCopyButtonClick( e ) {
    e.preventDefault();

    var jqIcon = $( this ).find( '.cpt-clip-pin' );
    jqIcon.addClass( 'cpt-animation-copied animated' );
    setTimeout( function() {
      jqIcon.removeClass( 'cpt-animation-copied animated' );
    }, 1100 );

    self.copy( jqIcon.closest( '[data-element_type]' ).attr( 'data-model-id' ) );
  } );


  $( '#vc_no-content-helper' ).on( 'click', '.cpt-clip-btn-paste', function onPasteButtonClick( e ) {
    e.preventDefault();
    self.initPaste( $( this ) );
  } );
  jqVisualComposerContent.on( 'click', '.cpt-clip-btn-paste', function onPasteButtonClick( e ) {
    e.preventDefault();
    self.initPaste( $( this ) );
  } );


  // Add controls on init
  jqVisualComposerContent.find( '.vc_controls' ).each( function iterateVcControls() {
    self.addControls( $( this ) );
  });

  // Add popup
  this.jqClipDialogOverlay.appendTo( $( 'body' ) );
  this.jqClipDialog.appendTo( $( 'body' ) );

  // Close popup
  this.jqClipDialogOverlay.on( 'click', function( e ) {
    self.hidePasteDialog();
  } );
  this.jqClipDialog.on( 'click', '.cpt-clip-dialog-btn-close', function( e ) {
    self.hidePasteDialog();
  } );
  $( document ).keyup(function onKeyUp( e ) {
    if ( ! self.jqClipDialog.is( ':visible' ) ) {
      return;
    }

    switch ( e.keyCode ) {
      // Enter
      case 13:
        // Update item if was in edit mode
        self.jqClipDialog.find( '.cpt-clip-dialog-item.edit-mode .cpt-clip-dialog-item-btn[rel="update"]' ).click(); 
      break;
      // Escape
      case 27:
        self.hidePasteDialog();
      break;
    }
  });  

  // Switch panels
  this.jqClipDialog.on( 'click', '.cpt-clip-dialog-tools-btn', function( e ) {
    e.preventDefault();
    e.stopPropagation();
    e.stopImmediatePropagation();

    // Update buttons
    $( '.cpt-clip-dialog-tools-btn.active' ).removeClass( 'active' );
    $( this ).addClass( 'active' );

    // Switch panels
    var type = $( this ).attr( 'rel' );
    if ( 'import' !== type ) {
      self.refreshDialogList( type );
    }
    else {
      self.initImportPanel();
    }

    $( '.cpt-clip-dialog-panel.active' ).removeClass( 'active' );
    $( '.cpt-clip-dialog-panel[rel="' + type + '"]' ).addClass( 'active' );
  } );


  // Actions
  this.jqClipDialog.on( 'click', '.cpt-clip-dialog-item-btn', function onDialogItemAction( e ) {
    e.preventDefault();
    e.stopPropagation();
    e.stopImmediatePropagation();


    var action = $( this ).attr( 'rel' );
    var jqItem = $( this ).closest( '.cpt-clip-dialog-item' );
    var uid = jqItem.attr( 'rel' );
    var jqPanel = $( this ).closest( '.cpt-clip-dialog-panel' );
    var panelType = jqPanel.attr( 'rel' );

    switch ( action ) {
      case 'edit':
        self.jqClipDialog.find( '.cpt-clip-dialog-item.edit-mode' ).removeClass( 'edit-mode' );
        window.jqItem = jqItem;
        jqItem.addClass( 'edit-mode' );
        jqItem.find( '.cpt-clip-dialog-item-title' ).first().get( 0 ).select();
      break;
      case 'update':
        jqItem.removeClass( 'edit-mode' );
        window.getSelection().removeAllRanges();
        jqItem.find( '.cpt-clip-dialog-item-title' ).attr( 'title', jqItem.find( '.cpt-clip-dialog-item-title' ).val() );
        self.updateClip( panelType, uid, 'title', jqItem.find( '.cpt-clip-dialog-item-title' ).val() );
      break;
      case 'favorites':
        $( this ).toggleClass( 'active' );
        if ( self.moveToFavorites( uid ) ) {
          jqPanel.find( '.cpt-clip-dialog-item[rel="' + uid + '"]' ).find( '.cpt-clip-dialog-item-btn[rel="remove"]' ).click();
        }
      break;
      case 'remove':
        self.removeClip( panelType, uid );
        jqItem.slideUp( 200, function() {
          jqItem.remove();
        });
      break;
      case 'export':
        self.exportItem( panelType, uid );
      break;
    }

  } );

  // Paste
  this.jqClipDialog.on( 'click', '.cpt-clip-dialog-item-title-wrapper', function onDialogItemPaste( e ) {
    e.preventDefault();
    e.stopPropagation();
    e.stopImmediatePropagation();

    var jqItem = $( this ).closest( '.cpt-clip-dialog-item' );
    if ( jqItem.hasClass( 'edit-mode' ) ) {
      return;
    }
    var jqPanel = $( this ).closest( '.cpt-clip-dialog-panel' );
    var panelType = jqPanel.attr( 'rel' );

    var uid = jqItem.attr( 'rel' );
    var clipItems = self.load();

    var clip = _.find( clipItems[ panelType ], function( item ) {
      return item.uid === uid;
    } );

    if ( ! clip ) {
      self.displayNotification( 'error', this.translate( 'Clip not found' ) );
      return false;
    }

    self.paste( self.jqClipDialog.data( 'pasteToModel' ), clip.shortcodes  );
  } );


  // Import
  this.jqClipDialog.on( 'click', '.cpt-clip-dialog-import-start-btn', function onImportStartClick( e ) {
    e.preventDefault();
    self.startImport();
  } );

  this.jqClipDialog.on( 'click', '.cpt-clip-dialog-import-finish-btn', function onImportFinishClick( e ) {
    e.preventDefault();
    self.finishImport();
  } );

};

clips.prototype.initImportPanel = function clipsInitImportPanel() {
};

clips.prototype.finishImport = function clipsFinishImport() {
  this.jqClipDialog.find( '.cpt-clip-dialog-import-section-finish' ).slideUp();
  this.jqClipDialog.find( '.cpt-clip-dialog-import-details' ).html( '' );

  if ( this.importedClip ) {
    this.importItem( this.importedClip );
    this.importedClip = false;
  }
  else {
    this.displayNotification( 'info', this.translate( 'Nothing  to import' ) );
    return;
  }
};

clips.prototype.startImport = function clipsStartImport() {
  this.importedClip = this.parseImportExportCode( this.jqClipDialog.find( '.cpt-clip-dialog-import-input' ).val() );
  this.jqClipDialog.find( '.cpt-clip-dialog-import-input' ).val( '' );
  if ( ! this.importedClip ) {
    return;
  }

  this.jqClipDialog.find( '.cpt-clip-dialog-import-details' ).html( 'Do you want to import clip "' + this.importedClip.title + '"?' );
  this.jqClipDialog.find( '.cpt-clip-dialog-import-section-finish' ).slideDown();
};

clips.prototype.parseImportExportCode = function clipsParseImportExportCode( code ) {
  if ( ! this.isImportExportCodeHasValidPrefix( code ) ) {
    this.displayNotification( 'error', this.translate( 'Invalid clip code' ) );
    return false;
  }

  code = code.substr( this.exportImportCodePrefix.length );
  var clipItem;
  try {
    clipItem = JSON.parse( window.atob( code ) );
  }
  catch ( err ) {
    this.displayNotification( 'error', this.translate( 'Invalid clip code' ) );
    return false;
  }

  if ( ! clipItem.title || ! clipItem.shortcodes || ! _.isArray( clipItem.shortcodes ) || ! clipItem.uid ) {
    this.displayNotification( 'error', this.translate( 'Clip is invalid' ) );
    return false;
  }

  return clipItem;
};

clips.prototype.isImportExportCodeHasValidPrefix = function clipsIsImportExportCodeHasValidPrefix( code ) {
  return _.isString( code ) && ( 0 === code.indexOf( this.exportImportCodePrefix ) );
};

clips.prototype.importItem = function clipsExportItem( item ) {
  if ( this.addClip( 'recent', item.title, item.shortcodes ) ) {
    this.displayNotification( 'success', this.translate( 'Clip has been successfully imported!' ) );
  }
  else {
    this.displayNotification( 'error', this.translate( 'Something went wrong. Clip can not be imported!' ) );
  }
};

clips.prototype.exportItem = function clipsExportItem( listType, uid ) {
  var clipItems = this.load();

  var list = clipItems[ listType ];
  var item = _.find( list, function( item ) {
    return item.uid === uid;
  } );
  
  if ( ! item ) {
    this.displayNotification( 'error', this.translate( 'Clip not found' ) );
    return;
  }


  var exportCode;
  try {
    exportCode = this.exportImportCodePrefix + window.btoa( JSON.stringify( item ) );
  }
  catch ( err ) {
    this.displayNotification( 'error', this.translate( 'There was an error exporting clip: ' ) + err );
    return;
  }

  // If supports copy API, do this transparently and automatically
  // otherwise, show input field with export code
  if ( this.copyToClipboard( exportCode ) ) {
    this.displayNotification( 'success', this.translate( 'Clip "{title}" has been successfully exported and copied to your clipboard. You can now import it into another place.' ).replace( '{title}', item.title ) );
  }
  else {
    this.displayNotification( 'success', this.translate( 'Clip "{title}" has been exported. Copy the following export code below and import into another place' ).replace( '{title}', item.title ) + ' <input type="text" class="cpt-clip-dialog-item-export-input" value="' + exportCode + '">' );
  }

};

clips.prototype.copyToClipboard = function clipsCopyToClipboard( text ) {
  if ( ! this.isCopyApiSupported() ) {
    return false;
  }

  var succeeded = false;
  var isRTL = document.documentElement.getAttribute( 'dir' ) == 'rtl';
  var yPosition = window.pageYOffset || document.documentElement.scrollTop;
  var textarea = document.createElement( 'textarea' );

  textarea.style.fontSize = '12pt';
  textarea.style.border = '0';
  textarea.style.padding = '0';
  textarea.style.margin = '0';
  textarea.style.position = 'absolute';
  textarea.style[ isRTL ? 'right' : 'left' ] = '-9999px';
  textarea.style.top = yPosition + 'px';
  textarea.setAttribute( 'readonly', '' );
  textarea.value = text;

  document.body.appendChild( textarea );
  textarea.select();

  try {
    succeeded = document.execCommand( 'copy' );
  }
  catch ( err ) {
    succeeded = false;
  }  

  window.getSelection().removeAllRanges();
  document.body.removeChild( textarea );

  return succeeded;
};

clips.prototype.isCopyApiSupported = function clipsIsCopyApiSupported() {
  return !! document.queryCommandSupported && !! document.queryCommandSupported( 'copy' );
};

clips.prototype.initPaste = function clipsInitPaste( jqButton ) {
  var self = this;

  var modelId = jqButton ? jqButton.closest( '[data-element_type]' ).attr( 'data-model-id' ) : false;
  var model = modelId ? window.vc.app.views[ modelId ].model : false;

  this.jqClipDialog.data( 'pasteToModel', model );
  this.showClipDialog( jqButton );
};

clips.prototype.showClipDialog = function clipsShowClipDialog( jqForElement ) {
  var self = this;

  // Prevent more than one dialog at once
  if ( this.jqClipDialog.is( ':visible' ) ) {
    this.hidePasteDialog();
    return;
  }

  // Update items
  this.refreshDialogList( 'recent' );
  this.refreshDialogList( 'favorites' );

  var top, left;
  var width = this.jqClipDialog.width();
  var height = this.jqClipDialog.height();

  // Position dialog
  if ( jqForElement && jqForElement.length ) {
    jqForElement = jqForElement.first();
    top = jqForElement.offset().top - height / 2;
    left = jqForElement.offset().left - width / 2;
  }
  // Position it just in the middle of the screen
  else {
    top = $( document ).scrollTop() + $( window ).height() / 2 - height / 2;
    left = $( window ).width() / 2 - width / 2;
  }

  // If cut on top
  if ( top < $( document ).scrollTop() ) {
    top += height / 2;
  }
  // If cut at bottom
  else if ( top + height > $( document ).scrollTop() + $( window ).height() ) {
    top -= height / 2;
  }
  this.jqClipDialog.css( 'top', top + 'px' );
  this.jqClipDialog.css( 'left', left + 'px' );

  // Display
  this.jqClipDialogOverlay.fadeIn( 300 );
  this.jqClipDialog.fadeIn( 300 );
};

clips.prototype.hidePasteDialog = function clipsHidePasteDialog() {
  this.jqClipDialogOverlay.fadeOut( 200 );
  this.jqClipDialog.fadeOut( 200 );
};

clips.prototype.replaceAll = function clipsReplaceAll( subject, search, replacement ) {
  return subject.replace( new RegExp( search, 'g' ), replacement );
};

clips.prototype.getUnregisteredShortcodes = function clipsGetUnregisteredShortcodes( shortcodes ) {
  var shortcodeMapped;
  shortcodes = _.isArray( shortcodes ) ? shortcodes : [];
  shortcodes = _.filter( shortcodes, function iterateShortcodes( shortcode ) {
    shortcodeMapped = vc.getMapped( shortcode ? shortcode.shortcode : '' );
    return ! shortcodeMapped || ! shortcodeMapped.name;
  } );
  shortcodes = _.map( shortcodes, function( item ) { return item.shortcode; } );
  return shortcodes;
};

clips.prototype.refreshDialogList = function clipsRefreshDialogList( listType ) {
  this.dialogClipItems = this.load();
  var self = this;

  var template = this.dialogItemTemplates[ listType ];

  var items = this.dialogClipItems[ listType ];
  var panelHtml = '';
  var unregisteredShortcodes;
  _.each( items, function iterateItems( clip ) {
    var itemHtml = template;
    
    itemHtml = self.replaceAll( itemHtml, '{uid}', clip.uid );
    itemHtml = self.replaceAll( itemHtml, '{title}', clip.title );

    // Icon
    var shortcodeMapped, shortcode = _.isArray( clip.shortcodes ) && clip.shortcodes[ 0 ] ? clip.shortcodes[ 0 ].shortcode : false;
    var icon = '';
    if ( shortcode ) {
      shortcodeMapped = vc.getMapped( shortcode );
      if ( shortcodeMapped.name ) {
        icon = shortcodeMapped.icon ? shortcodeMapped.icon : '';

        if ( icon.match( /\.(png|jpg|jpeg)/i ) ) {
          icon = '<img class="cpt-clip-dialog-item-icon" src="' + icon + '" title="' + self.translate( 'Drag to reorder' ) + '" />';
        }
        else {
          icon = '<i class="cpt-clip-dialog-item-icon vc_general vc_element-icon ' + icon + '" title="' + self.translate( 'Drag to reorder' ) + '"></i>';
        }

        // Fix icons for Salient theme
        if ( icon.indexOf( 'icon-wpb-' ) != -1 ) {
          icon = '<span class="wpb-layout-element-button">' + icon + '</span>';
        }

        unregisteredShortcodes = self.getUnregisteredShortcodes( clip.shortcodes );
        if ( unregisteredShortcodes.length ) {
          icon += '<i class="cpt-clip-dialog-item-icon cpt-clip-icon-red dashicons-before dashicons-warning" title="' + self.translate( 'This clip contains unrecognized shortcodes, which will be skipped: ' ) + unregisteredShortcodes.join( ', ' ) + '"></i>';
        }
        
      }
      else {
        icon = '<i class="cpt-clip-dialog-item-icon cpt-clip-icon-red dashicons-before dashicons-dismiss" title="' + self.translate( 'This clip can not be used, because its main shortcode is unrecognized: ' ) + shortcode + '"></i>';
      }

      itemHtml = self.replaceAll( itemHtml, '{icon}', icon );

      panelHtml += itemHtml;
    }
  } );

  var jqList = this.jqClipDialog.find( '.cpt-clip-dialog-panel[rel="' + listType + '"]' );
  
  jqList.html( panelHtml );

  jqList.sortable({
    items: '> .cpt-clip-dialog-item',
    handle: '.cpt-clip-dialog-item-icon',
    update: function onClipsListSortUpdated( event, ui ) {
      var reorderedList = [];
      jqList.children( '.cpt-clip-dialog-item' ).each( function iterateClips() {
        var uid = $( this ).attr( 'rel' );
        var clip = _.find( items, function( item ) {
          return item.uid === uid;
        } );

        if ( clip ) {
          reorderedList.push( clip );
        }

      } );
      self.dialogClipItems[ listType ] = reorderedList;
      self.save( self.dialogClipItems );
    }
  });
};

clips.prototype.displayNotification = function clipsDisplayNotification( type, message, dismissable ) {
  var jqNotification = this.jqNotification.clone();
  jqNotification.addClass( 'cpt-clip-dialog-notification-' + type );
  jqNotification.html( message );
  jqNotification.appendTo( this.jqNotifications );
  jqNotification.slideDown( 300 );

  // Manually dismissable
  if ( dismissable ) {
    jqNotification.addClass( dismissable );
    jqNotification.prepend( '<i class="dashicons-before dashicons-no cpt-clip-dialog-notification-dismiss" title="' + this.translate( 'Dismiss' )  + '"></i>' );
    jqNotification.one( 'click', '.cpt-clip-dialog-notification-dismiss', function( e ) {
      e.preventDefault();
      jqNotification.off();
      jqNotification.slideUp( 200, function() {
        jqNotification.remove();
      }); 
    } );
  }
  // Automatically removed
  else {
    setTimeout( function() {
      jqNotification.slideUp( 200, function() {
        jqNotification.remove();
      }); 
    }, message.length * 100 );
  }

};


clips.prototype.paste = function clipsPaste( targetModel, shortcodes ) {
  if ( ! $.isArray( shortcodes ) || ! shortcodes.length ) {
    return;
  }

  var targetShortcode = targetModel ? targetModel.get( 'shortcode' ) : false;
  var targetShortcodeMapped = targetShortcode ? vc.getMapped( targetModel.get( 'shortcode' ) ) : false;
  var targetShortcodeNames = targetShortcodeMapped ? [ targetShortcode, targetShortcodeMapped.base ] : false;

  var asParentOnly = false, asParentExcept = false;
  if ( targetShortcodeMapped  && targetShortcodeMapped.as_parent ) {
    if ( targetShortcodeMapped.as_parent.only ) {
      asParentOnly = targetShortcodeMapped.as_parent.only.split( ',' );
      asParentOnly = _.map( asParentOnly, $.trim );
      asParentOnly = _.filter( asParentOnly, function( item ) { return item ? true : false } );
      asParentOnly = asParentOnly.length ? asParentOnly : false;
    }
    if ( targetShortcodeMapped.as_parent.except ) {
      asParentExcept = targetShortcodeMapped.as_parent.except.split( ',' );
      asParentExcept = _.map( asParentExcept, $.trim );
      asParentExcept = _.filter( asParentExcept, function( item ) { return item ? true : false } );
      asParentExcept = asParentExcept.length ? asParentExcept : false;
    }
  }


  var rootItem = shortcodes.shift();
  var rootItemMapped = vc.getMapped( rootItem.shortcode );
  if ( ! rootItemMapped ) return false;
  var rootItemNames = [ rootItem.shortcode, rootItemMapped.base ];

  var asChildOnly = false, asChildExcept = false;
  if ( rootItemMapped.as_child ) {
    if ( rootItemMapped.as_child.only ) {
      asChildOnly = rootItemMapped.as_child.only.split( ',' );
      asChildOnly = _.map( asChildOnly, $.trim );
      asChildOnly = _.filter( asChildOnly, function( item ) { return item ? true : false } );
      asChildOnly = asChildOnly.length ? asChildOnly : false;
    }
    if ( rootItemMapped.as_child.except ) {
      asChildExcept = rootItemMapped.as_child.except.split( ',' );
      asChildExcept = _.map( asChildExcept, $.trim );
      asChildExcept = _.filter( asChildExcept, function( item ) { return item ? true : false } );
      asChildExcept = asChildExcept.length ? asChildExcept : false;
    }
  }


  var action = 'unknown';


  // Pasting row
  if ( _.contains( rootItemNames, 'vc_row' ) ) {

    // Place after row
    if ( ! targetShortcode ) {
      action = 'AppendToRoot';
      rootItem.parent_id = false;
      delete rootItem.order;
    }
    else if ( _.contains( targetShortcodeNames, 'vc_row' ) ) {
      action = 'InsertAfterAsRoot';
      rootItem.parent_id = false;
    }
    else {
      this.displayNotification( 'error', this.translate( 'Row can be only inserted after another Row.' ) );
      return false;
    }

  }
  // Pasting inner row
  else if ( _.contains( rootItemNames, 'vc_row_inner' ) ) {

    // Placing as root element
    // TODO: Consider re-structuring vc_row_inner with its vc_column_inner to vc_row and vc_column
    if ( ! targetModel || ! targetModel.get( 'parent_id' ) ) {
      this.displayNotification( 'error', this.translate( 'Inner Row can not be pasted as root element.' ) );
      return false;
    }
    // Place after inner row or adding to non-container element
    else if ( _.contains( targetShortcodeNames, 'vc_row_inner' ) || ! targetShortcodeMapped.is_container ) {
      action = 'AddToParent';
    }
    // Append to container
    else if ( targetShortcodeMapped.is_container ) {
      action = 'Append';
    }
    // Otherwise, prohibited
    else {
      this.displayNotification( 'error', this.translate( 'Inner Row can be only inserted after another Inner Row or added to Column.' ) );
      return false;
    }

  }
  // Pasting column
  else if ( _.contains( rootItemNames, 'vc_column' ) ) {

    // Place inside Row
    if ( _.contains( targetShortcodeNames, 'vc_row' ) ) {
      action = 'Append';
    }
    // Append contents to Column
    else if ( _.contains( targetShortcodeNames, 'vc_column' ) ) {
      action = 'Merge';
    }
    // Invalid
    else {
      this.displayNotification( 'error', this.translate( 'Column can be only appended to Row or merged with another Column.' ) );
      return false;
    }

  }
  // Pasting Inner Column
  else if ( _.contains( rootItemNames, 'vc_column_inner' ) ) {

    // Place inside Inner Row
    if ( _.contains( targetShortcodeNames, 'vc_row_inner' ) ) {
      action = 'Append';
    }
    // Merge inner columns
    else if ( _.contains( targetShortcodeNames, 'vc_column_inner' ) ) {
      action = 'Merge';
    }
    // Merge inner column with column
    else if ( _.contains( targetShortcodeNames, 'vc_column' ) ) {
      action = 'Merge';
    }
    // Invalid
    else {
      this.displayNotification( 'error', this.translate( 'Inner Column can be only appended to Inner Row or merged with another Column or Inner Column.' ) );
      return false;
    }
  }
  // Other cases: not vc_row, vc_row_inner, vc_column, vc_column_inner
  else {

    // If appending new shortcode after all shortcodes as root
    if ( ! targetShortcode ) {
      action = 'AppendToRoot';

      // Wrap in a vc_row and vc_column
      var wrapperRowShortcodeParams = {
        shortcode: 'vc_row',
        id: vc_guid(),
        // order: 0, // Will be calculated later
        parent_id: false, // Place to content root
        cloned: false
      };
      var wrapperColumnShortcodeParams = {
        shortcode: 'vc_column',
        id: vc_guid(),
        order: 0,
        parent_id: wrapperRowShortcodeParams.id,
        cloned: false
      };

      // Flush root item order
      rootItem.order = 0;
      rootItem.parent_id = wrapperColumnShortcodeParams.id;

      // Push back the root item
      shortcodes.unshift( rootItem );

      // Add vc_column
      shortcodes.unshift( wrapperColumnShortcodeParams );

      // Change root item to the wrapper row
      rootItem = wrapperRowShortcodeParams;
    }
    // Insert after Row as root element => Wrap with Row and Column
    else if ( _.contains( targetShortcodeNames, 'vc_row' ) ) {
      action = 'InsertAfterAsRoot';

      // Wrap in a vc_row and vc_column
      var wrapperRowShortcodeParams = {
        shortcode: 'vc_row',
        id: vc_guid(),
        order: 0, // Will be calculated later
        parent_id: false, // Place to content root
        cloned: false
      };
      var wrapperColumnShortcodeParams = {
        shortcode: 'vc_column',
        id: vc_guid(),
        order: 0,
        parent_id: wrapperRowShortcodeParams.id,
        cloned: false
      };

      // Flush root item order
      rootItem.order = 0;
      rootItem.parent_id = wrapperColumnShortcodeParams.id;

      // Push back the root item
      shortcodes.unshift( rootItem );

      // Add vc_column
      shortcodes.unshift( wrapperColumnShortcodeParams );

      // Change root item to the wrapper row
      rootItem = wrapperRowShortcodeParams;
    }
    // Trying to place shortcode into same shortcode
    else if ( _.intersection( targetShortcodeNames, rootItemNames ).length ) {

      // Merge if both are containers
      if ( rootItemMapped.is_container && targetShortcodeMapped.is_container ) {
        action = 'Merge';
      }
      // Add to parent instead
      else {
        action = 'AddToParent';
      }

    }
    // Append contents to containers
    else if ( targetShortcodeMapped.is_container ) {
      action = 'AddAt';
    }
    // Appending to non-containers
    else if ( ! targetShortcodeMapped.is_container ) {
      action = 'AddToParent';
    }
    // Invalid
    else {
      this.displayNotification( 'error', this.translate( 'Unknown error.' ) );
      return false;
    }


  }

  
  // No need to do additional checks when routing this clip to parent or inserting as root element
  if ( ! _.contains( [ 'AddToParent', 'InsertAfterAsRoot' ], action ) ) {

    // No need to check for allowed aspects if merging containers
    if ( action !== 'Merge' ) {

      // Check if target container can accept pasted shortcode (as_parent property)
      if ( 
            ( asParentOnly && ! _.intersection( asParentOnly, rootItemNames ).length ) 
            || ( asParentExcept && _.intersection( asParentExcept, rootItemNames ).length )
          ) {
        this.displayNotification( 'error', this.translate( 'Pasted shortcode is not accepted by this container.' ) );
        return false;
      }
      // Check if pasted shortcode allows to be pasted into target container (as_child)
      else if ( 
            ( asChildOnly && ! _.intersection( asChildOnly, targetShortcodeNames ).length ) 
            || ( asChildExcept && _.intersection( asChildExcept, targetShortcodeNames ).length )
          ) {
        this.displayNotification( 'error', this.translate( 'Pasted shortcode does not allow to be pasted here.' ) );
        return false;
      }
      // Validate allowed containers if pasting container
      // NOTE: This seems to be misused in mapped vc shortcodes and therefore deprecated for now
      /*
      else if ( rootItemMapped.allowed_container_element && targetShortcodeMapped.is_container ) {

        // var allowedContainers = targetShortcodeMapped.allowed_container_element.split( ',' );
        var allowedContainers = rootItemMapped.allowed_container_element.split( ',' );
        allowedContainers = _.map( allowedContainers, function( item ) { return $.trim( item ); } );
        allowedContainers = _.filter( allowedContainers, function( item ) { return item ? true : false } );

        // Prevent adding containers to non-containers
        if ( ! _.intersection( allowedContainers, targetShortcodeNames ).length ) {
          this.displayNotification( 'error', this.translate( 'Pasting to this container is not allowed.' ) );
          return false;
        }

      }
      */
    
    }
    
    
    // Check if any of the pasted shortcodes nesting same shortcodes
    if ( this.isNestingShortcode( [].concat( [ rootItem ], shortcodes ), targetModel ) ) {
      this.displayNotification( 'error', this.translate( 'Nested shortcodes are prohibited in WordPress or otherwise you may get unexpected result.' ) );
      return false;
    }

  }



  switch ( action ) {
    case 'AddToParent':

      // In this case we need to take parent model, which should be a container
      var targetModelView = window.vc.app.views[ targetModel.get( 'parent_id' ) ];
      var targetParentModel = targetModelView ? targetModelView.model : false;
      var targetParentModelMapped = targetParentModel ? vc.getMapped( targetParentModel.get( 'shortcode' ) ) : false;
      if ( targetParentModel && targetParentModelMapped && targetParentModelMapped.is_container ) {
        vc.clone_index = vc.clone_index / 10;
        rootItem.order = parseFloat( targetModel.get( 'order' ) ) + vc.clone_index;
        shortcodes.unshift( rootItem );
        return this.paste( targetParentModel, shortcodes );
      }
      else {
        this.displayNotification( 'error', this.translate( 'Can not append to this element. Parent is not a container or undefined.' ) );
        return false;
      }

    break;
    case 'InsertAfterAsRoot':
      vc.clone_index = vc.clone_index / 10;
      rootItem.order = parseFloat( targetModel.get( 'order' ) ) + vc.clone_index;
      shortcodes.unshift( rootItem );
    break;
    case 'Append':
      rootItem.order = 0;
      rootItem.parent_id = targetModel.get( 'id' );
      shortcodes.unshift( rootItem );
    break;
    case 'AppendToRoot':
      shortcodes.unshift( rootItem );
    break;
    case 'AddAt':
      rootItem.parent_id = targetModel.get( 'id' );
      shortcodes.unshift( rootItem );
    break;
    case 'Merge':
      // When pasting Column to Column, we'll just add contents from one column to another
      var orderOffset = this.getLastChildOrder( targetModel.get( 'id' ) );

      var sortedChildren = _.filter( shortcodes, function( item ) { return item.parent_id === rootItem.id; } );
      sortedChildren.sort( function( a, b ) { return parseInt( a.order ) - parseInt( b.order ) } );
      sortedChildren = _.map( sortedChildren, function( item ) { return item.id; } );

      shortcodes = _.map( shortcodes, function( item, index ) {
        if ( item.parent_id === rootItem.id ) {
          item.parent_id = targetModel.get( 'id' );
          item.order = orderOffset = orderOffset + sortedChildren.indexOf( item.id ) * 0.1 + 1; // Ensure new items go in right order but after all other items
        }
        return item;        
      });
    break;
  }

  // Remove unknown shortcodes
  shortcodes = _.filter( shortcodes, function( item ) {
    return !! vc.getMapped( item.shortcode ).name;
  } );

  // Add shortcodes
  var idsMap = {};
  _.each( shortcodes, function iterateShortcodes( item, index ) {

    var newId = vc_guid();
    idsMap[ item.id ] = newId;

    var shortcodeParams = {
      shortcode: item.shortcode,
      id: newId,
      cloned: false,
      // order: order,
      parent_id: item.parent_id && idsMap[ item.parent_id ] ? idsMap[ item.parent_id ] : item.parent_id,
      params: item.params ? item.params : {}
    };

    if ( item.order > 0 ) {
      shortcodeParams.order = item.order;
    }

    window.vc.shortcodes.create( shortcodeParams );
  });

};

clips.prototype.getAscendants = function clipsGetAscendants( model, items ) {
  items = _.isArray( items ) ? items : [];

  if ( ! model ) return items;

  var modelView = window.vc.app.views[ model.get( 'parent_id' ) ];
  var parentModel = modelView ? modelView.model : false;

  if ( parentModel ) {
    items.push( parentModel );
    items = this.getAscendants( parentModel, items );
  }

  return items;
};

clips.prototype.isNestingShortcode = function clipsIsNestingShortcode( shortcodes, targetModel ) {

  // Get a list of names for that target shortcodes tree
  var targetModelsTree = this.getAscendants( targetModel );
  var targetShortcodeNames = _.map( targetModelsTree, function( model ) {
    return model.get( 'shortcode' );
  });

  // Get list of names of the pasted shortcodes
  var pastedShortcodeNames = _.map( shortcodes, function( item ) {
    return item.shortcode;
  } );

  // If they intersect, then there are nested shortcodes
  return _.intersection( pastedShortcodeNames, targetShortcodeNames ).length > 0;
};

clips.prototype.getLastChildOrder = function clipsGetLastChildOrder( modelId ) {
  var lastOrder = 0;
  if ( vc.shortcodes && vc.shortcodes.models ) {
    var children = _.filter( vc.shortcodes.models, function iterateVcShortcodes( item ) {
      return item.get( 'parent_id' ) === modelId;
    } );
    lastOrder = children.length ? _.max( children, function iterateChildren( item ) { return item.get( 'order' ); } ) : lastOrder;
    lastOrder = lastOrder ? lastOrder.get( 'order' ) : lastOrder;
  }
  return lastOrder;
};

clips.prototype.copy = function clipsCopy( modelId ) {
  if ( ! modelId ) return false;
  
  var model = window.vc.app.views[ modelId ].model;
  if ( ! model ) return false;

  var shortcodes = this.getCopiedShortcodes( model, true );

  var title = this.getTitle( model ); 
  this.addClip( 'recent', title, shortcodes );
};

clips.prototype.getTitle = function clipsGetCopiedShortcodes( model ) {
  var mapped = vc.getMapped( model.get( 'shortcode' ) );
  if ( ! mapped ) return false;
  var title = mapped.name; 

  var now = new Date();
  var H = now.getHours();
  var i = now.getMinutes();
  var s = now.getSeconds()

  H = H < 10 ? '0' + H : H;
  i = i < 10 ? '0' + i : i;
  s = s < 10 ? '0' + s : s;

  title += ' (' + H + ':' + i + ':' + s + ')';

  return title;
};

clips.prototype.getCopiedShortcodes = function clipsGetCopiedShortcodes( model, isRoot, items ) {
  var self = this;

  // Init items array if needed
  items = items ? items : [];

  // Append current item
  items.push({
    id: model.id,
    parent_id: isRoot ? false : model.get( 'parent_id' ),
    order: isRoot ? 0 : model.get( 'order' ),
    shortcode: model.get( 'shortcode' ),
    params: model.get( 'params' )
  });

  // Append all children
  var children = window.vc.shortcodes.where( { parent_id: model.id } );
  _.each( children, function iterateChildShortcodes( model ) {
    items = self.getCopiedShortcodes( model, false, items );
  } );

  return items;
};

/*
  Retrieves data from local storage
*/
clips.prototype.load = function clipsLoad() {
  var clipItems = localStorage.getItem( 'cpt_clips' );

  if ( clipItems && ( 'string' === typeof clipItems ) ) {
    try {
      clipItems = JSON.parse( clipItems );
    }
    catch ( err ) {
      clipItems = null;
    }
  }

  clipItems = null !== clipItems ? clipItems : {};
  clipItems = 'object' === typeof clipItems ? clipItems : {};
  clipItems.recent = $.isArray( clipItems.recent ) ? clipItems.recent : [];
  clipItems.favorites = $.isArray( clipItems.favorites ) ? clipItems.favorites : [];

  return clipItems;
};

/*
  Updates param in a clip by uid
*/
clips.prototype.updateClip = function clipsUpdateClip( listType, uid, param, value ) {
  var clipItems = this.load();

  var list = clipItems[ listType ];
  list = _.map( list, function( item ) {
    if ( item.uid === uid ) {
      item[ param ] = value;
    }
    return item;
  } );
  clipItems[ listType ] = list;
  
  return this.save( clipItems );
};

/*
  Removes clip by uid
*/
clips.prototype.removeClip = function clipsRemoveClip( listType, uid ) {
  var clipItems = this.load();

  var list = clipItems[ listType ];
  list = _.filter( list, function( item ) {
    return item.uid !== uid;
  } );
  clipItems[ listType ] = list;
  
  return this.save( clipItems );
};

/*
  Adds clip to the list
*/
clips.prototype.moveToFavorites = function clipsmoveToFavorites( uid ) {
  var clipItems = this.load();
  
  clip = _.find( clipItems.recent, function( item ) {
    return item.uid === uid;
  } );

  if ( clip ) {
    clipItems.favorites.unshift( clip );
    return this.save( clipItems );
  }
  else {
    return false;
  }
};

/*
  Adds clip to the list
*/
clips.prototype.addClip = function clipsAddClip( listType, title, shortcodes ) {
  var clipItems = this.load();
  
  var list = clipItems[ listType ];
  list.unshift( {
    title: title,
    shortcodes: shortcodes,
    uid: vc_guid()
  } );

  if ( ( listType === 'recent' ) && ( list.length > this.maxRecentItems ) ) {
    list = list.slice( 0, this.maxRecentItems );
  }

  clipItems[ listType ] = list;

  return this.save( clipItems );
};

/*
  Saves data to local storage
*/
clips.prototype.save = function clipsSave( items ) {
  items = ! $.isArray( items ) && ( null !== items ) ? items : {};
  items = $.extend( { recent: [], favorites: [] }, items );
  
  try {
    localStorage.setItem( 'cpt_clips', JSON.stringify( items ) );
    return true;
  }
  catch ( err ) {
    return false;
  }
};

clips.prototype.hasLocalStorage = function clipsHasLocalStorage() {
  var variable = 'cpt_lc_test';
  try {
    localStorage.setItem( variable, variable );
    localStorage.removeItem( variable );
    return true;
  } catch(e) {
    return false;
  }  
};

clips.prototype.addControls = function clipsAddControls( jqControls ) {
  var self = this;

  if ( ! jqControls.find( '.cpt-clip-btn' ).length && ! jqControls.hasClass( 'bottom-controls' ) ) {

    // Row
    if ( jqControls.hasClass( 'vc_controls-row' ) ) {
      jqControls.find( '.vc_row_edit_clone_delete' ).find( '.vc_control.column_delete' ).last().after( self.jqClipRowBtn.clone() );
    }
    // Column/Container
    else if ( jqControls.hasClass( 'vc_control-column' ) ) {
      jqControls.find( '.vc_control' ).last().before( self.jqClipContainerBtn.clone() );
    }
    // Element
    else {
      jqControls.find( '.vc_control-btn' ).last().before( self.jqClipElementBtn.clone() );
    }

  }
};

var Clips = new clips();

})( window, jQuery );