(function( window, $, Backbone, undefined ) {

  var DynamicShortcodeValues = {};


  // Define DynamicShortcodeValues model
  DynamicShortcodeValues.Item = Backbone.Model.extend({
    defaults: {
      searchType: 'attr', // attr|content|text
      searchValue: '',
      replacementType: '',
      replacementValue: '',
      replacementNaAction: ''
    }
  });


  // Define DynamicShortcodeValues collection
  DynamicShortcodeValues.Collection = Backbone.Collection.extend({
    model: DynamicShortcodeValues.Item,
    sync: function() {}, // No backend
  });


  // Instantiate DynamicShortcodeValues collection
  DynamicShortcodeValues.items = new DynamicShortcodeValues.Collection();


  // Define main app View
  DynamicShortcodeValues.AppView = Backbone.View.extend({
    templates: {}, // Keeps pre-compiled templates
    templateSettings: {
      escape : /<%-([\s\S]+?)%>/g,
      evaluate : /<%([\s\S]+?)%>/g,
      interpolate : /<%=([\s\S]+?)%>/g
    },
    el: '#cptdsv_param',
    initialize: function initializeView() {
      var self;

      self = this;
      $( function onDomReady() {
        var models, excludedParams, shortcodeParamsOptions, supportedParamTypes;

        // VC loads script even if this param type is not added to the shortcode
        // Therefore, we need to check if the field itself is there.
        if ( ! $( '#cptdsv_value' ).length ) {
          return;
        }

        shortcodeParamsOptions = [];
        excludedParams = cptdsv.excludedShortcodeParams;
        supportedParamTypes = cptdsv.supportedParamTypes;

        // Instantinate collection view
        DynamicShortcodeValues.collectionView = new DynamicShortcodeValues.CollectionView({ el: '#cptdsv_values_list' });


        // Pre-compile templates
        $( 'script.cptdsv-tpl' ).each( function iterateTemplates( index, item ) {
          self.templates[ $( item ).attr( 'data-name' ) ] = _.template( $( item ).html(), self.templateSettings  );
        } );


        // Get currently edited shortcode params
        self.shortcode = $( '#vc_ui-panel-edit-element.vc_active' ).attr( 'data-vc-shortcode' );
        self.shortcodeParams = vc.map[ self.shortcode ].params;
        _.each( self.shortcodeParams, function( item ) {
          
          // Required
          if ( ! item.param_name ) {
            return;
          }

          // Skip known
          if ( _.contains( excludedParams, item.param_name ) ) {
            return;
          }
          
          // Skip unsupported param types
          if ( ! _.contains( supportedParamTypes, item.type ) ) {
            return;
          }
          
          shortcodeParamsOptions.push({
            label: item.heading ? item.heading + ' (' + item.param_name + ')' : item.param_name,
            value: item.param_name,
            // value: item.integrated_shortcode_field ? item.integrated_shortcode_field + item.param_name : item.param_name,
          });
        } );

        self.dsvOptions = _.extend( { searchValue: shortcodeParamsOptions }, cptdsv.valueOptions );

        // Remove attr from search type options if no shortcode attributes available
        if ( ! shortcodeParamsOptions.length ) {
          self.dsvOptions.searchType = _.without( self.dsvOptions.searchType, _.findWhere( self.dsvOptions.searchType, { value: 'attr' } ) );
        }

        // Parse existing values
        models = self.parseScValue( $( '#cptdsv_value' ).val() );

        // Prepare default model
        self.defaultModel = new DynamicShortcodeValues.Item();

        // Init existing models
        setTimeout( function() {
          if ( models && models.length ) {
            DynamicShortcodeValues.items.set( models );
          }
          else {

            // If no attributes available, switch the default search type
            if ( ! _.contains( self.dsvOptions.searchType, 'attr' ) ) {
              var defaultModel;
              defaultModel = new DynamicShortcodeValues.Item();
              defaultModel.set( 'searchType', self.dsvOptions.searchType.length ? self.dsvOptions.searchType[ 0 ].value : '' );
              self.defaultModel = defaultModel;
            }
            else {
              DynamicShortcodeValues.items.set( [] );
            }

            DynamicShortcodeValues.collectionView.render();

          }
        }, 0 );

      });
    },
    parseScValue: function parseScValue( value ){
      var result, items, search, replacement;

      result = [];
      items = value.split( ',' );
      if ( ! items.length ) return result;

      _.each( items, function( item ) {
        item = item.split( '=' );
        if ( 2 != item.length ) return;

        search = item[ 0 ].split( ':' );
        if ( 2 != search.length ) return;

        replacement = item[ 1 ].split( ':' );
        if ( 3 != replacement.length ) return;

        result.push({
          searchType: $.trim( search[ 0 ] ),
          searchValue: $.trim( search[ 1 ] ),
          replacementType: $.trim( replacement[ 0 ] ),
          replacementValue: $.trim( replacement[ 1 ] ),
          replacementNaAction: $.trim( replacement[ 2 ] )
        });
      } );

      return result;
    },
    saveScValue: function saveScValue( models ){
      var result, item;

      result = [];
      // {search_type}:{search_name}={replacement_type}:{replacement_name}:{na_action},
      _.each( models, function( model ) {
        item = model.toJSON();
        result.push( item.searchType + ':' + item.searchValue + '=' + item.replacementType + ':' + item.replacementValue + ':' + item.replacementNaAction );
      } );

      this.$el.find( '#cptdsv_value' ).val( result.join( ',' ) );
    },
    render: function renderView(){
      return this; // enable chained calls
    },
    events: {
      'click .btn-add-item': 'addItem',
    },
    addItem: function addItem( e ) {
      e.preventDefault();
      DynamicShortcodeValues.items.add( this.defaultModel.toJSON() );
    }
  });


  // Define item view
  DynamicShortcodeValues.ItemView = Backbone.View.extend({
    tagName: 'div',
    className: 'cptdsv-item',
    model: new DynamicShortcodeValues.Item(),
    initialize: function initializeItemView() {
      this.template = DynamicShortcodeValues.app.templates[ 'dynamic-value' ];
    },
    render: function renderItemView(){
      var index;
      
      index = this.model.collection.indexOf( this.model );

      this.$el.html( this.template( {
        model: this.model.toJSON(),
        index: index,
        options: DynamicShortcodeValues.app.dsvOptions,
        templates: DynamicShortcodeValues.app.templates,
        i18n: cptdsv.i18n
      } ) );


      this.$el.find( 'select' )
        .select2({});

      return this;
    },
    events: {
      'click .btn-remove-item': 'remove',
      'input input[data-prop]': 'update',
      'change select': 'update'
    },
    update: function updateItemView( e ) {
      var self = this, jqTarget, updatedModel;

      jqTarget = $( e.target );

      updatedModel = {};
      jqTarget.closest( '.cptdsv-item' ).find( '[data-prop]' ).each( function( index, item ) {
        item = $( item );
        updatedModel[ item.attr( 'data-prop' ) ] = item.val();
      });
      this.model.set( updatedModel );

      if ( _.contains( [ 'searchType', 'replacementType' ], jqTarget.attr( 'data-prop' ) ) ) {
        this.render();
      }

    },
    remove: function removeItemView( e ) {
      e.preventDefault();
      this.model.destroy();
    }
  });


  // Define collection view
  DynamicShortcodeValues.CollectionView = Backbone.View.extend({
    model: DynamicShortcodeValues.items,
    initialize: function initializeCollectionView() {
      var self = this;
      this.model.on( 'add', this.render, this );
      this.model.on( 'remove', this.render, this );
      this.model.on( 'change', this.save, this );
    },
    save: function save(){
      if ( DynamicShortcodeValues.app ) {
        DynamicShortcodeValues.app.saveScValue( this.model.toArray() );
      }
    },
    render: function renderCollectionView(){
      var self = this, models;

      this.save();

      this.$el.html( '' );

      models = this.model.toArray();

      if ( models.length ) {
        _.each( models, function( item ) {
          self.$el.append( ( new DynamicShortcodeValues.ItemView( { model: item } ) ).render().$el );
        } );
      }
      else {
        self.$el.append( DynamicShortcodeValues.app.templates[ 'no-dynamic-values' ]( this.model.toJSON() ) );
      }

      return this; // enable chained calls
    },
    events: {
    }
  });


  // Init app
  DynamicShortcodeValues.app = new DynamicShortcodeValues.AppView();


  if ( ! window.CPTemplates ) {
    window.CPTemplates = {};
  }
  CPTemplates.DynamicShortcodeValues = DynamicShortcodeValues;


})( window, jQuery, Backbone );
