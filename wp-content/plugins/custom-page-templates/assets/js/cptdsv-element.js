(function( window, $, Backbone, undefined ) {


  // Element View in Visual Composer Editor
  function initElementView() {
    window.DynamicShortcodeValuesElementView = vc.shortcode_view.extend( {
      elementTemplate: false,
      $wrapper: false,
      changeShortcodeParams: function ( model ) {
        var params, template, content, title;
        var items, search, replacement;
        
        window.DynamicShortcodeValuesElementView.__super__.changeShortcodeParams.call( this, model );
        params = _.extend( {}, model.get( 'params' ) );
        if ( ! this.$wrapper ) {
          this.$wrapper = this.$el.find( '.wpb_element_wrapper' );
        }
        if ( ! this.elementTemplate ) {
          this.elementTemplate = this.$wrapper.html();
        }
        if ( _.isObject( params ) ) {

          content = [];
          items = params.cptdsv ? params.cptdsv.split( ',' ) : [];
          if ( items.length ) {

            _.each( items, function( item ) {
              item = item.split( '=' );
              if ( 2 != item.length ) return;

              search = item[ 0 ].split( ':' );
              if ( 2 != search.length ) return;

              replacement = item[ 1 ].split( ':' );
              if ( 3 != replacement.length ) return;

              else if ( 'text' == item ) {
              }

              item = '';
              if ( 'attr' == search[ 0 ] ) {
                item = search[ 0 ];
                item += search[ 1 ] ? '->' + search[ 1 ] : '';
              }
              else {
                item += search[ 1 ] ? search[ 1 ] : '';
              }
              
              if ( 'content' != search[ 0 ] ) {
                item += ' => ';
              }

              item += '<strong>' + replacement[ 0 ] + '</strong>';
              item += replacement[ 1 ] ? '-><i>' + replacement[ 1 ] + '</i>' : '';
              item += replacement[ 2 ] ? '(' + replacement[ 2 ] + ')' : '';

              content.push( item );

            } );

          }

          content = content.join( '<br>' );

          template = _.template( this.elementTemplate, vc.templateOptions.custom );
          this.$wrapper.html( template( { content: content } ) );
        }
      }
    } );
  }


  $( function onDomReady() {
    initElementView();
  } );

})( window, jQuery, Backbone );
