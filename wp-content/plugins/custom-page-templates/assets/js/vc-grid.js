(function( window, $, undefined ) {

  if ( window.cptemplatesVcGrid ) {
    $( document ).ajaxSend( function( event, jqxhr, settings ) {

      if ( settings.data && ( -1 !== settings.data.indexOf( 'action=vc_get_vc_grid_data&' ) ) ) {
        settings.data += '&cpt_vcgrid_query_vars=' + encodeURIComponent( cptemplatesVcGrid.query_vars );
      }

    } );
  }

  if ( window.cptemplatesVcGridOverwrite && Object.keys( cptemplatesVcGridOverwrite ).length ) {
    $( document ).ajaxSend( function( event, jqxhr, settings ) {
      var shortcodeRe, matches, shortcodeId, overwriteParams;

      // Ensure we're captured the right request
      if ( ! settings || ! settings.data || ( -1 === settings.data.indexOf( 'action=vc_get_vc_grid_data&' ) ) ) {
        return;
      }

      // Prepare shortcode regexp rule
      shortcodeRe = /shortcode_id%5D=(.*?)&/i;

      // Grab the shortcode id from request data
      if ( ! ( matches = settings.data.match( shortcodeRe ) ) || ! ( shortcodeId = matches[ 1 ] ) ) {
        return;
      }
      shortcodeId = 'vc_gid:' + shortcodeId;

      // Check if we have params to be overwritten for this vc grid shortcode
      if ( ! ( overwriteParams = cptemplatesVcGridOverwrite.params[ shortcodeId ] ) ) {
        return;
      }

      settings.data += '&cpt_vc_grid_overwrite_params=' + encodeURIComponent( JSON.stringify( overwriteParams ) );
    } );
  }

} )( window, jQuery );
