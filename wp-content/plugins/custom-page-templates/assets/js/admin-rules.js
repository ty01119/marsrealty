(function( window, $, Backbone, undefined ) {

  var Rules = {};


  // Define Rule model
  Rules.Item = Backbone.Model.extend({
    defaults: {
      request: '',
      user: '',
      status: true
    }
  });


  // Define Rules collection
  Rules.Collection = Backbone.Collection.extend({
    model: Rules.Item,
    sync: function() {}, // No backend
  });


  // Instantiate Rules collection
  Rules.items = new Rules.Collection();


  // Define main app View
  Rules.AppView = Backbone.View.extend({
    templates: {}, // Keeps pre-compiled templates
    templateSettings: {
      escape : /<%-([\s\S]+?)%>/g,
      evaluate : /<%([\s\S]+?)%>/g,
      interpolate : /<%=([\s\S]+?)%>/g
    },
    el: '.cptemplates-rules',
    initialize: function initializeView() {
      var self;

      self = this;
      $( function onDomReady() {

        // Instantinate collection view
        Rules.collectionView = new Rules.CollectionView({ el: '#cptemplates-rules-list' });

        // Pre-compile templates
        self.noTemplate = _.template( '', self.templateSettings );
        $( 'script.cptemplates-tpl' ).each( function iterateTemplates( index, item ) {
          self.templates[ $( item ).attr( 'data-name' ) ] = _.template( $( item ).html(), self.templateSettings );
        } );

        // Setup existing rules
        if ( cptemplatesAdminRules.existingRules && cptemplatesAdminRules.existingRules.length ) {
          Rules.items.set( cptemplatesAdminRules.existingRules );
        }
        else {
          Rules.collectionView.render();
        }

      });
    },
    getTemplate: function( selector ) {
      return this.templates[ selector ] ? this.templates[ selector ] : this.noTemplate;
    },
    render: function renderRulesView(){
      return this; // enable chained calls
    },
    events: {
      'click .btn-add-rule': 'addRule',
    },
    addRule: function addRule( e ) {
      e.preventDefault();
      Rules.items.add( { request: '', user: '', status: '' } );
    }
  });


  // Define rule item view
  Rules.ItemView = Backbone.View.extend({
    tagName: 'div',
    className: 'cptemplates-rule',
    model: new Rules.Item(),
    initialize: function initializeItemView() {
      this.template = Rules.app.getTemplate( 'rule' );
    },
    render: function renderItemView(){
      var index;
      
      index = this.model.collection.indexOf( this.model );

      this.$el.html( this.template( {
        model: this.model.toJSON(),
        index: index,
        options: this.getEligibleModelOptions( cptemplatesAdminRules.options ),
        i18n: cptemplatesAdminRules.i18n,
        templates: {
          'select': Rules.app.getTemplate( 'select' ),
          'number': Rules.app.getTemplate( 'number' )
        }
      } ) );

      this.$el.find( 'select[multiple]' )
        .select2({ allowClear: true });

      this.$el.find( 'select[data-prop="request"],select[data-prop="template"]' )
        .select2({});

      return this;
    },
    events: {
      'click .btn-remove-rule': 'remove',
      'input input[data-prop]': 'update',
      'change select': 'update'
    },
    update: function updateRule( e ) {
      var self = this, jqTarget, eligibleOptionProps;

      jqTarget = $( e.target );
      eligibleOptionProps = Object.keys( this.getEligibleModelOptions( cptemplatesAdminRules.options ) );
      this.model.set( jqTarget.attr( 'data-prop' ), jqTarget.is( ':visible' ) || ( jqTarget.is( 'select' ) && $.inArray( jqTarget.attr( 'data-prop' ), eligibleOptionProps ) ) ? jqTarget.val() : '' );
      
      if ( 'request' == jqTarget.attr( 'data-prop' ) ) {
        this.render();
      }
    },
    remove: function removeRule( e ) {
      e.preventDefault();
      this.model.destroy();
    },
    getEligibleModelOptions: function getEligibleModelOptions( options ) {
      var model, eligibleGroups, resolved;

      model = this.model.toJSON();

      // Isolate changes
      options = _.extend( {}, options );

      _.each( options, function iterateGroups( groups, type ) {
        if ( ! _.isArray( groups ) ) {
          return true;
        }

        eligibleGroups = [];
        _.each( groups, function iterateOptions( group, index ) {
          resolved = true;
          if ( group.dependency ) {
            _.each( group.dependency, function iterateDependencies( value, key ) {
              if ( model[ key ] != value ) {
                resolved = false;
                return false;
              }
            } );

          }

          if ( resolved ) {
            eligibleGroups.push( group );
          }
        } );

        options[ type ] = eligibleGroups;
      } );

      return options;
    }
  });


  // Define collection view
  Rules.CollectionView = Backbone.View.extend({
    model: Rules.items,
    initialize: function initializeRulesView() {
      var self = this;
      this.model.on( 'add', this.render, this );
      this.model.on('remove', this.render, this);
    },
    render: function renderRulesView(){
      var self = this, models;

      this.$el.html( '' );

      models = this.model.toArray();
      if ( models.length ) {
        _.each( models, function( rule ) {
          self.$el.append( ( new Rules.ItemView( { model: rule } ) ).render().$el );
        } );
      }
      else {
        self.$el.append( Rules.app.getTemplate( 'norules' )( this.model.toJSON() ) );
      }


      return this; // enable chained calls
    },
    events: {
    }
  });


  // Init app
  Rules.app = new Rules.AppView();


  if ( ! window.CPTemplates ) {
    window.CPTemplates = {};
  }
  CPTemplates.Rules = Rules;

})( window, jQuery, Backbone );
