<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_template' ) ) :

final class cptemplates_template {


  /**
   * Instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;

  
  /**
   * Currently matched template post
   *
   * @since 1.0.0
   * @var object|boolean $applied_template_post
   */
  private $applied_template_post = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    if ( ! is_admin() ) {
      add_action( 'template_include', array( $this, 'change_template' ), 50 );
    }
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_template instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  render_template
   *
   *  Performs all custom template rendering logic
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (boolean) true if default template should be applied or false if specific page template file has been applied
   */

  public function render_template() {
    global $post;

    if ( ! $this->applied_template_post ) {
      return false;
    }

    // Check if should use one of the existing page templates
    $template_file = cptemplates_cptemplate()->get_page_template_file( $this->applied_template_post->ID );
    $template_file = $template_file ? locate_template( $template_file ) : null;

    // Allow 3rd parties to adjust existing template file
    $template_file = apply_filters( 'cptemplates/render_template/template_file', $template_file );

    // Load existing page template if has been chosen for this template and corresponding file exists
    if ( $template_file && file_exists( $template_file ) ) {
      include( $template_file );
      return false;
    }
    // Otherwise, render default template
    else {
      return true;
    }
  }


  /**
   *  apply_template_to_the_content
   *
   *  Performs all custom template rendering logic
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (boolean) true if default template should be applied or false if existing page template has been applied
   */

  public function apply_template_to_the_content( $content = '' ) {
    // remove_filter( 'the_content', array( $this, 'apply_template_to_the_content' ), 999 );

    // Start output buffering
    ob_start();

    // Render applied template content
    if ( $this->applied_template_post ) {

      // Get template's post content
      $template_content = $this->applied_template_post->post_content;

      // Allow 3rd parties to template content
      $template_content = apply_filters( 'cptemplates/cptemplate/template_content', $template_content );

      // Customize template container wrapper
      $template_wrappers_option = cptemplates_get_option( 'template_wrappers' );
      if ( 'woocommerce' == $template_wrappers_option ) {
        do_action( 'woocommerce_before_main_content' );
        do_action( 'cptemplates/cptemplate/before_template_content' );
        echo do_shortcode( $template_content );
        do_action( 'cptemplates/cptemplate/after_template_content' );
        do_action( 'woocommerce_after_main_content' );
      }
      elseif ( 'custom' == $template_wrappers_option ) {
        echo cptemplates_get_option( 'template_wrapper_before', '' );
        do_action( 'cptemplates/cptemplate/before_template_content' );
        echo do_shortcode( $template_content );
        do_action( 'cptemplates/cptemplate/after_template_content' );
        echo cptemplates_get_option( 'template_wrapper_after', '' );
      }
      else {
        do_action( 'cptemplates/cptemplate/before_template_content' );
        echo do_shortcode( $template_content );
        do_action( 'cptemplates/cptemplate/after_template_content' );
      }

    }
    // Otherwise, render post's content only
    else {
      echo $content;
    }

    // Get out buffer content
    $template_output = ob_get_clean();

    return $template_output;
  }


  /**
   *  the_content
   *
   *  Displays template content
   *
   *  @type  function
   *  @date  12/11/17
   *  @since 1.0.4
   *
   *  @param (function) $custom_content_foo
   *  @return  N/A
   */

  public function the_content( $custom_content_foo = null ) {
    echo $this->get_the_content( $custom_content_foo );
  }


  /**
   *  get_the_content
   *
   *  Displays template content
   *
   *  @type  function
   *  @date  12/11/17
   *  @since 1.0.4
   *
   *  @param (function) $custom_content_foo
   *  @return  N/A
   */

  public function get_the_content( $custom_content_foo = null ) {

    // Start output buffering
    ob_start();

    // Render applied template content
    if ( $this->applied_template_post ) {

      // Get template's post content
      $template_content = $this->applied_template_post->post_content;

      // Allow 3rd parties to template content
      $template_content = apply_filters( 'cptemplates/cptemplate/template_content', $template_content );

      // Customize template container wrapper
      $template_wrappers_option = cptemplates_get_option( 'template_wrappers' );
      if ( 'woocommerce' == $template_wrappers_option ) {
        do_action( 'woocommerce_before_main_content' );
        do_action( 'cptemplates/cptemplate/before_template_content' );
        echo do_shortcode( $template_content );
        do_action( 'cptemplates/cptemplate/after_template_content' );
        do_action( 'woocommerce_after_main_content' );
      }
      elseif ( 'custom' == $template_wrappers_option ) {
        echo cptemplates_get_option( 'template_wrapper_before', '' );
        do_action( 'cptemplates/cptemplate/before_template_content' );
        echo do_shortcode( $template_content );
        do_action( 'cptemplates/cptemplate/after_template_content' );
        echo cptemplates_get_option( 'template_wrapper_after', '' );
      }
      else {
        do_action( 'cptemplates/cptemplate/before_template_content' );
        echo do_shortcode( $template_content );
        do_action( 'cptemplates/cptemplate/after_template_content' );
      }

    }
    // Otherwise, render post's content only
    else {

      // Capture content
      ob_start();
      if ( $custom_content_foo && is_callable( $custom_content_foo ) ) {
        $custom_content_foo();
      }
      else {
        the_content();
      }
      $content = ob_get_clean();

      echo $content;
    }

    // Get out buffer content
    $template_output = ob_get_clean();

    return $template_output;
  }


  /**
   *  lock_applying_template
   *
   *  Locks applying template
   *
   *  @type  function
   *  @date  12/11/17
   *  @since 1.0.4
   *
   *  @param N/A
   *  @return  N/A
   */

  public function lock_applying_template() {
    _doing_it_wrong( __FUNCTION__, 'This method has been deprecated and has no effect', '1.0.4' );
  }


  /**
   *  lock_applying_template
   *
   *  Unlocks applying filters on wp_trim_excerpt hook (after get_the_excerpt() function)
   *
   *  @type  function
   *  @date  12/11/17
   *  @since 1.0.4
   *
   *  @param N/A
   *  @return N/A
   */

  public function unlock_applying_template() {
    _doing_it_wrong( __FUNCTION__, 'This method has been deprecated and has no effect', '1.0.4' );
  }


  /**
   *  change_template
   *
   *  Changes the template (if applicable) for the current request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $template
   *  @return  (string)
   */

  public function change_template( $template ) {

    $current_template = $template;

    // If we have template defined for the currently requested page
    if ( $this->apply_template() ) {

      // Look by hierarchy from more specific down to general
      $templates = array();
      $templates []= sprintf( 'cptemplate-%s.php', $this->get_current_request_name() );
      $templates []= sprintf( 'cptemplate.php' );

      // Allow 3rd parties to adjust template files list
      $templates = (array)apply_filters( 'cptemplates/custom_template_files', $templates, $template );
  
      // Allow to customize default page template in theme
      if ( $template_file = locate_template( $templates ) ) {
        $template = $template_file;
      }
      // Use default template
      else {
        foreach ( $templates as $template_filename ) {
          $template_filepath = cptemplates_get_setting( 'path' ) . 'templates/' . $template_filename;
          if ( file_exists( $template_filepath ) ) {
            $template = $template_filepath;
            break;
          }
        }
      }
    }


    // If template has been changed
    if ( $current_template != $template ) {
      // Set custom CSS class
      add_filter( 'body_class', array( $this, 'body_class' ), 10, 2 );

      // Notify 3rd parties
      do_action( 'cptemplates/template_changed', $template, $current_template );
    }


    // Global post object must be set in order to work properly on system pages
    if ( ! get_post() ) {
      $this->setup_fake_post();
    }

    return $template;
  }


  /**
   *  body_class
   *
   *  Appends custom css class to body tag when template has been applied
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function body_class( $classes, $class ) {
    $additional_classes = array( 'cptemplate' );

    if ( $template_post = $this->get_applied_template_post() ) {
      $additional_classes []= 'cptemplate-' . $template_post->post_name;
    }

    return array_merge( $classes, $additional_classes );
  }


  /**
   *  setup_fake_post
   *
   *  Creates fake post and registers it in the system
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function setup_fake_post() {
    global $wp, $wp_query;

    // $post_id = -999; // Avoid clashing with existing posts
    $post_id = $this->applied_template_post ? $this->applied_template_post->ID : -999; // Avoid clashing with existing posts
    $post = new stdClass();
    $post->ID = $post_id; 
    $post->post_author = 1;
    $post->post_date = current_time( 'mysql' );
    $post->post_date_gmt = current_time( 'mysql', 1 );
    $post->post_title = '';
    $post->post_content = '';
    $post->post_status = 'publish';
    $post->comment_status = 'closed';
    $post->ping_status = 'closed';
    $post->post_name = 'cptemplate-fake-post-' . uniqid(); // Append random number to avoid clash
    $post->post_type = 'page';
    $post->filter = 'raw'; // important

    // Convert to WP_Post object
    $wp_post = new WP_Post( $post );

    // Enough to set global for get_post()
    $GLOBALS[ 'post' ] = $wp_post;

    // There is no queried object on system pages, like 404 error or search
    if ( ! get_queried_object() ) {

      // Add the fake post to the cache
      wp_cache_add( $post_id, $wp_post, 'posts' );

      // Update the main query
      $wp_query->post = $wp_post;
      $wp_query->posts = array( $wp_post );
      $wp_query->queried_object = $wp_post;
      $wp_query->queried_object_id = $post_id;
      $wp_query->found_posts = 1;
      $wp_query->post_count = 1;
      $wp_query->max_num_pages = 1; 

      // Update globals
      $GLOBALS[ 'wp_query' ] = $wp_query;
      $wp->register_globals();

    }

  }


  /**
   *  get_applied_template_post
   *
   *  Retrieve the currently matched template for the main query
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (object|false) template post or false if nothing found
   */

  public function get_applied_template_post() {
    return $this->applied_template_post;
  }


  /**
   *  apply_template
   *
   *  Finds the template match for the currently queried page
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (object|false) template post or false if nothing found
   */
  private function apply_template() {
    $template = '';
    $template_post = false;

    // Custom post/term templates
    $is_singular = is_singular();
    $is_tax = is_tax() || is_category() || is_tag() ? true : false;
    if ( $is_tax || $is_singular ) {

      // Get template for the currently queried object
      $queried_object = get_queried_object();
      $object_type = $is_tax ? 'term' : 'post';
      $object_id = $is_tax ? $queried_object->term_id : $queried_object->ID;
      $templates = cptemplates_rules()->get_custom_rules( $object_id, $object_type, $optimized = true );

      // Loop through template and pick the right one by checking all conditions
      $template = $this->pick_suitable_template( $templates );

      // Allow third-party plugins to change it
      $template = apply_filters( "cptemplates/apply_custom_template/{$object_type}", $template, $queried_object );

      // Ensure that template has proper value
      $template = cptemplates_rules()->sanitize_template( $template );
      
      // Template override is disabled
      if ( 'disable' == $template ) {
        return false;
      }
      elseif ( is_numeric( $template ) ) {
        $template_post = get_post( $template );
      }
      // Otherwise, current page object has no custom settings and general rules should apply

    }

    // Check global template rule settings if no custom tempate defined or found
    if ( ! $template ) {

      // Get template id by matching request rules
      $templates = $this->get_templates_by_request();

      // Loop through template and pick the right one by checking all conditions
      $template = $this->pick_suitable_template( $templates );

      // Allow third-party plugins to change it
      $template = apply_filters( 'cptemplates/apply_request_template', $template );

      // Ensure that template has proper value
      $template = cptemplates_rules()->sanitize_template( $template );

      // Request template must by template post id
      if ( is_numeric( $template ) ) {
        $template_post = get_post( $template );
      }
    }


    // Notify 3rd parties
    do_action( 'cptemplates/template_applied', $template_post );

    return $this->applied_template_post = $template_post;
  }


  /**
   *  pick_suitable_template
   *
   *  Searches for a template and validates conditions for the current request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (int|boolean) template post id or false if nothing found
   */

  public function pick_suitable_template( $templates = array() ) {
    $template_id = false;

    // Loop templates sorted by priority in ascending order
    foreach ( $templates as $template ) {

      // Conditions stored as serialized array
      $conditions = is_serialized( $template->conditions ) ? unserialize( $template->conditions ) : $template->conditions;

      // Validate
      $valid = cptemplates_rules()->validate_rule_conditions( $conditions, $template->template_id );

      // Use the first satisfying template
      if ( true === $valid ) {
        $template_id = $template->template_id;
        break;
      }

    }

    return $template_id;
  }


  /**
   *  get_templates_by_request
   *
   *  Retrieves templates from DB for the current request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (array) list of template objects
   */

  public function get_templates_by_request( $request = false ) {
    $template_id = 0;
    
    // Request must be identified
    $request = $request ? $request : $this->get_current_request_name();
    if ( ! $request ) {
      return array();
    }


    // Get all active templates from DB that match the current request
    global $wpdb;
    $templates = $wpdb->get_results( 
      $wpdb->prepare("SELECT template_id, conditions FROM {$wpdb->prefix}cptemplates WHERE request='%s' OR request='sitewide' ORDER BY priority ASC", $request ),
      OBJECT
    );

    return is_array( $templates ) ? $templates : array();
  }


  /**
   *  get_current_request_name
   *
   *  Parses current request and returns valid name for using in plugin
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (string) name for the current request
   */

  public function get_current_request_name() {

    if ( is_robots() || is_feed() || is_trackback() ) {
      return '';
    }

    if ( isset( $this->current_request_name ) ) {
      return $this->current_request_name;
    }

    // Bail early if 3rd party redefines it
    if ( $request_name = apply_filters( 'cptemplates/get_current_request_name', '' ) ) {
    }


    // Identify specific WooCommerce requests
    elseif ( cptemplates_is_woocommerce_plugin_active() ) {
      
      if ( is_cart() ) {
        $request_name = 'wc_cart';
      }
      elseif ( is_checkout() ) {
        $request_name = 'wc_checkout';
      }
      elseif ( is_shop() ) {
        $request_name = 'wc_shop';
      }
      elseif ( is_wc_endpoint_url( 'orders' ) ) {
        $request_name = 'wc_account_orders';
      }
      elseif ( is_wc_endpoint_url( 'view-order' ) ) {
        $request_name = 'wc_account_view_order';
      }
      elseif ( is_wc_endpoint_url( 'downloads' ) ) {
        $request_name = 'wc_account_downloads';
      }
      elseif ( is_wc_endpoint_url( 'edit-account' ) ) {
        $request_name = 'wc_account_edit_account';
      }
      elseif ( is_wc_endpoint_url( 'edit-address' ) ) {
        $request_name = 'wc_account_edit_address';
      }
      elseif ( is_wc_endpoint_url( 'payment-methods' ) ) {
        $request_name = 'wc_account_payment_methods';
      }
      elseif ( is_wc_endpoint_url( 'lost-password' ) ) {
        $request_name = 'wc_account_lost_password';
      }
      elseif ( is_account_page() ) {
        $request_name = 'wc_account_dashboard';
      }

    }

    // Standard requests
    if ( ! $request_name ) {

      if ( is_404() ) {
        $request_name = 'error_404';
      }
      elseif ( is_search() ) {
        $request_name = 'search';
      }
      elseif ( is_front_page() ) {
        $request_name = 'front';
      }
      elseif ( is_home() ) {
        $request_name = 'home';
      }
      elseif ( is_post_type_archive() ) {
        $request_name = 'custom_post_type_archive_' . get_post_type();
      }
      elseif ( is_tax() || is_category() || is_tag() ) {
        $term = get_queried_object();
        $request_name = 'taxonomy_' . $term->taxonomy;
      }
      elseif ( is_singular() ) {
        $request_name = 'post_type_' . get_post_type();
      }
      elseif ( is_date() ) {
        $request_name = 'date';
      }
      elseif ( is_author() ) {
        $request_name = 'author';
      }

    }

    // cptemplates_php_debug( 'request_name', $request_name );
    // exit;

    $this->current_request_name = (string)$request_name;

    return $request_name;
  }


}


/**
 *  cptemplates_template
 *
 *  The main function responsible for returning cptemplates_template instance
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_template instance
 */

function cptemplates_template() {
  return cptemplates_template::instance();
}


// initialize
cptemplates_template();

endif;


?>