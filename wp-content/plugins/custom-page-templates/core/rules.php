<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_rules' ) ) :

final class cptemplates_rules {

  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_rules
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_rules instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
  }


  /**
   *  get_custom_rules
   *
   *  Retrieves saved rules for a given post or term
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param  (int) $object_id
   *  @param  (string) $object_type
   *  @param  (boolean) $optimized
   *  @return  (array)
   */

  public function get_custom_rules( $object_id, $object_type, $optimized = false ) {
    $object_id = (int)$object_id;
    $object_type = (string)$object_type;
    if ( ! $object_id || ! $object_type || ! in_array( $object_type, array( 'post', 'term' ) ) ) {
      return array();
    }

    $optimized = (boolean)$optimized;
    $meta_key = $optimized ? 'cptemplate_custom_rules_optimized' : 'cptemplate_custom_rules';
    $rules = get_metadata( $object_type, $object_id, $meta_key, true );
    
    return $rules ? $rules : array();
  }


  /**
   *  sanitize_template
   *
   *  Sanitizes template value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (string|int) template post ID, empty string or disable
   */

  public function sanitize_template( $template = '' ) {
    if ( ( '' !== $template ) && ( 'disable' !== $template ) ) {
      $template = (int)$template;

      // Back to default if invalid post id
      if ( ! $template ) {
        $template = '';
      }
      // Ensure that post template exists
      else {
        $template_post = get_post( $template );

        // Fallback to default of post doesn't exist or if wrong post id supplied
        if ( ! $template_post || ( 'cptemplate' !== $template_post->post_type ) ) {
          $template = '';
        }

      }

    }

    return $template;
  }


  /**
   *  validate_rule_conditions
   *
   *  Verifies if rule's conditions satisfying the current request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $conditions
   *  @param (int) $template_id
   *  @return  (boolean) true if conditions are met or false on failure
   */

  public function validate_rule_conditions( $conditions = array(), $template_id ) {

    // Invalid if conditions are of incorrect type
    if ( ! is_array( $conditions ) ) {
      return false;
    }

    // Valid if no conditions were set
    if ( empty( $conditions ) ) {
      return true;
    }

    // Valid by default
    $result = true;

    // Check each type
    foreach ( $conditions as $type => $values ) {

      $is_valid = false;

      $method = 'validate_rule_conditions_type_' . $type;
      if ( method_exists( $this, $method ) ) {
        $is_valid = $this->$method( $values, $template_id );
      }

      // Allow 3rd parties to validate conditions by type after built-in validation has been performed. Useful for custom conditions type
      $is_valid = apply_filters( "cptemplates/validate_rule_conditions/{$type}", $is_valid, $values, $template_id );

      // If at least one of the condition types hasn't passed, stop validating and return
      if ( ! $is_valid ) {
        $result = false;
        break;
      }

    }

    // Allow 3rd parties to validate custom conditions
    $result = apply_filters( 'cptemplates/validate_rule_conditions', $result, $conditions, $template_id );


    return $result;
  }


  /**
   *  validate_rule_conditions_type_post_formats
   *
   *  Verifies if post formats conditions satisfying the current request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $values
   *  @param (int) $template_id
   *  @return  (boolean) true if conditions are met or false on failure
   */

  public function validate_rule_conditions_type_post_formats( $values = array(), $template_id ) {
    $result = false;

    $values = (array)$values;

    // Standard post format isn't like other formats
    if ( false === get_post_format() ) {
      return in_array( 'standard', $values ) ? true : false;
    }
    elseif ( has_post_format( $values ) ) {
      return true;
    }

    return false;
  }


  /**
   *  validate_rule_conditions_type_post_terms
   *
   *  Verifies if any of the post terms conditions satisfying the current request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $values
   *  @param (int) $template_id
   *  @return  (boolean) true if conditions are met or false on failure
   */

  public function validate_rule_conditions_type_post_terms( $values = array(), $template_id ) {

    // Loop taxonomy terms
    foreach ( (array)$values as $taxonomy => $term_ids ) {

      if ( has_term( $term_ids, $taxonomy ) ) {
        return true;
      }

    }

    return false;
  }


  /**
   *  validate_rule_conditions_type_user
   *
   *  Verifies if any of user conditions satisfying the current request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $values
   *  @param (int) $template_id
   *  @return  (boolean) true if conditions are met or false on failure
   */

  public function validate_rule_conditions_type_user( $values = array(), $template_id = 0 ) {

    // Logged status check
    if ( isset( $values[ 'statuses' ] ) && is_array( $values[ 'statuses' ] ) && ! empty( $values[ 'statuses' ] ) ) {
      
      if ( in_array( 'logged_in', $values[ 'statuses' ] ) && is_user_logged_in() ) {
        return true;
      }
      
      if ( in_array( 'logged_out', $values[ 'statuses' ] ) && ! is_user_logged_in() ) {
        return true;
      }

    }


    // User Roles check
    if ( isset( $values[ 'roles' ] ) && is_array( $values[ 'roles' ] ) && ! empty( $values[ 'roles' ] ) ) {

      if ( ! isset( $this->current_user_roles ) ) {
        $user = wp_get_current_user();
        $this->current_user_roles = (array)$user->roles;
      }

      if ( array_intersect( $this->current_user_roles, $values[ 'roles' ] ) ) {
        return true;
      }

    }


    return false;
  }


}


/**
 *  cptemplates_rules
 *
 *  The main function responsible for returning cptemplates_rules object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_rules instance
 */

function cptemplates_rules() {
  return cptemplates_rules::instance();
}


// initialize
cptemplates_rules();


endif; // class_exists check

?>