<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_modules' ) ) :

final class cptemplates_modules {


  /**
   * Plugin instance
   *
   * @since 1.1.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.1.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_modules
   *
   *  @type  function
   *  @date  15/10/17
   *  @since 1.1.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Include all modules
    $modules = array_filter( glob( cptemplates_get_path( 'modules/*' ) ), 'is_dir' );
    foreach ( $modules as $name ) {
      $filename = sprintf( '%s/%s.php', $name, basename( $name ) );
      if ( file_exists( $filename ) ) {
        include $filename;
      }
    }

  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  15/10/17
   *  @since 1.1.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_modules instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


}


/**
 *  cptemplates_modules
 *
 *  The main function responsible for returning cptemplates_modules object
 *
 *  @type  function
 *  @date  15/10/17
 *  @since 1.1.0
 *
 *  @param N/A
 *  @return (object) cptemplates_modules instance
 */

function cptemplates_modules() {
  return cptemplates_modules::instance();
}


// initialize
cptemplates_modules();


endif; // class_exists check

?>