<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_user_content_restriction' ) ) :

final class cptemplates_user_content_restriction {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_user_content_restriction
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_user_content_restriction instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    if ( ! is_admin() ) {
      add_shortcode( 'cptucr', array( $this, 'shortcode' ) );
      add_filter( 'pre_do_shortcode_tag', array( $this, 'maybe_restrict_content' ), 5, 4 );
    }
  }


  /**
   *  shortcode
   *
   *  Process restricted content shortcode
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $atts
   *  @return  (string)
   */

  public function shortcode( $atts, $content = null ) {

    // Parse attributes
    $atts = shortcode_atts(
      array(
        'restrict' => '',
        'custom_filter' => '',
      ),
      $atts,
      'cptucr' // Enable filtering
    );

    // Prepare
    extract( $atts );
    $restrict = trim( $restrict );
    $custom_filter = trim( $custom_filter );

    // No reason to check if there are no conditions or no content
    if ( ( ! $restrict && ! $custom_filter ) || ! $content ) {
      return $content;
    }
    // Needs validation
    else {
      return $this->is_restricted( $restrict, $custom_filter, 'cptucr', $atts ) ? '' : $content;
    }

  }


  /**
   *  maybe_restrict_content
   *
   *  Controls the shortcodes display
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function maybe_restrict_content( $flag, $tag, $atts, $m ) {
    $restrict = isset( $atts[ 'cptucr' ] ) ? trim( $atts[ 'cptucr' ] ) : '';
    $custom_filter = isset( $atts[ 'cptucr_custom_filter' ] ) ? trim( $atts[ 'cptucr_custom_filter' ] ) : '';

    // Check only if standard or custom filter applied
    if ( $restrict || $custom_filter ) {

      if ( $this->is_restricted( $restrict, $custom_filter, $tag, $atts ) ) {
        $flag = ''; // Anything except false value will be returned instead of processing shortcode
      }

    }

    return $flag;    
  }


  /**
   *  is_restricted
   *
   *  Parses restriction string and determines if content should be restricted
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $restrict
   *  @return  (boolean)
   */

  public function is_restricted( $restrict = '', $custom_filter = '', $tag, $atts ) {

    // Sanitize
    $restrict = (string)$restrict;
    $restrict = preg_split( "/(,| )/", $restrict );
    $restrict = array_map( 'trim', $restrict );
    $restrict = array_filter( $restrict );

    // Prepare
    $statuses = array();
    $roles = array();

    // Parse
    // Restriction string can be like this: "logged_in,administrator,guest"
    foreach ( $restrict as $item ) {
      if ( ( 'logged_in' === $item ) || 'logged_out' === $item ) {
        $statuses []= $item;
      }
      else {
        $roles []= $item;
      }
    }

    // Use the same rules as for templates
    $user_conditions = array();
    if ( ! empty( $statuses ) ) {
      $user_conditions[ 'statuses' ] = $statuses;
    }
    if ( ! empty( $roles ) ) {
      $user_conditions[ 'roles' ] = $roles;
    }

    // Validate
    $is_restricted = ! cptemplates_rules()->validate_rule_conditions_type_user( $user_conditions );

    // If user specified custom filter for this shortcode
    if ( $custom_filter ) {
      $is_restricted = apply_filters( $custom_filter, $is_restricted, $tag, $atts, $user_conditions );
    }

    return $is_restricted;
  }


}


/**
 *  cptemplates_user_content_restriction
 *
 *  The main function responsible for returning cptemplates_user_content_restriction object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_user_content_restriction instance
 */

function cptemplates_user_content_restriction() {
  return cptemplates_user_content_restriction::instance();
}


// initialize
cptemplates_user_content_restriction();


endif; // class_exists check

?>