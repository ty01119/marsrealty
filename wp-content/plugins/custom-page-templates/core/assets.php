<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_assets' ) ) :

final class cptemplates_assets {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_assets
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_assets instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
    
    $scripts = array(
      'cptemplates-admin-rules' => array(
        'src' => cptemplates_get_url( 'assets/js/admin-rules.js' ),
        'dependency' => array( 'jquery', 'backbone', 'select2' ),
      ),
      'cptemplates-vc-grid' => array(
        'src' => cptemplates_get_url( 'assets/js/vc-grid.js' ),
        'dependency' => array( 'vc_grid' ),
      ),
      'cptemplates-dsv-acf-google-maps' => array(
        'src' => cptemplates_get_url( 'assets/js/acf-google-maps.js' ),
        'dependency' => array( 'jquery' ),
      ),
      'cptemplates-clips' => array(
        'src' => cptemplates_get_url( 'assets/js/clips.js' ),
        'dependency' => array( 'jquery', 'backbone' ),
      ),
    );

    // Allow 3rd party to modify
    $scripts = apply_filters( 'cptemplates/register_scripts', $scripts );


    $styles = array(
      'cptemplates' => array(
        'src' => cptemplates_get_url( 'assets/css/frontend.css' ),
        'dependency' => array(),
      ),
      'cptemplates-admin' => array(
        'src' => cptemplates_get_url( 'assets/css/admin.css' ),
        'dependency' => array(),
      ),
      'cptemplates-dsv-acf-google-maps' => array(
        'src' => cptemplates_get_url( 'assets/css/acf-google-maps.css' ),
        'dependency' => array(),
      ),
    );
    
    // Allow 3rd party to modify
    $styles = apply_filters( 'cptemplates/register_styles', $styles );


    // Register scripts
    $version = cptemplates_get_setting( 'version' );
    foreach ( $scripts as $handle => $item ) {
      wp_register_script( $handle, $item[ 'src' ], $item[ 'dependency' ], $version, true );
    }
    
    // Register styles
    foreach ( $styles as $handle => $item ) {
      wp_register_style( $handle, $item[ 'src' ], $item[ 'dependency' ], $version );
    }

  }


}


/**
 *  cptemplates_assets
 *
 *  The main function responsible for returning cptemplates_assets object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_assets instance
 */

function cptemplates_assets() {
  return cptemplates_assets::instance();
}


// initialize
cptemplates_assets();


endif; // class_exists check

?>