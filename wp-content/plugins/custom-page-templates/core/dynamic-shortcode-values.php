<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_dynamic_shortcode_values' ) ) :

final class cptemplates_dynamic_shortcode_values {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_dynamic_shortcode_values
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_dynamic_shortcode_values instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    if ( wp_doing_ajax() || ! is_admin() ) {
      add_shortcode( 'cptdsv', array( $this, 'shortcode' ) );

      add_filter( 'pre_do_shortcode_tag', array( $this, 'maybe_replace_values' ), 10, 4 );
      add_filter( 'cptemplates/dynamic_shortcode_values/get_meta_value', array( $this, 'get_meta_value' ), 10, 2 );
      add_filter( 'cptemplates/dynamic_shortcode_values/get_object_value', array( $this, 'get_object_value' ), 10, 2 );
      add_filter( 'cptemplates/dynamic_shortcode_values/get_http_request_value', array( $this, 'get_http_request_value' ), 10, 2 );
      add_filter( 'cptemplates/dynamic_shortcode_values/get_filter_value', array( $this, 'get_filter_value' ), 10, 2 );
      add_filter( 'cptemplates/dynamic_shortcode_values/get_action_value', array( $this, 'get_action_value' ), 10, 2 );
      add_filter( 'cptemplates/dynamic_shortcode_values/get_template_value', array( $this, 'get_template_value' ), 10, 2 );
    }
  }


  /**
   *  shortcode
   *
   *  Process dynamic values shortcode
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $atts
   *  @return  (string)
   */

  public function shortcode( $atts, $content = null ) {
    return $content;
  }


  /**
   *  maybe_replace_values
   *
   *  Controls the shortcodes display
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function maybe_replace_values( $flag, $tag, $atts, $m ) {
    // Get shortcode content
    $content = isset( $m[ 5 ] ) ? $m[ 5 ] : '';

    // Allow 3rd parties to provide custom processor
    $custom_processed = apply_filters( 'cptemplates/dynamic_shortcode_values/replace', null, $tag, $atts, $content );
    if ( null !== $custom_processed ) {
      return $custom_processed;
    }

    // Replace values
    $replaced_values = $this->replace_values( $tag, $atts, $content );

    // Change only if something has been replaced
    if ( FALSE === $replaced_values ) {
      return $flag;
    }

    // If not array, then the shortcode must be cleared
    if ( ! is_array( $replaced_values ) ) {
      return '';
    }
 
    // Get variables
    extract( $replaced_values );

    // Compose new shortcode
    $shortcode_atts = array();
    foreach ( $atts as $key => $value ) {
      $shortcode_atts []= sprintf( '%s="%s"', $key, $value );
    }
    $shortcode = sprintf( '[%s %s]', $tag, implode( ' ', $shortcode_atts ) );
    if ( $content ) {
      $shortcode .= sprintf( '%s[/%s]', $content, $tag );
    }

    // Return new shortcode processed
    return do_shortcode( $shortcode );
  }


  /**
   *  replace_values
   *
   *  Performs shortcode values replacement if needed
   *
   *  Param format: {search_type}:{search_name}={replacement_type}:{replacement_name}:{na_action},...
   *  Consider value formatting: {search_type}:{search_name}={replacement_type}:{replacement_name}:{na_action},...
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function replace_values( $tag, $atts = array(), $content = '' ) {

    $dynamic_values = isset( $atts[ 'cptdsv' ] ) ? trim( $atts[ 'cptdsv' ] ) : '';
    // Process only if dynamic values were specified
    if ( ! $dynamic_values ) {
      return false;
    }

    // Parse dynamic values settings
    $dynamic_values = explode( ',', $dynamic_values );
    if ( empty( $dynamic_values ) ) {
      return false;
    }

    // Remove cptdsv attr to avoid processing next time
    unset( $atts[ 'cptdsv' ] );

    // Replace
    foreach ( $dynamic_values as $item ) {
      $parts = explode( '=', $item );

      // Get search and replacement values
      $search = isset( $parts[ 0 ] ) ? $parts[ 0 ] : '';
      $replacement = isset( $parts[ 1 ] ) ? $parts[ 1 ] : '';
      if ( ! $search || ! $replacement ) {
        continue;
      }

      // Parse search
      $search = explode( ':', $search );
      $search_type = isset( $search[ 0 ] ) ? $search[ 0 ] : '';
      $search_name = isset( $search[ 1 ] ) ? $search[ 1 ] : '';
      if (
        ! $search_type ||
        ( ! $search_name && ( 'content' != $search_type ) )
      ) {
        continue;
      }
      
      // Compute value
      $replacement = explode( ':', $replacement );
      $replacement_type = isset( $replacement[ 0 ] ) ? $replacement[ 0 ] : '';
      $replacement_name = isset( $replacement[ 1 ] ) ? $replacement[ 1 ] : '';
      $replacement_na_action = isset( $replacement[ 2 ] ) ? $replacement[ 2 ] : '';
      if ( ! $replacement_type || ! $replacement_name ) {
        continue;
      }

      // Get value and allow 3rd parties to adjust
      $value = apply_filters( "cptemplates/dynamic_shortcode_values/get_{$replacement_type}_value", null, $replacement_name, $tag, $atts );
      
      // Skip if no handler defined or value is not available
      if ( null === $value ) {

        // Do Nothing
        if ( ! $replacement_na_action ) {
          continue;
        }
        // Clear placeholder
        elseif (
          ( ( 'remove' == $replacement_na_action ) && ( 'text' == $search_type ) ) ||
          ( ( 'clear' == $replacement_na_action ) && ( 'text' == $search_type ) )
        ) {
          $value = '';
        }
        elseif ( 'remove' == $replacement_na_action ) {
          unset( $atts[ $search_name ] );
          continue;
        }
        elseif ( 'clear' == $replacement_na_action ) {
          $atts[ $search_name ] = '';
          continue;
        }
        elseif ( 'remove_shortcode' == $replacement_na_action ) {
          return '';
        }

        // continue;
      }

      // Format value and allow 3rd parties to adjust
      $value = apply_filters( "cptemplates/dynamic_shortcode_values/format_{$search_type}_value", $value, $search_name, $tag, $atts ); // General
      $value = apply_filters( "cptemplates/dynamic_shortcode_values/format_{$search_type}_{$search_name}_value", $value, $tag, $atts ); // Specific

      // Only string is allowed
      $value = $this->convert_to_string( $value );

      // Replace placeholder in content and all attributes
      if ( 'text' == $search_type ) {
        $content = str_replace( $search_name, $value, $content );
        $atts = array_map( function( $attr ) use ( $search_name, $value ) {
          return str_replace( $search_name, $value, $attr );
        }, $atts );
      }
      elseif ( 'attr' == $search_type ) {
        // Quotes should not be used in shortcode params
        $value = str_replace( '"', "'", $value );
        $atts[ $search_name ] = $value;
      }
      elseif ( 'content' == $search_type ) {
        $content = $value;
      }
      // Undefined search type
      else {
        continue;
      }

    }

    return array( 
      'atts' => $atts,
      'content' => $content,
    );
  }


  /**
   *  convert_to_string
   *
   *  Converts provided object to string
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function convert_to_string( $value ) {
    // Convert simple arrays if no formatting filter has been applied
    if ( is_array( $value ) ) {
      $value = array_values( $value );
      $value = array_filter( $value, function( $item ) {
        return is_string( $item ) || is_numeric( $item );
      } );
      $value = implode( ',', $value );
    }

    // Only string is allowed
    $value = is_string( $value ) || is_numeric( $value ) ? trim( strval( $value ) ) : '';

    return $value;
  }


  /**
   *  get_object_value
   *
   *  Hook to retrieve currently queried object property
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $name
   *  @return  (mixed)
   */

  public function get_object_value( $value, $name ) {
    $queried_object = get_queried_object();
    if ( ! $queried_object ) {
      return $value;
    }

    return isset( $queried_object->$name ) ? $queried_object->$name : $value;
  }


  /**
   *  get_meta_value
   *
   *  Hook to retrieve meta value for post, term or user
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $name
   *  @return  (mixed)
   */

  public function get_meta_value( $default_value, $name ) {
    $queried_object = get_queried_object();
    if ( ! $queried_object ) {
      return $default_value;
    }

    $queried_object_id = get_queried_object_id();

    if ( is_a( $queried_object, 'WP_Post' ) ) {
      $meta_type = 'post';
    }
    elseif ( is_a( $queried_object, 'WP_Term' ) ) {
      $meta_type = 'term';
    }
    elseif ( is_a( $queried_object, 'WP_User' ) ) {
      $meta_type = 'user';
    }
    else {
      return $default_value;
    }

    $meta_value = get_metadata( $meta_type, $queried_object_id, $name, true );

    // We need to explicitely return default value if meta value is not there at all
    // e.g. avoid mixing with empty string/array, 0 and false values
    if ( ! $meta_value ) {
      $meta_cache = wp_cache_get( $queried_object_id, $meta_type . '_meta' );
      $meta_value = isset( $meta_cache[ $name ] ) ? $meta_value : null;
    }

    return null === $meta_value ? $default_value : $meta_value;
  }


  /**
   *  get_filter_value
   *
   *  Hook to retrieve data by applying specified filter hook
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $name
   *  @return  (mixed)
   */

  public function get_filter_value( $value, $name ) {
    return apply_filters( $name, $value );
  }


  /**
   *  get_action_value
   *
   *  Hook to retrieve data by capturing output of specified action
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $name
   *  @return  (mixed)
   */

  public function get_action_value( $value, $name ) {
    if ( ! has_action( $name ) ) {
      return $value;
    }

    ob_start();
    do_action( $name );
    return ob_get_clean();
  }


  /**
   *  get_http_request_value
   *
   *  Hook to retrieve data from HTTP request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $name
   *  @return  (mixed)
   */

  public function get_http_request_value( $value, $name ) {
    return isset( $_REQUEST[ $name ] ) ? $_REQUEST[ $name ] : $value;
  }


  /**
   *  get_template_value
   *
   *  Hook to retrieve data from theme's template file
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $name
   *  @return  (mixed)
   */

  public function get_template_value( $value, $name ) {
    ob_start();
    locate_template( $name, true );
    $result = ob_get_clean();
    return $result ? $result : $value;
  }


}


/**
 *  cptemplates_dynamic_shortcode_values
 *
 *  The main function responsible for returning cptemplates_dynamic_shortcode_values object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_dynamic_shortcode_values instance
 */

function cptemplates_dynamic_shortcode_values() {
  return cptemplates_dynamic_shortcode_values::instance();
}


// initialize
cptemplates_dynamic_shortcode_values();


endif; // class_exists check

?>