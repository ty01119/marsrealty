<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/**
 *  cptemplates_get_option
 *
 *  Retrieves plugin option
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param $name (string) setting name
 *  @param $value (mixed) default value
 *  @return  (mixed)
 */

if ( ! function_exists( 'cptemplates_get_option' ) ) :
  function cptemplates_get_option( $name, $default_value = null ) {
    $field_name = sprintf( 'cpt_%s', $name );
    $field_value = get_field( $field_name, 'option' );
    return ! is_null( $field_value ) ? $field_value : $default_value;
  }
endif;


/**
 *  cptemplates_get_setting
 *
 *  alias of cptemplates()->get_setting()
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param $name (string) setting name
 *  @param $value (mixed) default value
 *  @return  (mixed)
 */

if ( ! function_exists( 'cptemplates_get_setting' ) ) :
  function cptemplates_get_setting( $name, $value = null ) {
    return cptemplates()->get_setting( $name, $value );
  }
endif;


/**
 *  cptemplates_update_setting
 *
 *  alias of cptemplates()->update_setting()
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param $name (string) setting name
 *  @param $value (mixed) default value
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_update_setting' ) ) :
  function cptemplates_update_setting( $name, $value ) {
    return cptemplates()->update_setting( $name, $value );
  }
endif;


/**
 *  cptemplates_include_all
 *
 *  alias of cptemplates()->include_all()
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param $path (string)
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_include_all' ) ) :
  function cptemplates_include_all( $path = '' ) {
    return cptemplates()->include_all( $path );
  }
endif;


/**
 *  cptemplates_php_debug
 *
 *  Nice variable debug output
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param $title (string) variable name
 *  @param $value (mixed) variable value
 *  @param $show (boolean) wether to make debug visible on front-end or hide
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_php_debug' ) ) :
  function cptemplates_php_debug( $title = '', $value = '', $show = true ){
    $value = isset( $value ) ? ( is_array( $value ) || is_object( $value ) ? sprintf( '<pre>%s</pre>', print_r( $value, true ) ) : $value ) : 'Not set';
    $output = sprintf( '%s: %s<br />', $title ? $title : '(not set)', $value );
    printf( '<div class="cptemplates_php_debug" style="display:%s">%s</div>', $show ? 'block' : 'none', $output );

    // cptemplates_log( $title, $value ); // TMP
  }
endif;


/**
 *  cptemplates_log
 *
 *  Variable debug output to file
 *
 *  @type  function
 *  @date  08/01/18
 *  @since 2.0.5
 *
 *  @param $title (string) variable name
 *  @param $value (mixed) variable value
 *  @param $show (boolean) wether to make debug visible on front-end or hide
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_log' ) ) :
  function cptemplates_log( $heading = '', $content = '' ){
    // Create Folder if not exists
    $folder_path = sprintf( '%swp-content/logs/custom_page_templates/%s/%s/', ABSPATH, date( 'Y' ), date( 'm' ) );
    if ( ! file_exists( $folder_path ) ) {
      if ( ! mkdir( $folder_path, 0777, true ) ) return false; // Stop if folder can't be created
    }

    // Create file per date
    $filename = $folder_path . date( 'd' ).'.txt';

    $dt = new DateTime();
    $log_time = $dt->format('H:i:s');

    $heading = $heading ? '========= '.$heading.' ('.$log_time.') =========' : '========= ('.$log_time.') =========';
    file_put_contents( $filename, $heading . "\n", FILE_APPEND );

    if ( $content ) {
      if ( is_array( $content ) || is_object( $content ) ) {
        $content = print_r( $content, true );
      }

      file_put_contents( $filename,  $content . "\n\n", FILE_APPEND ); 
    }

    return true;    
  }
endif;


/**
 *  cptemplates_get_url
 *
 *  Retrieve URL relative to the plugin
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param $uri (string) uri relative to the plugin's folder
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_get_url' ) ) :
  function cptemplates_get_url( $uri = '' ) {
    return cptemplates_get_setting( 'url' ) . $uri;
  }
endif;


/**
 *  cptemplates_get_path
 *
 *  Retrieve Path relative to the plugin
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param $path (string) path relative to the plugin's folder
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_get_path' ) ) :
  function cptemplates_get_path( $path = '' ) {
    return cptemplates_get_setting( 'path' ) . $path;
  }
endif;


/**
 *  cptemplates_display_admin_notice
 *
 *  Formats and outputs notice HTML in admin
 *
 *  @type  function
 *  @date  01/02/18
 *  @since 3.0.0
 *
 *  @param $path (string) path relative to the plugin's folder
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_display_admin_notice' ) ) :
  function cptemplates_display_admin_notice( $message = null, $type = 'success', $is_dismissible = true ) {
    if ( ! $message ) return;

    printf(
      '<div class="notice notice-%s %s"><p>%s</p></div>',
      $type,
      $is_dismissible ? 'is-dismissible' : '',
      $message
    );
  }
endif;


/**
 *  cptemplates_is_access_allowed
 *
 *  Checks if current user is eligible to access requested resource
 *  Allow all for administrator and optionally block others if such is configired in options
 *
 *  @type  function
 *  @date  01/02/18
 *  @since 3.0.0
 *
 *  @param N/A
 *  @return  (boolean)
 */

if ( ! function_exists( 'cptemplates_is_access_allowed' ) ) :
  function cptemplates_is_access_allowed() {
    return current_user_can( 'administrator' ) || ! get_field( 'cpt_restrict_access', 'option' );
  }
endif;


/**
 *  cptemplates_get_template_content
 *
 *  Retrieves template content
 *
 *  @type  function
 *  @date  19/12/17
 *  @since 2.0.0
 *
 *  @param (function) $custom_content_foo
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_get_template_content' ) ) :
  function cptemplates_get_template_content( $custom_content_foo = null ) {
    return cptemplates_template()->get_the_content( $custom_content_foo );
  }
endif;


/**
 *  cptemplates_template_content
 *
 *  Displays template content
 *
 *  @type  function
 *  @date  19/12/17
 *  @since 2.0.0
 *
 *  @param (function) $custom_content_foo
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_template_content' ) ) :
  function cptemplates_template_content( $custom_content_foo = null ) {
    cptemplates_template()->the_content( $custom_content_foo );
  }
endif;


/**
 *  cptemplates_get_terms
 *
 *  Safely retrieve terms based on current WP version
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param $args (array) arguments
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_get_terms' ) ) :
  function cptemplates_get_terms( $args = array() ) {
    global $wp_version;
    return version_compare( $wp_version, '4.5', '>=' ) ? get_terms( $args ) : get_terms( $args[ 'taxonomy' ], $args );
  }
endif;


/**
 *  cptemplates_is_plugin_active
 *
 *  Checks if plugin is active by applying regular expression
 *
 *  @type  function
 *  @date  15/10/17
 *  @since 1.0.3
 *
 *  @param $re (string) regular expression to search a plugin
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_is_plugin_active' ) ) :
  function cptemplates_is_plugin_active( $re ) {
    $active_plugins = (array)get_option( 'active_plugins', array() );
    $matches = preg_grep( $re, $active_plugins );
    return count( $matches ) > 0;
  }
endif;


/**
 *  cptemplates_vc_is_inline
 *
 *  Checks if it's inline frontend editor mode in Visual Composer
 *
 *  @type  function
 *  @date  12/11/17
 *  @since 1.0.3
 *
 *  @param N/A
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_vc_is_inline' ) ) :
  function cptemplates_vc_is_inline() {
    return function_exists( 'vc_is_inline' ) ? vc_is_inline() : false;
  }
endif;


/**
 *  cptemplates_vc_is_frontend_editor
 *
 *  Checks if it's frontend editor mode in Visual Composer
 *
 *  @type  function
 *  @date  12/11/17
 *  @since 1.0.3
 *
 *  @param N/A
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_vc_is_frontend_editor' ) ) :
  function cptemplates_vc_is_frontend_editor() {
    return function_exists( 'vc_is_frontend_editor' ) ? vc_is_frontend_editor() : ( function_exists( 'vc_is_editor' ) ? vc_is_editor() : false );
  }
endif;


/**
 *  cptemplates_is_visualcomposer_plugin_active
 *
 *  Checks if Visual Composer plugin is active
 *
 *  @type  function
 *  @date  15/10/17
 *  @since 1.0.3
 *
 *  @param N/A
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_is_visualcomposer_plugin_active' ) ) :
  function cptemplates_is_visualcomposer_plugin_active() {
    return cptemplates_is_plugin_active( '#(.*?)(js_composer|wp_bakery)(.*?)/js_composer\.php$#i' );
  }
endif;


/**
 *  cptemplates_is_woocommerce_plugin_active
 *
 *  Checks if WooCommerce plugin is active
 *
 *  @type  function
 *  @date  15/10/17
 *  @since 1.0.3
 *
 *  @param N/A
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_is_woocommerce_plugin_active' ) ) :
  function cptemplates_is_woocommerce_plugin_active() {
    return class_exists( 'WooCommerce' ) || cptemplates_is_plugin_active( '#^woocommerce(.*?)/woocommerce\.php$#i' );
  }
endif;


/**
 *  cptemplates_is_acf_plugin_active
 *
 *  Checks if ACF plugin is active
 *
 *  @type  function
 *  @date  13/11/17
 *  @since 1.0.5
 *
 *  @param N/A
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_is_acf_plugin_active' ) ) :
  function cptemplates_is_acf_plugin_active() {
    return class_exists( 'acf_pro' ) || class_exists( 'acf' ) || cptemplates_is_plugin_active( '#^advanced\-custom\-fields/acf\.php$#i' ) || cptemplates_is_plugin_active( '#^advanced\-custom\-fields\-pro(.*?)/acf\.php$#i' );
  }
endif;


/**
 *  cptemplates_is_wpml_plugin_active
 *
 *  Checks if WPML plugin is active
 *
 *  @type  function
 *  @date  03/02/18
 *  @since 3.0.0
 *
 *  @param N/A
 *  @return  N/A
 */

if ( ! function_exists( 'cptemplates_is_wpml_plugin_active' ) ) :
  function cptemplates_is_wpml_plugin_active() {
    return class_exists( 'SitePress' ) || cptemplates_is_plugin_active( '#^sitepress\-multilingual\-cms/sitepress\.php$#i' );
  }
endif;


/**
 *  cptemplates_get_editable_roles
 *
 *  Safely retrieve available user roles
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return  (array)
 */

if ( ! function_exists( 'cptemplates_get_editable_roles' ) ) :
  function cptemplates_get_editable_roles() {
    // Ensure we have the required function
    if ( ! function_exists( 'get_editable_roles' ) ) {
      require_once ABSPATH . 'wp-admin/includes/user.php';
    }

    return (array)get_editable_roles();
  }
endif;


/**
 *  cptemplates_is_woocommerce_version
 *
 *  Checks if WP is using specific WooCommerce version
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return  (array)
 */

if ( ! function_exists( 'cptemplates_is_woocommerce_version' ) ) :
  function cptemplates_is_woocommerce_version( $version ) {
    global $woocommerce;
    return version_compare( $woocommerce->version, $version, ">=" );
  }
endif;


/**
 *  cptemplates_is_jupiter_theme
 *
 *  Checks if WP is using Jupiter theme
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return  (array)
 */

if ( ! function_exists( 'cptemplates_is_jupiter_theme' ) ) :
  function cptemplates_is_jupiter_theme() {
    return function_exists( 'mk_theme_options' ) || ( 'Jupiter' == get_option( 'template' ) );
  }
endif;


/**
 *  cptemplates_is_avada_theme
 *
 *  Checks if WP is using Avada theme
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return  (array)
 */

if ( ! function_exists( 'cptemplates_is_avada_theme' ) ) :
  function cptemplates_is_avada_theme() {
    return class_exists( 'Avada' ) || ( 'Avada' == get_option( 'template' ) );
  }
endif;


/**
 *  cptemplates_is_betheme_theme
 *
 *  Checks if WP is using Betheme theme
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return  (array)
 */

if ( ! function_exists( 'cptemplates_is_betheme_theme' ) ) :
  function cptemplates_is_betheme_theme() {
    return 'betheme' == get_option( 'template' );
  }
endif;


/**
 *  cptemplates_is_udesign_theme
 *
 *  Checks if WP is using U-Design theme
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return  (array)
 */

if ( ! function_exists( 'cptemplates_is_udesign_theme' ) ) :
  function cptemplates_is_udesign_theme() {
    return defined( 'UDESIGN_VERSION' ) || ( 'u-design' == get_option( 'template' ) );
  }
endif;


?>