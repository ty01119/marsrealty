<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_custom_taxonomies' ) ) :

final class cptemplates_custom_taxonomies {


  /**
   * Plugin instance
   *
   *  @since 3.0.0
   *  @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   *  @since 3.0.0
   *  @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_custom_taxonomies
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_custom_taxonomies instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
    
    $this->register_taxonomies();
  }


  /**
   *  register_taxonomies
   *
   *  Registers in the system all saved taxonomies
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function register_taxonomies() {
    $admin_statuses = array( 'private', 'draft' );
    $is_admin_user = current_user_can( 'administrator' );
    
    foreach ( $this->get_taxonomies() as $post_id => $args ) {

      $status = isset( $args[ 'status' ] ) ? $args[ 'status' ] : 'draft';

      // Register just published post types, or draft|private if user is admin
      if ( ( 'publish' === $status ) || ( $is_admin_user && in_array( $status, $admin_statuses ) ) ) {

        // Allow 3rd parties to control
        if ( apply_filters( 'cptemplates/custom_taxonomies/register', true, $args ) ) {
          unset( $args[ 'status' ] );
          
          $taxonomy = isset( $args[ 'taxonomy' ] ) ? $args[ 'taxonomy' ] : '';
          unset( $args[ 'taxonomy' ] );
          
          $object_type = isset( $args[ 'object_type' ] ) ? $args[ 'object_type' ] : '';
          unset( $args[ 'object_type' ] );

          register_taxonomy( $taxonomy, $object_type, $args );
        }

      }
      
    }

  }


  /**
   *  get_taxonomies
   *
   *  Retrieve saved taxonomies from options table
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *
   *  @return  N/A
   */

  public function get_taxonomies() {
    $taxonomies = get_option( 'cpt_custom_taxonomies' );
    return is_array( $taxonomies ) ? $taxonomies : array();
  }


  /**
   *  set_taxonomies
   *
   *  Saved taxonomies to options table
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $taxonomies (array)
   *
   *  @return  N/A
   */

  public function set_taxonomies( $taxonomies = array() ) {
    $taxonomies = is_array( $taxonomies ) ? $taxonomies : array();

    // Allow 3rd parties to adjust
    $taxonomies = apply_filters( 'cptemplates/custom_taxonomies/set_taxonomies', $taxonomies );
    
    update_option( 'cpt_custom_taxonomies', $taxonomies, true );

    update_option( 'cpt_needs_rewrite_rules_update', true, true );
  }


}


/**
 *  cptemplates_custom_taxonomies
 *
 *  The main function responsible for returning cptemplates_custom_taxonomies object
 *
 *  @type  function
 *  @date  29/08/17
 *  @since 1.0.2
 *
 *  @param N/A
 *  @return (object) cptemplates_custom_taxonomies instance
 */

function cptemplates_custom_taxonomies() {
  return cptemplates_custom_taxonomies::instance();
}


// initialize
cptemplates_custom_taxonomies();


endif; // class_exists check

?>