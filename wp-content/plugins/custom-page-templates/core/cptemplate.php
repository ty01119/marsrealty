<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_cptemplate' ) ) :

final class cptemplates_cptemplate {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_cptemplate
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    if ( cptemplates_is_access_allowed() ) {
      add_action( 'init', array( $this, 'init' ) );
      add_action( 'cptemplates/template_applied', array( $this, 'template_applied' ) );
    }
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_cptemplate instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
    
    $this->register_post_type();
  }

  
  /**
   *  template_applied
   *
   *  Performs actions if template has been applied for the current request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function template_applied( $template_post ) {
    if ( ! is_admin() ) {
      add_action( 'admin_bar_menu', array( $this, 'admin_bar_menu' ), 80 );
    }
  }
  
  
  /**
   *  admin_bar_menu
   *
   *  Performs actions if template has been applied for the current request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function admin_bar_menu( $admin_bar ) {
    $template_post = cptemplates_template()->get_applied_template_post();

    if ( ! $template_post ) {
      return;
    }

    $admin_bar->add_menu( array(
      'id'     => 'cptemplate',
      'parent' => null,
      'group'  => null,
      'title'  => __( 'Edit Template', 'cptemplates' ),
      'href'   => get_edit_post_link( $template_post->ID ),
    ) );    
  }
  

  /**
   *  register_post_type
   *
   *  Create cptemplate custom post type
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function register_post_type() {
    
    $labels = array(
      'menu_name'             => __( 'Page Templates', 'cptemplates' ),
      'name'                  => __( 'Custom Page Templates', 'cptemplates' ),
      'singular_name'         => __( 'Custom Page Template', 'cptemplates' ),
      'all_items'             => __( 'All Templates', 'cptemplates' ),
      'add_new_item'          => __( 'Add New Template', 'cptemplates' ),
      'new_item'              => __( 'New Template', 'cptemplates' ),
      'edit_item'             => __( 'Edit Template', 'cptemplates' ),
      'update_item'           => __( 'Update Template', 'cptemplates' ),
      'view_item'             => __( 'View Template', 'cptemplates' ),
      'search_items'          => __( 'Search Templates', 'cptemplates' ),
    );
    $args = array(
      'labels'                => $labels,
      'supports'              => array( 'title', 'editor' ),
      'hierarchical'          => false,
      'public'                => true,
      'show_ui'               => true,
      'show_in_menu'          => true,
      'menu_icon'             => 'dashicons-welcome-widgets-menus',
      'menu_position'         => 30,
      'show_in_admin_bar'     => true,
      'show_in_nav_menus'     => false,
      'can_export'            => true,
      'has_archive'           => false,   
      'exclude_from_search'   => true,
      'publicly_queryable'    => false,
      'rewrite'               => false,
      'query_var'             => false,
      'capability_type'       => 'post',
    );

    register_post_type( 'cptemplate', $args );
  }


  /**
   *  get_templates
   *
   *  Retrieve list of all available templates
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (array) 
   */

  public function get_templates() {
    if ( isset( $this->templates ) ) {
      return $this->templates;
    }

    $args = array(
      'post_type' => 'cptemplate',
      'posts_per_page' => -1,
      'post_status' => array( 'publish', 'private' ),
    );
    $query = new WP_Query( $args );
    $items = is_array( $query->posts ) ? $query->posts : array();

    $templates = array();
    foreach ( $items as $item ) {
      $templates[ (string)$item->ID ] = $item->post_title ? $item->post_title : __( '(No title)', 'cptemplates' );
    }

    $default_templates = array( '' => __( 'Default', 'cptemplates' ), 'disable' => __( 'Disable Custom Template', 'cptemplates' ) );

    $this->templates = $default_templates + $templates;

    return $this->templates;
  }


  /**
   *  get_page_template_file
   *
   *  Retrieve page template filename
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (int) $template_id
   *  @return  (boolean|string) 
   */

  public function get_page_template_file( $template_id = 0 ) {
    $template_id = (int)$template_id;
    if ( ! $template_id ) {
      return false;
    }

    $template_file = get_post_meta( $template_id, 'cpt_template_file', true );

    // Temporary backwards compatibility
    // $template_file = $template_file ? $template_file : get_post_meta( $template_id, 'cpt_existing_template', true );
    if ( ! $template_file && ( $template_file = get_post_meta( $template_id, 'cpt_existing_template', true ) ) ) {
      cptemplates_cptemplate()->set_page_template_file( $template_id, $template_file ); // Copy template to new postmeta key
      delete_post_meta( $template_id, 'cpt_existing_template' ); // Remove old post meta key
    }

    return $template_file;
  }


  /**
   *  set_page_template_file
   *
   *  Retrieve page template filename
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (int) $template_id
   *  @param (string) $filename
   *  @return  N/A
   */

  public function set_page_template_file( $template_id = 0, $filename = '' ) {
    $template_id = (int)$template_id;
    if ( ! $template_id ) {
      return null;
    }

    $filename = (string)$filename;
    $filename = trim( $filename );
    if ( $filename && locate_template( $filename ) ) {
      update_post_meta( $template_id, 'cpt_template_file', $filename );
    }
    else {
      delete_post_meta( $template_id, 'cpt_template_file' );
    }
  }


}


/**
 *  cptemplates_cptemplate
 *
 *  The main function responsible for returning cptemplates_cptemplate object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_cptemplate instance
 */

function cptemplates_cptemplate() {
  return cptemplates_cptemplate::instance();
}


// initialize
cptemplates_cptemplate();


endif; // class_exists check

?>