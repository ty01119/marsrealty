<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_custom_post_types' ) ) :

final class cptemplates_custom_post_types {


  /**
   * Plugin instance
   *
   *  @since 3.0.0
   *  @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   *  @since 3.0.0
   *  @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_custom_post_types
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_custom_post_types instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
    
    $this->register_custom_post_types();
  }


  /**
   *  register_custom_post_types
   *
   *  Registers in the system all saved post types
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function register_custom_post_types() {
    $admin_statuses = array( 'private', 'draft' );
    $is_admin_user = current_user_can( 'administrator' );
    
    foreach ( $this->get_post_types() as $post_id => $args ) {

      $status = isset( $args[ 'status' ] ) ? $args[ 'status' ] : 'draft';

      // Register just published post types, or draft|private if user is admin
      if ( ( 'publish' === $status ) || ( $is_admin_user && in_array( $status, $admin_statuses ) ) ) {

        // Allow 3rd parties to control
        if ( apply_filters( 'cptemplates/custom_post_types/register', true, $args ) ) {
          unset( $args[ 'status' ] );
          
          $post_type = isset( $args[ 'post_type' ] ) ? $args[ 'post_type' ] : '';
          unset( $args[ 'post_type' ] );

          register_post_type( $post_type, $args );
        }

      }
      
    }

  }


  /**
   *  get_post_types
   *
   *  Retrieve saved post types from options table
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *
   *  @return  N/A
   */

  public function get_post_types() {
    $post_types = get_option( 'cpt_custom_post_types' );
    return is_array( $post_types ) ? $post_types : array();
  }


  /**
   *  set_post_types
   *
   *  Saved post types to options table
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_types (array)
   *
   *  @return  N/A
   */

  public function set_post_types( $post_types = array() ) {
    $post_types = is_array( $post_types ) ? $post_types : array();

    // Allow 3rd parties to adjust
    $post_types = apply_filters( 'cptemplates/custom_post_types/set_post_types', $post_types );
    
    update_option( 'cpt_custom_post_types', $post_types, true );

    update_option( 'cpt_needs_rewrite_rules_update', true, true );
  }


}


/**
 *  cptemplates_custom_post_types
 *
 *  The main function responsible for returning cptemplates_custom_post_types object
 *
 *  @type  function
 *  @date  29/08/17
 *  @since 1.0.2
 *
 *  @param N/A
 *  @return (object) cptemplates_custom_post_types instance
 */

function cptemplates_custom_post_types() {
  return cptemplates_custom_post_types::instance();
}


// initialize
cptemplates_custom_post_types();


endif; // class_exists check

?>