<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( cptemplates_template()->render_template() ) {
  get_header();
  cptemplates_template_content();
  get_footer();
}
