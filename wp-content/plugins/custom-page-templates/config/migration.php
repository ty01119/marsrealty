<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*********************************************************/
/* AUTOMATICALLY MIGRATE SETTINGS FROM REDUX TO ACF
/*********************************************************/
if ( is_admin() && cptemplates_is_access_allowed() && ( $redux_options = get_option( 'cptemplates' ) ) ) {
  $skip_fields = array( 'last_tab' );
  foreach ( $redux_options as $key => $value ) {

    if ( ! in_array( $key, $skip_fields ) ) {
      $field_name = sprintf( 'cpt_%s', $key );
      update_field( $field_name, $value, 'option' );
    }

  }

  // Remove old redux options
  delete_option( 'cptemplates' );
  delete_option( 'cptemplates-transients' );
}
