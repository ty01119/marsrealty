<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( is_admin() && cptemplates_is_access_allowed() ) {
  add_action( 'admin_menu', 'cptemplates_admin_menu_pages' );
  add_action( 'admin_menu', 'cptemplates_reorder_admin_menu_pages', 100 ); // ACF hooks at 99
}

function cptemplates_admin_menu_pages() {

  // Create Plugin Options page
  acf_add_options_page( array(
    'page_title'  => __( 'Custom Page Templates Settings', 'cptemplates' ),
    'menu_title'  => __( 'CPT Settings', 'cptemplates' ),
    'menu_slug'   => 'cptemplates',
    'capability'  => 'edit_posts',
    'redirect'  => true,
  ) );


  acf_add_options_sub_page( array(
    'page_title'  => __( 'CPT General Settings', 'cptemplates' ),
    'menu_title'  => __( 'General', 'cptemplates' ),
    'menu_slug'   => 'cptemplates_general',
    'parent_slug'   => 'cptemplates',
  ) );


  acf_add_options_sub_page( array(
    'page_title'  => __( 'Custom Option Pages', 'cptemplates' ),
    'menu_title'  => __( 'Option Pages', 'cptemplates' ),
    'menu_slug'   => 'cptemplates_custom_option_pages',
    'parent_slug'   => 'cptemplates',
  ) );

}

function cptemplates_reorder_admin_menu_pages() {
  global $submenu;

  $options_menu = isset( $submenu[ 'cptemplates_general' ] ) ? $submenu[ 'cptemplates_general' ] : false;
  if ( ! $options_menu ) return;

  $menu_order = array(
    'cptemplates_general' => 1,
    'edit.php?post_type=cpt_cpt' => 2,
    'edit.php?post_type=cpt_tax' => 3,
    'cptemplates_custom_option_pages' => 4,
  );

  usort( $options_menu, function( $a, $b ) use ( $menu_order ) {
    $ao = isset( $menu_order[ $a[ 2 ] ] ) ? $menu_order[ $a[ 2 ] ] : 999;
    $bo = isset( $menu_order[ $b[ 2 ] ] ) ? $menu_order[ $b[ 2 ] ] : 999;
    return $ao > $bo;
  });

  $submenu[ 'cptemplates_general' ] = $options_menu;
}
