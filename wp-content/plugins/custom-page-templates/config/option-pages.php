<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
  'key' => 'group_5a779011978b3',
  'title' => __( 'Custom Options Pages', 'cptemplates' ),
  'fields' => array (
    array (
      'key' => 'field_5a77903131f9a',
      'label' => __( 'Options Pages', 'cptemplates' ),
      'name' => 'cptemplates_options_pages',
      'type' => 'repeater',
      'instructions' => '',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array (
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'collapsed' => 'field_5a77905855d4c',
      'min' => 0,
      'max' => 0,
      'layout' => 'row',
      'button_label' => __( 'Add Options Page', 'cptemplates' ),
      'sub_fields' => array (
        array (
          'key' => 'field_5a77905855d4c',
          'label' => __( 'Page Title', 'cptemplates' ),
          'name' => 'page_title',
          'type' => 'text',
          'instructions' => '',
          'required' => 1,
          'conditional_logic' => 0,
          'wrapper' => array (
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array (
          'key' => 'field_5a77907945367',
          'label' => __( 'Menu Title', 'cptemplates' ),
          'name' => 'menu_title',
          'type' => 'text',
          'instructions' => __( 'Optional. Same as Page Title by default.', 'cptemplates' ),
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array (
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array (
          'key' => 'field_5a77908ac89b7',
          'label' => __( 'Menu Slug', 'cptemplates' ),
          'name' => 'menu_slug',
          'type' => 'text',
          'instructions' => __( 'Optional. Derives from page title by default.', 'cptemplates' ),
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array (
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => '',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array (
          'key' => 'field_5a7790afcc716',
          'label' => __( 'Capability', 'cptemplates' ),
          'name' => 'capability',
          'type' => 'text',
          'instructions' => __( 'Optional. Uses edit_posts by default.', 'cptemplates' ),
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array (
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'default_value' => '',
          'placeholder' => __( 'e.g. edit_posts, manage_options', 'cptemplates' ),
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
        array (
          'key' => 'field_5a7790c4e0037',
          'label' => __( 'Subpages', 'cptemplates' ),
          'name' => 'subpages',
          'type' => 'repeater',
          'instructions' => '',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array (
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'row_template' => 0,
          'collapsed' => 'field_5a7790d72fa59',
          'min' => 0,
          'max' => 0,
          'layout' => 'table',
          'button_label' => __( 'Add Subpage', 'cptemplates' ),
          'sub_fields' => array (
            array (
              'key' => 'field_5a7790d72fa59',
              'label' => __( 'Page Title', 'cptemplates' ),
              'name' => 'page_title',
              'type' => 'text',
              'instructions' => '',
              'required' => 1,
              'conditional_logic' => 0,
              'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
              ),
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'maxlength' => '',
            ),
            array (
              'key' => 'field_5a7790eab4205',
              'label' => __( 'Menu Title', 'cptemplates' ),
              'name' => 'menu_title',
              'type' => 'text',
              'instructions' => __( 'Optional. Same as Page Title by default.', 'cptemplates' ),
              'required' => 0,
              'conditional_logic' => 0,
              'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
              ),
              'default_value' => '',
              'placeholder' => '',
              'prepend' => '',
              'append' => '',
              'maxlength' => '',
            ),
          ),
        ),
      ),
    ),
  ),
  'location' => array (
    array (
      array (
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'cptemplates_custom_option_pages',
      ),
    ),
  ),
  'menu_order' => 1,
  'position' => 'normal',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => 1,
  'description' => __( 'CPT Option Pages', 'cptemplates' ),
));

endif;


if ( is_admin() ) {

  $options_pages = get_field( 'cptemplates_options_pages', 'option' );

  // Bail early if no options pages added or invalid
  if ( ! $options_pages || ! is_array( $options_pages ) || empty( $options_pages ) ) {
    return false;
  }

  foreach ( $options_pages as $options_page ) {

    // Page title is required
    $options_page[ 'page_title' ] = isset( $options_page[ 'page_title' ] ) ? trim( $options_page[ 'page_title' ] ) : '';
    if ( ! $options_page[ 'page_title' ] ) {
      continue;
    }

    // Menu title is optional, using page title if not specified
    $options_page[ 'menu_title' ] = isset( $options_page[ 'menu_title' ] ) ? trim( $options_page[ 'menu_title' ] ) : '';
    $options_page[ 'menu_title' ] = $options_page[ 'menu_title' ] ? $options_page[ 'menu_title' ] : $options_page[ 'page_title' ];

    // Menu slug is optional, uses menu title if not specified and sanitizes it
    $options_page[ 'menu_slug' ] = isset( $options_page[ 'menu_slug' ] ) ? trim( $options_page[ 'menu_slug' ] ) : '';
    $options_page[ 'menu_slug' ] = $options_page[ 'menu_slug' ] ? $options_page[ 'menu_slug' ] : $options_page[ 'menu_title' ];
    $options_page[ 'menu_slug' ] = sanitize_title_with_dashes( $options_page[ 'menu_slug' ] );

    // Capatibility is optional, uses edit_posts by default
    $options_page[ 'capability' ] = isset( $options_page[ 'capability' ] ) ? trim( $options_page[ 'capability' ] ) : '';
    $options_page[ 'capability' ] = $options_page[ 'capability' ] ? $options_page[ 'capability' ] : 'edit_posts';

    // Prepare and Sanitize subpages
    $subpages = array();
    $options_page[ 'subpages' ] = isset( $options_page[ 'subpages' ] ) && is_array( $options_page[ 'subpages' ] ) ? $options_page[ 'subpages' ] : array();
    foreach ( $options_page[ 'subpages' ] as $subpage ) {

      // Subpage title is required
      $subpage[ 'page_title' ] = isset( $subpage[ 'page_title' ] ) ? trim( $subpage[ 'page_title' ] ) : '';
      if ( ! $subpage[ 'page_title' ] ) {
        continue;
      }

      // Menu title is optional, using page title if not specified
      $subpage[ 'menu_title' ] = isset( $subpage[ 'menu_title' ] ) ? trim( $subpage[ 'menu_title' ] ) : '';
      $subpage[ 'menu_title' ] = $subpage[ 'menu_title' ] ? $subpage[ 'menu_title' ] : $subpage[ 'page_title' ];

      $subpages []= array(
        'page_title' => $subpage[ 'page_title' ],
        'menu_title' => $subpage[ 'menu_title' ],
        'parent_slug' => $options_page[ 'menu_slug' ],
      );
    }
    unset( $options_page[ 'subpages' ] );

    // Root page will redirect to the first sub-page if exists
    $options_page[ 'redirect' ] = true;

    // Create Options Page
    acf_add_options_page( $options_page );

    // Add Subpages
    foreach ( $subpages as $subpage ) {
      acf_add_options_sub_page( $subpage );
    }
    
  }

}
