<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_woocommerce' ) ) :

final class cptemplates_integrate_woocommerce {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_woocommerce
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Include all WC components
    if ( cptemplates_is_woocommerce_plugin_active() ) {
      cptemplates_include_all( 'integrations/woocommerce' );
      add_action( 'wp_head', 'wp_reset_query' );  
    }
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_woocommerce instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


}


/**
 *  cptemplates_integrate_woocommerce
 *
 *  The main function responsible for returning cptemplates_integrate_woocommerce object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_woocommerce instance
 */

function cptemplates_integrate_woocommerce() {
  return cptemplates_integrate_woocommerce::instance();
}


// initialize
cptemplates_integrate_woocommerce();


endif; // class_exists check

?>