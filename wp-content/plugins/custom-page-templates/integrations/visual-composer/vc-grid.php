<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_vc_vc_grid' ) ) :

final class cptemplates_integrate_vc_vc_grid {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;


  /**
   * Contains vc grid shortcode tags that could be overwritten
   *
   * @since 1.0.0
   * @var boolean $vc_grid_shortcodes
   */
  private $vc_grid_shortcodes = array( 'vc_basic_grid', 'vc_media_grid', 'vc_masonry_grid', 'vc_masonry_media_grid' );


  /**
   * Contains params that need to overwrite vc grid's params
   *
   * @since 1.0.0
   * @var boolean $vc_grid_overwrite_params
   */
  private $vc_grid_overwrite_params = array();


  /**
   * Contains current post in the VC Grid loop
   *
   * @since 2.0.5
   * @var mixed $vc_grid_loop_post
   */
  private $vc_grid_loop_post = null;


  /**
   * Flag to lock or unlock replacements in DSV
   *
   * @since 2.0.5
   * @var mixed $lock_dsv_replacements
   */
  private $lock_dsv_replacements = true;
  

  /**
   * Unique tag name used for shortcode tags encoding/decoding
   *
   * @since 2.0.5
   * @var mixed $cpt_dsv_vc_grid_tag
   */
  private $cpt_dsv_vc_grid_tag = 'cpt_dsv_vc_grid_tag';
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_vc_vc_grid
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ), 50 ); // Init late as we need to ensure that all shortcodes have been registered already
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_vc_vc_grid instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Fake shortcode, never processed
    add_shortcode( $this->cpt_dsv_vc_grid_tag, function( $atts =[], $content = '' ) {
      return 'FAKE ONE';
    } );

    if ( ! is_admin() ) {
      add_action( 'cptemplates/template_applied', array( $this, 'cptemplate_applied' ) );

    }
    else {
      $this->setup_vc_grid_shortcode_override_params();
    }

    // Allow 3rd party to adjust shortcode tags
    $this->vc_grid_shortcodes = apply_filters( 'cptemplates/vc_grid/shortcodes', $this->vc_grid_shortcodes );
    $this->vc_grid_shortcodes = is_array( $this->vc_grid_shortcodes ) ? $this->vc_grid_shortcodes : array();

    // VC Grid params overwrite hooks
    if ( ! wp_doing_ajax() ) {
      add_filter( 'do_shortcode_tag', array( $this, 'prepare_vc_grid_params_overwrite' ), 999, 4 );
      add_action( 'wp_footer', array( $this, 'enqueue_vc_grid_params_overwrite_script' ) );
    }
    else {
      add_action( 'wp_ajax_vc_get_vc_grid_data', array( $this, 'get_grid_data_for_ajax' ), 5 );
      add_action( 'wp_ajax_nopriv_vc_get_vc_grid_data', array( $this, 'get_grid_data_for_ajax' ), 5 );
    }


    // Listen to ajax requests for Grid's content. Hook earlier than standard VC hooks
    if ( wp_doing_ajax() ) {
      add_action( 'wp_ajax_vc_get_vc_grid_data', array( $this, 'maybe_setup_overrides' ), 5 );
      add_action( 'wp_ajax_nopriv_vc_get_vc_grid_data', array( $this, 'maybe_setup_overrides' ), 5 );

      // do_action_ref_array( 'cptemplates/vc_grid_override_query/' . $this->cptoverride, array( &$query, $this->cptoverride_shortcode, $this->cptoverride_vc_request_param ) );
      add_action( 'cptemplates/vc_grid_override_query/main_loop', array( $this, 'override_vc_grid_query_main_loop' ), 10, 4 );
      add_action( 'cptemplates/vc_grid_override_query/related_posts', array( $this, 'override_vc_grid_query_related_posts' ), 10, 4 );
    }
  }


  /**
   *  prepare_vc_grid_params_overwrite
   *
   *  Controls the shortcodes display
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function prepare_vc_grid_params_overwrite( $output, $tag, $atts, $m ) {

    // Check if this is supported tag
    if ( in_array( $tag, $this->vc_grid_shortcodes ) ) {
      $grid_id = $atts[ 'grid_id' ];
      unset( $atts[ 'grid_id' ] );
      $this->vc_grid_overwrite_params[ $grid_id ] = $atts;
    }

    return $output;
  }


  /**
   *  enqueue_vc_grid_params_overwrite_script
   *
   *  Controls the shortcodes display
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function enqueue_vc_grid_params_overwrite_script() {
    if ( empty( $this->vc_grid_overwrite_params ) ) {
      return;
    }

    // Enqueue required script if not enqueued yet
    if ( ! wp_script_is( 'cptemplates-vc-grid', 'enqueued' ) ) {
      wp_enqueue_script( 'cptemplates-vc-grid' );
    }
  
    // Supply overwrite params to JS script
    wp_localize_script(
      'cptemplates-vc-grid',
      'cptemplatesVcGridOverwrite',
      array(
        'params' => $this->vc_grid_overwrite_params
      )
    );

  }


  /**
   *  vc_basic_grid_atts
   *
   *  Outputs shortcode data for ajax request, called on any page
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function vc_basic_grid_atts( $out = array(), $pairs = array(), $atts = array(), $shortcode = null ) {
    $overwrite_params = vc_request_param( 'cpt_vc_grid_overwrite_params' );
    try {
      $overwrite_params = rawurldecode( $overwrite_params );
      $overwrite_params = stripslashes( $overwrite_params );
      $overwrite_params = json_decode( $overwrite_params );
      $overwrite_params = (array)$overwrite_params;
    } catch (Exception $e) {
    }

    if ( is_array( $overwrite_params ) && ! empty( $overwrite_params ) ) {
      $out = array_merge( $out, $overwrite_params );
    }

    return $out;
  }


  /**
   *  get_grid_data_for_ajax
   *
   *  Outputs shortcode data for ajax request, called on any page
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function get_grid_data_for_ajax() {

    if ( ! vc_request_param( 'cpt_vc_grid_overwrite_params' ) ) {
      return;
    }

    // Get grid shortcode
    $tag = str_replace( '.', '', vc_request_param( 'tag' ) );
    $shortcode_fishbone = visual_composer()->getShortCode( $tag );
    if ( ! is_object( $shortcode_fishbone ) || ! vc_get_shortcode( $tag ) ) {
      return;
    }

    // Make sure it's of proper class
    $vc_grid = $shortcode_fishbone->shortcodeClass();
    if ( ! is_a( $vc_grid, 'WPBakeryShortCode_VC_Basic_Grid' ) ) {
      return;
    }

    // Hook to replace shortcode params. Hook early to set the initial params and allow 3rd parties to overwrite later
    add_filter( 'shortcode_atts_' . $tag, array( $this, 'vc_basic_grid_atts' ), 0, 4 );


    // Hook to listed the_post evens during VC Grig loop
    add_action( 'the_post', array( $this, 'capture_vc_grid_post' ), 10, 2 );

    // Listen to DSV request to set queried object
    add_filter( 'cptemplates/dynamic_shortcode_values/replace_value/queried_object', array( $this, 'cpt_dsv_set_queried_object' ) );

    // Custom replacements processor for VC Grids
    add_filter( 'cptemplates/dynamic_shortcode_values/replace', array( $this, 'cpt_dsv_custom_replacement_processor' ), 10, 4 );

    return;
  }


  /**
   *  cpt_dsv_custom_replacement_processor
   *
   *  Locks or unlocks DSV to replace shortcode values
   *
   *  @type  function
   *  @date  08/01/18
   *  @since 2.0.5
   *
   *  @param $post WP_Post
   *  @param $wp_query WP_Query
   *
   *  @return  N/A
   */

  public function cpt_dsv_custom_replacement_processor( $value, $tag, $atts, $content ) {
  
    // Apply just for those with DSV replacements
    if ( ! isset( $atts[ 'cptdsv' ] ) ) {
      return $value;
    }

    // Encode
    if ( $this->lock_dsv_replacements ) {

      // Compose temporar shortcode
      $shortcode_atts = array();
      $shortcode_atts []= sprintf( '%s="%s"', $this->cpt_dsv_vc_grid_tag, $tag ); // Keep real tag in attribute
      foreach ( $atts as $key => $value ) {
        $shortcode_atts []= sprintf( '%s="%s"', $key, $value );
      }
      $shortcode = sprintf( '[%s %s]', $this->cpt_dsv_vc_grid_tag, implode( ' ', $shortcode_atts ) );

      $content = is_string( $content ) && $content ? $content : '';
      $shortcode .= sprintf( '%s[/%s]', $content, $this->cpt_dsv_vc_grid_tag );
    }
    // Decode shortcodes
    else {

      // Get real tag
      $tag = isset( $atts[ $this->cpt_dsv_vc_grid_tag ] ) ? $atts[ $this->cpt_dsv_vc_grid_tag ] : $tag;
      unset( $atts[ $this->cpt_dsv_vc_grid_tag ] );

      // Replace values
      $replaced_values = cptemplates_dynamic_shortcode_values()->replace_values( $tag, $atts, $content );

      // Change only if something has been replaced
      if ( FALSE === $replaced_values ) {
        return $flag;
      }

      // If not array, then the shortcode must be cleared
      if ( ! is_array( $replaced_values ) ) {
        return '';
      }
   
      // Get variables
      extract( $replaced_values );

      // Compose new shortcode
      $shortcode_atts = array();
      foreach ( $atts as $key => $value ) {
        $shortcode_atts []= sprintf( '%s="%s"', $key, $value );
      }
      $shortcode = sprintf( '[%s %s]', $tag, implode( ' ', $shortcode_atts ) );

      $content = is_string( $content ) && $content ? $content : '';
      $shortcode .= sprintf( '%s[/%s]', $content, $tag );
      
      $shortcode = do_shortcode( $shortcode );
    }

    return $shortcode;
  }


  /**
   *  capture_vc_grid_post
   *
   *  Just saves currently setup post in variable to be used later for DSV replacements
   *
   *  @type  function
   *  @date  08/01/18
   *  @since 2.0.5
   *
   *  @param $post WP_Post
   *  @param $wp_query WP_Query
   *
   *  @return  N/A
   */

  public function capture_vc_grid_post( $post, $wp_query ) {
    $this->vc_grid_loop_post = clone $post;

    // Unlock replacements when post in VC Grid loop becomes available
    $this->lock_dsv_replacements = false;
  }


  /**
   *  cpt_dsv_set_queried_object
   *
   *  Sets queried object to what has been saved during VC Grid loop
   *
   *  @type  function
   *  @date  08/01/18
   *  @since 2.0.5
   *
   *  @param $mixed
   *
   *  @return  mixed
   */

  public function cpt_dsv_set_queried_object( $queried_object ) {
    if ( $this->vc_grid_loop_post ) {
      $queried_object = $this->vc_grid_loop_post;
    }

    return $queried_object;
  }


  /**
   *  setup_vc_grid_shortcode_override_params
   *
   *  Adds override param to vc grid shortcodes
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function setup_vc_grid_shortcode_override_params() {

    $override_options = array(
      __( 'None', 'cptemplates' ) => '',
      __( 'Main loop', 'cptemplates' ) => 'main_loop',
      __( 'Related posts', 'cptemplates' ) => 'related_posts',
    );

    // Allow 3rd parties to easily adjust options
    $override_options = apply_filters( 'cptemplates/vc_grid_override_options', $override_options );

    $taxonomies = get_taxonomies( array( 'public' => true, 'show_ui' => true ), 'objects' );
    $taxonomy_options = array();
    foreach ( $taxonomies as $taxonomy ) {
      $option_label = sprintf( '%s (%s)', $taxonomy->label, $taxonomy->name );
      $taxonomy_options [ $option_label ] = $taxonomy->name;
    }

    // Allow 3rd parties to easily adjust options
    $taxonomy_options = apply_filters( 'cptemplates/vc_grid_override_taxonomy_options', $taxonomy_options );

    // Prepare param attributes
    $attributes = array(
      array(
        'type' => 'dropdown',
        'param_name' => 'cptoverride',
        'heading' => __( 'Query override', 'cptemplates' ),
        'value' => $override_options,
      ),
      array(
        'type' => 'checkbox',
        'param_name' => 'cptoverride_related',
        'heading' => __( 'Taxonomies', 'cptemplates' ),
        'description' => __( 'Pick taxonomies to find related posts by', 'cptemplates' ),
        'value' => $taxonomy_options,
        'dependency' => array(
          'element' => 'cptoverride',
          'value' => array( 'related_posts' ),
        ),
      ),
    );

    // Allow 3rd parties to extend attributes
    $attributes = apply_filters( 'cptemplates/vc_grid_override_attributes', $attributes );

    // Allow 3rd parties to adjust
    $vc_grids = apply_filters( 'cptemplates/vc_grid_override_grids', $this->vc_grid_shortcodes );  

    // Make sure the required function exists
    if ( method_exists( 'WPBMap', 'getAllShortCodes' ) ) {

      // Append user content restriction param to all registered shortcodes in VC
      $all_shortcodes = WPBMap::getAllShortCodes();
      // foreach ( $all_shortcodes as $name => $settings ) {
      foreach ( $vc_grids as $vc_grid_tag ) {

        // Make sure the shortcode exists and has params
        if ( ! isset( $all_shortcodes[ $vc_grid_tag ][ 'params' ] ) ) {
          continue;
        }

        // Append query override options
        vc_add_params( $vc_grid_tag, $attributes );
      }

    }

  }

  /**
   *  cptemplate_applied
   *
   *  Prepares for rendering custom template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function cptemplate_applied( $template_post = null ) {
    if ( ! $template_post ) {
      return;
    }

    $this->template_post = $template_post;

    $this->setup_vc_grid_for_templates();

    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ) );
  }


  /**
   *  enqueue_assets
   *
   *  Enqueue required scripts and styles
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function enqueue_assets() {
    global $wp_query;

    wp_enqueue_script( 'cptemplates-vc-grid' );
    wp_localize_script(
      'cptemplates-vc-grid',
      'cptemplatesVcGrid',
      array(
        'query_vars' => base64_encode( serialize( $wp_query->query_vars ) )
      )
    );

  }


  /**
   *  maybe_setup_overrides
   *
   *  Checks if current request need to be altered
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function maybe_setup_overrides() {
    $vc_request_param = vc_request_param( 'data' );

    $id = isset( $vc_request_param[ 'shortcode_id' ] ) ? $vc_request_param[ 'shortcode_id' ] : false;
    if ( ! $id ) {
      return;
    }
    
    $tag = vc_request_param( 'tag' );
    if ( ! $tag ) {
      return;
    }

    $page_id = isset( $vc_request_param[ 'page_id' ] ) ? (int)$vc_request_param[ 'page_id' ] : false;
    if ( ! $page_id ) {
      return;
    }

    $shortcode_fishbone = visual_composer()->getShortCode( $tag );
    $vc_grid = $shortcode_fishbone->shortcodeClass();
    $shortcode = method_exists( $vc_grid, 'findPostShortcodeById' ) ? $vc_grid->findPostShortcodeById( $page_id, $id ) : false;
    if ( ! is_array( $shortcode ) ) {
      return;
    }

    // Check if current vc grid's query need to be overriden
    if ( isset( $shortcode[ 'atts' ][ 'cptoverride' ] ) && ( $this->cptoverride = trim( $shortcode[ 'atts' ][ 'cptoverride' ] ) ) ) {
      $this->cptoverride_shortcode = $shortcode;
      $this->cptoverride_vc_request_param = $vc_request_param;

      try {
        $this->query_vars = unserialize( base64_decode( vc_request_param( 'cpt_vcgrid_query_vars' ) ) );

        // Pagination in VC Grids is on frontend, so  must query all posts at once
        // allow limiting this to what has been set in settings
        $cptoverride_params = $this->vc_basic_grid_atts();
        $posts_per_page = isset( $cptoverride_params[ 'max_items' ] ) ? intval( $cptoverride_params[ 'max_items' ] ) : 0;
        $this->query_vars[ 'posts_per_page' ] = $posts_per_page ? $posts_per_page : -1;
      } catch (Exception $e) {
        $this->query_vars = array();
      }

      add_action( 'pre_get_posts', array( $this, 'override_vc_grid_query' ) );
    }

  }


  /**
   *  override_vc_grid_query
   *
   *  Modifies query used for getting VC Grid data
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (object) $query
   *  @return  N/A
   */

  public function override_vc_grid_query( $query ) {
    do_action_ref_array( 'cptemplates/vc_grid_override_query/' . $this->cptoverride, array( &$query, $this->cptoverride_shortcode, $this->cptoverride_vc_request_param, $this->query_vars ) );
  }


  /**
   *  override_vc_grid_query_main_loop
   *
   *  Fixes vc grid settings when used in custom page template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (object) $query
   *  @param (array) $shortcode
   *  @param (array) $vc_request_param
   *  @param (array) $query_vars
   *  @return  N/A
   */

  public function override_vc_grid_query_main_loop( $query, $shortcode, $vc_request_param, $query_vars ) {
    if ( is_array( $query_vars ) ) {
      $query_vars[ 'post_type' ] = 'any';
      $query->query_vars = $query_vars;
    }
  }


  /**
   *  override_vc_grid_query_related_posts
   *
   *  Fixes vc grid settings when used in custom page template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (object) $query
   *  @param (array) $shortcode
   *  @param (array) $vc_request_param
   *  @param (array) $query_vars
   *  @return  N/A
   */

  public function override_vc_grid_query_related_posts( $query, $shortcode, $vc_request_param, $query_vars ) {

    // On single post page, VC grid sends post id and query vars have post name key
    $post_name = isset( $query_vars[ 'name' ] ) ? sanitize_title_with_dashes( $query_vars[ 'name' ] ) : false;
    $post_id = $post_name && isset( $_POST[ 'vc_post_id' ] ) ? $_POST[ 'vc_post_id' ] : false;
    $related_taxonomies = isset( $shortcode[ 'atts' ][ 'cptoverride_related' ] ) ? $shortcode[ 'atts' ][ 'cptoverride_related' ] : '';
    $related_taxonomies = explode( ',', $related_taxonomies );
    $related_taxonomies = array_map( 'trim', $related_taxonomies );
    $related_taxonomies = array_filter( $related_taxonomies );
    $tax_query = array();
    
    if ( $post_id && ! empty( $related_taxonomies ) ) {


      foreach ( $related_taxonomies as $taxonomy ) {
        $terms = wp_get_post_terms( $post_id, $taxonomy, array( 'fields' => 'ids' ) );
        if ( ! is_array( $terms ) ) {
          continue;
        }

        $tax_query []= array(
          'taxonomy' => $taxonomy,
          'terms' => $terms,
          'field' => 'id',
          'operator' => 'IN'
        );
      }

    }

    if ( ! empty( $tax_query ) ) {
      $tax_query[ 'relation' ] = 'OR';

      $query_vars[ 'tax_query' ] = isset( $query_vars[ 'tax_query' ] ) && is_array( $query_vars[ 'tax_query' ] ) ? $query_vars[ 'tax_query' ] : array();
      $query_vars[ 'tax_query' ] []= $tax_query;

      $query_vars[ 'name' ] = '';
      $post_type = isset( $query_vars[ 'post_type' ] ) ? $query_vars[ 'post_type' ] : '';
      if ( $post_type ) {
        $query_vars[ $post_type ] = '';
      }

      $query->query_vars = $query_vars;
    }
    else {
      $query_vars[ 'post_type' ] = '';
      $query->query_vars = $query_vars;
    }

  }


  /**
   *  setup_vc_grid_for_templates
   *
   *  Fixes vc grid settings when used in custom page template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function setup_vc_grid_for_templates() {

    // Check in post settings if any post grid defined
    $vc_post_settings = get_post_meta( $this->template_post->ID, '_vc_post_settings', true );
    if (
        ! is_array( $vc_post_settings ) ||
        ! isset( $vc_post_settings[ 'vc_grid_id' ] ) ||
        ! isset( $vc_post_settings[ 'vc_grid_id' ][ 'shortcodes' ] ) ||
        ! is_array( $vc_post_settings[ 'vc_grid_id' ][ 'shortcodes' ] )
      ) {
      return;
    }
    
    // Get ids for all grids defined in custom template
    $grid_ids = array_keys( (array)$vc_post_settings[ 'vc_grid_id' ][ 'shortcodes' ] );
    $grid_ids = array_filter( $grid_ids );

    // No reason to continue if no grid defined for the template
    if ( ! count( $grid_ids ) ) {
      return;
    }


    // Define known grid shortcodes and allow third parties to adjust
    $grid_shortcodes = (array)apply_filters( 'cptemplates/integrate_visual_composer/grid_shortcodes', $this->vc_grid_shortcodes );

    // Define known grid attr names and allow third parties to adjust
    $grid_settings_attr_names = (array)apply_filters( 'cptemplates/integrate_visual_composer/grid_settings_attr_names', array(
      'data-vc-grid-settings'
    ) );

    $this->vc_grid_settings = array(
      'grid_ids' => $grid_ids,
      'grid_shortcodes' => $grid_shortcodes,
      'grid_settings_attr_names' => $grid_settings_attr_names,
    );


    // Apply filter to modify output of the grids
    add_action( 'cptemplates/cptemplate/before_template_content', function() {
      add_filter( 'vc_shortcode_content_filter_after', array( $this, 'adjust_template_vc_grids' ), 10, 2 );
    } );

    add_action( 'cptemplates/cptemplate/after_template_content', function() {
      remove_filter( 'vc_shortcode_content_filter_after', array( $this, 'adjust_template_vc_grids' ), 10, 2 );
    } );

  }


  /**
   *  adjust_template_vc_grids
   *
   *  Replaces vc grid settings for the template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function adjust_template_vc_grids( $output, $shortcode ) {
    extract( $this->vc_grid_settings );

    // Apply to the grid shortcodes only
    if ( in_array( $shortcode, $grid_shortcodes ) ) {

      // Loop through possible grid settings attribute nmaes
      foreach ( $grid_settings_attr_names as $attr ) {
        
        // Escape characters
        // $pattern = preg_quote( "$attr=\"(.*?)\"", '/' );
        $pattern = "/$attr=\"(.*?)\"/i";

        if ( preg_match_all( $pattern, $output, $matches, PREG_PATTERN_ORDER ) ) {

          // Get matches and ensure that all present
          $settings_attr_found = isset( $matches[ 0 ][ 0 ] ) ? $matches[ 0 ][ 0 ] : '';
          $settings_content_found = isset( $matches[ 1 ][ 0 ] ) ? $matches[ 1 ][ 0 ] : '';
          if ( ! $settings_attr_found || ! $settings_content_found ) {
            continue;
          }

          try {
            // Parse settings
            $settings = html_entity_decode( $settings_content_found );
            $settings = htmlspecialchars_decode( $settings );
            $settings = (array)json_decode( $settings );

            // Get shortcode and page id
            $shortcode_id = isset( $settings[ 'shortcode_id' ] ) ? $settings[ 'shortcode_id' ] : '';
            if ( ! $shortcode_id ) {
              continue;
            }

            // Skip if we don't have this id in settings
            if ( ! in_array( $shortcode_id, $grid_ids ) ) {
              continue;
            }

            // Replace page_id that was set by default to the current post, to the template id where grid has been made
            $settings[ 'page_id' ] = $this->template_post->ID;

            // Replace settings in grid's HTML
            $output = str_replace( $settings_content_found, esc_attr( json_encode( $settings ) ), $output );

          } catch (Exception $e) {
          }

        }

      }

    }

    return $output;
  }


}


/**
 *  cptemplates_integrate_vc_vc_grid
 *
 *  The main function responsible for returning cptemplates_integrate_vc_vc_grid object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_vc_vc_grid instance
 */

function cptemplates_integrate_vc_vc_grid() {
  return cptemplates_integrate_vc_vc_grid::instance();
}


// initialize
cptemplates_integrate_vc_vc_grid();


endif; // class_exists check

?>