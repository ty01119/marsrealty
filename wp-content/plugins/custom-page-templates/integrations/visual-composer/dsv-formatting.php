<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_vc_dsv_formatting' ) ) :

final class cptemplates_integrate_vc_dsv_formatting {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;


  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   * Contains all supported types by current class
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $supported_param_types = array();
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_vc_dsv_formatting
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    $this->supported_param_types = array(
      // General VC
      'textarea_raw_html', 'exploded_textarea', 'colorpicker', 'vc_link', 'css_editor',

      // Ultimate Addons for Visual Composer
      'number', 'attach_image', 'attach_images', 'posttypes', 'datetimepicker', 'ult_switch', 'ult_img_single', 'animator', 'ult_select2', 'ult_button', 'ultimate_google_fonts',
      'ultimate_border', 'ultimate_boxshadow', 'gradient', 'icon_manager', 'ultimate_margins', 'ultimate_navigation', 'radio_image_box', 'ultimate_responsive',

      // Massive Addons for Visual Composer
      'mpc_text', 'mpc_css', 'mpc_align', 'mpc_layout_select', 'mpc_icon', 'mpc_slider', 'mpc_typography', 'mpc_preset',
      'mpc_datetime', 'mpc_colorpicker', 'mpc_list', 'mpc_shadow', 'mpc_split', 'mpc_animation',
    );
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_vc_dsv_formatting instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Needed for admin only
    if ( is_admin() ) {
      add_filter( 'cptemplates/dynamic_shortcode_values/supported_param_types', array( $this, 'supported_param_types' ) );
    }
    else {

      foreach ( $this->supported_param_types as $type ) {
        $hook = sprintf( 'cptemplates/dynamic_shortcode_values/format_attr_type_%s_value', $type );
        $method = sprintf( 'format_attr_%s_value', $type );
        add_filter( $hook, array( $this, $method ), 10, 4 );
      }
      
      add_filter( 'cptemplates/dynamic_shortcode_values/format_attr_value', array( $this, 'format_attr_value' ), 10, 4 );

    }
  }


  /**
   *  supported_param_types
   *
   *  Extends supported param types
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $supported_param_types
   *  @return  (array)
   */

  public function supported_param_types( $supported_param_types ) {
    $supported_param_types = array_merge( $supported_param_types, $this->supported_param_types );
    return $supported_param_types;
  }


  /**
   *  format_attr_value
   *
   *  Helper hook that allows to format value by param type
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_value( $value, $search_name, $tag, $atts ) {

    // Make sure the required function exists
    if ( method_exists( 'WPBMap', 'getAllShortCodes' ) ) {

      // Get shortcode settings from VC
      $shortcode = WPBMap::getShortCode( $tag );

      // Check if we've retrieved valid format
      if ( $shortcode && isset( $shortcode[ 'params' ] ) && is_array( $shortcode[ 'params' ] ) ) {

        // Find param by name
        $param_settings = array_filter( $shortcode[ 'params' ], function( $param ) use ( $search_name ) {
          return isset( $param[ 'param_name' ] ) && ( $param[ 'param_name' ] == $search_name );
        } );

        // If we've found param settings
        if ( ! empty( $param_settings ) ) {
          $param_settings = reset( $param_settings );
          $param_type = isset( $param_settings[ 'type' ] ) ? $param_settings[ 'type' ] : '';

          // If param type defined
          if ( $param_type ) {
            $value = apply_filters( "cptemplates/dynamic_shortcode_values/format_attr_type_{$param_type}_value", $value, $search_name, $tag, $atts ); // General
          }

        }

      }

    }

    return $value;
  }


  /**
   *  __call
   *
   *  Captures all formatting requests and just returns value. In DSV core it will be converted to string
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function __call( $method, $args ) {
    $value = isset( $args[ 0 ] ) ? $args[ 0 ] : '';
    if ( preg_match( '/^format_attr_(.*?)/i', $method ) ) {
      return $value;
    }
  }


  /* ============================================================ */
  /* Visual Composer
  /* ============================================================ */


  /**
   *  format_attr_textarea_raw_html_value
   *
   *  Formats value to be a valid base64 encoded
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_textarea_raw_html_value( $value, $search_name, $tag, $atts ) {

    // It's required that value replaced is a string
    $value = is_object( $value ) ? '' : $value; // Object should never be returned... but if so, leave it empty
    $value = is_array( $value ) ? implode( ', ', $value ) : (string)$value;

    // Don't change if it's encoded already
    // Seems there is no 100% reliable way to detect base64 encoded string. So, we should just minimize the risk as much as possible
    if ( preg_match( '%^[a-zA-Z0-9/+]*={0,2}$%', $value ) || base64_decode( $value, true ) ) {
      // preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $s);
      return $value;
    }

    return base64_encode( $value );
  }


  /**
   *  format_attr_exploded_textarea_value
   *
   *  Formats value, where each line will be imploded with comma (,)
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_exploded_textarea_value( $value, $search_name, $tag, $atts ) {

    // It's required that value replaced is a string
    $value = is_object( $value ) ? '' : $value; // Object should never be returned... but if so, leave it empty
    $value = is_array( $value ) ? implode( ', ', $value ) : (string)$value;

    // Replace new lines with comma
    $value = str_replace( array( "\r\n", "\r", "\n" ), ',', $value );

    return $value;
  }


  /**
   *  format_attr_colorpicker_value
   *
   *  Simply prepends # to the value if it's not there
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_colorpicker_value( $value, $search_name, $tag, $atts ) {

    // It's required that value replaced is a string
    $value = is_object( $value ) ? '' : $value; // Object should never be returned... but if so, leave it empty
    $value = is_array( $value ) ? implode( ', ', $value ) : (string)$value;

    // Replace new lines with comma
    if ( ! preg_match( '/^#/', $value) ) {
      $value = '#'.$value;
    }

    return $value;
  }


  /**
   *  format_attr_vc_link_value
   *
   *  Ensures that the supplied value is formatted properly for vc_link attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_vc_link_value( $value, $search_name, $tag, $atts ) {
    // Determine what is going to be replaced
    if ( in_array( $value, array( '_blank', '_self', '_parent', '_top' ) ) ) {
      $prop = 'target';
    }
    elseif ( in_array( $value, array( 'follow', 'nofollow' ) ) ) {
      $prop = 'rel';
    }
    // elseif ( preg_match( '/(%2F%2F|\/\/)/i', $value ) ) {
    elseif ( preg_match( '/(%2F%2F)|(\/\/)|(mailto:.+@.+\..+)|(mailto%3A.+%40.+\..+)/i', $value ) ) {
      $prop = 'url';

      // Encode URL if it wasn't yet
      if ( FALSE === strpos( $value, '%2F' ) ) {
        $value = urlencode( $value );
      }
    }
    else {
      $prop = 'title';
    }

    $link_settings_str = isset( $atts[ $search_name ] ) ? trim( $atts[ $search_name ] ) : '';
    $link_settings = array();
    $link_props = array( 'url', 'title', 'target', 'rel' );

    // Check if current link has settings
    if ( FALSE !== strpos( $atts[ $search_name ], '|' ) ) {

      // Extract link settings
      foreach ( $link_props as $link_prop ) {
        if ( preg_match( "/$link_prop\:(.*?)(\||$)/i", $link_settings_str, $matches ) ) {
          $link_settings[ $link_prop ] = isset( $matches[ 1 ] ) ? $matches[ 1 ] : '';
        }
      }

    }


    // Replace value for particular property
    $link_settings[ $prop ] = $value;

    // Format result
    array_walk( $link_settings, function( &$item, $key ) {
      $item = sprintf( '%s:%s', $key, $item ? $item : '' );
    });
    $link_settings = array_values( $link_settings );
    $value = implode( '|', $link_settings );

    return $value;
  }


  /* ============================================================ */
  /* Ultimate Addons for Visual Composer
  /* ============================================================ */


  /**
   *  format_attr_number_value
   *
   *  Formats value to be a valid number
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_number_value( $value, $search_name, $tag, $atts ) {
    if ( is_numeric( $value ) ) {
      return FALSE !== strpos( strval( $value ), '.' ) ? floatval( $value ) : intval( $value );
    }
    else {
      return 0;
    }
  }


  /**
   *  format_attr_attach_image_value
   *
   *  Ensures that the supplied value is integer
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_attach_image_value( $value, $search_name, $tag, $atts ) {

    // If array was supplied, we'll take just the first item
    if ( is_array( $value ) && isset( $value[ 0 ] ) && is_numeric( $value[ 0 ] ) ) {
      $value = intval( $value[ 0 ] );
    }

    // For numbers, just make sure it's an integer
    elseif ( is_numeric( $value ) ) {
      $value = intval( $value );
    }
    
    // Validate string format
    elseif ( is_string( $value ) ) {

      // String must be comma-separated image IDs
      if ( preg_match( '/(\d*,?)+/i', $value ) ) {
        $value = explode( ',', $value );
        $value = $value[ 0 ];
        $value = is_numeric( $value ) ? intval( $value ) : '';
      }
      // Empty value if string format is invalid
      else {
        $value = '';
      }

    }
    
    // Empty value for all other unknown cases
    else {
      $value = '';
    }

    return $value;
  }
  

  /**
   *  format_attr_attach_images_value
   *
   *  Ensures that the supplied value is integer
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_attach_images_value( $value, $search_name, $tag, $atts ) {

    // Array can by supplied
    if ( is_array( $value ) ) {

      // Retrieve just numeric values
      $new_value = array_filter( $value, function( $item ) {
        return is_numeric( $item );
      });

      // Format string to the comma-separated image IDs
      $new_value = implode( ',' , $new_value );

      // Empty string if new value couldn't be defined
      $value = $new_value ? $new_value : '';

    }

    // Validate strings
    elseif ( is_string( $value ) ) {
      
      // String must be comma-separated image IDs
      if ( preg_match( '/(\d*,?)+/i', $value ) ) {
        $value = explode( ',', $value );

        // Retrieve just numeric values
        $new_value = array_filter( $value, function( $item ) {
          return is_numeric( $item );
        });

        // Format string to the comma-separated image IDs
        $new_value = implode( ',' , $new_value );

        // Empty string if new value couldn't be defined
        $value = $new_value ? $new_value : '';

      }
      // Empty value if string format is invalid
      else {
        $value = '';
      }

    }

    // Validate numbers
    elseif ( is_numeric( $value ) ) {
      $value = intval( $value );
    }

    // Empty value for all other unknown cases
    else {
      $value = '';
    }

    return $value;
  }


  /**
   *  format_attr_posttypes_value
   *
   *  Ensures that the supplied value is formatted properly for posttypes attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_posttypes_value( $value, $search_name, $tag, $atts ) {

    // Array can by supplied
    if ( is_array( $value ) ) {

      // Retrieve just numeric values
      $new_value = array_filter( $value, function( $item ) {
        return is_string( $item ) && post_type_exists( $item );
      });

      // Format string to the comma-separated image IDs
      $new_value = implode( ',' , $new_value );

      // Empty string if new value couldn't be defined
      $value = $new_value ? $new_value : '';

    }

    // Validate strings
    elseif ( is_string( $value ) ) {
      
      // String must be comma-separated image IDs
      if ( preg_match( '/(\d*,?)+/i', $value ) ) {
        $value = explode( ',', $value );

        // Retrieve just numeric values
        $new_value = array_filter( $value, function( $item ) {
          return is_string( $item ) && post_type_exists( $item );
        });

        // Format string to the comma-separated image IDs
        $new_value = implode( ',' , $new_value );

        // Empty string if new value couldn't be defined
        $value = $new_value ? $new_value : '';

      }
      // Empty value if string format is invalid
      else {
        $value = '';
      }

    }

    // Empty value for all other unknown cases
    else {
      $value = '';
    }

    return $value;
  }


  /**
   *  format_attr_ult_switch_value
   *
   *  Ensures that the supplied value is formatted properly for ult_switch attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_ult_switch_value( $value, $search_name, $tag, $atts ) {
    return in_array( $value, array( 'off', 0, '0', '', 'no', 'unchecked', 'disable', 'disabled' ) ) ? 'off' : 'on';
  }


  /**
   *  format_attr_datetimepicker_value
   *
   *  Ensures that the supplied value is formatted properly for datetimepicker attribute (2017/05/17 12:22:27)
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_datetimepicker_value( $value, $search_name, $tag, $atts ) {
    if ( is_a( $value, 'DateTime' ) ) {
      $value = $value->format( 'Y/m/d H:i:s' );
    }
    elseif ( is_numeric( $value ) ) {
      $value = date( 'Y/m/d H:i:s', (int)$value );
    }
    elseif ( is_string( $value ) ) {
      if ( ! date_create_from_format( 'Y/m/d H:i:s', $value ) ) {
        $value = '';
      }
    }
    else {
      $value = '';
    }

    return $value;
  }


  /**
   *  format_attr_ult_img_single_value
   *
   *  Ensures that the supplied value is formatted properly for ult_img_single attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_ult_img_single_value( $value, $search_name, $tag, $atts ) {

    // Do nothing if it has been formatted already by 3rd party script
    if ( preg_match( '/^id\^(.*?)\|url\^(.*?)\|caption\^(.*?)\|alt\^(.*?)\|title\^(.*?)\|description\^(.*?)/i', $value ) ) {
      return $value;
    }

    $attachment = false;
    $image_data = array();

    // Replacement could be an image ID
    if ( is_numeric( $value ) ) {
      $attachment = get_post( intval( $value ) );
    }
    // Or Image URL
    elseif ( is_string( $value ) || ( FALSE !== strpos( $value, 'http' ) ) ) {
      global $wpdb;
      $query_result = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid LIKE '%s';", $value ) );
      $attachment_id = ! empty( $query_result ) ? (int)$query_result[ 0 ] : false;
      $attachment = $attachment_id ? get_post( $attachment_id ) : false;
    }

    // Check if found post is attachment
    if ( is_a( $attachment, 'WP_Post' ) && ( 'attachment' == $attachment->post_type ) ) {

      // Set image data
      $image_data[ 'id' ] = $attachment->ID;
      $image_data[ 'url' ] = $attachment->guid;
      $image_data[ 'caption' ] = $attachment->post_excerpt;
      $image_data[ 'alt' ] = get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true );
      $image_data[ 'title' ] = $attachment->post_title;
      $image_data[ 'description' ] = $attachment->post_content;

      // Format value
      // Example: banner_image="id^2343|url^http://domain.com/wp-content/uploads/2017/05/image.jpg|caption^null|alt^null|title^image_title|description^null"      
      array_walk( $image_data, function( &$item, $key ) {
        $item = sprintf( '%s^%s', $key, $item ? $item : 'null' );
      });
      $image_data = array_values( $image_data );
      $value = implode( '|', $image_data );
    }

    return $value;
  }


  /**
   *  format_attr_animator_value
   *
   *  Ensures that the supplied value is formatted properly for animator attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_animator_value( $value, $search_name, $tag, $atts ) {

    if ( ! isset( $this->ult_animations ) ) {
      $animations_config = function_exists( 'ultimate_get_animation_json' ) ? ultimate_get_animation_json() : false;
      $animations_config = $animations_config ? json_decode( $animations_config, true ) : false;
      $animations_config = is_array( $animations_config ) ? $animations_config : array();

      $ult_animations = array();
      $animations_config = array_values( $animations_config );
      array_walk( $animations_config, function( $item ) use ( &$ult_animations ) {
        $ult_animations = array_merge( $ult_animations, array_keys( $item ) );
      }, $animations_config );

      $this->ult_animations = $ult_animations;
    }

    $value = cptemplates_dynamic_shortcode_values()->convert_to_string( $value );

    $value = in_array( $value, $this->ult_animations ) ? $value : '';

    return $value;
  }


  /**
   *  format_ult_select2_value
   *
   *  Ensures that the supplied value is formatted properly for ult_select2 attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_ult_select2_value( $value, $search_name, $tag, $atts ) {

    if ( ! isset( $this->ult_select2 ) ) {
      $config = function_exists( 'ultimate_get_banner2_json' ) ? ultimate_get_banner2_json() : false;
      $config = $config ? json_decode( $config, true ) : false;
      $config = is_array( $config ) ? $config : array();

      $ult_select2 = array();
      $config = array_values( $config );
      array_walk( $config, function( $item ) use ( &$ult_select2 ) {
        $ult_select2 = array_merge( $ult_select2, array_values( $item ) );
      }, $config );

      $this->ult_select2 = $ult_select2;
    }

    $value = cptemplates_dynamic_shortcode_values()->convert_to_string( $value );

    $value = in_array( $value, $this->ult_select2 ) ? $value : '';

    return $value;
  }


  /**
   *  format_attr_ult_button_value
   *
   *  Ensures that the supplied value is formatted properly for ult_button attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_ult_button_value( $value, $search_name, $tag, $atts ) {

    if ( ! isset( $this->ult_icon_positions ) ) {
      $config = function_exists( 'ultimate_get_icon_position_json' ) ? ultimate_get_icon_position_json() : false;
      $config = $config ? json_decode( $config, true ) : false;
      $config = is_array( $config ) ? $config : array();

      $items = array();
      $config = array_values( $config );
      array_walk( $config, function( $item ) use ( &$items ) {
        $items = array_merge( $items, array_values( $item ) );
      }, $config );

      $this->ult_icon_positions = $items;
    }

    $value = cptemplates_dynamic_shortcode_values()->convert_to_string( $value );

    $value = in_array( $value, $this->ult_icon_positions ) ? $value : '';

    return $value;
  }


  /**
   *  format_attr_ultimate_google_fonts_value
   *
   *  Ensures that the supplied value is formatted properly for ultimate_google_fonts attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_ultimate_google_fonts_value( $value, $search_name, $tag, $atts ) {

    if ( ! isset( $this->ultimate_google_fonts ) ) {
      $config = get_option( 'ultimate_selected_google_fonts' );
      $config = is_array( $config ) && ! empty( $config ) ? $config : array();
      
      $items = array_map( function( $item ) {
        $font_family = $item[ 'font_family' ];
        return sprintf( 'font_family:%s|font_call:%s', $font_family, str_replace( ' ', '+', $font_family ) );
      }, $config );

      $config = array_values( $config );
      $this->ultimate_google_fonts = $items;
    }

    $value = cptemplates_dynamic_shortcode_values()->convert_to_string( $value );

    $value = in_array( $value, $this->ultimate_google_fonts ) ? $value : null;

    return $value;
  }


  /* ============================================================ */
  /* Massive Addons for Visual Composer
  /* ============================================================ */


  /**
   *  format_attr_mpc_align_value
   *
   *  Ensures that the supplied value is formatted properly for mpc_align attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_mpc_align_value( $value, $search_name, $tag, $atts ) {
    if ( ! isset( $this->mpc_alignments ) ) {
      $this->alignments = array(
        // Large grid
        'top-left',    'top-center',    'top-right',
        'middle-left', 'middle-center', 'middle-right',
        'bottom-left', 'bottom-center', 'bottom-right',

        // Medium grid
        'top-left',    'top-center',    'top-right',
        'bottom-left', 'bottom-center', 'bottom-right',

        // Other
        'left', 'center', 'right',
      );
    }

    // Ensure value is one of the known
    $value = in_array( $value, $this->alignments ) ? $value : '';

    return $value;
  }


  /**
   *  format_attr_mpc_slider_value
   *
   *  Ensures that the supplied value is formatted properly for mpc_slider attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_mpc_slider_value( $value, $search_name, $tag, $atts ) {
    return $this->format_attr_number_value( $value, $search_name, $tag, $atts );
  }


  /**
   *  format_attr_mpc_typography_value
   *
   *  Ensures that the supplied value is formatted properly for mpc_typography attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_mpc_typography_value( $value, $search_name, $tag, $atts ) {
    if ( ! isset( $this->mpc_typography ) ) {
      $presets = get_option( 'mpc_presets_typography' );
      if ( $presets ) {
        try {
          $presets = json_decode( $presets, true );
        } catch (Exception $e) {
          $presets = array();
        }
      }
      else {
        $presets = array();
      }
      $presets = is_array( $presets ) ? array_keys( $presets ) : array();
      $this->mpc_typography = $presets;
    }

    // Ensure value is one of the known
    $value = in_array( $value, $this->mpc_typography ) ? $value : '';

    return $value;
  }


  /**
   *  format_attr_mpc_preset_value
   *
   *  Ensures that the supplied value is formatted properly for mpc_preset attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_mpc_preset_value( $value, $search_name, $tag, $atts ) {
    if ( ! isset( $this->mpc_presets ) ) {
      $this->mpc_presets = array();
    }
    
    if ( ! isset( $this->mpc_presets[ $tag ] ) ) {
      $presets = get_option( 'mpc_presets_' . $tag );
      if ( $presets ) {
        try {
          $presets = json_decode( $presets, true );
        } catch (Exception $e) {
          $presets = array();
        }
      }
      else {
        $presets = array();
      }
      $presets = is_array( $presets ) ? array_keys( $presets ) : array();
      $this->mpc_presets[ $tag ] = $presets;
    }

    // Ensure value is one of the known
    $value = in_array( $value, $this->mpc_presets[ $tag ] ) ? $value : '';

    return $value;
  }


  /**
   *  format_attr_mpc_datetime_value
   *
   *  Ensures that the supplied value is formatted properly for mpc_datetime attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_mpc_datetime_value( $value, $search_name, $tag, $atts ) {
    return $this->format_attr_datetimepicker_value(  $value, $search_name, $tag, $atts  );
  }


  /**
   *  format_attr_mpc_colorpicker_value
   *
   *  Ensures that the supplied value is formatted properly for mpc_colorpicker attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_mpc_colorpicker_value( $value, $search_name, $tag, $atts ) {
    return $this->format_attr_colorpicker_value(  $value, $search_name, $tag, $atts  );
  }


  /**
   *  format_attr_mpc_shadow_value
   *
   *  Ensures that the supplied value is formatted properly for mpc_shadow attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_mpc_shadow_value( $value, $search_name, $tag, $atts ) {
    // 1px 1px 3px #545454
    if ( ! preg_match( '/(\d(px)? ){3}#([a-f0-9]{3}){1,2}/i', $value ) ) {
      $value = '';
    }
    return $value;
  }


  /**
   *  format_attr_mpc_split_value
   *
   *  Ensures that the supplied value is formatted properly for mpc_split attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_mpc_split_value( $value, $search_name, $tag, $atts ) {
    $value = cptemplates_dynamic_shortcode_values()->convert_to_string( $value );
    $value = $value ? preg_replace( "/(\r\n)|(\n\r)|(\n)|(\r)/", "|||", $value ) : '';
    return $value;
  }


  /**
   *  format_attr_mpc_animation_value
   *
   *  Ensures that the supplied value is formatted properly for mpc_animation attribute
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (string) $search_name
   *  @param (string) $tag
   *  @param (array) $atts
   *  @return  (string)
   */

  public function format_attr_mpc_animation_value( $value, $search_name, $tag, $atts ) {
    $loop_list = class_exists( 'MPC_Snippets' ) && isset( MPC_Snippets::$animations_loop_list ) ? MPC_Snippets::$animations_loop_list : array();

    // Ensure value is one of the known
    $value = in_array( $value, $loop_list ) ? $value : '';

    return $value;
  }


}


/**
 *  cptemplates_integrate_vc_dsv_formatting
 *
 *  The main function responsible for returning cptemplates_integrate_vc_dsv_formatting object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_vc_dsv_formatting instance
 */

function cptemplates_integrate_vc_dsv_formatting() {
  return cptemplates_integrate_vc_dsv_formatting::instance();
}


// initialize
cptemplates_integrate_vc_dsv_formatting();


endif; // class_exists check

?>