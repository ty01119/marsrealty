<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_vc_woocommerce' ) ) :

final class cptemplates_integrate_vc_woocommerce {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_vc_woocommerce
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_vc_woocommerce instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Check dependency
    if ( ! cptemplates_is_woocommerce_plugin_active() ) {
      return;
    }

    // Needed for admin only
    if ( is_admin() ) {
      $this->integrate();
    }

  }


  /**
   *  integrate_shortcode
   *
   *  Integrates dynamic shortcode value shortcode
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function integrate() {

    // Check dependency
    if ( ! class_exists( 'cptemplates_integrate_wc_shortcodes' ) ) {
      return;
    }

    $shortcodes = cptemplates_integrate_wc_shortcodes()->get_shortcodes();

    // Allow 3rd party to filter
    $shortcodes = apply_filters( 'cptemplates/woocommerce/shortcodes', $shortcodes );

    foreach ( $shortcodes as $shortcode ) {

      $params = array();

      if ( 'cptwc_login_form' == $shortcode[ 'tag' ] ) {
        $params []= array(
          'type' => 'textfield',
          'heading' => __( 'Message', 'cptemplates' ),
          'param_name' => 'message',
          'value' => '',
          'description' => __( 'This message will be shown above the login form', 'cptemplates' ),
        );
        $params []= array(
          'type' => 'textfield',
          'heading' => __( 'Redirect', 'cptemplates' ),
          'param_name' => 'redirect',
          'value' => '',
          'description' => __( 'Customers will be redirected to the specified URL', 'cptemplates' ),
        );
      }
      elseif ( ( 'cptwc_product_related_products' == $shortcode[ 'tag' ] ) || ( 'cptwc_product_upsell' == $shortcode[ 'tag' ] ) ) {
        $params []= array(
          'type' => 'textfield',
          'heading' => __( 'Posts per page', 'cptemplates' ),
          'param_name' => 'posts_per_page',
          'value' => '4',
          'description' => __( 'Amount of posts to be shown', 'cptemplates' ),
        );
        $params []= array(
          'type' => 'textfield',
          'heading' => __( 'Columns', 'cptemplates' ),
          'param_name' => 'columns',
          'value' => '4',
          'description' => __( 'Amount of columns', 'cptemplates' ),
        );
        $params []= array(
          'type' => 'dropdown',
          'heading' => __( 'Columns', 'cptemplates' ),
          'param_name' => 'orderby',
          'value' => array(
            __( 'Random', 'cptemplates' ) => 'rand',
            __( 'None', 'cptemplates' ) => 'none',
            __( 'ID', 'cptemplates' ) => 'ID',
            __( 'Author', 'cptemplates' ) => 'author',
            __( 'Title', 'cptemplates' ) => 'title',
            __( 'Name', 'cptemplates' ) => 'name',
            __( 'Type', 'cptemplates' ) => 'type',
            __( 'Date', 'cptemplates' ) => 'date',
            __( 'Date modified', 'cptemplates' ) => 'modified',
            __( 'Post parent', 'cptemplates' ) => 'parent',
            __( 'Comments count', 'cptemplates' ) => 'comment_count',
            __( 'Menu order', 'cptemplates' ) => 'menu_order',
          ),
          'std' => 'rand',
          'description' => __( 'Amount of columns', 'cptemplates' ),
        );

      }

      $params []= array(
        'type' => 'textfield',
        'heading' => __( 'Extra classes', 'cptemplates' ),
        'param_name' => 'el_class',
        'value' => '',
        'description' => __( 'If specified, the rendered shortcode will be wrapped with additional tag and these classes', 'cptemplates' ),
      );

      vc_map( array(
        'name' => $shortcode[ 'label' ],
        'description' => $shortcode[ 'description' ],
        'base' => $shortcode[ 'tag' ],
        'class' => 'cptwc',
        'icon' => 'cpt-vc-icon-logo',
        'category' => 'WooCommerce',
        'params' => $params
      ) );
    }


    // Structured data
    $params = array();
    $params []= array(
      'type' => 'dropdown',
      'heading' => __( 'Type', 'cptemplates' ),
      'param_name' => 'type',
      'holder' => 'div',
      'value' => apply_filters( 'cptemplates/woocommerce/structured_data_type_vc_options', array() ),
      'std' => '',
      'description' => __( 'Generates WooCommerce structured data', 'cptemplates' ),
    );

    vc_map( array(
      'name' => __( 'WC Structured Data', 'cptemplates' ),
      'description' => __( 'Generates various WooCommerce structured data', 'cptemplates' ),
      'base' => 'cptemplates_wc_structured_data',
      // 'class' => 'cptwc',
      'icon' => 'cpt-vc-icon-logo',
      'category' => 'WooCommerce',
      'params' => $params
    ) );

  }


}


/**
 *  cptemplates_integrate_vc_woocommerce
 *
 *  The main function responsible for returning cptemplates_integrate_vc_woocommerce object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_vc_woocommerce instance
 */

function cptemplates_integrate_vc_woocommerce() {
  return cptemplates_integrate_vc_woocommerce::instance();
}


// initialize
cptemplates_integrate_vc_woocommerce();


endif; // class_exists check

?>