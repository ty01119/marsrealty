<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_vc_user_content_restriction' ) ) :

final class cptemplates_integrate_vc_user_content_restriction {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_vc_user_content_restriction
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    // add_action( 'init', array( $this, 'init' ), 50 ); // Init late as we need to ensure that all shortcodes have been registered already
    add_action( 'admin_init', array( $this, 'init' ), 50 ); // Init late as we need to ensure that all shortcodes have been registered already
    add_filter( 'cptemplates/dynamic_shortcode_values/excluded_shortcode_params', array( $this, 'dsv_excluded_shortcode_params' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_vc_user_content_restriction instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Needed for admin only
    if ( is_admin() ) {
      $this->integrate_user_content_restriction();
    }

  }


  /**
   *  dsv_excluded_shortcode_params
   *
   *  Excludes user content restriction params from dynamic shortcode values
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $excluded_shortcode_params
   *  @return (array)
   */

  public function dsv_excluded_shortcode_params( $excluded_shortcode_params ) {
    $excluded_shortcode_params = array_merge( $excluded_shortcode_params, array( 'cptucr', 'cptucr_custom_filter' ) );
    return $excluded_shortcode_params;
  }


  /**
   *  integrate_user_content_restriction
   *
   *  Integrates with user content restriction
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function integrate_user_content_restriction() {

    // Prepare user role options
    $user_role_options = array(
      __( 'User is logged in', 'cptemplates' ) => 'logged_in',
      __( 'Guest user (logged out)', 'cptemplates' ) => 'logged_out',
    );
    $all_roles = cptemplates_get_editable_roles();
    foreach ( $all_roles as $role_name => $role ) {
      $user_role_options[ $role[ 'name' ] ] = $role_name;
    }

    // Prepare param attributes
    $attributes = array(
      array(
        'type' => 'checkbox',
        'param_name' => 'cptucr',
        'value' => $user_role_options,
        'heading' => __( 'Choose user conditions', 'cptemplates' ),
        'description' => __( 'Content will be rendered if user matches any of the checked conditions. Uncheck all to disable restriction.', 'cptemplates' ),
        'group' => __( 'Restrictions', 'cptemplates' ),
      ),
      array(
        'type' => 'textfield',
        'param_name' => 'cptucr_custom_filter',
        'value' => '',
        'heading' => __( 'Custom filter hook', 'cptemplates' ),
        'description' => __( 'Input the name of your custom filter hook in PHP', 'cptemplates' ),
        'group' => __( 'Restrictions', 'cptemplates' ),
      ),
    );    

    // Allow 3rd parties to add custom restriction attributes
    $attributes = apply_filters( 'cptemplates/user_content_restriction/vc_attributes', $attributes );  

    // Make sure the required function exists
    if ( method_exists( 'WPBMap', 'getAllShortCodes' ) ) {

      // Append user content restriction param to all registered shortcodes in VC
      $all_shortcodes = WPBMap::getAllShortCodes();
      foreach ( $all_shortcodes as $name => $settings ) {

        // Not all VC shortcodes have params
        if ( isset( $settings[ 'params' ] ) ) {
          vc_add_params( $name, $attributes );
        }

      }

    }

  }


}


/**
 *  cptemplates_integrate_vc_user_content_restriction
 *
 *  The main function responsible for returning cptemplates_integrate_vc_user_content_restriction object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_vc_user_content_restriction instance
 */

function cptemplates_integrate_vc_user_content_restriction() {
  return cptemplates_integrate_vc_user_content_restriction::instance();
}


// initialize
cptemplates_integrate_vc_user_content_restriction();


endif; // class_exists check

?>