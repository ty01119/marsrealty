<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_vc_dynamic_shortcode_values' ) ) :

final class cptemplates_integrate_vc_dynamic_shortcode_values {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;


  /**
   * Contains Dynamic Values param attributes
   *
   * @since 3.0.13
   * @var boolean $integrated_shortcodes
   */
  private $param_attributes = array();
    

  /**
   * Contains list of shortcodes where dsv shoult not be integrated
   *
   * @since 3.0.13
   * @var boolean $integrated_shortcodes
   */
  private $excluded_shortcodes = array();
    

  /**
   * Contains list of shortcodes where dsv has been integrated already
   *
   * @since 3.0.13
   * @var boolean $integrated_shortcodes
   */
  private $integrated_shortcodes = array();
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_vc_dynamic_shortcode_values
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    // add_action( 'init', array( $this, 'init' ), 50 ); // Init late as we need to ensure that all shortcodes have been registered already
    add_action( 'admin_init', array( $this, 'init' ), 50 ); // Init late as we need to ensure that all shortcodes have been registered already

    add_shortcode( 'cptdsvgitem', array( $this, 'shortcode' ) );
    add_filter( 'vc_grid_item_shortcodes', array( $this, 'mapGridItemShortcodes' ) );

    add_shortcode( 'cptdsv', array( $this, 'shortcode' ) );
    
    /*
      @Since 1.0.3
      Support editing page content with template applied in VC Front Editor
    */

    // if ( cptemplates_vc_is_inline() ) {
    //   add_action( 'cptemplates/template_applied', array( $this, 'preserve_cptemplate_content_in_vc_fronteditor' ) );
    // }
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_vc_dynamic_shortcode_values instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Needed for admin only
    if ( is_admin() ) {
      $this->integrate_admin();
    }

  }


  /**
   *  shortcode
   *
   *  Renders shortcode
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $atts
   *  @param (null|string) $content
   *  @return (string)
   */

  public function shortcode( $atts, $content = null ) {
    return $content;
  }


  /**
   *  integrate_admin
   *
   *  Integrates with dynamic shortcode values
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function integrate_admin() {
    $this->integrate_shortcode();
    $this->integrate_param();
  }


  /**
   *  integrate_shortcode
   *
   *  Integrates dynamic shortcode value shortcode
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function integrate_shortcode() {
    $params = array();

    $params []= array(
      'type' => 'textarea_html',
      'heading' => __( 'Dynamic Value', 'cptemplates' ),
      'param_name' => 'content',
      'value' => '',
      'description' => __( 'The content provided in the field could be replaced with the value configured under "Dynamic Values" tab.', 'cptemplates' ),
    );

    vc_map( array(
      'name' => __( 'Dynamic Value', 'cptemplates' ),
      'description' => __( 'Renders dynamically defined value', 'cptemplates' ),
      'base' => 'cptdsv',
      'class' => 'cptdsv',
      'icon' => 'cpt-vc-icon-logo',
      'category' => 'Content',
      'admin_enqueue_js' => cptemplates_get_url( 'assets/js/cptdsv-element.js' ),
      'js_view' => 'DynamicShortcodeValuesElementView',
      'custom_markup' => '<h4 class="wpb_element_title"> <i class="vc_general vc_element-icon cpt-vc-icon-logo"></i> Dynamic Value</h4>{{{ content }}}',
      'params' => $params
    ) );

  }


  /**
   *  Registers shortcode in VC Grid Item
   *
   *  @since 1.2.9
   *
   *  @param $value string
   *  @param $data array
   *
   *  @return string
   */
  public function mapGridItemShortcodes( $shortcodes ) {
    $params = array();

    $params []= array(
      'type' => 'textarea_html',
      'heading' => __( 'Dynamic Value', 'cptemplates' ),
      'param_name' => 'content',
      'value' => '',
      'description' => __( 'The content provided in the field could be replaced with the value configured under "Dynamic Values" tab.', 'cptemplates' ),
    );

    $shortcode = array(
      'cptdsvgitem' => array(

        'name' => __( 'Dynamic Value', 'cptemplates' ),
        'description' => __( 'Renders dynamically defined value', 'cptemplates' ),
        'base' => 'cptdsvgitem',
        'class' => 'cptdsv',
        'icon' => 'cpt-vc-icon-logo',
        'category' => 'Content',
        'admin_enqueue_js' => cptemplates_get_url( 'assets/js/cptdsv-element.js' ),
        'js_view' => 'DynamicShortcodeValuesElementView',
        'custom_markup' => '<h4 class="wpb_element_title"> <i class="vc_general vc_element-icon cpt-vc-icon-logo"></i> Dynamic Value</h4>{{{ content }}}',
        'params' => $params,
        'post_type' => Vc_Grid_Item_Editor::postType(),
      )
    );

    return $shortcodes + $shortcode;    
  }


  /**
   *  integrate_param
   *
   *  Integrates dynamic shortcode values param
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function integrate_param() {

    $js_url = cptemplates_get_url( 'assets/js/cptdsv.js' );
    if ( ! cptemplates_integrate_visual_composer()->add_shortcode_param( 'cptdsv', array( $this, 'render_param' ), $js_url ) ) {
      return false;
    }

    $this->configure_options();

    // Init param attributes
    $this->param_attributes = array(
      array(
        'type' => 'cptdsv',
        'param_name' => 'cptdsv',
        'value' => '',
        'group' => __( 'Dynamic Values', 'cptemplates' ),
      ),
    );

    // Allow 3rd parties to add custom restriction attributes
    $this->param_attributes = apply_filters( 'cptemplates/dynamic_shortcode_values/vc_attributes', $this->param_attributes );  

    // Allow 3rd parties to add custom restriction attributes
    $this->excluded_shortcodes = apply_filters( 'cptemplates/dynamic_shortcode_values/excluded_shortcodes', $this->excluded_shortcodes );  

    // Ininitially integrate param for all existing shortcodes
    $this->do_param_integration();

    /*
      Integrate Dynamic Values param for all shortcodes added later on
      @since 3.0.13
    */
    add_action( 'vc_mapper_call_activities_before', array( $this, 'do_param_integration' ) );
  }


  /**
   *  integrate_param
   *
   *  Integrates dynamic shortcode values param
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function do_param_integration() {

    // Make sure the required function exists
    if ( method_exists( 'WPBMap', 'getAllShortCodes' ) ) {

      // Append user content restriction param to all registered shortcodes in VC
      $all_shortcodes = WPBMap::getAllShortCodes();
      foreach ( $all_shortcodes as $name => $settings ) {

        // Prevent integration for all shortcodes that have been integrated already
        // Shortcodes may be initialized on various stages, including lazy initialization with leanMap method
        if ( in_array( $name, $this->integrated_shortcodes ) ) {
          continue;
        }

        // Prevent integration for all excluded shortcodes
        if ( in_array( $name, $this->excluded_shortcodes ) ) {
          continue;
        }

        // Not all VC shortcodes have params
        if ( isset( $settings[ 'params' ] ) ) {
          $this->integrated_shortcodes []= $name;
          vc_add_params( $name, $this->param_attributes );
        }

      }

    }

  }


  /**
   *  render_param
   *
   *  Renders cptdsv param 
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function render_param( $settings, $value ) {
    ob_start();
    $this->include_templates();
    ?>
    <div id="cptdsv_param">
      <input id="cptdsv_value" name="cptdsv" type="hidden" class="wpb_vc_param_value" value="<?= $value ?>">
      <?= do_action( 'cptemplates/vc_dynamic_shortcode_values/before_rules_list' ) ?>
      <div class="cptdsv-values">
        <div class="cptdsv-values-head">
          <div class="cptdsv-col"><?= __( 'Replace this', 'cptemplates' ) ?></div>
          <div class="cptdsv-col"><?= __( 'With this', 'cptemplates' ) ?></div>
          <div class="cptdsv-col"></div>
        </div>
        <div class="cptdsv-values-list" id="cptdsv_values_list"></div>
      </div>
      <?= do_action( 'cptemplates/vc_dynamic_shortcode_values/after_rules_list' ) ?>
      <p class="cptdsv-item-add"><a href="#" class="button button-secondary button-large btn-add-item"><?= __( 'Add New', 'cptemplates' ) ?></a></p>
    </div>
    <?php
    return ob_get_clean();
  }


  /**
   *  get_i18n_strings
   *
   *  Retrieves i18n strings
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function get_i18n_strings() {
    return array(
      'search_value_placeholder' => __( 'Input your custom placeholder', 'cptemplates' ),
      'replacement_value_placeholder' => __( 'Input your custom value', 'cptemplates' ),
    );
  }


  /**
   *  render_param
   *
   *  Renders cptdsv param 
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function include_templates() {
    ?>
    <script type="text/template" class="cptdsv-tpl" data-name="dynamic-value">
      <div class="cptdsv-col">
        <%= templates.dropdown( { index: index, options: options.searchType, prop: 'searchType', value: model.searchType } ) %>

        <% if ( 'attr' == model.searchType ) { %>
          <%= templates.dropdown( { options: options.searchValue, prop: 'searchValue', value: model.searchValue } ) %>
        <% } else if ( 'content' == model.searchType ) { %>
          <%= templates.hidden( { prop: 'searchValue' } ) %>
        <% } else { %>
          <%= templates.text( { prop: 'searchValue', value: model.searchValue, placeholder: i18n.search_value_placeholder } ) %>
        <% } %>
      </div>

      <div class="cptdsv-col">
        <%= templates.dropdown( { index: index, options: options.replacementType, prop: 'replacementType', value: model.replacementType } ) %>

        <% if ( model.replacementType ) { %>
          <% if ( options.replacementValue[ model.replacementType ] ) { %>
            <%= templates.dropdown( { index: index, options: options.replacementValue[ model.replacementType ], prop: 'replacementValue', value: model.replacementValue } ) %>
          <% } else { %>
            <%= templates.text( { prop: 'replacementValue', value: model.replacementValue, placeholder: i18n.replacement_value_placeholder } ) %>
          <% } %>
        <% } %>

        <%= templates.dropdown( { index: index, options: options.replacementNaAction, prop: 'replacementNaAction', value: model.replacementNaAction } ) %>
      </div>

      <div class="cptdsv-col cptdsv-remove">
        <a href="#" class="button button-secondary button-large btn-remove-item"><?= __( 'x', 'cptemplates' ) ?></a>
      </div>
    </script>

    <script type="text/template" class="cptdsv-tpl" data-name="dropdown">
      <select data-prop="<%= prop %>">
        <% _( options ).each( function( item ) { %>
          <% if ( item.options ) { %>
            <optgroup label="<%= item.label %>">
              <% _( item.options ).each( function( option ) { %>
                <option value="<%= option.value %>" <%= ( option.value == value ) || ( _.isArray( value ) && ( -1 != _.indexOf( value, option.value + '' ) ) ) ? 'selected' : '' %> ><%= option.label %></option>
              <% }); %>
            </optgroup>
          <% } else { %>
            <option value="<%= item.value %>" <%= item.value == value ? 'selected' : '' %> ><%= item.label %></option>
          <% } %>
        <% }); %>
      </select>
    </script>

    <script type="text/template" class="cptdsv-tpl" data-name="text">
      <input type="text" data-prop="<%= prop %>" value="<%= value ? value : '' %>" placeholder="<%= placeholder %>">
    </script>

    <script type="text/template" class="cptdsv-tpl" data-name="hidden">
      <input type="hidden" data-prop="<%= prop %>" value="">
    </script>

    <script type="text/template" class="cptdsv-tpl" data-name="no-dynamic-values">
      <p><?= sprintf( '%s <a href="#" class="btn-add-item">%s</a>', __( 'No replacements configured. Click to', 'cptemplates' ), __( 'create new', 'cptemplates' ) ) ?></p>
    </script>
    <?php
  }


  /**
   *  configure_options
   *
   *  Configures options
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function configure_options() {

    $value_options = array(
      'meta' => array(
        'label' => __( 'Queried object meta', 'cptemplates' ),
        'type' => 'textfield',
        'group' => __( 'Generic', 'cptemplates' ),
      ),
      'object' => array(
        'label' => __( 'Queried object property', 'cptemplates' ),
        'type' => 'textfield',
        'group' => __( 'Generic', 'cptemplates' ),
      ),
      'filter' => array(
        'label' => __( 'Filter hook', 'cptemplates' ),
        'type' => 'textfield',
        'group' => __( 'Generic', 'cptemplates' ),
      ),
      'action' => array(
        'label' => __( 'Action output', 'cptemplates' ),
        'type' => 'textfield',
        'group' => __( 'Generic', 'cptemplates' ),
      ),
      'http_request' => array(
        'label' => __( 'HTTP Request', 'cptemplates' ),
        'type' => 'textfield',
        'group' => __( 'Generic', 'cptemplates' ),
      ),
      'template' => array(
        'label' => __( 'Theme Template', 'cptemplates' ),
        'type' => 'textfield',
        'group' => __( 'Generic', 'cptemplates' ),
      ),
    );

    // Allow 3rd parties to adjust
    $value_options = apply_filters( 'cptemplates/dynamic_shortcode_values/value_options', $value_options );

    // Prepare options in right format
    $this->value_options = $this->make_js_value_options( $value_options );


    // Supported param types
    $supported_param_types = array( 'textfield', 'textarea', 'autocomplete', 'el_id', 'dropdown', 'checkbox', 'animation_style' );

    // Allow 3rd parties to adjust supported params
    $supported_param_types = apply_filters( 'cptemplates/dynamic_shortcode_values/supported_param_types', $supported_param_types );  

    // No duplicates
    $this->supported_param_types = array_unique( $supported_param_types );


    // Exclude shortcode params. Allow 3rd parties to adjust
    $excluded_shortcode_params = apply_filters( 'cptemplates/dynamic_shortcode_values/excluded_shortcode_params', array() );  
    
    // Content param should be excluded, because it's not a valid shortcode param, but shortcode's content
    // cptdsv is the own param and should never be overriden
    $must_exclude_params = array( 'content', 'cptdsv' );
    $excluded_shortcode_params = array_merge( $excluded_shortcode_params, $must_exclude_params );
    
    // No duplicates
    $this->excluded_shortcode_params = array_unique( $excluded_shortcode_params );


    // Add listeners to print options script
    add_action( 'vc_backend_editor_enqueue_js_css', array( $this, 'print_options' ) );
    add_action( 'vc_frontend_editor_enqueue_js_css', array( $this, 'print_options' ) );
  }


  /**
   *  make_js_value_options
   *
   *  Creates proper options
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function make_js_value_options( $value_options ) {    
    $groups = array();
    $nogroup_name = __( 'Ungrouped', 'cptemplates' );

    // Group value options
    foreach ( $value_options as $name => $item ) {
      $item[ 'name' ] = $name;
      $group_name = isset( $item[ 'group' ] ) ? $item[ 'group' ] : $nogroup_name;

      if ( ! isset( $groups[ $group_name ] ) ) {
        $groups[ $group_name ] = array();
      }

      $groups[ $group_name ] []= $item;
    }


    $replacement_types = array();
    $replacement_values = array();

    // Create not selected type
    $replacement_types []= array(
      'label' => __( '--- Choose replacement type ---', 'cptemplates' ),
      'value' => ''
    );

    foreach ( $groups as $group_name => $items ) {

      // Create group options
      $options = array();
      foreach ( $items as $item ) {
        $options []= array(
          'label' => $item[ 'label' ],
          'value' => $item[ 'name' ]
        );

        $choice_groups = array();
        if ( isset( $item[ 'choices' ] ) && is_array( $item[ 'choices' ] ) && ! empty( $item[ 'choices' ] ) ) {

          $has_not_selected = false;

          // Loop through groups
          foreach ( (array)$item[ 'choices' ] as $group_label => $group_items) {

            $choices = array();
            foreach ( $group_items as $choice_value => $choice_label ) {
              $choices []= array(
                'label' => $choice_label,
                'value' => $choice_value
              );

              // Flag to create or not the not selected option item
              if ( '' === $choice_value ) {
                $has_not_selected = true;
              }
            }

            // Append choices if any
            if ( ! empty( $choices ) ) {
              $choice_groups []= array(
                'label' => $group_label,
                'options' => $choices,
              );
            }

          }


          // Create not selected type at the beginning
          if ( ! $has_not_selected ) {
            array_unshift( $choice_groups, array(
              'label' => __( '--- Choose replacement value ---', 'cptemplates' ),
              'value' => ''
            ) );
          }

          // Save choice groups
          $replacement_values[ $item[ 'name' ] ] = $choice_groups;
        }

      }

      $replacement_types []= array(
        'label' => $group_name,
        'options' => $options
      );

    }


    $search_types = array(
      array(
        'label' => __( 'Shortcode attribute', 'cptemplates' ),
        'value' => 'attr'
      ),
      array(
        'label' => __( 'Shortcode content', 'cptemplates' ),
        'value' => 'content'
      ),
      array(
        'label' => __( 'Custom text placeholder', 'cptemplates' ),
        'value' => 'text'
      ),
    );


    $replacement_na_actions = array(
      array(
        'label' => __( '--- Choose action if value is not available ---', 'cptemplates' ),
        'value' => ''
      ),
      array(
        'label' => __( 'Remove attribute or placeholder', 'cptemplates' ),
        'value' => 'remove'
      ),
      array(
        'label' => __( 'Clear attribute or placeholder', 'cptemplates' ),
        'value' => 'clear'
      ),
      array(
        'label' => __( 'Remove this shortcode', 'cptemplates' ),
        'value' => 'remove_shortcode'
      ),
    );

    $result = array(
      'replacementType' => $replacement_types,
      'replacementValue' => $replacement_values,
      'replacementNaAction' => $replacement_na_actions,
      'searchType' => $search_types,
    );

    return $result;
  }


  /**
   *  print_options
   *
   *  Prints options script
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function print_options() {
    $options = array(
      'valueOptions' => $this->value_options,
      'supportedParamTypes' => $this->supported_param_types,
      'excludedShortcodeParams' => $this->excluded_shortcode_params,
      'i18n' => $this->get_i18n_strings(),
    );
    printf( '<script type="text/javascript">var cptdsv = %s;</script>', json_encode( $options ) );
  }


}


/**
 *  cptemplates_integrate_vc_dynamic_shortcode_values
 *
 *  The main function responsible for returning cptemplates_integrate_vc_dynamic_shortcode_values object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_vc_dynamic_shortcode_values instance
 */

function cptemplates_integrate_vc_dynamic_shortcode_values() {
  return cptemplates_integrate_vc_dynamic_shortcode_values::instance();
}


// initialize
cptemplates_integrate_vc_dynamic_shortcode_values();


endif; // class_exists check

?>