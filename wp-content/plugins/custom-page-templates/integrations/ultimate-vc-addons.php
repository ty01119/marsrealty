<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_ultimate_vc_addons' ) ) :

final class cptemplates_integrate_ultimate_vc_addons {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_ultimate_vc_addons
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Check if Visual Composer installed
    $this->is_uvca_active = cptemplates_is_plugin_active( '#^Ultimate_VC_Addons(.*?)/Ultimate_VC_Addons\.php$#i' );

    // Include all Visual Composer integrations if it's active
    if ( $this->is_uvca_active ) {
      if ( ! is_admin() ) {
        add_action( 'cptemplates/template_applied', array( $this, 'cptemplate_applied' ) );
      }
    }
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_ultimate_vc_addons instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  cptemplate_applied
   *
   *  Prepares for rendering custom template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function cptemplate_applied( $template_post = null ) {
    if ( ! $template_post ) {
      return;
    }

    $this->template_post = $template_post;

    add_filter( 'ultimate_front_scripts_post_content', array( $this, 'post_content' ), 10, 2 );
  }


  /**
   *  post_content
   *
   *  Appends template's content to the currently queried object content for shortcodes identifying
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $post_content
   *  @param (object) $post
   *  @return  (string)
   */

  public function post_content( $post_content, $post ) {
    return $post_content . ' ' . $this->template_post->post_content;
  }


}


/**
 *  cptemplates_integrate_ultimate_vc_addons
 *
 *  The main function responsible for returning cptemplates_integrate_ultimate_vc_addons object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_ultimate_vc_addons instance
 */

function cptemplates_integrate_ultimate_vc_addons() {
  return cptemplates_integrate_ultimate_vc_addons::instance();
}


// initialize
cptemplates_integrate_ultimate_vc_addons();


endif; // class_exists check

?>