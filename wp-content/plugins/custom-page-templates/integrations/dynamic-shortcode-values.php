<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_dynamic_shortcode_values' ) ) :

final class cptemplates_integrate_dynamic_shortcode_values {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_dynamic_shortcode_values
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Include all DSV components
    cptemplates_include_all( 'integrations/dynamic-shortcode-values' );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_dynamic_shortcode_values instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


}


/**
 *  cptemplates_integrate_dynamic_shortcode_values
 *
 *  The main function responsible for returning cptemplates_integrate_dynamic_shortcode_values object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_dynamic_shortcode_values instance
 */

function cptemplates_integrate_dynamic_shortcode_values() {
  return cptemplates_integrate_dynamic_shortcode_values::instance();
}


// initialize
cptemplates_integrate_dynamic_shortcode_values();


endif; // class_exists check

?>