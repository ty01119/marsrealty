<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_wc_structured_data' ) ) :

final class cptemplates_integrate_wc_structured_data {


  /**
   * Plugin instance
   *
   * @since 3.0.7
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 3.0.7
   * @var boolean $initialized
   */
  private $initialized = false;


  /**
   * Contains list of all supported structured data types
   *
   * @since 3.0.7
   * @var boolean $shortcodes
   */
  private $allowed_types = array( 'product', 'website' );
  

  /**
   * Contains list of all supported structured data types for Visual Composer
   *
   * @since 3.0.7
   * @var boolean $shortcodes
   */
  private $vc_type_options;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_wc_structured_data
   *
   *  @type  function
   *  @date  03/04/18
   *  @since 3.0.7
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    $this->vc_type_options = array(
      __( 'Product', 'cptemplates' ) => 'product',
      __( 'Website', 'cptemplates' ) => 'website',
    );    

    add_shortcode( 'cptemplates_wc_structured_data', array( $this, 'shortcode' ) );
    add_filter( 'cptemplates/woocommerce/structured_data_type_vc_options', array( $this, 'populate_vc_type_options' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  03/04/18
   *  @since 3.0.7
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_wc_structured_data instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  shortcode
   *
   *  Force WooCommerce to generate structured data
   *
   *  @type  function
   *  @date  03/04/18
   *  @since 3.0.7
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function shortcode( $atts = array(), $content = null ) {
    $type = isset( $atts[ 'type' ] ) ?  strval( $atts[ 'type' ] ) : '';
    if ( in_array( $type, $this->allowed_types ) ) {
      $method = sprintf( 'generate_%s_data', $type );
      $wc_sd = WC()->structured_data;
      if ( method_exists( $wc_sd, $method ) ) {
        $wc_sd->$method();
      }
    }
    return '';
  }


  /**
   *  populate_vc_type_options
   *
   *  Populates types for Visual Composer
   *
   *  @type  function
   *  @date  03/04/18
   *  @since 3.0.7
   *
   *  @param (array) $type_options
   *  @return  (array)
   */

  public function populate_vc_type_options( $type_options = array() ) {
    return array_merge( $this->vc_type_options, $type_options );
  }


}


/**
 *  cptemplates_integrate_wc_structured_data
 *
 *  The main function responsible for returning cptemplates_integrate_wc_structured_data object
 *
 *  @type  function
 *  @date  03/04/18
 *  @since 3.0.7
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_wc_structured_data instance
 */

function cptemplates_integrate_wc_structured_data() {
  return cptemplates_integrate_wc_structured_data::instance();
}


// initialize
cptemplates_integrate_wc_structured_data();


endif; // class_exists check

?>