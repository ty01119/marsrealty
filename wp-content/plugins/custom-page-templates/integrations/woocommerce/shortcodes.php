<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_wc_shortcodes' ) ) :

final class cptemplates_integrate_wc_shortcodes {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
    

  /**
   * Contains list of all supported shortcodes
   *
   * @since 1.0.0
   * @var boolean $shortcodes
   */
  private $shortcodes;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_wc_shortcodes
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    $this->shortcodes = array(
      // Global
      'woocommerce_content' => array(
        'label' => __( 'Page Content', 'cptemplates' ),
        'description' => __( 'Displays complete WooCommerce page content', 'cptemplates' ),
      ),
      'page_title' => array(
        'label' => __( 'Page Title', 'cptemplates' ),
        'description' => __( 'Displays current page title', 'cptemplates' ),
      ),
      'breadcrumbs' => array(
        'label' => __( 'Breadcrumbs', 'cptemplates' ),
        'description' => __( 'Displays breadcrumbs', 'cptemplates' ),
      ),
      'sidebar' => array(
        'label' => __( 'Sidebar', 'cptemplates' ),
        'description' => __( 'Displays shop sidebar', 'cptemplates' ),
      ),
      'shop_messages' => array(
        'label' => __( 'Shop messages', 'cptemplates' ),
        'description' => __( 'Displays WooCommerce messages', 'cptemplates' ),
      ),
      'login_form' => array(
        'label' => __( 'Login form', 'cptemplates' ),
        'description' => __( 'Displays WooCommerce login form', 'cptemplates' ),
      ),
      'logout_button' => array(
        'label' => __( 'Logout button', 'cptemplates' ),
        'description' => __( 'Displays logout button', 'cptemplates' ),
      ),
      'proceed_to_checkout' => array(
        'label' => __( 'Proceed to checkout', 'cptemplates' ),
        'description' => __( 'Displays proceed to checkout button', 'cptemplates' ),
      ),
      
      // Archive
      'archive_description' => array(
        'label' => __( 'Archive description', 'cptemplates' ),
        'description' => __( 'Displays description of the current product category or tag', 'cptemplates' ),
      ),
      'archive_subcategories' => array(
        'label' => __( 'Subcategories', 'cptemplates' ),
        'description' => __( 'Displays subcategories of the current product category', 'cptemplates' ),
      ),
      'archive_products' => array(
        'label' => __( 'Products', 'cptemplates' ),
        'description' => __( 'Displays products list of the current product category or tag', 'cptemplates' ),
      ),
      
      // Product
      'product_title' => array(
        'label' => __( 'Product title', 'cptemplates' ),
        'description' => __( 'Displays current product title', 'cptemplates' ),
      ),
      'product_price' => array(
        'label' => __( 'Product price', 'cptemplates' ),
        'description' => __( 'Displays current product price', 'cptemplates' ),
      ),
      'product_images' => array(
        'label' => __( 'Product images', 'cptemplates' ),
        'description' => __( 'Displays current product images', 'cptemplates' ),
      ),
      'product_sale_flash' => array(
        'label' => __( 'Product sale flash', 'cptemplates' ),
        'description' => __( 'Displays current product sale flash', 'cptemplates' ),
      ),
      'product_thumbnails' => array(
        'label' => __( 'Product thumbnails', 'cptemplates' ),
        'description' => __( 'Displays current product thumbnails', 'cptemplates' ),
      ),
      'product_excerpt' => array(
        'label' => __( 'Product short description', 'cptemplates' ),
        'description' => __( 'Displays current product short description', 'cptemplates' ),
      ),
      'product_description' => array(
        'label' => __( 'Product description', 'cptemplates' ),
        'description' => __( 'Displays current product description', 'cptemplates' ),
      ),
      'product_additional_information' => array(
        'label' => __( 'Product additional information', 'cptemplates' ),
        'description' => __( 'Displays current product additional information', 'cptemplates' ),
      ),
      'product_data_tabs' => array(
        'label' => __( 'Product tabs', 'cptemplates' ),
        'description' => __( 'Displays current product tabs', 'cptemplates' ),
      ), 
      'product_rating' => array(
        'label' => __( 'Product rating', 'cptemplates' ),
        'description' => __( 'Displays current product rating', 'cptemplates' ),
      ),
      'product_reviews' => array(
        'label' => __( 'Product reviews', 'cptemplates' ),
        'description' => __( 'Displays current product reviews', 'cptemplates' ),
      ),
      'product_sharing' => array(
        'label' => __( 'Product sharing', 'cptemplates' ),
        'description' => __( 'Displays current product social sharing buttons', 'cptemplates' ),
      ),
      'product_meta' => array(
        'label' => __( 'Product meta', 'cptemplates' ),
        'description' => __( 'Displays current product meta', 'cptemplates' ),
      ),
      'product_wishlist' => array(
        'label' => __( 'Product wishlist', 'cptemplates' ),
        'description' => __( 'Displays add to wishlist button', 'cptemplates' ),
      ),
      'product_add_to_cart' => array(
        'label' => __( 'Product add to cart', 'cptemplates' ),
        'description' => __( 'Displays current product add to cart button', 'cptemplates' ),
      ),
      'product_related_products' => array(
        'label' => __( 'Related products', 'cptemplates' ),
        'description' => __( 'Displays related products for the current product', 'cptemplates' ),
      ),
      'product_upsell' => array(
        'label' => __( 'Product up-sells', 'cptemplates' ),
        'description' => __( 'Displays current product up-sells', 'cptemplates' ),
      ),

      // Cart
      // 'cart_contents', 'cart_totals', 'cart_estimate_shipping', 'cart_coupon',

      // Checkout
    );

    array_walk( $this->shortcodes, function( &$item, $key ) {
      $item[ 'tag' ] = 'cptwc_' . $key;
    });

    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_wc_shortcodes instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Check dependency
    if ( ! cptemplates_is_woocommerce_plugin_active() ) {
      return;
    }

    $this->add_shortcodes();
  }


  /**
   *  get_shortcodes
   *
   *  Retrieves all supported woocommerce shortcodes
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function get_shortcodes() {
    return $this->shortcodes;
  }


  /**
   *  add_shortcodes
   *
   *  Registers shortcodes
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function add_shortcodes() {
    foreach ( $this->shortcodes as $method => $shortcode ) {
      add_shortcode ( $shortcode[ 'tag' ], array( $this, $method ) );
    }
  }


  /**
   *  prepare_shortcode_result
   *
   *  Applies additional formatting to the shortcode
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  private function prepare_shortcode_result( $result, $atts = array(), $content = null, $tag = '' ) {
    extract ( shortcode_atts ( array (
      'el_class' => ''
    ), $atts ) );

    $result = trim( $result );
    $result = $result && ! empty( $el_class ) ? sprintf( '<div class="%s">%s</div>', $el_class, $result ) : $result;

    // Allow 3rd party to adjust
    $result = apply_filters( 'cptemplates/woocommerce/' . $tag, $result, $atts, $content );

    return $result;
  }


  /**
   *  woocommerce_content
   *
   *  Retrieves WooCommerce content for the current request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function woocommerce_content( $atts = array(), $content = null ) {
    rewind_posts();
    ob_start();
    woocommerce_content();
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'woocommerce_content' ][ 'tag' ] );
  }


  /**
   *  page_title
   *
   *  Retrieves WooCommerce page title for the current request
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function page_title( $atts = array(), $content = null ) {
    $result = woocommerce_page_title( false );
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'page_title' ][ 'tag' ] );
  }


  /**
   *  breadcrumbs
   *
   *  Retrieves WooCommerce Breadcrumbs
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function breadcrumbs( $atts = array(), $content = null ) {
    ob_start();
    woocommerce_breadcrumb();
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'breadcrumbs' ][ 'tag' ] );
  }


  /**
   *  sidebar
   *
   *  Retrieves shop sidebar
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function sidebar( $atts = array(), $content = null ) {
    ob_start();
    woocommerce_get_sidebar();
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'sidebar' ][ 'tag' ] );
  }


  /**
   *  proceed_to_checkout
   *
   *  Retrieves Proceed to Checkout button
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function proceed_to_checkout( $atts = array(), $content = null ) {
    ob_start();
    woocommerce_button_proceed_to_checkout();
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'proceed_to_checkout' ][ 'tag' ] );
  }


  /**
   *  shop_messages
   *
   *  Retrieves shop messages
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function shop_messages( $atts = array(), $content = null ) {
    $result = WC_Shortcodes::shop_messages( $atts );
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'shop_messages' ][ 'tag' ] );
  }


  /**
   *  login_form
   *
   *  Retrieves Login Form
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function login_form( $atts = array(), $content = null ) {
    ob_start();
    woocommerce_login_form( $atts );
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'login_form' ][ 'tag' ] );
  }


  /**
   *  logout_button
   *
   *  Retrieves logout button
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function logout_button( $atts = array(), $content = null ) {
    extract ( shortcode_atts ( array (
      'el_class' => ''
    ), $atts ) );

    $result = sprintf( '<a href="%s" class="button wc-auth-logout %s">%s</a>', wc_logout_url(), $el_class, __( 'Logout', 'woocommerce' ) );

    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'logout_button' ][ 'tag' ] );
  }


  /**
   *  archive_description
   *
   *  Retrieves archive description
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function archive_description( $atts = array(), $content = null ) {
    ob_start();
    if ( is_tax( array( 'product_cat', 'product_tag' ) ) ) {
      woocommerce_taxonomy_archive_description();
    }
    elseif ( is_post_type_archive( 'product' ) ) {
      woocommerce_product_archive_description();
    }
    else {}
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'archive_description' ][ 'tag' ] );
  }


  /**
   *  archive_subcategories
   *
   *  Retrieves archive subcategories
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function archive_subcategories( $atts = array(), $content = null ) {
    ob_start();
    woocommerce_product_subcategories();
    $categories = ob_get_clean();
    $categories = trim( $categories );

    if ( $categories ) {
      ob_start();
      woocommerce_product_loop_start();
      echo $categories;
      woocommerce_product_loop_end();
      $result = ob_get_clean();
    }

    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'archive_subcategories' ][ 'tag' ] );
  }



  /**
   *  archive_products
   *
   *  Retrieves archive products
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function archive_products( $atts = array(), $content = null ) {
    ob_start();

    if ( have_posts() ) {

      do_action('woocommerce_before_shop_loop');

      woocommerce_product_loop_start();

      while ( have_posts() ) {

        the_post();
        wc_get_template_part( 'content', 'product' );
      }

      woocommerce_product_loop_end();

      do_action('woocommerce_after_shop_loop');

    }
    else {
      wc_get_template( 'loop/no-products-found.php' );      
    }

    $products = ob_get_clean();
    $products = trim( $products );
    $result = $products ? $products : '';
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'archive_products' ][ 'tag' ] );
  }


  /**
   *  product_title
   *
   *  Retrieves single product title template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_title( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }

    ob_start();
    if ( cptemplates_is_jupiter_theme() ) {
      printf( '<h1 itemprop="name" class="single_product_title entry-title">%s</h1>', get_the_title() );
    }
    elseif ( cptemplates_is_avada_theme() ) {
      get_template_part( 'templates/wc-single-title' );
    }
    else {
      woocommerce_template_single_title();
    }
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_title' ][ 'tag' ] );
  }


  /**
   *  product_price
   *
   *  Retrieves single product price template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_price( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }

    ob_start();
    if ( cptemplates_is_jupiter_theme() && is_a( $product, 'WC_Product' ) ) {
      ?>
      <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        <p class="price"><?php echo $product->get_price_html(); ?></p>
        <meta itemprop="price" content="<?php echo esc_attr( $product->get_display_price() ); ?>" />
        <meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
        <link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
      </div>
      <?php
    }
    else {
      woocommerce_template_single_price();
    }
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_price' ][ 'tag' ] );
  }


  /**
   *  product_images
   *
   *  Retrieves single product images template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_images( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }

    if ( cptemplates_is_jupiter_theme() ) {
      $atts[ 'el_class' ] = ! isset( $atts[ 'el_class' ] ) ? 'cpt-position-relative cpt-overflow-hidden' : $atts[ 'el_class' ];
    }
    
    ob_start();
    if ( class_exists( 'JCKWooThumbs' ) ) {
      $JCKWooThumbs = new JCKWooThumbs;
      $JCKWooThumbs->show_product_images();
    }
    elseif ( cptemplates_is_betheme_theme() ) {
      printf( '<div class="product"><div class="product_wrapper" style="padding:0;"><div class="product_image_wrapper">' );
      woocommerce_show_product_images();
      printf( '</div></div></div>' );
    }
    else {
      woocommerce_show_product_images();
    }
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_images' ][ 'tag' ] );
  }


  /**
   *  product_sale_flash
   *
   *  Retrieves single product sale flash template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_sale_flash( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }

    ob_start();
    woocommerce_show_product_sale_flash();
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_sale_flash' ][ 'tag' ] );
  }


  /**
   *  product_thumbnails
   *
   *  Retrieves single product thumbnails template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_thumbnails( $atts = array(), $content = null ) {
    global $post, $product, $woocommerce;

    // Avoid errors if shortcode has been fired in wrong context
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }
    
    ob_start();
    if ( cptemplates_is_jupiter_theme() && is_a( $product, 'WC_Product' ) ) {

      $attachment_ids = $product->get_gallery_attachment_ids();

      if ( $attachment_ids ) {
        $loop     = 0;
        $columns  = apply_filters( 'woocommerce_product_thumbnails_columns', 3 );
        ?>
        <div class="thumbnails <?php echo 'columns-' . $columns; ?>"><?php

          foreach ( $attachment_ids as $attachment_id ) {
            $classes = array( 'zoom' );
            if ( $loop === 0 || $loop % $columns === 0 ) {
              $classes[] = 'first';
            }
            if ( ( $loop + 1 ) % $columns === 0 ) {
              $classes[] = 'last';
            }
            $image_class = implode( ' ', $classes );
            $props       = wc_get_product_attachment_props( $attachment_id, $post );
            if ( ! $props['url'] ) {
              continue;
            }
            echo apply_filters(
              'woocommerce_single_product_image_thumbnail_html',
              sprintf(
                '<a href="%s" class="%s" title="%s" data-rel="prettyPhoto[product-gallery]">%s</a>',
                esc_url( $props['url'] ),
                esc_attr( $image_class ),
                esc_attr( $props['caption'] ),
                wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ), 0, $props )
              ),
              $attachment_id,
              $post->ID,
              esc_attr( $image_class )
            );
            $loop++;
          }

        ?></div>
        <?php
      }
    }
    elseif ( cptemplates_is_betheme_theme() ) {
      printf( '<div class="product"><div class="product_wrapper" style="padding:0;"><div class="product_image_wrapper">' );
      woocommerce_show_product_thumbnails();
      printf( '</div></div></div>' );
    }
    else {
      woocommerce_show_product_thumbnails();
    }

    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_thumbnails' ][ 'tag' ] );
  }


  /**
   *  product_excerpt
   *
   *  Retrieves single product excerpt template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_excerpt( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $post, $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }
    
    ob_start();
    if ( cptemplates_is_jupiter_theme() && is_a( $product, 'WC_Product' ) ) {
      if ( $post->post_excerpt ) {
        ?>
        <div itemprop="description">
          <?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt ) ?>
        </div>
        <?php
      }
    }
    else {
      woocommerce_template_single_excerpt();
    }
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_excerpt' ][ 'tag' ] );
  }


  /**
   *  product_description
   *
   *  Retrieves single product description template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_description( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $post, $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }

    ob_start();
    if ( $post->post_content ) {
      echo do_shortcode( $post->post_content );
    }
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_description' ][ 'tag' ] );
  }


  /**
   *  product_additional_information
   *
   *  Retrieves single product additional information template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_additional_information( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }

    ob_start();
    woocommerce_product_additional_information_tab();
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_additional_information' ][ 'tag' ] );
  }


  /**
   *  product_data_tabs
   *
   *  Retrieves single product tabs template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_data_tabs( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }

    if ( cptemplates_is_udesign_theme() ) {
      $atts[ 'el_class' ] = ! isset( $atts[ 'el_class' ] ) ? 'product' : $atts[ 'el_class' ];
    }

    ob_start();
    woocommerce_output_product_data_tabs();
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_data_tabs' ][ 'tag' ] );
  }


  /**
   *  product_reviews
   *
   *  Retrieves single product reviews template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_reviews( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }

    ob_start();
    if ( comments_open() ) {
      comments_template();
    }
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_reviews' ][ 'tag' ] );
  }


  /**
   *  product_rating
   *
   *  Retrieves single product rating template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_rating( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }
    
    ob_start();
    if ( cptemplates_is_jupiter_theme() && is_a( $product, 'WC_Product' ) ) {
      $rating_count = $product->get_rating_count();
      $review_count = $product->get_review_count();
      $average      = $product->get_average_rating();

      if ( $rating_count > 0 ) : ?>
        <div class="woocommerce-product-rating" itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
          <div class="star-rating" title="<?php printf( __( 'Rated %s out of 5', 'woocommerce' ), $average ); ?>">
            <span style="width:<?php echo ( ( $average / 5 ) * 100 ); ?>%">
              <strong itemprop="ratingValue" class="rating"><?php echo esc_html( $average ); ?></strong> <?php printf( __( 'out of %s5%s', 'woocommerce' ), '<span itemprop="bestRating">', '</span>' ); ?>
              <?php printf( _n( 'based on %s customer rating', 'based on %s customer ratings', $rating_count, 'woocommerce' ), '<span itemprop="ratingCount" class="rating">' . $rating_count . '</span>' ); ?>
            </span>
          </div>
          <?php if ( comments_open() ) : ?><a href="#reviews" class="woocommerce-review-link" rel="nofollow">(<?php printf( _n( '%s customer review', '%s customer reviews', $review_count, 'woocommerce' ), '<span itemprop="reviewCount" class="count">' . $review_count . '</span>' ); ?>)</a><?php endif ?>
        </div>
      <?php endif;
    }
    else {
      woocommerce_template_single_rating();
    }

    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_rating' ][ 'tag' ] );
  }


  /**
   *  product_sharing
   *
   *  Retrieves single product sharing template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_sharing( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }

    ob_start();
    if ( cptemplates_is_avada_theme() ) {
      get_template_part( 'templates/wc-after-single-product-summary' );
    }
    else {
      woocommerce_template_single_sharing();
    }
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_sharing' ][ 'tag' ] );
  }


  /**
   *  product_meta
   *
   *  Retrieves single product meta template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_meta( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }

    ob_start();
    woocommerce_template_single_meta();
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_meta' ][ 'tag' ] );
  }


  /**
   *  product_add_to_cart
   *
   *  Retrieves single product add to cart template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_add_to_cart( $atts = array(), $content = null ) {
    // Avoid errors if shortcode has been fired in wrong context
    global $product;
    if ( ! is_a( $product, 'WC_Product' ) ) {
      return;
    }

    if ( cptemplates_is_jupiter_theme() ) {
      $atts[ 'el_class' ] = ! isset( $atts[ 'el_class' ] ) ? 'cpt-position-relative' : $atts[ 'el_class' ];
    }
    elseif ( cptemplates_is_udesign_theme() ) {
      $atts[ 'el_class' ] = ! isset( $atts[ 'el_class' ] ) ? 'product' : $atts[ 'el_class' ];
    }
    ob_start();
    woocommerce_template_single_add_to_cart();
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_add_to_cart' ][ 'tag' ] );
  }


  /**
   *  product_wishlist
   *
   *  Retrieves single product wishlist template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_wishlist( $atts = array(), $content = null ) {
    if ( shortcode_exists( 'yith_wcwl_add_to_wishlist' ) ) {
      $result = do_shortcode( '[yith_wcwl_add_to_wishlist]' );
    }
    else {
      $result = '';
    }
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_wishlist' ][ 'tag' ] );
  }


  /**
   *  product_related_products
   *
   *  Retrieves single product ralated products template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_related_products( $atts = array(), $content = null ) {
    ob_start();
    if ( cptemplates_is_avada_theme() ) {
      global $avada_woocommerce;
      if ( $avada_woocommerce && method_exists( $avada_woocommerce, 'output_related_products' ) ) {
        $avada_woocommerce->output_related_products();
      }
    }
    else {
      woocommerce_output_related_products( $atts );
    }
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_related_products' ][ 'tag' ] );
  }


  /**
   *  product_upsell
   *
   *  Retrieves single product up-sell products template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function product_upsell( $atts = array(), $content = null ) {
    ob_start();
    if ( cptemplates_is_avada_theme() ) {
      global $avada_woocommerce;
      if ( $avada_woocommerce && method_exists( $avada_woocommerce, 'upsell_display' ) ) {
        $avada_woocommerce->upsell_display();
      }
    }
    else {
      woocommerce_upsell_display( $atts );
    }
    $result = ob_get_clean();
    return $this->prepare_shortcode_result( $result, $atts, $content, $this->shortcodes[ 'product_upsell' ][ 'tag' ] );
  }


}


/**
 *  cptemplates_integrate_wc_shortcodes
 *
 *  The main function responsible for returning cptemplates_integrate_wc_shortcodes object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_wc_shortcodes instance
 */

function cptemplates_integrate_wc_shortcodes() {
  return cptemplates_integrate_wc_shortcodes::instance();
}


// initialize
cptemplates_integrate_wc_shortcodes();


endif; // class_exists check

?>