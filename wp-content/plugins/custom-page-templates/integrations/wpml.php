<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_wpml' ) ) :

final class cptemplates_integrate_wpml {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_wpml
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {

    if ( cptemplates_is_wpml_plugin_active() ) {
      add_filter( 'cptemplates/custom_post_types/admin/save_args', array( $this, 'custom_post_types_save_args' ), 10, 3 );
      add_filter( 'cptemplates/custom_post_types/register', array( $this, 'filter_custom_type_registration' ), 10, 2 );
      
      add_filter( 'cptemplates/custom_taxonomies/admin/save_args', array( $this, 'custom_post_types_save_args' ), 10, 3 );
      add_filter( 'cptemplates/custom_taxonomies/register', array( $this, 'filter_custom_type_registration' ), 10, 2 );
    }

  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_wpml instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  custom_post_types_save_args
   *
   *  Appends language information to the post typw
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $args (array) Registration object arguments
   *  @param $post_id (int)
   *  @param $post (WP_Post)
   *
   *  @return (array)
   */

  public function custom_post_types_save_args( $args, $post_id, $post ) {
    $args[ 'lang' ] = ICL_LANGUAGE_CODE;
    return $args;
  }


  /**
   *  filter_custom_type_registration
   *
   *  Prevents custom post type registration if language doesn't match
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $flag (boolean) Either register or not
   *  @param $args (array) Registration object arguments
   *
   *  @return (boolean)
   */

  public function filter_custom_type_registration( $flag, $args ) {
    global $sitepress;

    if ( is_a( $sitepress, 'SitePress' ) ) {
      $args[ 'lang' ] = isset( $args[ 'lang' ] ) ? $args[ 'lang' ] : $sitepress->get_default_language();
      return $args[ 'lang' ] == ICL_LANGUAGE_CODE;
    }

    return $flag;
  }


}


/**
 *  cptemplates_integrate_wpml
 *
 *  The main function responsible for returning cptemplates_integrate_wpml object
 *
 *  @type  function
 *  @date  03/02/18
 *  @since 3.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_wpml instance
 */

function cptemplates_integrate_wpml() {
  return cptemplates_integrate_wpml::instance();
}


// initialize
cptemplates_integrate_wpml();


endif; // class_exists check

?>