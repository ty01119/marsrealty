<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_yoast_seo' ) ) :

final class cptemplates_integrate_yoast_seo {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_yoast_seo
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    add_action( 'add_meta_boxes', array( $this, 'remove_metabox' ), 100 );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_yoast_seo instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  remove_metabox
   *
   *  Removes Yoast SEO metabox from edit custom page template edit screen
   *  Yoast SEO expects that media scripts will be loaded, but they are not on edit screen,
   *  which causes errors and breaks whole frontend functionality
   *
   *  @type  function
   *  @date  31/01/18
   *  @since 2.1.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function remove_metabox() {
    remove_meta_box( 'wpseo_meta', 'cptemplate', 'normal' );
  }


}


/**
 *  cptemplates_integrate_yoast_seo
 *
 *  The main function responsible for returning cptemplates_integrate_yoast_seo object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_yoast_seo instance
 */

function cptemplates_integrate_yoast_seo() {
  return cptemplates_integrate_yoast_seo::instance();
}


// initialize
cptemplates_integrate_yoast_seo();


endif; // class_exists check

?>