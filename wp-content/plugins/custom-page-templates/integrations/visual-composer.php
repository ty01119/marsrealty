<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_visual_composer' ) ) :

final class cptemplates_integrate_visual_composer {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_visual_composer
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Include all Visual Composer integrations if it's active
    if ( cptemplates_is_visualcomposer_plugin_active() ) {
      cptemplates_include_all( 'integrations/visual-composer' );

      add_action( 'vc_after_init', array( $this, 'set_vc_editor_post_types_support' ) );

      add_action( 'init', array( $this, 'maybe_integrate_frontend_editor' ), 50 );

      if ( ! is_admin() ) {
        add_action( 'cptemplates/template_applied', array( $this, 'cptemplate_applied' ) );
      }

    }

  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_visual_composer instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  maybe_integrate_frontend_editor
   *
   *  Support editing page content with template applied in VC Front Editor
   *
   *  @type  function
   *  @date  12/11/17
   *  @since 1.0.4
   *
   *  @param N/A
   *  @return  N/A
   */

  public function maybe_integrate_frontend_editor() {
    if ( cptemplates_vc_is_inline() && ! cptemplates_vc_is_frontend_editor() && vc_is_page_editable() ) {
      add_action( 'cptemplates/template_applied', array( $this, 'integrate_vc_frontend_editor' ) );
    }
  }


  /**
   *  integrate_vc_frontend_editor
   *
   *  Visual Compser adds the_post action to do parsing content stuff
   *  It vanishes all the_content filters and sets own at 9999 priority
   *
   *  @type  function
   *  @date  12/11/17
   *  @since 1.0.4
   *
   *  @param (mixed) $template_post
   *  @return  n/a
   */

  public function integrate_vc_frontend_editor( $template_post ) {
    add_filter( 'cptemplates/dynamic_shortcode_values/get_post_value', array( $this, 'vc_post_content_inline_editing' ), 15, 2 );
  }


  /**
   *  vc_post_content_inline_editing
   *
   *  Post main content rendered with DSV must be replaced with special VC tag
   *  Afterwards, VC adds editable shortcodes via frontend JS
   *
   *  @type  function
   *  @date  12/11/17
   *  @since 1.0.4
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (string)
   */

  public function vc_post_content_inline_editing( $value, $name ) {
    if ( 'content' === $name ) {

      // Used by VC JS. Parent node of this anchor will be used as container for inline editing
      $value = '<span id="vc_inline-anchor" style="display:none !important;"></span>';

      // Need to wrap with another div, because otherwise all siblings will be vanished by editor
      $value = sprintf( '<div class="cptemplates-vc-inline-editor-wrapper">%s</div>', $value );

    }

    return $value;
  }


  /**
   *  set_vc_editor_post_types_support
   *
   *  Enables Visual Composer for the cptemplate post type by default
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function set_vc_editor_post_types_support() {

    // Verify if required functions exist
    if ( function_exists( 'vc_set_default_editor_post_types' ) && function_exists( 'vc_default_editor_post_types' ) ) { 

      // Get default post types from VC
      $default_post_types = vc_default_editor_post_types();

      // Append cptemplate post type to the list
      $default_post_types[]= 'cptemplate';
       
      // Update settings
      vc_set_default_editor_post_types( $default_post_types );
    }

  }


  /**
   *  cptemplate_applied
   *
   *  Prepares for rendering custom template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function cptemplate_applied( $template_post = null ) {
    if ( ! $template_post ) {
      return;
    }

    $this->template_post = $template_post;

    add_action( 'wp_print_styles', array( $this, 'print_cptemplate_custom_css' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_vc_assets' ) );
  }


  /**
   *  enqueue_vc_assets
   *
   *  Enqueues Visual Composer's base scripts and styles
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function enqueue_vc_assets() {
    if ( ! wp_style_is( 'js_composer_front' ) ) {
      wp_enqueue_style( 'js_composer_front' );
    }
    if ( ! wp_script_is( 'wpb_composer_front_js' ) ) {
      wp_enqueue_script( 'wpb_composer_front_js' );
    }
  }


  /**
   *  print_cptemplate_custom_css
   *
   *  Prints custom CSS created for custom page template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function print_cptemplate_custom_css( $content = '' ) {

    $applied_template_post = cptemplates_template()->get_applied_template_post();

    // Add custom CSS for shortcodes
    $post_custom_css = get_post_meta( $applied_template_post->ID, '_wpb_post_custom_css', true );
    $shortcodes_custom_css = get_post_meta( $applied_template_post->ID, '_wpb_shortcodes_custom_css', true );

    // Concatenate CSS
    $custom_css = '';
    $custom_css .= $post_custom_css ? $post_custom_css : '';
    $custom_css .= $shortcodes_custom_css ? $shortcodes_custom_css : '';
    $custom_css = trim( $custom_css );

    if ( $custom_css ) {
      printf( '<style>%s</style>', $custom_css );
    }

    return $content;
  }


  /**
   *  add_shortcode_param
   *
   *  Safely adds param to VC shortcode
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $name
   *  @param (callable) $callback
   *  @param (string) $js_url
   *  @return  (boolean)
   */

  public function add_shortcode_param( $name, $callback, $js_url ) {

    if ( function_exists( 'vc_add_shortcode_param' ) ) {
      vc_add_shortcode_param( $name, $callback, $js_url );
      return true;
    }
    elseif ( function_exists( 'add_shortcode_param' ) ) {
      add_shortcode_param( $name, $callback, $js_url );
      return true;
    }
    else {
      return false;
    }

  }


}


/**
 *  cptemplates_integrate_visual_composer
 *
 *  The main function responsible for returning cptemplates_integrate_visual_composer object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_visual_composer instance
 */

function cptemplates_integrate_visual_composer() {
  return cptemplates_integrate_visual_composer::instance();
}


// initialize
cptemplates_integrate_visual_composer();


endif; // class_exists check

?>