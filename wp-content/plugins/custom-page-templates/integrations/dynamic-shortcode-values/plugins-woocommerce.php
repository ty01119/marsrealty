<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_dsv_plugins_woocommerce' ) ) :

final class cptemplates_integrate_dsv_plugins_woocommerce {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_dsv_plugins_woocommerce
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    // Check dependency
    if ( ! cptemplates_is_woocommerce_plugin_active() ) {
      return;
    }

    add_filter( 'cptemplates/dynamic_shortcode_values/value_options', array( $this, 'value_options' ) );
    add_filter( 'cptemplates/dynamic_shortcode_values/get_woocommerce_value', array( $this, 'get_value' ), 10, 2 );

  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_dsv_plugins_woocommerce instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  value_options
   *
   *  Appends post value options to the list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $value_options
   *  @return  (array)
   */

  public function value_options( $value_options ) {

    // Append options used in Visual Composer
    $value_options[ 'woocommerce' ] = array(
      'label' => __( 'WooCommerce', 'cptemplates' ),
      'type' => 'dropdown',
      'group' => __( 'Plugins', 'cptemplates' ),
      'choices' => array(

        __( 'Global', 'cptemplates' ) => array(
          'template_woocommerce_content' => __( 'WooCommerce Content Template', 'cptemplates' ),
          'template_page_title' => __( 'Page Title Template', 'cptemplates' ),
          'template_breadcrumbs' => __( 'Breadcrumbs Template', 'cptemplates' ),
          'template_sidebar' => __( 'Shop Sidebar Template', 'cptemplates' ),
          'template_shop_messages' => __( 'Shop Messages Template', 'cptemplates' ),
          'template_login_form' => __( 'Login Form Template', 'cptemplates' ),
          'template_logout_button' => __( 'Logout Button Template', 'cptemplates' ),
          'template_proceed_to_checkout' => __( 'Proceed to Checkout Button Template', 'cptemplates' ),
        ),

        __( 'Product Archive', 'cptemplates' ) => array(
          'template_archive_description' => __( 'Archive Description', 'cptemplates' ),
          'template_archive_subcategories' => __( 'Archive Subcategories', 'cptemplates' ),
          'template_archive_products' => __( 'Archive Products', 'cptemplates' ),
          'category_image_id' => __( 'Category Image ID', 'cptemplates' ),
          'category_image_url' => __( 'Category Image URL', 'cptemplates' ),
        ),
        
        __( 'Single Product Templates', 'cptemplates' ) => array(
          'template_product_title' => __( 'Product Title Template', 'cptemplates' ),
          'template_product_price' => __( 'Product Price Template', 'cptemplates' ),
          'template_product_images' => __( 'Product Images Template', 'cptemplates' ),
          'template_product_sale_flash' => __( 'Product Sale Flash Template', 'cptemplates' ),
          'template_product_thumbnails' => __( 'Product Thumbnails Template', 'cptemplates' ),
          'template_product_excerpt' => __( 'Product Short Description Template', 'cptemplates' ),
          'template_product_description' => __( 'Product Description Template', 'cptemplates' ),
          'template_product_additional_information' => __( 'Product Additional Information Template', 'cptemplates' ),
          'template_product_data_tabs' => __( 'Product Tabs Template', 'cptemplates' ),
          'template_product_rating' => __( 'Product Rating Template', 'cptemplates' ),
          'template_product_reviews' => __( 'Product Reviews Template', 'cptemplates' ),
          'template_product_sharing' => __( 'Product Sharing Template', 'cptemplates' ),
          'template_product_meta' => __( 'Product Meta Template', 'cptemplates' ),
          'template_product_wishlist' => __( 'Product Wishlist', 'cptemplates' ),
          'template_product_add_to_cart' => __( 'Product Add to Cart Template', 'cptemplates' ),
          'template_product_related_products' => __( 'Related Products Template', 'cptemplates' ),
          'template_product_upsell' => __( 'Products Up-sells Template', 'cptemplates' ),
        ),

        __( 'Single Product Data', 'cptemplates' ) => array(
          'product_id' => __( 'Product ID', 'cptemplates' ),
          'product_title' => __( 'Product Title', 'cptemplates' ),
          'product_description' => __( 'Product Description', 'cptemplates' ),
          'product_short_description' => __( 'Product Short Description', 'cptemplates' ),
          'product_url' => __( 'Product URL', 'cptemplates' ),
          'product_sku' => __( 'Product SKU', 'cptemplates' ),
          
          'product_price' => __( 'Product Price', 'cptemplates' ),
          'product_price_excluding_tax' => __( 'Product Price Excluding Tax', 'cptemplates' ),
          'product_price_including_tax' => __( 'Product Price Including Tax', 'cptemplates' ),
          'product_regular_price' => __( 'Product Regular Price', 'cptemplates' ),
          'product_sale_price' => __( 'Product Sale Price', 'cptemplates' ),
          
          'product_stock_status' => __( 'Product Stock Status', 'cptemplates' ),
          'product_availability_text' => __( 'Product Availability Text', 'cptemplates' ),
          'product_availability_class' => __( 'Product Availability Class', 'cptemplates' ),
          
          'product_average_rating' => __( 'Average Rating', 'cptemplates' ),
          'product_rating_count' => __( 'Rating Count', 'cptemplates' ),
          'product_review_count' => __( 'Review Count', 'cptemplates' ),

          'product_weight' => __( 'Product Weight', 'cptemplates' ),
          'product_width' => __( 'Product Width', 'cptemplates' ),
          'product_height' => __( 'Product Height', 'cptemplates' ),
          'product_length' => __( 'Product Length', 'cptemplates' ),

          'product_image_id' => __( 'Product Image ID', 'cptemplates' ),
          'product_gallery_image_ids' => __( 'Product Gallery Image IDs', 'cptemplates' ),

          'product_category_ids' => __( 'Related Category IDs', 'cptemplates' ),
          'product_category_slugs' => __( 'Related Category Slugs', 'cptemplates' ),
          'product_tag_ids' => __( 'Related Tag IDs', 'cptemplates' ),
          'product_tag_slugs' => __( 'Related Tag Slugs', 'cptemplates' ),
          'product_attribute_ids' => __( 'Related Attribute IDs', 'cptemplates' ),
          'product_attribute_slugs' => __( 'Related Attribute Slugs', 'cptemplates' ),

          'product_related_ids' => __( 'Related Product IDs', 'cptemplates' ),
          'product_up_sells_ids' => __( 'Product Up-sells IDs', 'cptemplates' ),
          'product_cross_sells_ids' => __( 'Product Cross-sells IDs', 'cptemplates' ),

          'product_add_to_cart_url' => __( 'Product Add to Cart URL', 'cptemplates' ),
          'product_single_add_to_cart_text' => __( 'Product Add to Cart Text', 'cptemplates' ),
        ),

        __( 'Single Product Snippets', 'cptemplates' ) => array(
          'snippet_product_link' => __( 'Product Link Snippet', 'cptemplates' ),
          'snippet_product_price' => __( 'Product Price Snippet', 'cptemplates' ),
          'snippet_product_price_excluding_tax' => __( 'Product Price Excluding Tax Snippet', 'cptemplates' ),
          'snippet_product_price_including_tax' => __( 'Product Price Including Tax Snippet', 'cptemplates' ),
          'snippet_product_regular_price' => __( 'Product Regular Price Snippet', 'cptemplates' ),
          'snippet_product_sale_price' => __( 'Product Sale Price Snippet', 'cptemplates' ),
          'snippet_product_stock_status' => __( 'Product Stock Status Snippet', 'cptemplates' ),
          'snippet_product_rating' => __( 'Product Rating Snippet', 'cptemplates' ),
          'snippet_product_attributes' => __( 'Product Attributes Snippet', 'cptemplates' ),
          'snippet_product_dimensions' => __( 'Product Dimensions Snippet', 'cptemplates' ),
          'snippet_product_categories' => __( 'Product Categories Snippet', 'cptemplates' ),
          'snippet_product_tags' => __( 'Product Tags Snippet', 'cptemplates' ),
          'snippet_product_add_to_cart_button' => __( 'Product Add to Cart Button Snippet', 'cptemplates' ),
        ),

        __( 'Product Lists', 'cptemplates' ) => array(
          'recent_products_ids' => __( 'Recent Product IDs', 'cptemplates' ),
          'featured_products_ids' => __( 'Featured Product IDs', 'cptemplates' ),
          'sale_products_ids' => __( 'Sale Product IDs', 'cptemplates' ),
          'best_selling_products_ids' => __( 'Best Selling Product IDs', 'cptemplates' ),
          'top_rated_products_ids' => __( 'Top Rated Product IDs', 'cptemplates' ),
        ),


      ),
    );

    return $value_options;
  }


  /**
   *  get_value
   *
   *  Retrieves post post object value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function get_value( $value, $name ) {
    $method = "get_{$name}";
    if ( method_exists( $this, $method ) ) {
      return $this->$method( $value );
    }
    else {
      return $value;
    }
  }


  /**
   *  get_object_property
   *
   *  Retrieves object property if exists
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  private function get_object_property( $prop, $default_value ) {
    // Allow 3rd parties to adjust. Particularly it's used with VC Grids DSV replacements in this plugin
    $queried_object = apply_filters( 'cptemplates/dynamic_shortcode_values/replace_value/queried_object', get_queried_object() );

    if ( ! $queried_object ) {
      return $default_value;
    }

    return isset( $queried_object->$prop ) ? $queried_object->$prop : $default_value;
  }


  /**
   *  get_global_wc_product
   *
   *  Retrieves WC_Product defined in global $product variable
   *  Optionally retrieve by ID or slug
   *
   *  @type  function
   *  @date  25/09/17
   *  @since 1.0.3
   *
   *  @param (mixed) $value
   *  @return  (object|boolean)
   */

  private function get_global_wc_product() {
    global $product;

    if ( is_a( $product, 'WC_Product' ) ) {
      return $product;
    }

    // Some weird themes change this object to product slug or ID
    $query_args = array(
      'post_type' => 'product',
      'posts_per_page' => 1,
    );
    if ( is_string( $product ) ) {
      $product_post = get_page_by_path( $product, OBJECT, 'product' );
      $query_args[ 'name' ] = $product;
    }
    elseif ( is_numeric( $product ) ) {
      $query_args[ 'p' ] = $product;
    }
    else {
      return false;
    }

    $the_query = new WP_Query( $query_args );
    $product_post = ! empty( $the_query->posts ) ? $the_query->posts[ 0 ] : false;
    $wc_product = $product_post ? new WC_Product( $product_post->ID ) : false;

    // Reset global product variable to object, which should always be so in WooCommerce
    if ( is_a( $wc_product, 'WC_Product' ) ) {
      $product = $wc_product;
    }

    return $wc_product;
  }


  /**
   *  get_object_meta
   *
   *  Retrieves object meta value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_object_meta( $prop, $default_value ) {
    return cptemplates_dynamic_shortcode_values()->get_meta_value( $default_value, $prop );
  }


  /* ============================================================ */
  /* GLOBAL
  /* ============================================================ */


  /**
   *  get_template_woocommerce_content
   *
   *  Retrieves rendered template for woocommerce content
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_woocommerce_content( $value ) {
    return cptemplates_integrate_wc_shortcodes()->woocommerce_content();
  }


  /**
   *  get_template_page_title
   *
   *  Retrieves rendered template for page title
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_page_title( $value ) {
    return cptemplates_integrate_wc_shortcodes()->page_title();
  }


  /**
   *  get_template_breadcrumbs
   *
   *  Retrieves rendered template for breadcrumbs
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_breadcrumbs( $value ) {
    return cptemplates_integrate_wc_shortcodes()->breadcrumbs();
  }


  /**
   *  get_template_sidebar
   *
   *  Retrieves rendered template for sidebar
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_sidebar( $value ) {
    return cptemplates_integrate_wc_shortcodes()->sidebar();
  }


  /**
   *  get_template_shop_messages
   *
   *  Retrieves rendered template for shop messages
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_shop_messages( $value ) {
    return cptemplates_integrate_wc_shortcodes()->shop_messages();
  }


  /**
   *  get_template_login_form
   *
   *  Retrieves rendered template for login form
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_login_form( $value ) {
    return cptemplates_integrate_wc_shortcodes()->login_form();
  }


  /**
   *  get_template_logout_button
   *
   *  Retrieves rendered template for logout form
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_logout_button( $value ) {
    return cptemplates_integrate_wc_shortcodes()->logout_button();
  }


  /**
   *  get_template_proceed_to_checkout
   *
   *  Retrieves rendered template for proceed to checkout
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_proceed_to_checkout( $value ) {
    return cptemplates_integrate_wc_shortcodes()->proceed_to_checkout();
  }


  /* ============================================================ */
  /* PRODUCT ARCHIVE
  /* ============================================================ */


  /**
   *  get_template_archive_description
   *
   *  Retrieves rendered template for archive description
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_archive_description( $value ) {
    return cptemplates_integrate_wc_shortcodes()->archive_description();
  }


  /**
   *  get_template_archive_subcategories
   *
   *  Retrieves rendered template for archive subcategories
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_archive_subcategories( $value ) {
    return cptemplates_integrate_wc_shortcodes()->archive_subcategories();
  }


  /**
   *  get_template_archive_products
   *
   *  Retrieves rendered template for archive products
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_archive_products( $value ) {
    return cptemplates_integrate_wc_shortcodes()->archive_products();
  }


  /**
   *  get_category_image_id
   *
   *  Retrieves image id of the archive
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_category_image_id( $value ) {
    // Allow 3rd parties to adjust. Particularly it's used with VC Grids DSV replacements in this plugin
    $queried_object = apply_filters( 'cptemplates/dynamic_shortcode_values/replace_value/queried_object', get_queried_object() );

    $thumbnail_id = is_product_category() ? (int)get_woocommerce_term_meta( $queried_object->term_id, 'thumbnail_id', true ) : false;
    return $thumbnail_id ? $thumbnail_id : $value;
  }


  /**
   *  get_category_image_url
   *
   *  Retrieves image URL of the archive
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_category_image_url( $value ) {
    $thumbnail_id = $this->get_category_image_id( false );
    $url = $thumbnail_id ? wp_get_attachment_url( $thumbnail_id ) : false;
    return $url ? $url : $value;
  }


  /* ============================================================ */
  /* SINGLE PRODUCT TEMPLATES
  /* ============================================================ */


  /**
   *  get_template_product_title
   *
   *  Retrieves rendered template for single product title
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_title( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_title();
  }


  /**
   *  get_template_product_price
   *
   *  Retrieves rendered template for single product price
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_price( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_price();
  }


  /**
   *  get_template_product_images
   *
   *  Retrieves rendered template for single product images
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_images( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_images();
  }


  /**
   *  get_template_product_sale_flash
   *
   *  Retrieves rendered template for single product images
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_sale_flash( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_sale_flash();
  }


  /**
   *  get_template_product_thumbnails
   *
   *  Retrieves rendered template for single product thumbnails
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_thumbnails( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_thumbnails();
  }


  /**
   *  get_template_product_excerpt
   *
   *  Retrieves rendered template for single product excerpt
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_excerpt( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_excerpt();
  }


  /**
   *  get_template_product_description
   *
   *  Retrieves rendered template for single product description
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_description( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_description();
  }


  /**
   *  get_template_product_additional_information
   *
   *  Retrieves rendered template for single product additional information
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_additional_information( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_additional_information();
  }


  /**
   *  get_template_product_data_tabs
   *
   *  Retrieves rendered template for single product tabs
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_data_tabs( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_data_tabs();
  }


  /**
   *  get_template_product_rating
   *
   *  Retrieves rendered template for single product rating
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_rating( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_rating();
  }


  /**
   *  get_template_product_reviews
   *
   *  Retrieves rendered template for single product reviews
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_reviews( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_reviews();
  }


  /**
   *  get_template_product_sharing
   *
   *  Retrieves rendered template for single product sharing
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_sharing( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_sharing();
  }


  /**
   *  get_template_product_meta
   *
   *  Retrieves rendered template for single product meta
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_meta( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_meta();
  }


  /**
   *  get_template_product_wishlist
   *
   *  Retrieves rendered template for single product wishlist
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_wishlist( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_wishlist();
  }


  /**
   *  get_template_product_add_to_cart
   *
   *  Retrieves rendered template for single product add to cart
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_add_to_cart( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_add_to_cart();
  }


  /**
   *  get_template_product_related_products
   *
   *  Retrieves rendered template for single product related products
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_related_products( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_related_products();
  }


  /**
   *  get_template_product_upsell
   *
   *  Retrieves rendered template for single product upsell
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_template_product_upsell( $value ) {
    return cptemplates_integrate_wc_shortcodes()->product_upsell();
  }


  /* ============================================================ */
  /* SINGLE PRODUCT DATA
  /* ============================================================ */


  /**
   *  get_product_id
   *
   *  Retrieves post id
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_id( $value ) {
    return $this->get_object_property( 'ID', $value );
  }


  /**
   *  get_product_title
   *
   *  Retrieves post title
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_title( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_title() : $value;
  }


  /**
   *  get_product_description
   *
   *  Retrieves post content
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_description( $value ) {
    $post_content = $this->get_object_property( 'post_content', false );
    return $post_content ? do_shortcode( $post_content ) : $value;
  }


  /**
   *  get_product_short_description
   *
   *  Retrieves product short description
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_short_description( $value ) {
    $post_excerpt = $this->get_object_property( 'post_excerpt', false );
    return $post_excerpt ? do_shortcode( $post_excerpt ) : $value;
  }


  /**
   *  get_product_url
   *
   *  Retrieves product URL
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_url( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    return $post_id ? get_permalink( $post_id ) : $value;
  }


  /**
   *  get_product_sku
   *
   *  Retrieves product sku
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_sku( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_sku() : $value;
  }


  /**
   *  get_product_price
   *
   *  Retrieves product price
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_price( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_price() : $value;
  }


  /**
   *  get_product_price_excluding_tax
   *
   *  Retrieves product price excluding tax
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_price_excluding_tax( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_price_excluding_tax() : $value;
  }


  /**
   *  get_product_price_including_tax
   *
   *  Retrieves product price including tax
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_price_including_tax( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_price_including_tax() : $value;
  }


  /**
   *  get_product_regular_price
   *
   *  Retrieves product regular price
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_regular_price( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_regular_price() : $value;
  }


  /**
   *  get_product_sale_price
   *
   *  Retrieves product sale price
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_sale_price( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_sale_price() : $value;
  }


  /**
   *  get_product_stock_status
   *
   *  Retrieves product stock status
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_stock_status( $value ) {
    return $this->get_object_meta( '_stock_status', $value );
  }


  /**
   *  get_product_availability_text
   *
   *  Retrieves product availability text
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_availability_text( $value ) {
    $product = $this->get_global_wc_product();
    $text = '';

    $availability = is_a( $product, 'WC_Product' ) ? $product->get_availability() : false;

    return is_array( $availability ) && isset( $availability[ 'availability' ] ) ? $availability[ 'availability' ] : $value;
  }


  /**
   *  get_product_availability_class
   *
   *  Retrieves product availability class
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_availability_class( $value ) {
    $product = $this->get_global_wc_product();
    $text = '';

    $availability = is_a( $product, 'WC_Product' ) ? $product->get_availability() : false;

    return is_array( $availability ) && isset( $availability[ 'class' ] ) ? $availability[ 'class' ] : $value;
  }


  /**
   *  get_product_average_rating
   *
   *  Retrieves product average rating
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_average_rating( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_average_rating() : $value;
  }


  /**
   *  get_product_rating_count
   *
   *  Retrieves product rating count
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_rating_count( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_rating_count() : $value;
  }


  /**
   *  get_product_review_count
   *
   *  Retrieves product review count
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_review_count( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_review_count() : $value;
  }


  /**
   *  get_product_weight
   *
   *  Retrieves product weight
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_weight( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_weight() : $value;
  }


  /**
   *  get_product_width
   *
   *  Retrieves product width
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_width( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_width() : $value;
  }


  /**
   *  get_product_height
   *
   *  Retrieves product height
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_height( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_height() : $value;
  }


  /**
   *  get_product_length
   *
   *  Retrieves product length
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_length( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_length() : $value;
  }


  /**
   *  get_product_image_id
   *
   *  Retrieves single product featured image ID
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_image_id( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_image_id() : $value;
  }


  /**
   *  get_product_gallery_image_ids
   *
   *  Retrieves single product gallery image IDs
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_gallery_image_ids( $value ) {
    $product = $this->get_global_wc_product();

    if ( is_a( $product, 'WC_Product' ) ) {
      $ids = method_exists( $product, 'get_gallery_image_ids' ) ? $product->get_gallery_image_ids() : $product->get_gallery_attachment_ids();
      $ids = is_array( $ids ) ? implode( ',', $ids ) : '';
    }
    else {
      $ids = '';
    }

    return $ids;
  }


  /**
   *  get_product_category_ids
   *
   *  Retrieves IDs of the product categories
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_category_ids( $value ) {
    $product = $this->get_global_wc_product();

    if ( is_a( $product, 'WC_Product' ) ) {
      $terms = wp_get_post_terms( $product->get_id(), 'product_cat', array( 'fields' => 'ids' ) );
      $terms = is_array( $terms ) ? implode( ',', $terms ) : '';
    }
    else {
      $terms = '';
    }

    return $terms;
  }


  /**
   *  get_product_category_slugs
   *
   *  Retrieves slugs of the product categories
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_category_slugs( $value ) {
    $product = $this->get_global_wc_product();

    if ( is_a( $product, 'WC_Product' ) ) {
      $terms = wp_get_post_terms( $product->get_id(), 'product_cat', array( 'fields' => 'id=>slug' ) );
      $terms = is_array( $terms ) ? implode( ',', array_values( $terms ) ) : '';
    }
    else {
      $terms = '';
    }

    return $terms;
  }


  /**
   *  get_product_tag_ids
   *
   *  Retrieves IDs of the product tags
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_tag_ids( $value ) {
    $product = $this->get_global_wc_product();

    if ( is_a( $product, 'WC_Product' ) ) {
      $terms = wp_get_post_terms( $product->get_id(), 'product_tag', array( 'fields' => 'ids' ) );
      $terms = is_array( $terms ) ? implode( ',', $terms ) : '';
    }
    else {
      $terms = '';
    }

    return $terms;
  }


  /**
   *  get_product_tag_slugs
   *
   *  Retrieves slugs of the product tags
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_tag_slugs( $value ) {
    $product = $this->get_global_wc_product();

    if ( is_a( $product, 'WC_Product' ) ) {
      $terms = wp_get_post_terms( $product->get_id(), 'product_tag', array( 'fields' => 'id=>slug' ) );
      $terms = is_array( $terms ) ? implode( ',', array_values( $terms ) ) : '';
    }
    else {
      $terms = '';
    }

    return $terms;
  }


  /**
   *  get_product_attribute_ids
   *
   *  Retrieves IDs of the product attributes
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_attribute_ids( $value ) {
    $product = $this->get_global_wc_product();

    if ( is_a( $product, 'WC_Product' ) ) {
      $terms = wp_get_post_terms( $product->get_id(), wc_get_attribute_taxonomy_names(), array( 'fields' => 'ids' ) );
      $terms = is_array( $terms ) ? implode( ',', $terms ) : '';
    }
    else {
      $terms = '';
    }

    return $terms;
  }


  /**
   *  get_product_attribute_slugs
   *
   *  Retrieves slugs of the product attributes
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_attribute_slugs( $value ) {
    $product = $this->get_global_wc_product();

    if ( is_a( $product, 'WC_Product' ) ) {
      $terms = wp_get_post_terms( $product->get_id(), wc_get_attribute_taxonomy_names(), array( 'fields' => 'id=>slug' ) );
      $terms = is_array( $terms ) ? implode( ',', array_values( $terms ) ) : '';
    }
    else {
      $terms = '';
    }

    return $terms;
  }


  /**
   *  product_related_ids
   *
   *  Retrieves IDs of the related products
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_related_ids( $value ) {
    $product = $this->get_global_wc_product();
    $related_product_ids = is_a( $product, 'WC_Product' ) ? $product->get_related( 10 ) : false;
    return $related_product_ids ? implode( ',', $related_product_ids ) : $value;
  }


  /**
   *  get_product_up_sells_ids
   *
   *  Retrieves IDs of the up-sells products
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_up_sells_ids( $value ) {
    $product = $this->get_global_wc_product();
    $related_product_ids = is_a( $product, 'WC_Product' ) ? $product->get_upsells() : false;
    return $related_product_ids ? implode( ',', $related_product_ids ) : $value;
  }


  /**
   *  get_product_cross_sells_ids
   *
   *  Retrieves IDs of the cross-sells products
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_cross_sells_ids( $value ) {
    $product = $this->get_global_wc_product();
    $related_product_ids = is_a( $product, 'WC_Product' ) ? $product->get_cross_sells() : false;
    return $related_product_ids ? implode( ',', $related_product_ids ) : $value;
  }


  /**
   *  get_product_add_to_cart_url
   *
   *  Retrieves product add to cart url
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_add_to_cart_url( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->add_to_cart_url() : $value;
  }


  /**
   *  get_product_single_add_to_cart_text
   *
   *  Retrieves single product add to cart text
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_product_single_add_to_cart_text( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->single_add_to_cart_text() : $value;
  }


  /* ============================================================ */
  /* SINGLE PRODUCT HTML SNIPPETS
  /* ============================================================ */


  /**
   *  get_snippet_product_link
   *
   *  Retrieves product Link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_link( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    $post_title = $this->get_object_property( 'post_title', false );
    $post_permalink = $post_id ? get_permalink( $post_id ) : false;
    return $post_permalink && $post_title ? sprintf( '<a href="%s">%s</a>', $post_permalink, $post_title ) : $value;
  }


  /**
   *  get_snippet_product_price
   *
   *  Retrieves formatted product price
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_price( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_price_html() : $value;
  }


  /**
   *  get_snippet_product_price_excluding_tax
   *
   *  Retrieves formatted regular product price excluding tax
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_price_excluding_tax( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? wc_price( $product->get_price_excluding_tax() ) : $value;
  }


  /**
   *  get_snippet_product_price_including_tax
   *
   *  Retrieves formatted regular product price excluding tax
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_price_including_tax( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? wc_price( $product->get_price_including_tax() ) : $value;
  }


  /**
   *  get_snippet_product_regular_price
   *
   *  Retrieves formatted regular product price
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_regular_price( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? wc_price( $product->get_regular_price() ) : $value;
  }


  /**
   *  get_snippet_product_sale_price
   *
   *  Retrieves formatted sale product price
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_sale_price( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? wc_price( $product->get_sale_price() ) : $value;
  }


  /**
   *  get_snippet_product_stock_status
   *
   *  Retrieves product stock status label
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_stock_status( $value ) {
    $product = $this->get_global_wc_product();

    if ( is_a( $product, 'WC_Product' ) ) {
      return $product->is_in_stock() ? __( 'In stock', 'woocommerce' ) : __( 'Out of stock', 'woocommerce' );
    }
    else {
      return $value;
    }
  }


  /**
   *  get_snippet_product_rating
   *
   *  Retrieves product rating
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_rating( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_rating_html() : $value;
  }


  /**
   *  get_snippet_product_attributes
   *
   *  Retrieves product attributes
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_attributes( $value ) {
    $product = $this->get_global_wc_product();

    if ( is_a( $product, 'WC_Product' ) ) {
      ob_start();
      $product->list_attributes();
      return ob_get_clean();
    }
    else {
      return $value;
    }
  }


  /**
   *  get_snippet_product_dimensions
   *
   *  Retrieves product dimensions
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_dimensions( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_dimensions() : $value;
  }


  /**
   *  get_snippet_product_categories
   *
   *  Retrieves product categories
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_categories( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_categories() : $value;
  }


  /**
   *  get_snippet_product_tags
   *
   *  Retrieves product tags
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_tags( $value ) {
    $product = $this->get_global_wc_product();
    return is_a( $product, 'WC_Product' ) ? $product->get_tags() : $value;
  }


  /**
   *  get_snippet_product_add_to_cart_button
   *
   *  Retrieves product add to cart button
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_snippet_product_add_to_cart_button( $value ) {
    $url = $this->get_product_add_to_cart_url( false );
    $text = $this->get_product_single_add_to_cart_text( false );

    $button = '';
    if ( $url && $text ) {
      $button = sprintf( '<a href="%s" class="single_add_to_cart_button button">%s</a>', $url, $text );
    }

    return $button;
  }


  /* ============================================================ */
  /* PRODUCT LISTS
  /* ============================================================ */


  /**
   *  get_recent_products_ids
   *
   *  Retrieves recent products IDs
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_recent_products_ids( $value ) {
    $atts = shortcode_atts( array(
      'per_page' => '12',
      'columns'  => '4',
      'orderby'  => 'date',
      'order'    => 'desc',
      'category' => '',  // Slugs
      'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
    ), array(), 'recent_products' );

    $query_args = array(
      'post_type'           => 'product',
      'post_status'         => 'publish',
      'ignore_sticky_posts' => 1,
      'posts_per_page'      => $atts[ 'per_page' ],
      'orderby'             => $atts[ 'orderby' ],
      'order'               => $atts[ 'order' ],
      'meta_query'          => WC()->query->get_meta_query()
    );

    // Allow 3rd party to adjust
    $query_args = apply_filters( 'woocommerce_shortcode_products_query', $query_args, $atts, 'recent_products' );

    // We need just IDs
    $query_args[ 'fields' ] = 'ids';
    $products = new WP_Query( $query_args );
    $product_ids = $products->posts;

    return $product_ids ? implode( ',', $product_ids ) : $value;
  }


  /**
   *  get_featured_products_ids
   *
   *  Retrieves featured products IDs
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_featured_products_ids( $value ) {
    $atts = shortcode_atts( array(
      'per_page' => '12',
      'columns'  => '4',
      'orderby'  => 'date',
      'order'    => 'desc',
      'category' => '',  // Slugs
      'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
    ), array(), 'featured_products' );

    $query_args = array(
      'post_type'           => 'product',
      'post_status'         => 'publish',
      'ignore_sticky_posts' => 1,
      'posts_per_page'      => $atts[ 'per_page' ],
      'orderby'             => $atts[ 'orderby' ],
      'order'               => $atts[ 'order' ],
    );

    if ( cptemplates_is_woocommerce_version( '3.0' ) ) {
      $query_args[ 'tax_query' ] = array(
        array(
          'taxonomy' => 'product_visibility',
          'field'    => 'name',
          'terms'    => 'featured',
        )        
      );
    }
    else {
      $meta_query   = WC()->query->get_meta_query();
      $meta_query[] = array(
        'key'   => '_featured',
        'value' => 'yes'
      );
      $query_args[ 'meta_query' ] = $meta_query;
    }

    // Allow 3rd party to adjust
    $query_args = apply_filters( 'woocommerce_shortcode_products_query', $query_args, $atts, 'featured_products' );

    // We need just IDs
    $query_args[ 'fields' ] = 'ids';
    
    $products = new WP_Query( $query_args );
    $product_ids = $products->posts;

    return $product_ids ? implode( ',', $product_ids ) : $value;
  }


  /**
   *  get_sale_products_ids
   *
   *  Retrieves sale products IDs
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_sale_products_ids( $value ) {
    $atts = shortcode_atts( array(
      'per_page' => '12',
      'columns'  => '4',
      'orderby'  => 'title',
      'order'    => 'asc',
      'category' => '', // Slugs
      'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
    ), array(), 'sale_products' );

    $query_args = array(
      'posts_per_page' => $atts[ 'per_page' ],
      'orderby'        => $atts[ 'orderby' ],
      'order'          => $atts[ 'order' ],
      'no_found_rows'  => 1,
      'post_status'    => 'publish',
      'post_type'      => 'product',
      'meta_query'     => WC()->query->get_meta_query(),
      'post__in'       => array_merge( array( 0 ), wc_get_product_ids_on_sale() )
    );

    // Allow 3rd party to adjust
    $query_args = apply_filters( 'woocommerce_shortcode_products_query', $query_args, $atts, 'sale_products' );

    // We need just IDs
    $query_args[ 'fields' ] = 'ids';
    
    $products = new WP_Query( $query_args );
    $product_ids = $products->posts;

    return $product_ids ? implode( ',', $product_ids ) : $value;
  }


  /**
   *  get_best_selling_products_ids
   *
   *  Retrieves best selling products IDs
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_best_selling_products_ids( $value ) {
    $atts = shortcode_atts( array(
      'per_page' => '12',
      'columns'  => '4',
      'category' => '',  // Slugs
      'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
    ), array(), 'best_selling_products' );

    $query_args = array(
      'post_type'           => 'product',
      'post_status'         => 'publish',
      'ignore_sticky_posts' => 1,
      'posts_per_page'      => $atts[ 'per_page' ],
      'meta_key'            => 'total_sales',
      'orderby'             => 'meta_value_num',
      'meta_query'          => WC()->query->get_meta_query()
    );

    // Allow 3rd party to adjust
    $query_args = apply_filters( 'woocommerce_shortcode_products_query', $query_args, $atts, 'best_selling_products' );

    // We need just IDs
    $query_args[ 'fields' ] = 'ids';
    
    $products = new WP_Query( $query_args );
    $product_ids = $products->posts;

    return $product_ids ? implode( ',', $product_ids ) : $value;
  }


  /**
   *  get_top_rated_products_ids
   *
   *  Retrieves top rated products IDs
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_top_rated_products_ids( $value ) {
    $atts = shortcode_atts( array(
      'per_page' => '12',
      'columns'  => '4',
      'orderby'  => 'title',
      'order'    => 'asc',
      'category' => '',  // Slugs
      'operator' => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
    ), array(), 'top_rated_products' );

    $query_args = array(
      'post_type'           => 'product',
      'post_status'         => 'publish',
      'ignore_sticky_posts' => 1,
      'orderby'             => $atts['orderby'],
      'order'               => $atts['order'],
      'posts_per_page'      => $atts['per_page'],
      'meta_query'          => WC()->query->get_meta_query()
    );

    // Allow 3rd party to adjust
    $query_args = apply_filters( 'woocommerce_shortcode_products_query', $query_args, $atts, 'top_rated_products' );

    // We need just IDs
    $query_args[ 'fields' ] = 'ids';
    
    add_filter( 'posts_clauses', array( 'WC_Shortcodes', 'order_by_rating_post_clauses' ) );
    $products = new WP_Query( $query_args );
    remove_filter( 'posts_clauses', array( 'WC_Shortcodes', 'order_by_rating_post_clauses' ) );
    $product_ids = $products->posts;

    return $product_ids ? implode( ',', $product_ids ) : $value;
  }


}


/**
 *  cptemplates_integrate_dsv_plugins_woocommerce
 *
 *  The main function responsible for returning cptemplates_integrate_dsv_plugins_woocommerce object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_dsv_plugins_woocommerce instance
 */

function cptemplates_integrate_dsv_plugins_woocommerce() {
  return cptemplates_integrate_dsv_plugins_woocommerce::instance();
}


// initialize
cptemplates_integrate_dsv_plugins_woocommerce();


endif; // class_exists check

?>