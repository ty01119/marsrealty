<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_dsv_common_user' ) ) :

final class cptemplates_integrate_dsv_common_user {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_dsv_common_user
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    add_filter( 'cptemplates/dynamic_shortcode_values/value_options', array( $this, 'value_options' ) );
    add_filter( "cptemplates/dynamic_shortcode_values/get_user_value", array( $this, 'get_value' ), 10, 2 );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_dsv_common_user instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  value_options
   *
   *  Appends post value options to the list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $value_options
   *  @return  (array)
   */

  public function value_options( $value_options ) {

    // Append options used in Visual Composer
    $value_options[ 'user' ] = array(
      'label' => __( 'User', 'cptemplates' ),
      'type' => 'dropdown',
      'group' => __( 'Common', 'cptemplates' ),
      'choices' => array(

        __( 'Basic', 'cptemplates' ) => array(
          'display_name' => __( 'Display Name', 'cptemplates' ),
          'meta_first_name' => __( 'First Name', 'cptemplates' ),
          'meta_last_name' => __( 'Last Name', 'cptemplates' ),
          'meta_description' => __( 'Description', 'cptemplates' ),
          'email' => __( 'Email', 'cptemplates' ),
          'email_link' => __( 'Email Link', 'cptemplates' ),
          'website' => __( 'Website', 'cptemplates' ),
          'website_link' => __( 'Website Link', 'cptemplates' ),
          'picture' => __( 'Picture', 'cptemplates' ),
          'picture_url' => __( 'Picture URL', 'cptemplates' ),
        ),

        __( 'Advanced', 'cptemplates' ) => array(
          'id' => __( 'ID', 'cptemplates' ),
          'nicename' => __( 'Nicename', 'cptemplates' ),
          'login' => __( 'Login', 'cptemplates' ),
          'status' => __( 'Status', 'cptemplates' ),
          'registered' => __( 'Date Registered', 'cptemplates' ),
        ),
        
        __( 'Admin', 'cptemplates' ) => array(
          'edit_user_url' => __( 'Edit User URL', 'cptemplates' ),
          'edit_user_link' => __( 'Edit User Link', 'cptemplates' ),
        ),

      ),
    );

    return $value_options;
  }


  /**
   *  get_value
   *
   *  Retrieves post post object value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function get_value( $value, $name ) {
    $method = "get_{$name}";
    if ( method_exists( $this, $method ) ) {
      return $this->$method( $value );
    }
    else {
      return $value;
    }
  }


  /**
   *  get_object_property
   *
   *  Retrieves object property if exists
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  private function get_object_property( $prop, $default_value ) {
    $user_object = wp_get_current_user();
    if ( ! $user_object || ! isset( $user_object->data ) || ! is_object( $user_object->data ) ) {
      return $default_value;
    }

    $user_data = $user_object->data;

    return isset( $user_data->$prop ) ? $user_data->$prop : $default_value;
  }


  /**
   *  fallback_user_name
   *
   *  If user name can't be identified, that fallback to this standard name
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  private function fallback_user_name( $prop ) {
    $user_object = wp_get_current_user();
    $role = $user_object->roles ? $user_object->roles[ 0 ] : false;

    if ( ! $role ) {
      $role_name = __( 'Guest', 'cptemplates' );
    }
    else {
      global $wp_roles;
      $role_object = isset( $wp_roles->roles[ $role ] ) ? $wp_roles->roles[ $role ] : false;
      $role_name = $role_object ? translate_user_role( $role_object[ 'name' ] ) : false;
      $role_name = $role_name ? $role_name : $role;
    }

    return apply_filters( 'cptemplates/dynamic_shortcode_values/fallback_user_name', $role_name, $prop );
  }


  /**
   *  get_id
   *
   *  Retrieves user id
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_id( $value ) {
    return $this->get_object_property( 'ID', $value );
  }


  /**
   *  get_nicename
   *
   *  Retrieves user nicename
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_nicename( $value ) {
    return $this->get_object_property( 'user_nicename', $value );
  }


  /**
   *  get_display_name
   *
   *  Retrieves user display_name
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_display_name( $value ) {
    $display_name = $this->get_object_property( 'display_name', $value );
    return $display_name ? $display_name : $this->fallback_user_name( 'display_name' );
  }


  /**
   *  get_email
   *
   *  Retrieves user email
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_email( $value ) {
    return $this->get_object_property( 'user_email', $value );
  }


  /**
   *  get_email_link
   *
   *  Retrieves user email_link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_email_link( $value ) {
    $email = $this->get_email( false );
    return $email ? sprintf( '<a href="mailto:%s">%s</a>', $email, $email ) : $value;
  }


  /**
   *  get_login
   *
   *  Retrieves user login
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_login( $value ) {
    return $this->get_object_property( 'user_login', $value );
  }


  /**
   *  get_status
   *
   *  Retrieves user status
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_status( $value ) {
    return $this->get_object_property( 'user_status', $value );
  }


  /**
   *  get_registered
   *
   *  Retrieves user registered
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_registered( $value ) {
    return mysql2date( get_option( 'date_format' ), $this->get_object_property( 'user_registered', $value ) );
  }


  /**
   *  get_website
   *
   *  Retrieves user website
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_website( $value ) {
    return $this->get_object_property( 'user_url', $value );
  }


  /**
   *  get_website_link
   *
   *  Retrieves user website_link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_website_link( $value ) {
    $website = $this->get_website( false );
    return $website ? sprintf( '<a href="%s" target="_blank">%s</a>', $website, $website ) : $value;
  }


  /**
   *  get_meta_first_name
   *
   *  Retrieves user meta first name
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_meta_first_name( $value ) {
    $user_id = $this->get_object_property( 'ID', false );
    $first_name = $user_id ? get_user_meta( $user_id, 'first_name', true ) : false;
    return $first_name ? $first_name : $value;
  }


  /**
   *  get_meta_last_name
   *
   *  Retrieves user meta last name
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_meta_last_name( $value ) {
    $user_id = $this->get_object_property( 'ID', false );
    $last_name = $user_id ? get_user_meta( $user_id, 'last_name', true ) : false;
    return $last_name ? $last_name : $value;
  }


  /**
   *  get_meta_description
   *
   *  Retrieves user meta description
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_meta_description( $value ) {
    $user_id = $this->get_object_property( 'ID', false );
    $description = $user_id ? get_user_meta( $user_id, 'description', true ) : false;
    return $description ? $description : $value;
  }


  /**
   *  get_edit_user_url
   *
   *  Retrieves user edit URL
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_edit_user_url( $value ) {
    $user_id = $this->get_object_property( 'ID', false );
    $url = $user_id ? get_edit_user_link( $user_id ) : false;
    return $url ? $url : $value;
  }


  /**
   *  get_edit_user_link
   *
   *  Retrieves user edit link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_edit_user_link( $value ) {
    $url = $this->get_edit_user_url( false );
    $display_name = $this->get_display_name( false );
    return $url && $display_name ? sprintf( '<a href="%s">%s</a>', $url, $display_name ) : $value;
  }


  /**
   *  get_picture
   *
   *  Retrieves user picture
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_picture( $value ) {
    $user = wp_get_current_user();
    $avatar = $user ? get_avatar( $user ) : false;
    return $avatar ? $avatar : $value;
  }


  /**
   *  get_picture_url
   *
   *  Retrieves user picture URL
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_picture_url( $value ) {
    $user = wp_get_current_user();
    $url = $user ? get_avatar_url( $user ) : false;
    return $url ? $url : $value;
  }


}


/**
 *  cptemplates_integrate_dsv_common_user
 *
 *  The main function responsible for returning cptemplates_integrate_dsv_common_user object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_dsv_common_user instance
 */

function cptemplates_integrate_dsv_common_user() {
  return cptemplates_integrate_dsv_common_user::instance();
}


// initialize
cptemplates_integrate_dsv_common_user();


endif; // class_exists check

?>