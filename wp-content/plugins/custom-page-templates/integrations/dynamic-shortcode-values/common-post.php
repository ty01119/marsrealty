<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_dsv_common_post' ) ) :

final class cptemplates_integrate_dsv_common_post {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_dsv_common_post
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    add_filter( 'cptemplates/dynamic_shortcode_values/value_options', array( $this, 'value_options' ) );
    add_filter( "cptemplates/dynamic_shortcode_values/get_post_value", array( $this, 'get_value' ), 10, 2 );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_dsv_common_post instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  value_options
   *
   *  Appends post value options to the list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $value_options
   *  @return  (array)
   */

  public function value_options( $value_options ) {

    // Append options used in Visual Composer
    $value_options[ 'post' ] = array(
      'label' => __( 'Post', 'cptemplates' ),
      'type' => 'dropdown',
      'group' => __( 'Common', 'cptemplates' ),
      'choices' => array(

        __( 'Basic', 'cptemplates' ) => array(
          'id' => __( 'ID', 'cptemplates' ),
          'title' => __( 'Title', 'cptemplates' ),
          'content' => __( 'Content', 'cptemplates' ),
          'excerpt' => __( 'Excerpt', 'cptemplates' ),
          'url' => __( 'URL', 'cptemplates' ),
          'link' => __( 'Link', 'cptemplates' ),
          'date' => __( 'Publish Date', 'cptemplates' ),
          'modified' => __( 'Modified Date', 'cptemplates' ),
        ),

        __( 'Advanced', 'cptemplates' ) => array(
          'name' => __( 'Name', 'cptemplates' ),
          'type' => __( 'Type', 'cptemplates' ),
          'format' => __( 'Format', 'cptemplates' ),
          'status' => __( 'Status', 'cptemplates' ),
        ),

        __( 'Featured Image', 'cptemplates' ) => array(
          'thumbnail_image' => __( 'Featured Image', 'cptemplates' ),
          'thumbnail_id' => __( 'Featured Image ID', 'cptemplates' ),
          'thumbnail_url' => __( 'Featured Image URL', 'cptemplates' ),
          'thumbnail_link' => __( 'Featured Image Link', 'cptemplates' ),
        ),

        __( 'Post Author', 'cptemplates' ) => array(
          'author_id' => __( 'Author ID', 'cptemplates' ),
          'author_name' => __( 'Author Name', 'cptemplates' ),
          'author_posts_url' => __( 'Author Posts URL', 'cptemplates' ),
          'author_posts_link' => __( 'Author Posts Link', 'cptemplates' ),
        ),

        __( 'Post Comments', 'cptemplates' ) => array(
          'comments_status' => __( 'Comments Status', 'cptemplates' ),
          'comments_count' => __( 'Comments Count', 'cptemplates' ),
          'comments_url' => __( 'Comments URL', 'cptemplates' ),
          'comments_link' => __( 'Comments Link', 'cptemplates' ),
          'comments_template' => __( 'Comments Template', 'cptemplates' ),
        ),

        __( 'Parent Post', 'cptemplates' ) => array(
          'parent_id' => __( 'Parent ID', 'cptemplates' ),
          'parent_title' => __( 'Parent Title', 'cptemplates' ),
          'parent_url' => __( 'Parent URL', 'cptemplates' ),
          'parent_link' => __( 'Parent Link', 'cptemplates' ),
        ),

        __( 'Next Post', 'cptemplates' ) => array(
          'next_post_id' => __( 'Next Post ID', 'cptemplates' ),
          'next_post_title' => __( 'Next Post Title', 'cptemplates' ),
          'next_post_url' => __( 'Next Post URL', 'cptemplates' ),
          'next_post_link' => __( 'Next Post Link', 'cptemplates' ),
        ),

        __( 'Previous Post', 'cptemplates' ) => array(
          'previous_post_id' => __( 'Previous Post ID', 'cptemplates' ),
          'previous_post_title' => __( 'Previous Post Title', 'cptemplates' ),
          'previous_post_url' => __( 'Previous Post URL', 'cptemplates' ),
          'previous_post_link' => __( 'Previous Post Link', 'cptemplates' ),
        ),

      ),
    );


    // Dynamically compose post terms list
    $taxonomies = get_taxonomies( array( 'public' => true, 'show_ui' => true, 'publicly_queryable' => true ), 'objects' );
    $options = array();
    foreach ( $taxonomies as $taxonomy => $item ) {
      $options[ 'post_terms_' . $taxonomy . '_links' ] = $item->label . ' ' . __( 'Links', 'cptemplates' ) . sprintf( ' (%s)', $taxonomy );
      $options[ 'post_terms_' . $taxonomy . '_ids' ] = $item->label . ' ' . __( 'IDs', 'cptemplates' ) . sprintf( ' (%s)', $taxonomy );
      $options[ 'post_terms_' . $taxonomy . '_slugs' ] = $item->label . ' ' . __( 'Slugs', 'cptemplates' ) . sprintf( ' (%s)', $taxonomy );
    }
    if ( ! empty( $options ) ) {
      $value_options[ 'post' ][ 'choices' ][ __( 'Post Terms', 'cptemplates' ) ] = $options;
    }


    return $value_options;
  }


  /**
   *  __call
   *
   *  Captures post terms method calls
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function __call( $method, $args ) {
    $choice = null;
    $size = null;
    $value = isset( $args[ 0 ] ) ? $args[ 0 ] : null;

    // if ( preg_match( '/get_post_terms_(.*?)(_.*?)?$/i', $method, $matches ) ) {
    if ( preg_match( '/get_post_terms_(.*?)(_(links|ids|slugs))?$/i', $method, $matches ) ) {
      $taxonomy = isset( $matches[ 1 ] ) ? $matches[ 1 ] : null;
      $method_suffix = isset( $matches[ 2 ] ) ? $matches[ 2 ] : null;
    }

    $method = $method_suffix ? "get_post_terms{$method_suffix}" : 'get_post_terms';

    if ( $method && method_exists( $this, $method ) ) {
      return $this->$method( $value, $taxonomy );
    }
    else {
      return $value;
    }

  }


  /**
   *  get_value
   *
   *  Retrieves post post object value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function get_value( $value, $name ) {
    $method = "get_{$name}";
    return $this->$method( $value );
    // if ( method_exists( $this, $method ) ) {
    //   return $this->$method( $value );
    // }
    // else {
    //   return $value;
    // }
  }


  /**
   *  get_object_property
   *
   *  Retrieves object property if exists
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  private function get_object_property( $prop, $default_value ) {
    // Allow 3rd parties to adjust. Particularly it's used with VC Grids DSV replacements in this plugin
    $queried_object = apply_filters( 'cptemplates/dynamic_shortcode_values/replace_value/queried_object', get_queried_object() );

    if ( ! $queried_object ) {
      return $default_value;
    }

    return isset( $queried_object->$prop ) ? $queried_object->$prop : $default_value;
  }


  /**
   *  get_id
   *
   *  Retrieves post id
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_id( $value ) {
    return $this->get_object_property( 'ID', $value );
  }


  /**
   *  get_title
   *
   *  Retrieves post title
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_title( $value ) {
    return $this->get_object_property( 'post_title', $value );
  }


  /**
   *  get_content
   *
   *  Retrieves post content
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_content( $value ) {
    $post_content = $this->get_object_property( 'post_content', false );
    return $post_content ? do_shortcode( $post_content ) : $value;
  }


  /**
   *  get_excerpt
   *
   *  Retrieves post excerpt
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_excerpt( $value ) {
    $post_excerpt = $this->get_object_property( 'post_excerpt', false );
    return $post_excerpt ? do_shortcode( $post_excerpt ) : $value;
  }


  /**
   *  get_url
   *
   *  Retrieves post URL
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_url( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    return $post_id ? get_permalink( $post_id ) : $value;
  }


  /**
   *  get_link
   *
   *  Retrieves post Link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_link( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    $post_title = $this->get_object_property( 'post_title', false );
    $post_permalink = $post_id ? get_permalink( $post_id ) : false;
    return $post_permalink && $post_title ? sprintf( '<a href="%s">%s</a>', $post_permalink, $post_title ) : $value;
  }


  /**
   *  get_thumbnail_image
   *
   *  Retrieves post thumbnail image
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_thumbnail_image( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    $thumbnail = $post_id ? get_the_post_thumbnail( $post_id ) : false;
    return $thumbnail ? $thumbnail : $value;
  }


  /**
   *  get_thumbnail_id
   *
   *  Retrieves post thumbnail id
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_thumbnail_id( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    $thumbnail_id = $post_id ? get_post_thumbnail_id( $post_id ) : false;
    return $thumbnail_id ? $thumbnail_id : $value;
  }


  /**
   *  get_thumbnail_url
   *
   *  Retrieves post thumbnail url
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_thumbnail_url( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    $thumbnail_url = $post_id ? get_the_post_thumbnail_url( $post_id ) : false;
    return $thumbnail_url ? $thumbnail_url : $value;
  }


  /**
   *  get_thumbnail_link
   *
   *  Retrieves post thumbnail link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_thumbnail_link( $value ) {
    $thumbnail_url = $this->get_thumbnail_url( $value );
    $thumbnail_image = $this->get_thumbnail_image( $value );
    return  $thumbnail_url && $thumbnail_image ? sprintf( '<a href="%s">%s</a>', $thumbnail_url, $thumbnail_image ) : $value;
  }


  /**
   *  get_author_id
   *
   *  Retrieves post author id
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_author_id( $value ) {
    return $this->get_object_property( 'post_author', $value );
  }


  /**
   *  get_author_name
   *
   *  Retrieves post author name
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_author_name( $value ) {
    $author_id = $this->get_object_property( 'post_author', false );
    $author_name = $author_id ? get_the_author_meta( 'display_name', $author_id ) : false;
    return $author_name ? $author_name : $value;
  }


  /**
   *  get_author_posts_url
   *
   *  Retrieves post author posts url
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_author_posts_url( $value ) {
    $author_id = $this->get_object_property( 'post_author', false );
    $author_posts_url = $author_id ? get_author_posts_url( $author_id ) : false;
    return $author_posts_url ? $author_posts_url : $value;
  }


  /**
   *  get_author_posts_link
   *
   *  Retrieves post author posts link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_author_posts_link( $value ) {
    $author_id = $this->get_object_property( 'post_author', false );
    $author_posts_url = $author_id ? get_author_posts_url( $author_id ) : false;
    $author_name = $author_id ? get_the_author_meta( 'display_name', $author_id ) : false;
    return $author_posts_url && $author_name ? sprintf( '<a href="%s">%s</a>', $author_posts_url, $author_name ) : $value;
  }


  /**
   *  get_name
   *
   *  Retrieves post name
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_name( $value ) {
    return $this->get_object_property( 'post_name', $value );
  }


  /**
   *  get_type
   *
   *  Retrieves post type
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_type( $value ) {
    return $this->get_object_property( 'post_type', $value );
  }


  /**
   *  get_format
   *
   *  Retrieves post format
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_format( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    $post_format = $post_id ? get_post_format( $post_id ) : false;
    return $post_format ? $post_format : $value;
  }


  /**
   *  get_status
   *
   *  Retrieves post status
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_status( $value ) {
    return $this->get_object_property( 'post_status', $value );
  }


  /**
   *  get_date
   *
   *  Retrieves post date
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_date( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    return $post_id ? get_the_date( '', $post_id ) : $value;
  }


  /**
   *  get_modified
   *
   *  Retrieves post modified date
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_modified( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    return $post_id ? get_the_modified_date( '', $post_id ) : $value;
  }


  /**
   *  get_comments_status
   *
   *  Retrieves post comments status
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_comments_status( $value ) {
    return $this->get_object_property( 'comment_status', $value );
  }


  /**
   *  get_comments_count
   *
   *  Retrieves post comments count
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_comments_count( $value ) {
    return $this->get_object_property( 'comment_count', $value );
  }


  /**
   *  get_comments_url
   *
   *  Retrieves post comments url
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_comments_url( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    return $post_id ? get_comments_link( $post_id ) : $value;
  }


  /**
   *  get_comments_link
   *
   *  Retrieves post comments link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_comments_link( $value ) {
    $comments_url = $this->get_comments_url( false );
    $comments_count = $this->get_comments_count( 0 );
    $comments_text = 1 == (int)$comments_count ? __( 'comment', 'cptemplates' ) : __( 'comments', 'cptemplates' );
    return $comments_url ? sprintf( '<a href="%s">%s %s</a>', $comments_url, $comments_count, $comments_text ) : $value;
  }


  /**
   *  get_comments_template
   *
   *  Renders post comments template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_comments_template( $value ) {
    ob_start();
    comments_template();
    return ob_get_clean();
  }


  /**
   *  get_parent_id
   *
   *  Retrieves post parent id
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_parent_id( $value ) {
    return $this->get_object_property( 'post_parent', $value );
  }


  /**
   *  get_parent_title
   *
   *  Retrieves post parent title
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_parent_title( $value ) {
    $parent_id = $this->get_object_property( 'post_parent', $value );
    return $parent_id ? get_the_title( $parent_id ) : $value;
  }


  /**
   *  get_parent_url
   *
   *  Retrieves post parent URL
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_parent_url( $value ) {
    $parent_id = $this->get_parent_id( false );
    return $parent_id ? get_permalink( $parent_id ) : $value;
  }


  /**
   *  get_parent_link
   *
   *  Retrieves post parent link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_parent_link( $value ) {
    $parent_id = $this->get_parent_id( false );
    $post_title = $parent_id ? get_the_title( $parent_id ) : false;
    $post_permalink = $parent_id ? get_permalink( $parent_id ) : false;
    return $post_permalink && $post_title ? sprintf( '<a href="%s">%s</a>', $post_permalink, $post_title ) : $value;
  }


  /**
   *  get_next_post_id
   *
   *  Retrieves next post ID
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_next_post_id( $value ) {
    $post = get_next_post();
    $post_id = is_a( $post, 'WP_Post' ) ? $post->ID : false;
    return $post_id ? $post_id : $value;
  }


  /**
   *  get_next_post_title
   *
   *  Retrieves next post title
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_next_post_title( $value ) {
    $post = get_next_post();
    $post_id = is_a( $post, 'WP_Post' ) ? $post->ID : false;
    return $post_id ? get_the_title( $post_id ) : $value;
  }


  /**
   *  get_next_post_url
   *
   *  Retrieves next post URL
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_next_post_url( $value ) {
    $post = get_next_post();
    $post_id = is_a( $post, 'WP_Post' ) ? $post->ID : false;
    return $post_id ? get_permalink( $post_id ) : $value;
  }


  /**
   *  get_next_post_link
   *
   *  Retrieves next post link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_next_post_link( $value ) {
    $post = get_next_post();
    $post_id = is_a( $post, 'WP_Post' ) ? $post->ID : false;
    $post_title = $post_id ? get_the_title( $post_id ) : false;
    $post_permalink = $post_id ? get_permalink( $post_id ) : false;
    return $post_permalink && $post_title ? sprintf( '<a href="%s">%s</a>', $post_permalink, $post_title ) : $value;
  }


  /**
   *  get_previous_post_id
   *
   *  Retrieves previous post ID
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_previous_post_id( $value ) {
    $post = get_previous_post();
    $post_id = is_a( $post, 'WP_Post' ) ? $post->ID : false;
    return $post_id ? $post_id : $value;
  }


  /**
   *  get_previous_post_title
   *
   *  Retrieves previous post title
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_previous_post_title( $value ) {
    $post = get_previous_post();
    $post_id = is_a( $post, 'WP_Post' ) ? $post->ID : false;
    return $post_id ? get_the_title( $post_id ) : $value;
  }


  /**
   *  get_previous_post_url
   *
   *  Retrieves previous post URL
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_previous_post_url( $value ) {
    $post = get_previous_post();
    $post_id = is_a( $post, 'WP_Post' ) ? $post->ID : false;
    return $post_id ? get_permalink( $post_id ) : $value;
  }


  /**
   *  get_previous_post_link
   *
   *  Retrieves previous post link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_previous_post_link( $value ) {
    $post = get_previous_post();
    $post_id = is_a( $post, 'WP_Post' ) ? $post->ID : false;
    $post_title = $post_id ? get_the_title( $post_id ) : false;
    $post_permalink = $post_id ? get_permalink( $post_id ) : false;
    return $post_permalink && $post_title ? sprintf( '<a href="%s">%s</a>', $post_permalink, $post_title ) : $value;
  }


  /**
   *  get_post_terms
   *
   *  Retrieves post terms by a given taxonomy
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  private function get_post_terms( $value, $taxonomy ) {
    // Allow 3rd parties to adjust. Particularly it's used with VC Grids DSV replacements in this plugin
    $queried_object = apply_filters( 'cptemplates/dynamic_shortcode_values/replace_value/queried_object', get_queried_object() );
    
    // Make sure we're querying on post page
    if ( ! $queried_object || ! is_a( $queried_object, 'WP_Post' ) ) {
      return $value;
    }
    
    // Make sure we're querying on post
    if ( ! is_a( $queried_object, 'WP_Post' ) ) {
      return $value;
    }

    // Get post terms for a given taxonomy
    $terms = get_the_terms( $queried_object, $taxonomy );

    return is_array( $terms ) || ! empty( $terms ) ? $terms : $value;
  }


  /**
   *  get_post_terms_links
   *
   *  Retrieves post term links by a given taxonomy
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_post_terms_links( $value, $taxonomy ) {
    $terms = $this->get_post_terms( false, $taxonomy );
    
    // Check if result is valid and if there are any terms
    if ( ! $terms ) {
      return $value;
    }

    // Convert terms to term links
    $list = array_map( function( $term ) {
      $term_link = get_term_link( $term->term_id );
      return is_string( $term_link ) ? sprintf( '<a class="cpt-post-terms-link" href="%s">%s</a>', get_term_link( $term->term_id ), $term->name ) : false;
    }, $terms );

    // Remove empty/invalid
    $list = array_filter( $list );

    return ! empty( $list ) ? implode( ', ', $list ) : $value;
  }


  /**
   *  get_post_terms_ids
   *
   *  Retrieves post term ids by a given taxonomy
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_post_terms_ids( $value, $taxonomy ) {
    $terms = $this->get_post_terms( false, $taxonomy );

    // Check if result is valid and if there are any terms
    if ( ! $terms ) {
      return $value;
    }

    // Convert terms to term ids
    $list = wp_list_pluck( $terms, 'term_id' );

    return ! empty( $list ) ? implode( ',', $list ) : $value;
  }


  /**
   *  get_post_terms_slugs
   *
   *  Retrieves post term ids by a given taxonomy
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_post_terms_slugs( $value, $taxonomy ) {
    $terms = $this->get_post_terms( false, $taxonomy );

    // Check if result is valid and if there are any terms
    if ( ! $terms ) {
      return $value;
    }

    // Convert terms to term ids
    $list = wp_list_pluck( $terms, 'slug' );

    return ! empty( $list ) ? implode( ',', $list ) : $value;
  }


}


/**
 *  cptemplates_integrate_dsv_common_post
 *
 *  The main function responsible for returning cptemplates_integrate_dsv_common_post object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_dsv_common_post instance
 */

function cptemplates_integrate_dsv_common_post() {
  return cptemplates_integrate_dsv_common_post::instance();
}


// initialize
cptemplates_integrate_dsv_common_post();


endif; // class_exists check

?>