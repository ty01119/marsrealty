<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_dsv_plugins_acf' ) ) :

final class cptemplates_integrate_dsv_plugins_acf {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_dsv_plugins_acf
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {

    add_filter( 'cptemplates/dynamic_shortcode_values/value_options', array( $this, 'value_options' ) );
    add_filter( 'cptemplates/dynamic_shortcode_values/get_acf_value', array( $this, 'get_value' ), 10, 2 );

    add_action( 'acf/render_field_settings', array( $this, 'acf_render_field_settings' ), 10, 1 );

    if ( apply_filters( 'cptemplates/dynamic_shortcode_values/enqueue_google_maps', true ) ) {
      add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_google_map_assets' ) );
    }
    
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_dsv_plugins_acf instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  acf_render_field_settings
   *
   *  Appends DSV settings to ACF fields
   *
   *  @type  function
   *  @date  07/09/17
   *  @since 1.0.2
   *
   *  @param (array) $field
   *  @return  n/a
   */

  public function acf_render_field_settings( $field ) {

    // DSV Formatting options
    acf_render_field_wrap(array(
      'label'     => __( 'DSV Formatting', 'cptemplates' ),
      'instructions'  => __( 'Choose how Dynamic Shortcode Values should handle value from this field','cptemplates' ),
      'type'      => 'radio',
      'name'      => 'format',
      'layout'    => 'vertical',
      'default_value' => array( 'replacement' ),
      'value'    => isset( $field[ 'dsv_formatting' ][ 'format' ] ) ? $field[ 'dsv_formatting' ][ 'format' ] : '',
      'prefix'    => $field[ 'prefix' ] . '[dsv_formatting]',
      'choices'   => array(
        'display'     => __( 'Format for display', 'cptemplates' ),
        'replacement'     => __( 'Format for replacement', 'cptemplates' ),
      ),
      'wrapper'   => array(
        'data-name' => 'dsv_formatting'
      )
    ), 'tr');
    
    acf_render_field_wrap(array(
      'label'     => '',
      'instructions'  => '',
      'type'      => 'checkbox',
      'name'      => 'disable',
      'layout'    => 'vertical',
      'default_value' => array( '3rd_party' ),
      'value'    => isset( $field[ 'dsv_formatting' ][ 'disable' ] ) ? $field[ 'dsv_formatting' ][ 'disable' ] : '',
      'prefix'    => $field[ 'prefix' ] . '[dsv_formatting]',
      'choices'   => array(
        'standard'      => __( 'No standard formatting', 'cptemplates' ),
        '3rd_party'      => __( 'No 3rd party formatting', 'cptemplates' ),
      ),
      'wrapper'   => array(
        'data-append' => 'dsv_formatting'
      )
    ), 'tr');

  }


  /**
   *  get_acf_dsv_formatting
   *
   *  Safely retrieves dsv formatting settings
   *
   *  @type  function
   *  @date  07/09/17
   *  @since 1.0.2
   *
   *  @param (array) $field_object
   *  @return  string
   */

  public function get_acf_dsv_formatting( $field_object ) {
    $dsv_formatting = is_array( $field_object ) && isset( $field_object[ 'dsv_formatting' ] ) ? $field_object[ 'dsv_formatting' ] : '';
    $dsv_formatting = is_array( $dsv_formatting ) ? $dsv_formatting : array();
    $dsv_formatting = array(
      'format' => isset( $dsv_formatting[ 'format' ] ) ? (array)$dsv_formatting[ 'format' ] : array( 'display' ),
      'disable' => isset( $dsv_formatting[ 'disable' ] ) ? (array)$dsv_formatting[ 'disable' ] : array(),
    );
    return $dsv_formatting;
  }


  /**
   *  value_options
   *
   *  Appends post value options to the list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $value_options
   *  @return  (array)
   */

  public function value_options( $value_options ) {

    if ( ! class_exists( 'acf' ) ) {
      return $value_options;
    }


    // Init
    $choices = array();
    $field_groups = function_exists( 'acf_get_field_groups' ) ? acf_get_field_groups() : apply_filters( 'acf/get_field_groups', array() );
    $ignore_field_types = array( 'message', 'tab' );
    if ( $field_groups ) {
      foreach ( $field_groups as $field_group ) {
        $id = isset( $field_group['id'] ) ? 'id' : ( isset( $field_group[ 'ID' ] ) ? 'ID' : 'id' );
        $field_group_id = $field_group[ $id ];

        // Local fields can't be retrieved
        if ( ! $field_group_id ) {
          continue;
        }

        $group_title = $field_group[ 'title' ];
        
        $group_fields = function_exists( 'acf_get_fields' ) ? acf_get_fields( $field_group_id ) : apply_filters( 'acf/field_group/get_fields', array(), $field_group_id );
        $group_field_options = array();
        if ( $group_fields ) {
          foreach ( $group_fields as $field ) {
            if ( in_array( $field[ 'type' ], $ignore_field_types ) ) {
              continue;
            }
            elseif ( 'clone' == $field[ 'type' ] ) {
              foreach ( $field[ 'sub_fields' ] as $sub_field ) {
                $group_field_options[ $sub_field[ 'name' ] ] = sprintf( '%s (%s)', $sub_field[ 'label' ], $sub_field[ 'type' ] );
              }
            }
            else {
              $group_field_options[ $field[ 'name' ] ] = sprintf( '%s (%s)', $field[ 'label' ], $field[ 'type' ] );
            }
          }
        }

        $choices[ $group_title ] = $group_field_options;
      }
    } 


    // Append options used in Visual Composer
    $value_options[ 'acf' ] = array(
      'label' => __( 'Advanced Custom Fields', 'cptemplates' ),
      'type' => 'dropdown',
      'group' => __( 'Plugins', 'cptemplates' ),
      'choices' => $choices,
    );

    return $value_options;
  }


  /**
   *  get_value
   *
   *  Retrieves post post object value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function get_value( $value, $name ) {
    if ( ! class_exists( 'acf' ) ) {
      return $value;
    }

    // Allow 3rd parties to adjust. Particularly it's used with VC Grids DSV replacements in this plugin
    $queried_object = apply_filters( 'cptemplates/dynamic_shortcode_values/replace_value/queried_object', get_queried_object() );

    if ( ! $queried_object ) {
      return $value;
    }
    // ACF can't get fields properly from term pages if page_id is not set
    elseif ( isset( $queried_object->term_id ) ) {
      $post_id = $queried_object->taxonomy . '_' . $queried_object->term_id;
    }
    // If it's post or page
    elseif ( isset( $queried_object->ID ) && $queried_object->ID ) {
      $post_id = $queried_object->ID; 
    }
    // Unknown case
    else {
      return $value;
    }

    // Checked required
    if ( ! $name ) {
      return $value;
    }

    // Get the field object
    $field_object = get_field_object( $name, $post_id );

    // Verify if field object is valid
    if ( ! $field_object || ! is_array( $field_object ) ) {
      return $value;
    }

    // Get field's value
    $value = $field_object[ 'value' ];
    
    // Format this value
    $value = $this->format_value( $value, $post_id, $field_object );

    return $value;
  }


  /**
   *  get_value
   *
   *  Retrieves post post object value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function format_value( $value, $post_id, $field_object ) {
    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    
    // Apply own formatting
    if ( ! in_array( 'standard', $dsv_formatting[ 'disable' ] ) ) {
      $format_method = sprintf( 'format_%s_value', $field_object[ 'type' ] );
      if ( method_exists( $this, $format_method ) ) {
        $value = $this->$format_method( $value, $post_id, $field_object );
        $value = $value ? $value : null;
      }
    }

    // Allow 3rd party to format
    if ( ! in_array( '3rd_party', $dsv_formatting[ 'disable' ] ) ) {
      $value = apply_filters( sprintf( 'cptemplates/dsv_acf/format_field_type_%s', $field_object[ 'type' ] ), $value, $post_id, $field_object );
      $value = apply_filters( sprintf( 'cptemplates/dsv_acf/format_field_name_%s', $field_object[ 'name' ] ), $value, $post_id, $field_object );
      $value = apply_filters( sprintf( 'cptemplates/dsv_acf/format_field_key_%s', $field_object[ 'key' ] ), $value, $post_id, $field_object );
    }

    return $value;
  }


  /**
   *  format_password_value
   *
   *  Formats password field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_password_value( $value, $post_id, $field_object ) {
    $value = str_repeat( "*", strlen( $value . '' ) );
    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
      $value = empty( $value ) ? null : $value;
    }
    
    return str_repeat( "*", strlen( $value . '' ) );
  }


  /**
   *  format_file_value
   *
   *  Formats file field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_file_value( $value, $post_id, $field_object ) {
    if ( ! $value ) {
      return $value;
    }

    if ( is_array( $value ) ) {
      $value = $field_object[ 'value' ];
      $file_id = $value[ 'id' ];
      $url = $value[ 'url' ];
      $title = $value[ 'title' ];
    }
    elseif ( is_numeric( $value ) ) {
      if ( $post = get_post( $value ) ) {
        $file_id = $value;
        $url = $post->guid;
        $title = $post->post_title;
      }
    }
    else {
      $file_id = $value;
      $url = $value;
      $title = $value;
    }

    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
      $value = sprintf( '<a href="%s" target="_blank">%s</a>', $url, $title );
    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
      $value = isset( $field_object[ 'return_format' ] ) && ( 'url' === $field_object[ 'return_format' ] ) ? $url : $file_id;
      $value = empty( $value ) ? null : $value;
    }

    return $value;
  }


  /**
   *  format_gallery_value
   *
   *  Formats gallery field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_gallery_value( $value, $post_id, $field_object ) {
    if ( ! $value ) {
      return $value;
    }

    if ( is_array( $value ) && ! empty( $value ) ) {
      $value = array_map( function( $item ) {
        return $item[ 'ID' ];
      }, $value );
      $value = implode( ',', $value );
    }

    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
      $value = sprintf( '[gallery ids="%s"]', $value );
      $value = do_shortcode( $value );
    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
      $value = empty( $value ) ? null : $value;
    }

    return $value;
  }


  /**
   *  format_visual_composer_value
   *
   *  Formats visual_composer field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_visual_composer_value( $value, $post_id, $field_object ) {
    if ( ! $value || ! class_exists( 'acf_field_visual_composer' ) ) {
      return $value;
    }

    if ( is_array( $value ) && ! empty( $value ) ) {
      $content = do_shortcode( $value[ 'content' ] );
      $css = $value[ 'css' ] ? sprintf( '<style type="text/css">%s</style>', $value[ 'css' ] ) : '';
      $value = acf_field_visual_composer::wrap_value( $content . $css, $field_object[ 'front_wrapper' ] );
    }

    return $value;
  }


  /**
   *  format_image_value
   *
   *  Formats image field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_image_value( $value, $post_id, $field_object ) {
    if ( ! $value ) {
      return $value;
    }

    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {

      $size = apply_filters( 'cptemplates/dsv_acf/format_field_type_image_size', 'medium', $post_id, $field_object );

      if ( is_array( $value ) ) {
        $value = wp_get_attachment_image( isset( $value[ 'ID' ] ) ? $value[ 'ID' ] : 0, $size );
      }
      elseif ( is_numeric( $value ) ) {
        $value = wp_get_attachment_image( $value, $size );
      }
      else {
        $value = sprintf( '<img src="%s">', $value );
      }

    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {

      if ( is_array( $value ) ) {
        $value = isset( $value[ 'ID' ] ) ? $value[ 'ID' ] : 0;
      }

      $value = empty( $value ) ? null : $value;
    }

    return $value;
  }


  /**
   *  format_select_value
   *
   *  Formats image field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_select_value( $value, $post_id, $field_object ) {
    if ( ! $value ) {
      return $value;
    }

    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
      if ( is_array( $value ) ) {
        $value = sprintf( '%s: %s', $value[ 'label' ], $value[ 'value' ] );
      }
    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
      if ( is_array( $value ) ) {
        $value = $value[ 'value' ];
      }
      $value = empty( $value ) ? null : $value;
    }

    return $value;
  }


  /**
   *  format_radio_value
   *
   *  Formats radio field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_radio_value( $value, $post_id, $field_object ) {
    return $this->format_select_value( $value, $post_id, $field_object );
  }


  /**
   *  format_checkbox_value
   *
   *  Formats checkbox field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_checkbox_value( $value, $post_id, $field_object ) {
    if ( ! $value ) {
      return $value;
    }

    if ( is_array( $value ) ) {

      $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
      if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
        $items = array();
        foreach ( $value as $item ) {
          $items []= is_array( $item ) ? sprintf( '%s: %s', $item[ 'label' ], $item[ 'value' ] ) : $item;
        }
        $value = sprintf( '<ul class="dsv-acf-checkboxes"><li>%s</li></ul>', implode( '</li><li>', $items ) );
      }
      elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
        $items = array();
        foreach ( $value as $item ) {
          $items []= is_array( $item ) ? $item[ 'value' ] : $item;
        }
        $value = implode( ',', $items );
      }

    }




    return $value;
  }


  /**
   *  format_true_false_value
   *
   *  Formats true_false field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_true_false_value( $value, $post_id, $field_object ) {

    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
      $value = $value ? 'yes' : 'no';
    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
      $value = $value ? '1' : null;
    }

    return $value;
  }


  /**
   *  format_post_object_value
   *
   *  Formats post_object field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_post_object_value( $value, $post_id, $field_object ) {
    if ( ! $value ) {
      return $value;
    }

    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
      if ( is_array( $value ) ) {
        $items = array_map( array( $this, 'get_post_link' ), $value );
        return sprintf( '<ul class="dsv-acf-post-links"><li>%s</li></ul>', implode( '</li><li>', $items ) );
      }
      else {
        return $this->get_post_link( $value );
      }
    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
      if ( is_array( $value ) ) {
        $items = array_map( function( $post ) {
          if ( $post && ! is_object( $post ) ) {
            $post = get_post( (int)$post );
          }
          return $post->ID;
        }, $value );
        $value = implode( ',', $items );
      }
      $value = empty( $value ) ? null : $value;
    }

    return $value;
  }


  /**
   *  format_relationship_value
   *
   *  Formats relationship field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_relationship_value( $value, $post_id, $field_object ) {
    return $this->format_post_object_value( $value, $post_id, $field_object );
  }


  /**
   *  get_post_link
   *
   *  Formats relationship field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function get_post_link( $value ) {
    $post = $value;
    if ( $post && ! is_object( $post ) ) {
      $post = get_post( (int)$post );
    }

    if ( is_a( $post, 'WP_Post' ) ) {
      $value = sprintf( '<a href="%s">%s</a>', get_permalink( $post->ID ), $post->post_title );
    }
    
    return $value;
  }


  /**
   *  format_page_link_value
   *
   *  Formats page_link field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_page_link_value( $value, $post_id, $field_object ) {
    if ( ! $value ) {
      return $value;
    }

    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {

      if ( is_array( $value ) ) {
        $items = array_map(function( $item ) {
          return sprintf( '<a href="%s">%s</a>', $item, $item );
        }, $value );

        $value = sprintf( '<ul class="dsv-acf-post-links"><li>%s</li></ul>', implode( '</li><li>', $items ) );
      }
      else {
        $value = sprintf( '<a href="%s" target="_blank">%s</a>', $value, $value );
      }

    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {

      if ( is_array( $value ) ) {
        $value = implode( ',', $value );
      }

      $value = empty( $value ) ? null : $value;
    }

    return $value;    
  }


  /**
   *  format_text_value
   *
   *  Formats text field type value
   *
   *  @type  function
   *  @date  28/10/17
   *  @since 1.0.3
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_text_value( $value, $post_id, $field_object ) {

    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
      $value = empty( $value ) ? null : $value;
    }

    return $value;
  }


  /**
   *  format_textarea_value
   *
   *  Formats textarea field type value
   *
   *  @type  function
   *  @date  12/12/17
   *  @since 2.0.3
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_textarea_value( $value, $post_id, $field_object ) {

    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
      $value = trim( $value );
      $value = empty( $value ) ? null : $value;
    }

    return $value;
  }


  /**
   *  format_email_value
   *
   *  Formats email field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_email_value( $value, $post_id, $field_object ) {

    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
      $value = sprintf( '<a href="mailto:%s">%s</a>', $value, $value );
    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
      $value = empty( $value ) ? null : $value;
    }

    return $value;
  }


  /**
   *  format_url_value
   *
   *  Formats url field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_url_value( $value, $post_id, $field_object ) {
    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
      $value = sprintf( '<a href="%s">%s</a>', $value, $value );
    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
      $value = empty( $value ) ? null : $value;
    }

    return $value;
  }


  /**
   *  format_taxonomy_value
   *
   *  Formats taxonomy field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_taxonomy_value( $value, $post_id, $field_object ) {
    if ( ! $value ) {
      return $value;
    }

    $taxonomy = $field_object[ 'taxonomy' ];

    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {

      if ( is_array( $value ) ) {
        $items = array_map(function( $item ) use ( $taxonomy ) {
          return $this->get_term_link( $item, $taxonomy );
        }, $value );

        return sprintf( '<ul class="dsv-acf-taxonomy-terms"><li>%s</li></ul>', implode( '</li><li>', $items ) );
      }
      else {
        return $this->get_term_link( $value, $taxonomy );
      }

    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {

      if ( is_array( $value ) ) {
        $items = array_map(function( $item ) use ( $taxonomy ) {
          return is_a( $item, 'WP_Term' ) ? $item->term_id : $item;
        }, $value );

        $value = implode( ',', $items );
      }
      else {
        $value = is_a( $value, 'WP_Term' ) ? $value->term_id : $value;
      }

      $value = empty( $value ) ? null : $value;
    }

    return $value;
  }


  /**
   *  get_term_link
   *
   *  Retrieves term link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function get_term_link( $value, $taxonomy = null ) {
    if ( is_object( $value ) ) {
      return sprintf( '<a href="%s">%s</a>', get_term_link( $value->term_id, $value->taxonomy ), $value->name );
    }
    else {
      if ( $term = get_term_by( 'id', (int)$value, $taxonomy ) ) {
        return sprintf( '<a href="%s">%s</a>', get_term_link( $term->term_id, $term->taxonomy ), $term->name );
      }
      else {
        return $value;
      }
    }
  }


  /**
   *  format_user_value
   *
   *  Formats user field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_user_value( $value, $post_id, $field_object ) {
    if ( ! $value ) {
      return $value;
    }


    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
  
      if ( is_array( $value ) && ! isset( $value[ 'ID' ] ) ) {
        $items = array_map(function( $item ) {
          return $this->get_user_link( $item );
        }, $value );

        $value = sprintf( '<ul class="dsv-acf-users"><li>%s</li></ul>', implode( '</li><li>', $items ) );
      }
      else {
        $value = $this->get_user_link( (object)$value );
      }

    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
  
      if ( is_array( $value ) && ! isset( $value[ 'ID' ] ) ) {
        $items = array_map(function( $item ) {
          if ( ! is_object( $item ) && ( ! $item = get_user_by( 'id', (int)$item ) ) ) {
            return false;
          }
          return $item->ID;
        }, $value );
        $items = array_filter( $items );
        $value = implode( ',', $items );
      }
      else {
        $value = (object)$value;
        $value = $value->ID;
      }

      $value = empty( $value ) ? null : $value;
    }

    return $value;
  }


  /**
   *  get_user_link
   *
   *  Retrieve link to user
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function get_user_link( $value ) {
    if ( ! is_object( $value ) && ( ! $value = get_user_by( 'id', (int)$value ) ) ) {
      return '';
    }

    $url = get_author_posts_url( $value->ID );
    $name = $value->user_firstname . ' ' . $value->user_lastname;
    $name = $name ? $name : $value->display_name;
    $name = $name ? $name : $value->nickname;
    $name = $name ? $name : $value->user_nicename;
    return $url && $name ? sprintf( '<a href="%s">%s</a>', $url, $name ) : '';
  }


  /**
   *  format_color_picker_value
   *
   *  Formats color_picker field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_color_picker_value( $value, $post_id, $field_object ) {
    $value = $value ? $value : 'transparent';
    
    $dsv_formatting = $this->get_acf_dsv_formatting( $field_object );
    if ( in_array( 'display', $dsv_formatting[ 'format' ] ) ) {
      $value = sprintf( '<div class="dsv-acf-color-picker" title="%s" style="width:20px;height:20px;border:1px solid #eee;background:%s;"></div>', $value, $value );
    }
    elseif ( in_array( 'replacement', $dsv_formatting[ 'format' ] ) ) {
      $value = empty( $value ) ? null : $value;
    }

    return $value;

  }


  /**
   *  format_repeater_value
   *
   *  Formats repeater field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_repeater_value( $value, $post_id, $field_object ) {
    $sub_fields = array();
    if ( isset( $field_object[ 'sub_fields' ] ) && is_array( $field_object[ 'sub_fields' ] ) && ! empty( $field_object[ 'sub_fields' ] ) ) {
      foreach ( $field_object[ 'sub_fields' ] as $sub_field_object ) {
        $sub_fields[ $sub_field_object[ 'name' ] ] = $sub_field_object;
      }
    }

    if ( $value && is_array( $value ) ) {

      $rows = array();
      foreach ( $value as $key => $item ) {
        $cols = array();
        foreach ( $item as $field_name => $sub_field_value ) {
          $sub_field_object = isset( $sub_fields[ $field_name ] ) ? $sub_fields[ $field_name ] : false;

          if ( $sub_field_object && is_array( $sub_field_object ) ) {
            $sub_field_object[ 'value' ] = $sub_field_value;
            $sub_field_value = $this->format_value( $sub_field_value, $post_id, $sub_field_object );
            $sub_field_value = is_array( $sub_field_value ) || is_object( $sub_field_value ) ? '' : $sub_field_value;
            $cols []= sprintf( '<div class="dsv-acf-repeater-column">%s</div>', $sub_field_value );
          }

        }
        $rows []= sprintf( '<div class="dsv-acf-repeater-row">%s</div>', implode( '', $cols ) );
      }
      $value = sprintf( '<div class="dsv-acf-repeater">%s</div>', implode( '', $rows ) );

    }

    return $value;
  }


  /**
   *  format_flexible_content_value
   *
   *  Formats flexible_content field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_flexible_content_value( $value, $post_id, $field_object ) {
    $layouts = array();
    foreach ( $field_object[ 'layouts' ] as $layout_object ) {
      $layout_name = $layout_object[ 'name' ];
      $layouts[ $layout_name ] = $layout_object;

      $layout_sub_fields = array();
      foreach ( $layout_object[ 'sub_fields' ] as $layout_sub_field_object ) {
        $layout_sub_fields[ $layout_sub_field_object[ 'name' ] ] = $layout_sub_field_object;
      }
      
      $layouts[ $layout_name ][ 'layout_sub_fields' ] = $layout_sub_fields;
    }


    $rows = array();
    foreach ( $value as $key => $item ) {
      $cols = array();
      $layout_object = $layouts[ $item[ 'acf_fc_layout' ] ];
      unset( $item[ 'acf_fc_layout' ] );

      foreach ( $item as $field_name => $sub_field_value ) {

        $sub_field_object = $layout_object[ 'layout_sub_fields' ][ $field_name ];
        $sub_field_object[ 'value' ] = $sub_field_value;

        $sub_field_value = $this->format_value( $sub_field_value, $post_id, $sub_field_object );
        $sub_field_value = is_array( $sub_field_value ) || is_object( $sub_field_value ) ? '' : $sub_field_value;

        $cols []= sprintf( '<div class="dsv-acf-fc-item">%s</div>', $sub_field_value );
      }
      $rows []= sprintf( '<div class="dsv-acf-fc-layout">%s</div>', implode( '', $cols ) );
    }
    $value = sprintf( '<div class="dsv-acf-fc">%s</div>', implode( '', $rows ) );

    return $value;
  }


  /**
   *  format_google_map_value
   *
   *  Formats google_map field type value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  public function format_google_map_value( $value, $post_id, $field_object ) {
    $zoom_level = cptemplates_get_option( 'google_maps_zoom_level', 14 );
    $zoom = apply_filters( 'cptemplates/dynamic_shortcode_values/google_map_zoom', intval( $zoom_level ), $post_id, $field_object );

    if ( isset( $value[ 'lat' ] ) && $value[ 'lng' ] ) {
      $value = sprintf(
        '<div class="dsv-acf-google-map acf-map" data-zoom="%s"><div class="marker" data-lat="%s" data-lng="%s"></div></div>',
        $zoom,
        $value[ 'lat' ],
        $value[ 'lng' ]
      );
    }
   
    return $value;
  }


  /**
   *  enqueue_google_map_assets
   *
   *  Enqueues JS and CSS required to render Google Maps
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (int) $post_id
   *  @param (array) $field_object
   *  @return  (string)
   */

  function enqueue_google_map_assets() {
    if ( ! class_exists( 'acf' ) ) {
      return;
    }

    if ( cptemplates_get_option( 'load_google_maps_js', true ) ) {
      $google_api_key = cptemplates_get_option( 'google_maps_api_key', '' );
      $google_api_key = ! $google_api_key && function_exists( 'acf_get_setting' ) ? acf_get_setting( 'google_api_key' ) : $google_api_key;
      $google_api_key = apply_filters( 'cptemplates/dynamic_shortcode_values/google_maps_api_key', $google_api_key );
      wp_enqueue_script( 'cptemplates-dsv-acf-google-maps-api', 'https://maps.googleapis.com/maps/api/js?key=' . $google_api_key, array(), null, true );
    }
    
    if ( cptemplates_get_option( 'load_google_maps_acf_js', true ) && ! wp_script_is( 'cptemplates-dsv-acf-google-maps', 'enqueued' ) ) {
      wp_enqueue_script( 'cptemplates-dsv-acf-google-maps' );
    }
    if ( cptemplates_get_option( 'load_google_maps_acf_css', true ) && ! wp_style_is( 'cptemplates-dsv-acf-google-maps', 'enqueued' ) ) {
      wp_enqueue_style( 'cptemplates-dsv-acf-google-maps' );
    }
  }


}


/**
 *  cptemplates_integrate_dsv_plugins_acf
 *
 *  The main function responsible for returning cptemplates_integrate_dsv_plugins_acf object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_dsv_plugins_acf instance
 */

function cptemplates_integrate_dsv_plugins_acf() {
  return cptemplates_integrate_dsv_plugins_acf::instance();
}


// initialize
cptemplates_integrate_dsv_plugins_acf();


endif; // class_exists check

?>