<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_dsv_common_wordpress' ) ) :

final class cptemplates_integrate_dsv_common_wordpress {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_dsv_common_wordpress
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    add_filter( 'cptemplates/dynamic_shortcode_values/value_options', array( $this, 'value_options' ) );
    add_filter( "cptemplates/dynamic_shortcode_values/get_wordpress_value", array( $this, 'get_value' ), 10, 2 );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_dsv_common_wordpress instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  value_options
   *
   *  Appends post value options to the list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $value_options
   *  @return  (array)
   */

  public function value_options( $value_options ) {

    // Append options used in Visual Composer
    $value_options[ 'wordpress' ] = array(
      'label' => __( 'WordPress', 'cptemplates' ),
      'type' => 'dropdown',
      'group' => __( 'Common', 'cptemplates' ),
      'choices' => array(

        __( 'Lists', 'cptemplates' ) => array(
          'recent_posts_ids' => __( 'Recent Posts IDs', 'cptemplates' ),
          'recent_posts_ids_except_current' => __( 'Recent Posts IDs Except Current', 'cptemplates' ),
          'main_query_posts_ids' => __( 'Main Query Posts IDs', 'cptemplates' ),
        ),

      ),
    );

    return $value_options;
  }


  /**
   *  get_value
   *
   *  Retrieves post post object value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function get_value( $value, $name ) {
    $method = "get_{$name}";
    if ( method_exists( $this, $method ) ) {
      return $this->$method( $value );
    }
    else {
      return $value;
    }
  }


  /**
   *  get_recent_posts_ids
   *
   *  Retrieves ids of the recent posts
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_recent_posts_ids( $value ) {
    $recent_posts = wp_get_recent_posts( array( 'numberposts' => 20 ) );
    $list = $recent_posts ? wp_list_pluck( $recent_posts, 'ID' ) : false;
    return $list ? implode( ',', $list ) : $value;
  }


  /**
   *  recent_posts_ids_except_current
   *
   *  Retrieves ids of the recent posts except the currently queried post
   *
   *  @type  function
   *  @date  14/09/17
   *  @since 1.0.3
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_recent_posts_ids_except_current( $value ) {
    $recent_posts = wp_get_recent_posts( array( 'numberposts' => 20 ) );
    $list = $recent_posts ? wp_list_pluck( $recent_posts, 'ID' ) : false;
    
    // Allow 3rd parties to adjust. Particularly it's used with VC Grids DSV replacements in this plugin
    $queried_object = apply_filters( 'cptemplates/dynamic_shortcode_values/replace_value/queried_object', get_queried_object() );

    if ( is_a( $queried_object, 'WP_Post' ) ) {
      $list = array_diff( $list, array( $queried_object->ID ) );
    }
    
    return $list ? implode( ',', $list ) : $value;
  }


  /**
   *  get_main_query_posts_ids
   *
   *  Retrieves ids of the currently queried posts
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_main_query_posts_ids( $value ) {
    global $wp_query;
    $posts = $wp_query->posts;
    $list = is_array( $posts ) && ! empty( $posts ) ? wp_list_pluck( $posts, 'ID' ) : false;
    return $list ? implode( ',', $list ) : $value;
  }


}


/**
 *  cptemplates_integrate_dsv_common_wordpress
 *
 *  The main function responsible for returning cptemplates_integrate_dsv_common_wordpress object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_dsv_common_wordpress instance
 */

function cptemplates_integrate_dsv_common_wordpress() {
  return cptemplates_integrate_dsv_common_wordpress::instance();
}


// initialize
cptemplates_integrate_dsv_common_wordpress();


endif; // class_exists check

?>