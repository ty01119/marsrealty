<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_dsv_common_attachment' ) ) :

final class cptemplates_integrate_dsv_common_attachment {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_dsv_common_attachment
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    add_filter( 'cptemplates/dynamic_shortcode_values/value_options', array( $this, 'value_options' ) );
    add_filter( "cptemplates/dynamic_shortcode_values/get_attachment_value", array( $this, 'get_value' ), 10, 2 );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_dsv_common_attachment instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  value_options
   *
   *  Appends post value options to the list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $value_options
   *  @return  (array)
   */

  public function value_options( $value_options ) {

    // Append options used in Visual Composer
    $value_options[ 'attachment' ] = array(
      'label' => __( 'Attachment', 'cptemplates' ),
      'type' => 'dropdown',
      'group' => __( 'Common', 'cptemplates' ),
      'choices' => array(

        __( 'Basic', 'cptemplates' ) => array(
          'id' => __( 'ID', 'cptemplates' ),
          'title' => __( 'Title', 'cptemplates' ),
          'description' => __( 'Description', 'cptemplates' ),
          'caption' => __( 'Caption', 'cptemplates' ),
          'alt_text' => __( 'Alt text', 'cptemplates' ),
          'mime' => __( 'MIME type', 'cptemplates' ),

          'post_url' => __( 'Attachment Post URL', 'cptemplates' ),
          'post_link' => __( 'Attachment Post Link', 'cptemplates' ),
        ),

        __( 'Full size', 'cptemplates' ) => array(
          'url' => __( 'URL', 'cptemplates' ),
          'image' => __( 'Image', 'cptemplates' ),
          'image_link' => __( 'Image Link', 'cptemplates' ),
          'image_width' => __( 'Image Width', 'cptemplates' ),
          'image_height' => __( 'Image Height', 'cptemplates' ),
        ),

      ),
    );


          // All image sizes
    $get_intermediate_image_sizes = get_intermediate_image_sizes();
    foreach ( $get_intermediate_image_sizes as $size ) {
      $options = array(
        'url_size_' . $size => __( 'URL', 'cptemplates' ),
        'image_size_' . $size => __( 'Image', 'cptemplates' ),
        'image_link_size_' . $size => __( 'Image Link', 'cptemplates' ),
        'image_width_size_' . $size => __( 'Image Width', 'cptemplates' ),
        'image_height_size_' . $size => __( 'Image Height', 'cptemplates' ),
      );

      $value_options[ 'attachment' ][ 'choices' ][ __( 'Size ', 'cptemplates' ) . $size ] = $options;
    }

    return $value_options;
  }


  /**
   *  __call
   *
   *  Captures dynamic image size method calls
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function __call( $method, $args ) {
    $choice = null;
    $size = null;
    $value = isset( $args[ 0 ] ) ? $args[ 0 ] : null;

    if ( preg_match( '/get_(.*?)_size_(.*?)$/i', $method, $matches ) ) {
      $choice = isset( $matches[ 1 ] ) ? $matches[ 1 ] : null;
      $size = isset( $matches[ 2 ] ) ? $matches[ 2 ] : null;
    }

    $method = $choice ? "get_custom_size_{$choice}" : null;
    if ( $method && method_exists( $this, $method ) ) {
      return $this->$method( $value, $size );
    }
    else {
      return $value;
    }

  }


  /**
   *  get_value
   *
   *  Retrieves attachment post object value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function get_value( $value, $name ) {
    $method = "get_{$name}";
    return $this->$method( $value );
  }


  /**
   *  get_object_property
   *
   *  Retrieves object property if exists
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  private function get_object_property( $prop, $default_value ) {
    // Allow 3rd parties to adjust. Particularly it's used with VC Grids DSV replacements in this plugin
    $queried_object = apply_filters( 'cptemplates/dynamic_shortcode_values/replace_value/queried_object', get_queried_object() );

    if ( ! $queried_object ) {
      return $default_value;
    }

    return isset( $queried_object->$prop ) ? $queried_object->$prop : $default_value;
  }


  /**
   *  get_id
   *
   *  Retrieves attachment id
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_id( $value ) {
    return $this->get_object_property( 'ID', $value );
  }


  /**
   *  get_title
   *
   *  Retrieves attachment title
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_title( $value ) {
    return $this->get_object_property( 'post_title', $value );
  }


  /**
   *  get_description
   *
   *  Retrieves attachment description
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_description( $value ) {
    $post_content = $this->get_object_property( 'post_content', false );
    return $post_content ? do_shortcode( $post_content ) : $value;
  }


  /**
   *  get_caption
   *
   *  Retrieves attachment caption
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_caption( $value ) {
    $post_excerpt = $this->get_object_property( 'post_excerpt', false );
    return $post_excerpt ? do_shortcode( $post_excerpt ) : $value;
  }


  /**
   *  get_alt_text
   *
   *  Retrieves attachment alt text
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_alt_text( $value ) {
    $post_id = $this->get_id( false );
    return $post_id ? get_post_meta( $post_id, '_wp_attachment_image_alt', true ) : $value;
  }


  /**
   *  get_mime
   *
   *  Retrieves attachment mime type
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_mime( $value ) {
    $post_id = $this->get_id( false );
    return $post_id ? get_post_mime_type( $post_id ) : $value;
  }


  /**
   *  get_post_url
   *
   *  Retrieves attachment post URL
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_post_url( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    return $post_id ? get_permalink( $post_id ) : $value;
  }


  /**
   *  get_post_link
   *
   *  Retrieves attachment post Link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_post_link( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    $post_title = $this->get_object_property( 'post_title', false );
    $post_permalink = $post_id ? get_permalink( $post_id ) : false;
    return $post_permalink && $post_title ? sprintf( '<a href="%s">%s</a>', $post_permalink, $post_title ) : $value;
  }


  /**
   *  get_url
   *
   *  Retrieves attachment URL
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_url( $value ) {
    return $this->get_object_property( 'guid', $value );
  }


  /**
   *  get_image
   *
   *  Retrieves attachment image
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_image( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    $image = wp_get_attachment_image( $post_id, 'full' );
    return $image ? $image : $value;
  }


  /**
   *  get_image_link
   *
   *  Retrieves attachment image link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_image_link( $value ) {
    $url = $this->get_url( false );
    $image = $this->get_image( false );
    return $image && $url ? sprintf( '<a href="%s">%s</a>', $url, $image ) : $value;
  }


  /**
   *  get_image_width
   *
   *  Retrieves attachment image width
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_image_width( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    $metadata = $post_id ? wp_get_attachment_metadata( $post_id ) : false;
    $width = isset( $metadata[ 'width' ] ) && intval( $metadata[ 'width' ] ) ? intval( $metadata[ 'width' ] ) : false;
    return $width ? $width : $value;
  }


  /**
   *  get_image_height
   *
   *  Retrieves attachment image height
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_image_height( $value ) {
    $post_id = $this->get_object_property( 'ID', false );
    $metadata = $post_id ? wp_get_attachment_metadata( $post_id ) : false;
    $height = isset( $metadata[ 'height' ] ) && intval( $metadata[ 'height' ] ) ? intval( $metadata[ 'height' ] ) : false;
    return $height ? $height : $value;
  }


  /**
   *  get_custom_size_url
   *
   *  Retrieves attachment custom size URL
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $size
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_custom_size_url( $value, $size ) {
    $post_id = $this->get_object_property( 'ID', false );
    $image_src = $post_id ? wp_get_attachment_image_src( $post_id, $size ) : false;
    $url = is_array( $image_src ) && isset( $image_src[ 0 ] ) ? $image_src[ 0 ] : false;
    return $url ? $url : $value;
  }


  /**
   *  get_custom_size_image
   *
   *  Retrieves attachment custom size image tag
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $size
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_custom_size_image( $value, $size ) {
    $post_id = $this->get_object_property( 'ID', false );
    $image = wp_get_attachment_image( $post_id, $size );
    return $image ? $image : $value;
  }


  /**
   *  get_custom_size_image_link
   *
   *  Retrieves attachment custom size image tag
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $size
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_custom_size_image_link( $value, $size ) {
    $url = $this->get_custom_size_url( $size, false );
    $image = $this->get_custom_size_image( $size, false );
    return $image && $url ? sprintf( '<a href="%s">%s</a>', $url, $image ) : $value;
  }


  /**
   *  get_custom_size_image_width
   *
   *  Retrieves attachment custom size width
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $size
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_custom_size_image_width( $value, $size ) {
    $post_id = $this->get_object_property( 'ID', false );
    $image_src = $post_id ? wp_get_attachment_image_src( $post_id, $size ) : false;
    $width = is_array( $image_src ) && isset( $image_src[ 1 ] ) ? $image_src[ 1 ] : false;
    return $width ? $width : $value;
  }


  /**
   *  get_custom_size_image_height
   *
   *  Retrieves attachment custom size height
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $size
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_custom_size_image_height( $value, $size ) {
    $post_id = $this->get_object_property( 'ID', false );
    $image_src = $post_id ? wp_get_attachment_image_src( $post_id, $size ) : false;
    $height = is_array( $image_src ) && isset( $image_src[ 2 ] ) ? $image_src[ 2 ] : false;
    return $height ? $height : $value;
  }


}


/**
 *  cptemplates_integrate_dsv_common_attachment
 *
 *  The main function responsible for returning cptemplates_integrate_dsv_common_attachment object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_dsv_common_attachment instance
 */

function cptemplates_integrate_dsv_common_attachment() {
  return cptemplates_integrate_dsv_common_attachment::instance();
}


// initialize
cptemplates_integrate_dsv_common_attachment();


endif; // class_exists check

?>