<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_dsv_common_term' ) ) :

final class cptemplates_integrate_dsv_common_term {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_dsv_common_term
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    add_filter( 'cptemplates/dynamic_shortcode_values/value_options', array( $this, 'value_options' ) );
    add_filter( "cptemplates/dynamic_shortcode_values/get_term_value", array( $this, 'get_value' ), 10, 2 );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_dsv_common_term instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  value_options
   *
   *  Appends post value options to the list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $value_options
   *  @return  (array)
   */

  public function value_options( $value_options ) {

    // Append options used in Visual Composer
    $value_options[ 'term' ] = array(
      'label' => __( 'Term', 'cptemplates' ),
      'type' => 'dropdown',
      'group' => __( 'Common', 'cptemplates' ),
      'choices' => array(

        __( 'Basic', 'cptemplates' ) => array(
          'id' => __( 'ID', 'cptemplates' ),
          'title' => __( 'Title', 'cptemplates' ),
          'description' => __( 'Description', 'cptemplates' ),
          'url' => __( 'URL', 'cptemplates' ),
          'link' => __( 'Link', 'cptemplates' ),
          'count' => __( 'Posts Count', 'cptemplates' ),
        ),

        __( 'Advanced', 'cptemplates' ) => array(
          'slug' => __( 'Slug', 'cptemplates' ),
          'group' => __( 'Group', 'cptemplates' ),
          'taxonomy' => __( 'Taxonomy', 'cptemplates' ),
          'taxonomy_id' => __( 'Term Taxonomy ID', 'cptemplates' ),
        ),

        __( 'Parent Term', 'cptemplates' ) => array(
          'parent_id' => __( 'Parent ID', 'cptemplates' ),
          'parent_title' => __( 'Parent Title', 'cptemplates' ),
          'parent_url' => __( 'Parent URL', 'cptemplates' ),
          'parent_link' => __( 'Parent Link', 'cptemplates' ),
          'parent_slug' => __( 'Parent Slug', 'cptemplates' ),
        ),

      ),
    );

    return $value_options;
  }


  /**
   *  get_value
   *
   *  Retrieves post post object value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function get_value( $value, $name ) {
    $method = "get_{$name}";
    if ( method_exists( $this, $method ) ) {
      return $this->$method( $value );
    }
    else {
      return $value;
    }
  }


  /**
   *  get_object_property
   *
   *  Retrieves object property if exists
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  private function get_object_property( $prop, $default_value ) {
    // Allow 3rd parties to adjust. Particularly it's used with VC Grids DSV replacements in this plugin
    $queried_object = apply_filters( 'cptemplates/dynamic_shortcode_values/replace_value/queried_object', get_queried_object() );

    if ( ! $queried_object ) {
      return $default_value;
    }

    return isset( $queried_object->$prop ) ? $queried_object->$prop : $default_value;
  }


  /**
   *  get_id
   *
   *  Retrieves term id
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_id( $value ) {
    return $this->get_object_property( 'term_id', $value );
  }


  /**
   *  get_title
   *
   *  Retrieves term title
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_title( $value ) {
    return $this->get_object_property( 'name', $value );
  }


  /**
   *  get_description
   *
   *  Retrieves term description
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_description( $value ) {
    $post_content = $this->get_object_property( 'description', false );
    return $post_content ? do_shortcode( $post_content ) : $value;
  }


  /**
   *  get_slug
   *
   *  Retrieves term slug
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_slug( $value ) {
    return $this->get_object_property( 'slug', $value );
  }


  /**
   *  get_group
   *
   *  Retrieves term group
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_group( $value ) {
    return $this->get_object_property( 'term_group', $value );
  }


  /**
   *  get_count
   *
   *  Retrieves term count
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_count( $value ) {
    return $this->get_object_property( 'count', $value );
  }


  /**
   *  get_taxonomy
   *
   *  Retrieves term taxonomy
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_taxonomy( $value ) {
    return $this->get_object_property( 'taxonomy', $value );
  }


  /**
   *  get_taxonomy_id
   *
   *  Retrieves term's term taxonomy id
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_taxonomy_id( $value ) {
    return $this->get_object_property( 'term_taxonomy_id', $value );
  }


  /**
   *  get_url
   *
   *  Retrieves term url
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_url( $value ) {
    $term_id = $this->get_id( false );
    $url = $term_id ? get_term_link( $term_id ) : false;
    return is_string( $url ) ? $url : $value;
  }


  /**
   *  get_link
   *
   *  Retrieves term link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_link( $value ) {
    $title = $this->get_title( false );
    $term_id = $this->get_id( false );
    $url = $term_id ? get_term_link( $term_id ) : false;
    return is_string( $url ) && $title ? sprintf( '<a href="%s">%s</a>', $url, $title ) : $value;
  }


  /**
   *  get_parent_id
   *
   *  Retrieves term parent ID
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_parent_id( $value ) {
    return $this->get_object_property( 'parent', $value );
  }


  /**
   *  get_parent_title
   *
   *  Retrieves term parent title
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_parent_title( $value ) {
    $parent_id = $this->get_parent_id( false );
    $taxonomy = $this->get_taxonomy( false );
    $parent_term = $parent_id && $taxonomy ? get_term_by( 'id', $parent_id, $taxonomy ) : false;
    return is_a( $parent_term, 'WP_Term' ) ? $parent_term->name : $value;
  }


  /**
   *  get_parent_url
   *
   *  Retrieves term parent url
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_parent_url( $value ) {
    $parent_id = $this->get_parent_id( false );
    $url = $parent_id ? get_term_link( $parent_id ) : false;
    return is_string( $url ) ? $url : $value;
  }


  /**
   *  get_parent_link
   *
   *  Retrieves term parent link
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_parent_link( $value ) {
    $parent_id = $this->get_parent_id( false );
    $url = $parent_id ? get_term_link( $parent_id ) : false;
    $title = $this->get_parent_title( false );
    return is_string( $url ) && $title ? sprintf( '<a href="%s">%s</a>', $url, $title ) : $value;
  }


  /**
   *  get_parent_slug
   *
   *  Retrieves term parent slug
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @return  (string)
   */

  public function get_parent_slug( $value ) {
    $parent_id = $this->get_parent_id( false );
    $taxonomy = $this->get_taxonomy( false );
    $parent_term = $parent_id && $taxonomy ? get_term_by( 'id', $parent_id, $taxonomy ) : false;
    return is_a( $parent_term, 'WP_Term' ) ? $parent_term->slug : $value;
  }


}


/**
 *  cptemplates_integrate_dsv_common_term
 *
 *  The main function responsible for returning cptemplates_integrate_dsv_common_term object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_dsv_common_term instance
 */

function cptemplates_integrate_dsv_common_term() {
  return cptemplates_integrate_dsv_common_term::instance();
}


// initialize
cptemplates_integrate_dsv_common_term();


endif; // class_exists check

?>