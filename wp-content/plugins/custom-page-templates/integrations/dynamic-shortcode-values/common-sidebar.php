<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_integrate_dsv_common_sidebar' ) ) :

final class cptemplates_integrate_dsv_common_sidebar {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_integrate_dsv_common_sidebar
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;

    add_filter( 'cptemplates/dynamic_shortcode_values/value_options', array( $this, 'value_options' ) );
    add_filter( "cptemplates/dynamic_shortcode_values/get_sidebar_value", array( $this, 'get_value' ), 10, 2 );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_integrate_dsv_common_sidebar instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  value_options
   *
   *  Appends post value options to the list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $value_options
   *  @return  (array)
   */

  public function value_options( $value_options ) {
    global $wp_registered_sidebars;

    $sidebars = isset( $wp_registered_sidebars ) ? $wp_registered_sidebars : array();
    if ( empty( $sidebars ) ) {
      return $value_options;
    }
    $sidebars = array_map( function( $item ) {
      return $item[ 'name' ];
    }, $sidebars );

    // Append options used in Visual Composer
    $value_options[ 'sidebar' ] = array(
      'label' => __( 'Sidebar', 'cptemplates' ),
      'type' => 'dropdown',
      'group' => __( 'Common', 'cptemplates' ),
      'choices' => array(
        __( 'Available Sidebars', 'cptemplates' ) => $sidebars,
      ),
    );

    return $value_options;
  }


  /**
   *  get_value
   *
   *  Retrieves attachment post object value
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (mixed) $value
   *  @param (mixed) $name
   *  @return  (array)
   */

  public function get_value( $value, $name ) {
    if ( is_active_sidebar( $name ) ) {
      ob_start();
      dynamic_sidebar( $name );
      $result = ob_get_clean();
      $result = trim( $result );
    }
    else {
      $result = '';
    }

    return $result ? $result : $value;
  }


}


/**
 *  cptemplates_integrate_dsv_common_sidebar
 *
 *  The main function responsible for returning cptemplates_integrate_dsv_common_sidebar object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_integrate_dsv_common_sidebar instance
 */

function cptemplates_integrate_dsv_common_sidebar() {
  return cptemplates_integrate_dsv_common_sidebar::instance();
}


// initialize
cptemplates_integrate_dsv_common_sidebar();


endif; // class_exists check

?>