<?php
/*
Plugin Name: Custom Page Templates
Plugin URI: http://custompagetemplates.com
Description: Customize any page template using your favorite page builder.
Version: 3.0.15
Author: Pavlo Reva
Author URI: https://codecanyon.net/user/pavelreva/portfolio/?ref=pavelreva
Copyright: Pavlo Reva
Text Domain: cptemplates
Domain Path: /lang
*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates' ) ) :

final class cptemplates {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Plugin settings
   *
   * @since 1.0.0
   * @var array $settings
   */
  private $settings;
  
  
  /**
   *  __construct
   *
   *  Initialize cptemplates
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    // Settings
    $this->settings = array(
      'version' => '3.0.15',
      'path' => plugin_dir_path( __FILE__ ),
      'url' => plugin_dir_url( __FILE__ ),
    );

    add_action( 'after_setup_theme', array( $this, 'setup' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  setup
   *
   *  Set up all required for plugin to function
   *
   *  @type  function
   *  @date  01/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function setup() {

    // The plugin is dependent on ACF PRO plugin
    // User may use ACF PRO bundled here, utilize own or shipped with a theme
    $active_plugins = (array)get_option( 'active_plugins', array() );
    $this->is_acf_free_active = ( class_exists( 'acf' ) && ! class_exists( 'acf_pro' ) ) || count( preg_grep( '#^advanced\-custom\-fields/acf\.php$#i', $active_plugins ) );
    $this->is_acf_pro_active = class_exists( 'acf_pro' ) || count( preg_grep( '#^advanced\-custom\-fields\-pro(.*?)/acf\.php$#i', $active_plugins ) );


    // Include ACF PRO shipped with this plugin only if no other ACF versions has been initialized
    if ( ! $this->is_acf_free_active && ! $this->is_acf_pro_active ) {

      $instance = $this;

      // Customize ACF path
      add_filter( 'acf/settings/path', function ( $path ) use ( $instance ) {
        return $instance->settings[ 'path' ] . 'vendor/advanced-custom-fields-pro/';
      } );

      // Customize ACF dir
      add_filter( 'acf/settings/dir', function ( $dir ) use ( $instance ) {
        return $instance->settings[ 'url' ] . 'vendor/advanced-custom-fields-pro/';
      } );

      // Include ACF
      include_once( $this->settings[ 'path' ] . 'vendor/advanced-custom-fields-pro/acf.php' );

      $this->is_acf_pro_active = class_exists( 'acf' );
      $this->is_own_acf_included = true;
    }
    else {
      $this->is_own_acf_included = false;
    }


    // Include helper functions
    require_once( $this->settings[ 'path' ] . 'core/helpers.php' );


    // Initialize plugin
    if ( $this->is_acf_pro_active ) {
      add_action( 'acf/init', array( $this, 'init' ), 999 );
    }


    // Admin notices
    if ( is_admin() ) {
      add_action( 'admin_notices', array( $this, 'admin_notices' ) );
    }


    return $this->is_acf_pro_active;
  }


  /**
   *  init
   *
   *  Initialize plugin
   *
   *  @type  function
   *  @date  01/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  (boolean)
   */

  public function init() {

    // Load translations
    load_plugin_textdomain( 'cptemplates', false, '/custom-page-templates/lang' );


    // Disable ACF updates automatic updates if used one included in the plugin
    if ( $this->is_own_acf_included ) {
      acf_update_setting( 'show_updates', false );
    }

    // Include config files
    $this->include_all( 'config' );

    // Include all plugin files
    $this->include_all( 'core' );
    $this->include_all( 'integrations' );

    // admin
    if( is_admin() && cptemplates_is_access_allowed() ) {
      $this->include_all( 'admin' );
    }

    if ( cptemplates_get_option( 'template_frontend_css', true ) ) {
      add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ) );
    }

  }


  /**
   *  admin_notices
   *
   *  Displays admin notices if any
   *
   *  @type  function
   *  @date  01/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function admin_notices() {
    if ( $this->is_acf_free_active ) {
      cptemplates_display_admin_notice(
        sprintf( 'Custom Page Templates plugin requires <a href="https://www.advancedcustomfields.com/pro/" target="_blank">%s</a> to function, which is bundled in the plugin. Please disable your FREE ACF plugin and enjoy the power of PRO version.', __( 'ACF PRO', 'cptemplates' ) ),
        'error',
        true
      );
    }
    elseif ( ! $this->is_acf_pro_active ) {
      cptemplates_display_admin_notice(
        sprintf( 'Something went wrong! <a href="https://www.advancedcustomfields.com/pro/" target="_blank">%s</a> is essential for using Custom Page Templates, but for some unknown reason it was not included. Please <a href="https://codecanyon.net/user/pavelreva/portfolio/?ref=pavelreva" target="_blank">contact author</a>', __( 'ACF PRO', 'cptemplates' ) ),
        'error',
        true
      );
    }
  }


  /**
   *  init
   *
   *  Enqueues required assets
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function enqueue_assets() {
    wp_enqueue_style( 'cptemplates' );
  }


  /**
   *  include_all
   *
   *  Includes all failes within a provided path
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param $path (string)
   *  @return  N/A
   */

  public function include_all( $path = '' ) {
    if ( ! $path ) return;

    $path = trailingslashit( $this->settings[ 'path' ] . $path );
    foreach ( glob( $path . '*.php' ) as $filename ) {
      include $filename;
    }

  }

  
  /**
   *  get_setting
   *
   *  Retrieve plugin's setting. Additionally allows 3rd party customization
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param $name (string) setting name
   *  @param $value (mixed) default value
   *  @return  (mixed)
   */
  
  function get_setting( $name, $value = null ) {
    $value = isset( $this->settings[ $name ] ) ? $this->settings[ $name ] : $value;
    return apply_filters( "cptemplates/get_setting/{$name}", $value );
  }


  /**
   *  update_setting
   *
   *  Updates plugin's setting
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param $name (string) setting name
   *  @param $value (mixed) default value
   *  @return  N/A
   */
  
  function update_setting( $name, $value ) {
    $this->settings[ $name ] = apply_filters( "cptemplates/update_setting/{$name}", $value, $this->settings[ $name ] );
  }


  /**
   *  plugin_activate
   *
   *  Creates table in DB (if needed) when the plugin is activated
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  N/A
   */
  
  public static function plugin_activate() {
    global $wpdb;

    $table_name = $wpdb->prefix . 'cptemplates';

    $collate = '';
    if ( $wpdb->has_cap( 'collation' ) ) {
      if ( ! empty( $wpdb->charset ) ) {
        $collate .= "DEFAULT CHARACTER SET $wpdb->charset";
      }
      if ( ! empty( $wpdb->collate ) ) {
        $collate .= " COLLATE $wpdb->collate";
      }
    }

    $sql = "CREATE TABLE IF NOT EXISTS {$table_name} (
    template_id bigint(20) NOT NULL,
    request varchar(50) NOT NULL,
    priority smallint(2) UNSIGNED NOT NULL,
    conditions mediumblob NOT NULL,
    KEY  (template_id),
    KEY request (request),
    KEY priority (priority)
    ) $collate";

    // Indexes:
    // template_id: used to update rules
    // request: used to find rules on front-end request
    // priority: used to sort found rules
    
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    $result = dbDelta( $sql );
  }
  

}


/**
 *  cptemplates
 *
 *  The main function responsible for returning cptemplates plugin
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates instance
 */

function cptemplates() {
  return cptemplates::instance();
}


// initialize
cptemplates();

// Register plugin's activation hook
register_activation_hook ( __FILE__, array( 'cptemplates', 'plugin_activate' ) );

endif; // class_exists check

?>