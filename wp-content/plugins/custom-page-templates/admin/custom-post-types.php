<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_admin_custom_post_types' ) ) :

final class cptemplates_admin_custom_post_types {


  /**
   * Plugin instance
   *
   *  @since 3.0.0
   *  @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   *  @since 3.0.0
   *  @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_admin_custom_post_types
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  
    add_action( 'save_post_cpt_cpt', array( $this, 'hook_pre_save_post' ), 10, 3 );
    add_action( 'trashed_post', array( $this, 'trashed_post' ) );
    add_action( 'untrashed_post', array( $this, 'untrashed_post' ) );
    add_action( 'delete_post', array( $this, 'delete_post' ) );

    add_filter( 'acf/load_field/key=field_5a76eec02594a', array( $this, 'load_taxonomies_options' ) );

    add_action( 'admin_notices', array( $this, 'admin_notices' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_admin_custom_post_types instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
    
    $this->register_post_type();
  }


  /**
   *  admin_notices
   *
   *  Displays post type update errors
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function admin_notices() {
    global $post;
    
    // Check if we're on cpt edit screen
    $current_screen = get_current_screen();
    if ( ! is_a( $current_screen, 'WP_Screen' ) ) return;
    if ( ! isset( $current_screen->base ) || ( 'post' !== $current_screen->base ) ) return;
    if ( ! isset( $current_screen->id ) || ( 'cpt_cpt' !== $current_screen->id ) ) return;

    // Get errors if any
    $errors = get_post_meta( $post->ID, 'cpt_errors', true );
    if ( ! $errors ) return;

    // Display errors
    foreach ( $errors as $key => $error ) {
      cptemplates_display_admin_notice(
        $error,
        'error',
        true
      );
    }

    // Clean errors list
    delete_post_meta( $post->ID, 'cpt_errors' );
  }


  /**
   *  register_post_type
   *
   *  Register post type for custom post types setup
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function register_post_type() {
    $labels = array(
      'menu_name'             => __( 'Post Types', 'cptemplates' ),
      'name'                  => __( 'Custom Post Types', 'cptemplates' ),
      'singular_name'         => __( 'Custom Post Type', 'cptemplates' ),
      'all_items'             => __( 'Post Types', 'cptemplates' ),
      'add_new_item'          => __( 'Add New Post Type', 'cptemplates' ),
      'new_item'              => __( 'New Post Type', 'cptemplates' ),
      'edit_item'             => __( 'Edit Post Type', 'cptemplates' ),
      'update_item'           => __( 'Update Post Type', 'cptemplates' ),
      'view_item'             => __( 'View Post Type', 'cptemplates' ),
      'search_items'          => __( 'Search Post Types', 'cptemplates' ),
    );
    $args = array(
      'labels'                => $labels,
      'supports'              => array( 'title' ),
      'hierarchical'          => false,
      'public'                => false,
      'show_ui'               => true,
      'show_in_menu'          => 'cptemplates_general',
      'menu_icon'             => '',
      'menu_position'         => 10,
      'show_in_admin_bar'     => false,
      'show_in_nav_menus'     => false,
      'can_export'            => true,
      'has_archive'           => false,   
      'exclude_from_search'   => true,
      'publicly_queryable'    => false,
      'rewrite'               => false,
      'query_var'             => false,
      'capability_type'       => 'post',
    );

    register_post_type( 'cpt_cpt', $args );  
  }



  /**
   *  hook_pre_save_post
   *
   *  This is an intermediary function that hooks to save_post only when cpt_cpt post_type is being saved
   *  mostly for performance and memory usage optimization reason
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int) The post ID.
   *  @param $post (WP_Post) The post object.
   *  @param $update (boolean) Whether this is an existing post being updated or not.
   *  @return  N/A
   */

  public function hook_pre_save_post( $post_id, $post, $update ) {
    // Hook later to have ACF data saved
    add_action( 'save_post', array( $this, 'hook_save_post' ), 20, 3 );
  }


  /**
   *  hook_save_post
   *
   *  Retrieve created post types
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int) The post ID.
   *  @param $post (WP_Post) The post object.
   *  @param $update (boolean) Whether this is an existing post being updated or not.
   *  @return  N/A
   */

  public function hook_save_post( $post_id, $post, $update ) {
    if ( ! $update ) return; // This is to prevent blocking post creation with wp_insert_post from ajax post
    if ( empty( $_POST ) ) return; // Prevent function execution on post deletetion
    if ( 'cpt_cpt' !== $post->post_type ) return; // Run for corresponding post type only
    if ( 'auto-draft' === $post->post_status ) return;

    $fields = get_fields( $post_id );

    $errors = array();
    $args = array();


    // Main Settings: Validate Post type
    $post_type_submitted = isset( $fields[ 'post_type' ] ) ? strval( $fields[ 'post_type' ] ) : '';
    $post_type = preg_replace( '/[^\w\d-_]/i', '_', $post_type_submitted );
    $post_type = substr( $post_type, 0, 20 );
    if ( $post_type != $post_type_submitted ) {
      $errors []= __( 'Post type is invalid', 'cptemplates' );
    }
    $args[ 'post_type' ] = $post_type;

    // Main Settings: Supported features
    $args[ 'supports' ] = isset( $fields[ 'supported_features' ] ) && is_array( $fields[ 'supported_features' ] ) ? $fields[ 'supported_features' ] : array();

    // Main Settings: Other Options
    $other_options = isset( $fields[ 'other_options' ] ) && is_array( $fields[ 'other_options' ] ) ? $fields[ 'other_options' ] : array();
    $args[ 'hierarchical' ] = in_array( 'hierarchical', $other_options );
    $args[ 'can_export' ] = in_array( 'can_export', $other_options );
    $args[ 'delete_with_user' ] = in_array( 'delete_with_user', $other_options );

    // Main Settings: Taxonomies
    $args[ 'taxonomies' ] = isset( $fields[ 'taxonomies' ] ) && is_array( $fields[ 'taxonomies' ] ) ? $fields[ 'taxonomies' ] : array();


    // Accessibility Settings: Accessibility Options
    $accessibility_options = isset( $fields[ 'accessibility_options' ] ) && is_array( $fields[ 'accessibility_options' ] ) ? $fields[ 'accessibility_options' ] : array();
    $args[ 'public' ] = in_array( 'public', $accessibility_options );
    $args[ 'publicly_queryable' ] = in_array( 'publicly_queryable', $accessibility_options );
    $args[ 'show_ui' ] = in_array( 'show_ui', $accessibility_options );
    $args[ 'exclude_from_search' ] = in_array( 'exclude_from_search', $accessibility_options );

    // Accessibility Settings: Capability
    $args[ 'capability_type' ] = isset( $fields[ 'capability_type' ] ) ? strtolower( $fields[ 'capability_type' ] ) : 'post';
    $args[ 'capability_type' ] = 'custom' === $args[ 'capability_type' ] ? ( isset( $fields[ 'custom_capability_type' ] ) ? strtolower( $fields[ 'custom_capability_type' ] ) : 'post' ) : $args[ 'capability_type' ];


    // Menu Settings: Menu Visibility
    $menu_visibility = isset( $fields[ 'menu_visibility' ] ) && is_array( $fields[ 'menu_visibility' ] ) ? $fields[ 'menu_visibility' ] : array();
    $args[ 'show_in_menu' ] = in_array( 'show_in_menu', $menu_visibility );
    $args[ 'show_in_nav_menus' ] = in_array( 'show_in_nav_menus', $menu_visibility );
    $args[ 'show_in_admin_bar' ] = in_array( 'show_in_admin_bar', $menu_visibility );


    // Menu Settings: Menu Position
    $args[ 'menu_position' ] = isset( $fields[ 'menu_position' ] ) ? strval( $fields[ 'menu_position' ] ) : null;
    $args[ 'menu_position' ] = 'custom' === $args[ 'menu_position' ] ? ( isset( $fields[ 'custom_menu_position' ] ) ? intval( $fields[ 'custom_menu_position' ] ) : null ) : $args[ 'menu_position' ];
    // $args[ 'menu_position' ] = null === $args[ 'menu_position' ] ? $args[ 'menu_position' ] : strval( floatval( $args[ 'menu_position' ] ) );
    $args[ 'menu_position' ] = null === $args[ 'menu_position' ] ? $args[ 'menu_position' ] : intval( $args[ 'menu_position' ] );

    // Menu Settings: Menu Icon
    $args[ 'menu_icon' ] = isset( $fields[ 'menu_icon' ] ) && trim( $fields[ 'menu_icon' ] ) ? trim( $fields[ 'menu_icon' ] ) : 'dashicons-admin-post';


    // Frontend Settings: Has archive
    $args[ 'has_archive' ] = isset( $fields[ 'has_archive' ] ) ? strval( $fields[ 'has_archive' ] ) : 'no';
    $args[ 'has_archive' ] = 'custom' === $args[ 'has_archive' ] ? ( isset( $fields[ 'custom_archive_slug' ] ) && trim( $fields[ 'custom_archive_slug' ] ) ? sanitize_title_with_dashes( trim( $fields[ 'custom_archive_slug' ] ) ) : $args[ 'has_archive' ] ) : $args[ 'has_archive' ];
    $args[ 'has_archive' ] = 'no' === $args[ 'has_archive' ] ? false : ( 'yes' === $args[ 'has_archive' ] ? true : $args[ 'has_archive' ] );

    // Frontend Settings: Rewrite
    $args[ 'rewrite' ] = isset( $fields[ 'rewrite' ] ) ? strval( $fields[ 'rewrite' ] ) : 'yes';
    if ( 'custom' === $args[ 'rewrite' ] ) {
      $rewrite_args = array();
      
      // Slug
      $rewrite_args[ 'slug' ] = isset( $fields[ 'custom_url_slug' ] ) ? sanitize_title_with_dashes( $fields[ 'custom_url_slug' ] ) : $post_type;
      
      // Rewrite options
      $rewrite_options = isset( $fields[ 'rewrite_options' ] ) && is_array( $fields[ 'rewrite_options' ] ) ? $fields[ 'rewrite_options' ] : array();
      $rewrite_args[ 'with_front' ] = in_array( 'with_front', $rewrite_options );
      $rewrite_args[ 'feeds' ] = in_array( 'feeds', $rewrite_options );
      $rewrite_args[ 'pages' ] = in_array( 'pages', $rewrite_options );

      $args[ 'rewrite' ] = $rewrite_args;
    }
    else {
      $args[ 'rewrite' ] = 'no' === $args[ 'rewrite' ] ? false : true;
    }

    // Frontend Settings: Query var
    $args[ 'query_var' ] = isset( $fields[ 'query_var' ] ) && trim( $fields[ 'query_var' ] ) ? trim( $fields[ 'query_var' ] ) : $post_type;


    // REST API Settings: Show in REST
    $args[ 'show_in_rest' ] = $fields[ 'show_in_rest' ] ? true : false;

    // REST API Settings: Rest Base
    if ( isset( $fields[ 'rest_base' ] ) && trim( $fields[ 'rest_base' ] ) ) {
      $args[ 'rest_base' ] = $fields[ 'rest_base' ];
    }

    // REST API Settings: Rest Controller Class
    if ( isset( $fields[ 'rest_controller_class' ] ) && trim( $fields[ 'rest_controller_class' ] ) ) {
      $args[ 'rest_controller_class' ] = $fields[ 'rest_controller_class' ];
    }
    

    // Label Settings
    $labels = array();
    $label_keys = array(
      'singular_name', 'menu_name', 'admin_bar_name', 'archives', 'attributes', 
      'parent_item_colon', 'all_items', 'add_new_item', 'add_new', 
      'new_item', 'edit_item', 'update_item', 'view_item', 'view_items',
      'search_items', 'not_found', 'not_found_in_trash', 
      'featured_image', 'set_featured_image', 'remove_featured_image', 'use_featured_image', 
      'insert_into_item', 'uploaded_to_this_item',
      'items_list', 'items_list_navigation', 'filter_items_list',
    );
    foreach ( $label_keys as $key ) {
      $label = isset( $fields[ $key ] ) ? trim( $fields[ $key ] ) : '';
      if ( $label ) {
        $labels[ $key ] = $label;
      }
    }
    $labels[ 'name' ] = $args[ 'label' ] = $post->post_title;
    $args[ 'labels' ] = $labels;


    // Prevent future status and notify user
    if ( 'future' === $post->post_status ) {
      $errors []= __( 'Post type was not updated, because Future status is not supported. Use publish, draft or private', 'cptemplates' );
    }
    elseif ( 'pending' === $post->post_status ) {
      $errors []= __( 'Post type was not updated, because Pending Review status is not supported. Use publish, draft or private', 'cptemplates' );
    }


    // Allow 3rd parties to validate and add own errors
    $errors = apply_filters( 'cptemplates/custom_post_types/admin/save_errors', $errors, $args, $post_id, $post );


    // Save custom post type if there are no errors and post status is publish
    if ( ! $errors ) {

      // Allow 3rd parties to adjust
      $args = apply_filters( 'cptemplates/custom_post_types/admin/save_args', $args, $post_id, $post );

      $this->update_post_type( $post_id, $post->post_status, $args );
    }


    // Save errors if any for notification on page reload
    if ( $errors ) {
      update_post_meta( $post_id, 'cpt_errors', $errors );
    }

  }


  /**
   *  update_post_type
   *
   *  Custom post type args are saved in options table all in a single place
   *  for fast registration.
   *  This function allows updating or deleting custom post type
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int)
   *  @param $args (array|null)
   *  @param $status (string)
   *  @return  N/A
   */

  private function update_post_type( $post_id, $status, $args = null ) {
    $post_types = cptemplates_custom_post_types()->get_post_types();

    if ( 'deleted' === $status ) {
      unset( $post_types[ $post_id ] );
    }
    else {

      // Args could be directly provided or automatically direved from the saved post types
      if ( ! is_array( $args ) ) {
        $args = isset( $post_types[ $post_id ] ) && is_array( $post_types[ $post_id ] ) ? $post_types[ $post_id ] : null;
      }

      // Do not update if args are invalid
      if ( ! $args ) {
        return;
      }

      $args[ 'status' ] = $status;
      $post_types[ $post_id ] = $args;

    }

    cptemplates_custom_post_types()->set_post_types( $post_types );
  }


  /**
   *  trashed_post
   *
   *  Change post type status when moved to trash
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int)
   *
   *  @return  N/A
   */

  public function trashed_post( $post_id ) {
    // Apply for corresponding type only
    if ( 'cpt_cpt' !== get_post_type( $post_id ) ) {
      return;
    }

    // Notify 3rd parties
    do_action( 'cptemplates/custom_post_types/admin/before_trash', $post_id );

    $this->update_post_type( $post_id, 'trash' );
  }


  /**
   *  untrashed_post
   *
   *  Restore post type status when restored from trash
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int)
   *
   *  @return  N/A
   */

  public function untrashed_post( $post_id ) {
    // Apply for corresponding type only
    if ( 'cpt_cpt' !== get_post_type( $post_id ) ) {
      return;
    }

    // Notify 3rd parties
    do_action( 'cptemplates/custom_post_types/admin/before_untrash', $post_id );

    $this->update_post_type( $post_id, get_post_status( $post_id ) );
  }


  /**
   *  delete_post
   *
   *  Finally, delete post type settings when corresponding post has been removed
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int)
   *
   *  @return  N/A
   */

  public function delete_post( $post_id ) {
    // Apply for corresponding type only
    if ( 'cpt_cpt' !== get_post_type( $post_id ) ) {
      return;
    }

    // Notify 3rd parties
    do_action( 'cptemplates/custom_post_types/admin/before_delete', $post_id );

    $this->update_post_type( $post_id, 'deleted' );
  }


  /**
   *  load_taxonomies_options
   *
   *  Dynamically populates taxonomies field's options with available taxonomies
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $field (acf_field)
   *  @return  N/A
   */

  public function load_taxonomies_options( $field ) {
    $field[ 'choices' ] = array();

    $taxonomies = get_taxonomies( array(), 'objects' );
    foreach  ( $taxonomies as $taxonomy => $item ) {
      $field['choices'][ $taxonomy ] = sprintf( '%s (%s)', $item->label, $taxonomy );
    }

    return $field;
  }


}


/**
 *  cptemplates_admin_custom_post_types
 *
 *  The main function responsible for returning cptemplates_admin_custom_post_types object
 *
 *  @type  function
 *  @date  29/08/17
 *  @since 1.0.2
 *
 *  @param N/A
 *  @return (object) cptemplates_admin_custom_post_types instance
 */

function cptemplates_admin_custom_post_types() {
  return cptemplates_admin_custom_post_types::instance();
}


// initialize
cptemplates_admin_custom_post_types();


endif; // class_exists check

?>