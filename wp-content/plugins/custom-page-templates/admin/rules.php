<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_admin_rules' ) ) :

final class cptemplates_admin_rules {

  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_admin_rules
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_admin_rules instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
  }


  /**
   *  preprocess_posted_rules
   *
   *  Performs various operations (sanitize, filter, sort) over submitted custom and template rules
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param  (array) $rules
   *  @param  (string) $type
   *  @return  (array)
   */

  public function preprocess_posted_rules( $rules = array(), $type = '' ) {
    if ( ! $type ) {
      return array();
    }

    // Sanitize
    $rules = array_map( function( $rule ) use ( $type ) {
      // Check required
      if ( ! isset( $rule[ 'status' ] ) || ! isset( $rule[ 'priority' ] ) ) {
        return false;
      }

      // Status can be just 0 or 1
      $rule[ 'status' ] = (int)( (boolean)$rule[ 'status' ] );
      
      // Priority must be > 1 and < 999
      $rule[ 'priority' ] = (int)$rule[ 'priority' ];
      $rule[ 'priority' ] = $rule[ 'priority' ] < 1 ? 1 : $rule[ 'priority' ];
      $rule[ 'priority' ] = $rule[ 'priority' ] > 999 ? 999 : $rule[ 'priority' ];
      
      // Custom specific checks
      if ( 'custom' == $type ) {

        // Template is required for custom rules
        if ( ! isset( $rule[ 'template' ] ) ) {
          return false;
        }

        // Sanitize template
        $rule[ 'template' ] = cptemplates_rules()->sanitize_template( isset( $rule[ 'template' ] ) ? $rule[ 'template' ] : '' );
      }

      return $rule;

    }, (array)$rules );


    // Remove invalid rules
    $rules = array_filter( $rules );


    // Sort by priority
    usort( $rules, function( $a, $b ) {
      $ap = $a[ 'priority' ];
      $bp = $b[ 'priority' ];
      return ( $ap < $bp ) ? -1 : ( ( $ap > $bp ) ? 1 : 0 );
    } );

    return $rules;
  }


  /**
   *  optimize_conditions
   *
   *  Transforms values for faster future processing on front-end
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param  (array) $conditions
   *  @return  (array)
   */

  public function optimize_conditions( $conditions = array() ) {

    $conditions = (array)$conditions;

    // Post terms
    if ( isset( $conditions[ 'post_terms' ] ) ) {

      $conditions[ 'post_terms' ] = array_map( function( $item ) {
        
        // Post terms are saved as {taxonomy}_{term_id}
        $sep_post = strrpos( $item, '_' );
        $taxonomy = substr( $item, 0, $sep_post );
        $term_id = (int)substr( $item, $sep_post + 1 );
        
        return $taxonomy && $term_id ? array( 'taxonomy' => $taxonomy, 'term_id' => $term_id ) : false;

      }, (array)$conditions[ 'post_terms' ] );

      // Remove empty
      $conditions[ 'post_terms' ] = array_filter( $conditions[ 'post_terms' ] );

      // Group by taxonomy to fully utilize wp's has_term function
      $post_terms = array();
      foreach ( $conditions[ 'post_terms' ] as $item ) {
        $taxonomy = $item[ 'taxonomy' ];
        
        if ( ! isset( $post_terms[ $taxonomy ] ) ) {
          $post_terms[ $taxonomy ] = array();
        }

        $post_terms[ $taxonomy ] []= $item[ 'term_id' ];
      }

      $conditions[ 'post_terms' ] = $post_terms;

    }


    // Post formats
    if ( isset( $conditions[ 'post_formats' ] ) ) {
      
      $conditions[ 'post_formats' ] = array_map( function( $item ) {
        
        // Post formats are saved as post_format_{format}
        return str_replace( 'post_format_', '', $item );

      }, (array)$conditions[ 'post_formats' ] );
     
      // Remove empty
      $conditions[ 'post_formats' ] = array_filter( $conditions[ 'post_formats' ] );

    }


    // User conditions
    if ( isset( $conditions[ 'user' ] ) ) {

      $statuses = array();
      $roles = array();
      $other = array();
      foreach ( (array)$conditions[ 'user' ] as $value ) {

        // Logged status
        if ( 'status_logged_in' == $value ) {
          $statuses []= 'logged_in';
        }
        elseif ( 'status_logged_out' == $value ) {
          $statuses []= 'logged_out';
        }
        // Roles
        elseif ( preg_match( '/^role_/i', $value ) ) {
          $roles []= preg_replace( '/^role_/i', '', $value );
        }
        // Unknown user conditions should be added to other
        else {
          $other []= $value;
        }

      }

      $user_conditions = array();
      if ( ! empty( $statuses ) ) {
        $user_conditions[ 'statuses' ] = $statuses;
      }
      if ( ! empty( $roles ) ) {
        $user_conditions[ 'roles' ] = $roles;
      }
      if ( ! empty( $other ) ) {
        $user_conditions[ 'other' ] = $other;
      }
      $conditions[ 'user' ] = $user_conditions;

    }

    // Allow 3rd parties to optimize rule conditions
    $conditions = apply_filters( 'cptemplates/optimize_rule_conditions', $conditions );

    return $conditions;
  }

}


/**
 *  cptemplates_admin_rules
 *
 *  The main function responsible for returning cptemplates_admin_rules object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_admin_rules instance
 */

function cptemplates_admin_rules() {
  return cptemplates_admin_rules::instance();
}


// initialize
cptemplates_admin_rules();


endif; // class_exists check

?>