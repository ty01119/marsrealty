<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_admin_template_file' ) ) :

final class cptemplates_admin_template_file {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_admin_template_file
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_admin_template_file instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
    
    add_action( 'add_meta_boxes', array( $this, 'add_metaboxes' ) );

    // Setup hooks for supported post types
    add_action( "manage_cptemplate_posts_columns", array( $this, 'posts_columns' ) );
    add_action( "manage_cptemplate_posts_custom_column", array( $this, 'posts_custom_column' ), 10, 2 );
    add_action( "save_post_cptemplate", array( $this, 'save_post' ), 10, 3 );
  }


  /**
   *  add_metaboxes
   *
   *  Adds template metabox
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param  N/A
   *  @return  N/A
   */

  public function add_metaboxes() {

    add_meta_box(
      'cptemplates-template-file', // ID
      __( 'Template File', 'cptemplates' ), // Title
      array( $this, 'template_file_metabox' ), // Callback
      'cptemplate', // Screen
      'side', // Context
      'default' // Priority
    );

  }


  /**
   *  template_file_metabox
   *
   *  Renders template file metabox
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param  N/A
   *  @return  N/A
   */

  public function template_file_metabox() {
    global $post;

    // Get current
    $template_file = cptemplates_cptemplate()->get_page_template_file( $post->ID );
    if ( ! $template_file ) {
      $template_file = '';
    }

    // Get all possible options
    $post_templates = $this->get_post_templates();

    // Render dropdown
    echo '<select name="cpt_template_file">';
    foreach ( $post_templates as $key => $item ) {
      
      if ( is_array( $item ) ) {
        printf( '<optgroup label="%s">', $key );
        
        foreach ( $item as $filename => $label ) {
          printf( '<option value="%s" %s>%s</option>', $filename, selected( $template_file, $filename, false ), $label );
        }

        printf( '</optgroup>' );
      }
      else {
        printf( '<option value="%s" %s>%s</option>', $key, selected( $template_file, $key, false ), $item );
      }

    }
    echo '</select>';

  }


  /**
   *  get_post_templates
   *
   *  Get templates for pages and posts in child and parent themes
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param  N/A
   *  @return  N/A
   */

  public function get_post_templates() {
    $wp_theme = wp_get_theme();

    // If you screw up your current theme and we invalidate your parent, most things still work. Let it slide.
    if ( $wp_theme->errors() && $wp_theme->errors()->get_error_codes() !== array( 'theme_parent_invalid' ) ) {
      return array();
    }

    $post_templates = array();
    $post_templates[ '' ] = __( 'Default', 'cptemplates' );

    // Loop through theme files
    $files = (array) $wp_theme->get_files( 'php', 1, true );

    $post_templates = $this->get_post_templates_from_files( $files, $post_templates );

    /*
      Since 2.0.0
      Allow 3rd parties to adjust
    */
    $post_templates = apply_filters( 'cptemplates/post_templates', $post_templates );

    // Translate
    if ( $wp_theme->load_textdomain() ) {
      foreach ( $post_templates as &$post_type ) {
        if ( is_array( $post_type ) ) {
          foreach ( $post_type as &$post_template ) {
            $post_template = translate( $post_template, $wp_theme->get( 'TextDomain' ) );
          }
        }
      }
    }

    return $post_templates;
  }


  /**
   *  get_post_templates_from_files
   *
   *  Parse files to filter suitable post templates
   *
   *  @type  function
   *  @date  03/12/17
   *  @since 2.0.0
   *
   *  @param (array) files
   *  @param (array) post_templates
   *  @return  (array) $post_templates
   */

  public function get_post_templates_from_files( $files = array(), $post_templates = array() ) {
    foreach ( $files as $file => $full_path ) {
      
      if ( ! preg_match( '|CPT\sName:(.*)$|mi', file_get_contents( $full_path ), $header ) ) {
        continue;
      }

      $types = array( 'page' );
      if ( preg_match( '|CPT\sType:(.*)$|mi', file_get_contents( $full_path ), $type ) ) {
        $types = explode( ',', _cleanup_header_comment( $type[1] ) );
      }

      foreach ( $types as $type ) {
        $type = sanitize_key( $type );
        if ( ! isset( $post_templates[ $type ] ) ) {
            $post_templates[ $type ] = array();
        }

        $post_templates[ $type ][ $file ] = _cleanup_header_comment( $header[ 1 ] );
      }
    }

    return $post_templates;
  }


  /**
   *  posts_columns
   *
   *  Adds template column to post columns list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) posts_columns
   *  @return  (array) posts_columns
   */

  public function posts_columns( $posts_columns ) {
    $posts_columns[ 'cpt_template_file' ] = __( 'Template File', 'cptemplates' );
    return $posts_columns;
  }


  /**
   *  posts_custom_column
   *
   *  Renders template column in posts list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) column_name
   *  @param (int) post_id
   *  @return  N/A
   */

  public function posts_custom_column( $column_name, $post_id ) {
    if ( 'cpt_template_file' == $column_name ) {
      $template_file = cptemplates_cptemplate()->get_page_template_file( $post_id );

      if ( $template_file ) {
        echo sprintf( '<span title="%s">%s</span>', $template_file, basename( $template_file ) );
      }
    }
  }


  /**
   *  save_post
   *
   *  Update post template on post update
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (int) post_id
   *  @param (object) post
   *  @param (boolean) update
   *  @return  N/A
   */

  public function save_post( $post_id, $post, $update ) {
    if ( ! $update ) return; // This is to prevent blocking post creation with wp_insert_post from ajax post
    if ( empty( $_POST ) ) return; // Prevent function execution on post deletetion

    cptemplates_cptemplate()->set_page_template_file( $post_id, isset( $_POST[ 'cpt_template_file' ] ) ? $_POST[ 'cpt_template_file' ] : '' );
  }


}


/**
 *  cptemplates_admin_template_file
 *
 *  The main function responsible for returning cptemplates_admin_template_file object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_admin_template_file instance
 */

function cptemplates_admin_template_file() {
  return cptemplates_admin_template_file::instance();
}


// initialize
cptemplates_admin_template_file();


endif; // class_exists check

?>