<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_admin_rule_options' ) ) :

final class cptemplates_admin_rule_options {

  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_admin_rule_options
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_admin_rule_options instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
  }


  /**
   *  get_template_options
   *
   *  Retrieve status options for a rule
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (array)
   */

  public function get_template_options() {
    $options = array();
    $templates = cptemplates_cptemplate()->get_templates();

    foreach ( $templates as $template_id => $template_title ) {
      $options []= array( 'value' => $template_id, 'label' => $template_title );
    }

    return $options;
  }


  /**
   *  get_status_options
   *
   *  Retrieve status options for a rule
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (array)
   */

  public function get_status_options() {
    $options = array(
      array( 'value' => '', 'label' => __( 'Disabled', 'cptemplates' ) ),
      array( 'value' => '1', 'label' => __( 'Enabled', 'cptemplates' ) ),
    );

    return $options;
  }


  /**
   *  get_request_options
   *
   *  Retrieve request options for a rule
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (array)
   */

  public function get_request_options() {
    // Bail early if we have composed the options list already
    if ( isset( $this->request_options ) ) {
      return $this->request_options;
    }

    $options = array();

    $post_types = cptemplates_admin_post()->get_post_types();
    $post_type_objects = array_map( function( $post_type ) {
      return get_post_type_object( $post_type );
    }, $post_types );
    $post_type_objects = array_filter( $post_type_objects );

    // General requests
    $options []= array(
      'label' => __( 'General', 'cptemplates' ),
      'options' => array(
        array( 'value' => 'sitewide', 'label' => __( 'Sitewide', 'cptemplates' ) ), // Since 1.0.6
        array( 'value' => 'front', 'label' => __( 'Front page', 'cptemplates' ) ),
        array( 'value' => 'home', 'label' => __( 'Blog Home', 'cptemplates' ) ),
        array( 'value' => 'error_404', 'label' => __( 'Error 404', 'cptemplates' ) ),
        array( 'value' => 'search', 'label' => __( 'Search', 'cptemplates' ) ),
        array( 'value' => 'date', 'label' => __( 'Date', 'cptemplates' ) ),
        array( 'value' => 'author', 'label' => __( 'Author', 'cptemplates' ) ),
      ),
    );

    // Posts
    $custom_post_type_archive_options = array();
    $post_type_options = array();
    foreach ( $post_type_objects as $post_type_object ) {

      // Post Type
      $post_type_options []= array(
        'value' =>  'post_type_' . $post_type_object->name,
        'label' => sprintf( '%s (%s)', $post_type_object->labels->singular_name, $post_type_object->name )
      );

      // Custom Archives
      if ( $post_type_object->has_archive ) {
        $custom_post_type_archive_options [] = array(
          'value' => 'custom_post_type_archive_' . $post_type_object->name,
          'label' => sprintf( '%s (%s)', $post_type_object->labels->archives, $post_type_object->name )
        );
      }
    }


    // Custom post archives
    if ( ! empty( $custom_post_type_archive_options ) ) {
      $options []= array(
        'label' => __( 'Custom Post Type Archive', 'cptemplates' ),
        'options' => $custom_post_type_archive_options,
      );
    }


    // Post Types
    if ( ! empty( $post_type_options ) ) {
      $options []= array(
        'label' => __( 'Post Type', 'cptemplates' ),
        'options' => $post_type_options,
      );
    }


    // Taxonomies
    $taxonomy_names = cptemplates_admin_term()->get_taxonomies();
    $taxonomy_objects = array_map( function( $taxonomy ) {
      return get_taxonomy( $taxonomy );
    }, $taxonomy_names );
    $taxonomy_objects = array_filter( $taxonomy_objects );

    $taxonomies = array();
    foreach ( $taxonomy_objects as $taxonomy_object ) {
      $taxonomies [] = array(
        'value' => 'taxonomy_' . $taxonomy_object->name,
        'label' => sprintf( '%s (%s)', $taxonomy_object->labels->singular_name, $taxonomy_object->name )
      );
    }

    if ( ! empty( $taxonomies ) ) {
      $options []= array(
        'label' => __( 'Taxonomy Term', 'cptemplates' ),
        'options' => $taxonomies,
      );
    }

    // WooCommerce
    if ( cptemplates_is_woocommerce_plugin_active() ) {
      $options []= array(
        'label' => __( 'WooCommerce', 'cptemplates' ),
        'options' => array(
          array( 'value' => 'wc_shop', 'label' => __( 'Shop', 'cptemplates' ) ),
          array( 'value' => 'wc_cart', 'label' => __( 'Cart', 'cptemplates' ) ),
          array( 'value' => 'wc_checkout', 'label' => __( 'Checkout', 'cptemplates' ) ),
          array( 'value' => 'wc_account_dashboard', 'label' => __( 'My Account: Dashboard', 'cptemplates' ) ),
          array( 'value' => 'wc_account_orders', 'label' => __( 'My Account: Orders', 'cptemplates' ) ),
          array( 'value' => 'wc_account_view_order', 'label' => __( 'My Account: View Order', 'cptemplates' ) ),
          array( 'value' => 'wc_account_downloads', 'label' => __( 'My Account: Downloads', 'cptemplates' ) ),
          array( 'value' => 'wc_account_edit_account', 'label' => __( 'My Account: Edit Account', 'cptemplates' ) ),
          array( 'value' => 'wc_account_edit_address', 'label' => __( 'My Account: Addresses', 'cptemplates' ) ),
          array( 'value' => 'wc_account_payment_methods', 'label' => __( 'My Account: Payment methods', 'cptemplates' ) ),
          array( 'value' => 'wc_account_lost_password', 'label' => __( 'My Account: Lost password', 'cptemplates' ) ),
        ),
      );
    }


    // Allow 3rd parties to adjust the option choices
    $options = apply_filters( 'cptemplates/get_rule_options/request', $options );

    // Save result for faster future calls
    $this->request_options = $options;

    return $options;
  }


  /**
   *  get_post_term_options
   *
   *  Retrieve post terms options for a rule
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (array)
   */

  public function get_post_term_options() {
    // Bail early if we have composed the options list already
    if ( isset( $this->post_term_options ) ) {
      return $this->post_term_options;
    }

    $options = array();
    $post_types = cptemplates_admin_post()->get_post_types();
    $post_type_post_terms = array();

    foreach ( $post_types as $post_type ) {
      $taxonomy_objects = get_object_taxonomies( $post_type, 'objects' );
      if ( ! empty( $taxonomy_objects ) ) {
        foreach ( $taxonomy_objects as $taxonomy => $taxonomy_object ) {
          $terms = cptemplates_get_terms( array(
            'taxonomy' => $taxonomy,
            'hide_empty' => false,
            'fields' => 'id=>name',
          ) );

          $term_options = array();
          foreach ( $terms as $term_id => $term_name ) {
            $term_options []= array( 'value' => $taxonomy . '_' . $term_id, 'label' => $term_name );
          }

          if ( ! empty( $term_options ) ) {
            $post_type_post_terms[ $post_type ] []= array(
              'taxonomy' => $taxonomy,
              'label' => $taxonomy_object->label,
              'terms' => $term_options,
            );
          }
        }

      }
    }


    if ( ! empty( $post_type_post_terms ) ) {
      foreach ( $post_type_post_terms as $post_type => $post_type_options ) {
        foreach ( $post_type_options as $item ) {
          $options []= array(
            'label' => $item[ 'label' ],
            'options' => $item[ 'terms' ],
            'multiple' => true,
            'dependency' => array(
              'request' => 'post_type_' . $post_type
            ),
          );
        }
      }
    }


    // Allow 3rd parties to adjust the option choices
    $options = apply_filters( 'cptemplates/get_rule_options/post_term', $options );

    // Save result for faster future calls
    $this->post_term_options = $options;

    return $options;
  }


  /**
   *  get_post_format_options
   *
   *  Retrieve post format options for a rule
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (array)
   */

  public function get_post_format_options() {
    // Bail early if we have composed the options list already
    if ( isset( $this->post_format_options ) ) {
      return $this->post_format_options;
    }

    $options = array();
    $post_types = cptemplates_admin_post()->get_post_types();
    $post_formats_supported = get_theme_support( 'post-formats' );
    $post_formats_supported = is_array( $post_formats_supported[0] ) ? $post_formats_supported[0] : false;

    $post_type_post_formats = array();
    foreach ( $post_types as $post_type ) {
      if ( $post_formats_supported && post_type_supports( $post_type, 'post-formats' ) ) {
        $post_type_post_formats []= $post_type;
      }
    }

    if ( ! empty( $post_type_post_formats ) ) {
      $post_format_options = array();
      $post_format_options []= array( 'value' => 'post_format_standard', 'label' => __( 'Standard', 'cptemplates' ) );
      foreach ( $post_formats_supported as $post_format ) {
        $post_format_options []= array( 'value' => 'post_format_' . $post_format, 'label' => ucfirst( $post_format ) );
      }

      foreach ( $post_type_post_formats as $post_type ) {
        $options []= array(
          'label' => __( 'Post Format', 'cptemplates' ),
          'options' => $post_format_options,
          'dependency' => array(
            'request' => 'post_type_' . $post_type
          ),
        );
      }
    }

    // Allow 3rd parties to adjust the option choices
    $options = apply_filters( 'cptemplates/get_rule_options/post_format', $options );

    // Save result for faster future calls
    $this->post_format_options = $options;

    return $options;
  }


  /**
   *  get_user_options
   *
   *  Retrieve user options for a rule
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (array)
   */

  public function get_user_options() {
    // Bail early if we have composed the options list already
    if ( isset( $this->user_options ) ) {
      return $this->user_options;
    }

    $options = array();

    // Logged status
    $options []= array(
      'label' => __( 'Logged Status', 'cptemplates' ),
      'options' => array(
        array( 'value' => 'status_logged_in', 'label' => __( 'Logged In', 'cptemplates' ) ),
        array( 'value' => 'status_logged_out', 'label' => __( 'Logged Out', 'cptemplates' ) ),
      ),
    );

    // Roles
    $roles = array();
    foreach ( cptemplates_get_editable_roles() as $role => $role_object ) {
      $roles []= array( 'value' => 'role_' . $role, 'label' => $role_object[ 'name' ] );
    }
    $options []= array(
      'label' => __( 'Roles', 'cptemplates' ),
      'options' => $roles,
    );


    // Allow 3rd parties to adjust the option choices
    $options = apply_filters( 'cptemplates/get_rule_options/user', $options );

    // Save result for faster future calls
    $this->user_options = $options;

    return $options;
  }


}


/**
 *  cptemplates_admin_rule_options
 *
 *  The main function responsible for returning cptemplates_admin_rule_options object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_admin_rule_options instance
 */

function cptemplates_admin_rule_options() {
  return cptemplates_admin_rule_options::instance();
}


// initialize
cptemplates_admin_rule_options();


endif; // class_exists check

?>