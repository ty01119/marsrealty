<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_admin_template_rules' ) ) :

final class cptemplates_admin_template_rules {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_admin_template_rules
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_admin_template_rules instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
    
    add_action( 'add_meta_boxes', array( $this, 'add_metaboxes' ) );

    // Setup hooks to update template rules
    add_action( 'save_post_cptemplate', array( $this, 'save_post' ), 10, 3 );
    add_action( 'trashed_post', array( $this, 'trashed_post' ) );
    add_action( 'before_delete_post', array( $this, 'trashed_post' ) ); // This is required in case if trash is disabled
    add_action( 'untrashed_post', array( $this, 'untrashed_post' ) );

    add_action( 'admin_print_scripts-post-new.php', array( $this, 'enqueue_assets' ) );
    add_action( 'admin_print_scripts-post.php', array( $this, 'enqueue_assets' ) );
  }


  /**
   *  trashed_post
   *
   *  Deletes template rules from DB when template post has been moved to trash or removed permanently
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (int) post_id
   *  @return  N/A
   */

  public function trashed_post( $post_id ) {
    $post_type = get_post_type( $post_id );
    if ( 'cptemplate' != $post_type ) {
      return;
    }

    $this->delete_template_rules( $post_id );
  }


  /**
   *  untrashed_post
   *
   *  Re-creates template rules in DB when template post has been restored from trash
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (int) post_id
   *  @return  N/A
   */

  public function untrashed_post( $post_id ) {
    $post_type = get_post_type( $post_id );
    if ( 'cptemplate' != $post_type ) {
      return;
    }

    $this->create_template_rules( $post_id );
  }


  /**
   *  save_post
   *
   *  Update template rules on post update
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (int) post_id
   *  @param (object) post
   *  @param (boolean) update
   *  @return  N/A
   */

  public function save_post( $post_id, $post, $update ) {
    if ( ! $update ) return; // This is to prevent blocking post creation with wp_insert_post from ajax post
    if ( empty( $_POST ) ) return; // Prevent function execution on post deletetion

    $rules = isset( $_POST[ 'cptemplates_rules' ] ) && is_array( $_POST[ 'cptemplates_rules' ] ) ? $_POST[ 'cptemplates_rules' ] : array();

    // Allow 3rd parties to modify
    $rules = apply_filters( 'cptemplates/save_template_post_rules', $rules, $post_id );

    // Pre-process posted rules
    $rules = cptemplates_admin_rules()->preprocess_posted_rules( $rules, 'template' );

    // Save config in template's postmeta
    update_post_meta( $post_id, 'cptemplates_rules', $rules );

    // Notify 3rd parties
    do_action( 'cptemplates/saved_template_post_rules', $rules, $post_id );

    $this->create_template_rules( $post_id );
  }


  /**
   *  create_template_rules
   *
   *  Creates template rules by template rules config saved in postmeta
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (int) post_id
   *  @return  N/A
   */

  public function create_template_rules( $post_id ) {
    $post_id = (int)$post_id;
    if ( ! $post_id ) {
      return;
    }

    // Remove existing rules by template_id
    $this->delete_template_rules( $post_id );

    // Check template post status
    $post_status = get_post_status( $post_id );

    // Should work just for published templates
    if ( 'publish' != $post_status ) {
      return;
    }

    // Get saved template rules
    $rules = get_post_meta( $post_id, 'cptemplates_rules', true );

    // Check if there are any rules created
    if ( ! $rules || ! is_array( $rules ) ) {
      return;
    }

    // Group rules by request
    $template_rules = array();
    foreach ( $rules as $rule ) {

      // Basic validation
      if ( ! is_array( $rule ) || empty( $rule ) ) {
        continue;
      }

      // Request
      $request = isset( $rule[ 'request' ] ) ? $rule[ 'request' ] : '';
      unset( $rule[ 'request' ] );
      if ( ! $request ) {
        continue;
      }

      if ( ! isset( $template_rules[ $request ] ) ) {
        $template_rules[ $request ] = array();
      }
      $template_rules[ $request ] []= $rule;

    }


    // Allow 3rd parties to modify
    $template_rules = apply_filters( 'cptemplates/create_template_rules', $template_rules, $post_id );


    // Update rules
    foreach ( (array)$template_rules as $request => $rules ) {
      $this->update_request_template_rules( $request, $post_id, $rules );
    }


    // Notify 3rd parties
    do_action( 'cptemplates/created_template_rules', $template_rules, $post_id );

  }


  /**
   *  update_request_template_rules
   *
   *  Updates template rules in DB
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @param (int) template_id
   *  @param (string) request
   *  @param (array) rules
   *  @return  N/A
   */

  public function update_request_template_rules( $request, $template_id, $rules ) {
    $template_id = (int)$template_id;
    if ( ! $template_id || ! is_string( $request ) || ! $request || ! is_array( $rules ) || empty( $rules ) ) {
      return false;
    }

    // Allow 3rd parties to modify
    $rules = apply_filters( 'cptemplates/update_request_template_rules', $rules, $request, $template_id );

    // Sanitize and validate supplied rules
    $new_rules = array();
    foreach ( $rules as $rule ) {

      // Status
      $status = $rule[ 'status' ];
      unset( $rule[ 'status' ] );


      // Priority
      $priority = isset( $rule[ 'priority' ] ) ? (int)$rule[ 'priority' ] : 1;
      unset( $rule[ 'priority' ] );

      // The rest is conditions
      $conditions = ! empty( $rule ) ? $rule : array();

      // Optimize conditions
      $conditions = cptemplates_admin_rules()->optimize_conditions( $conditions );

      // Append to the rules list
      $new_rules []= array(
        'template_id' => $template_id,
        'status' => $status,
        'request' => $request,
        'priority' => $priority,
        'conditions' => serialize( $conditions ),
      );
    
    }


    // Allow 3rd parties to modify
    $new_rules = apply_filters( 'cptemplates/save_request_template_rules', $new_rules );
    

    // Insert new rules
    global $wpdb;
    foreach ( $new_rules as $rule ) {

      // No sense to store disabled rules in DB
      if ( 1 == (int)$rule[ 'status' ] ) {
        unset( $rule[ 'status' ] );
        $wpdb->insert( $wpdb->prefix . 'cptemplates', $rule );
      }

    }

    // Cache rules
    wp_cache_set( 'cptemplates_' . $request, $new_rules, 'cptemplates' );

    // Notify 3rd parties
    do_action( 'cptemplates/saved_request_template_rules', $new_rules, $request, $template_id );
  }


  /**
   *  delete_template_rules
   *
   *  Removes all rules associated with the template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (int) template_id
   *  @return  N/A
   */

  public function delete_template_rules( $template_id ) {
    global $wpdb;
    $wpdb->delete( $wpdb->prefix . 'cptemplates', array(
      'template_id' => $template_id
    ));
  }


  /**
   *  add_metaboxes
   *
   *  Adds template metabox
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @return  N/A
   */

  public function add_metaboxes() {
    add_meta_box(
      'metabox-cptemplates-template-rules', // ID
      __( 'Rules', 'cptemplates' ), // Title
      array( $this, 'metabox_rules' ), // Callback
      'cptemplate', // Screen
      'normal', // Context
      'default' // Priority
    );

  }


  /**
   *  metabox_rules
   *
   *  Renders template rules metabox
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @return  N/A
   */

  public function metabox_rules() {
    include cptemplates_get_path( 'admin/view-templates/select.php' );
    include cptemplates_get_path( 'admin/view-templates/number.php' );
    include cptemplates_get_path( 'admin/view-templates/norules.php' );
    include cptemplates_get_path( 'admin/view-templates/template-rule.php' );
    ?>
    <div id="cptemplates-template-rules" class="cptemplates-rules">
      <?= do_action( 'cptemplates/admin_template_rules/before_rules_list' ) ?>
      <div id="cptemplates-rules-list"></div>
      <?= do_action( 'cptemplates/admin_template_rules/after_rules_list' ) ?>
      <p class="cptemplates-rules-add"><a href="#" class="button button-primary button-large btn-add-rule"><?= __( 'Add New Rule', 'cptemplates' ) ?></a></p>
    </div>
    <?php
  }


  /**
   *  enqueue_assets
   *
   *  Load required JS and CSS
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function enqueue_assets() {
    global $post;

    if ( 'cptemplate' != $post->post_type ) {
      return;
    }

    // Get saved rules
    $rules = get_post_meta( $post->ID, 'cptemplates_rules', true );
    $rules = $rules ? $rules : array();

    // Prepare options
    $options = array(
      'status' => cptemplates_admin_rule_options()->get_status_options(),
      'request' => cptemplates_admin_rule_options()->get_request_options(),
      'post_terms' => cptemplates_admin_rule_options()->get_post_term_options(),
      'post_formats' => cptemplates_admin_rule_options()->get_post_format_options(),
      'user' => cptemplates_admin_rule_options()->get_user_options(),
    );

    
    wp_enqueue_script( 'cptemplates-admin-rules' );
    wp_localize_script(
      'cptemplates-admin-rules',
      'cptemplatesAdminRules',
      array(
        'existingRules' => $rules,
        'options' => $options,
        'i18n' => cptemplates_admin()->get_i18n_strings(),
        'nonce' => wp_create_nonce( 'cptemplates-admin-rules' ),
      )
    );
  }


}


/**
 *  cptemplates_admin_template_rules
 *
 *  The main function responsible for returning cptemplates_admin_template_rules object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_admin_template_rules instance
 */

function cptemplates_admin_template_rules() {
  return cptemplates_admin_template_rules::instance();
}


// initialize
cptemplates_admin_template_rules();


endif; // class_exists check

?>