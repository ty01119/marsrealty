<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_admin_term' ) ) :

final class cptemplates_admin_term {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_admin_term
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    // Init later to allow all taxonomies to be registered
    add_action( 'init', array( $this, 'init' ), 20 );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_admin_term instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param  N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
    
    foreach ( $this->get_taxonomies() as $taxonomy ) {
      add_action( "{$taxonomy}_add_form_fields", array( $this, 'add_form_fields' ) );
      add_action( "{$taxonomy}_edit_form_fields", array( $this, 'edit_form_fields' ), 10, 2 );
      add_action( "create_{$taxonomy}", array( $this, 'save_term_template' ), 10, 2 );
      add_action( "edit_{$taxonomy}", array( $this, 'save_term_template' ), 10, 2 ); // We need just first 2 params
    }

    add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_assets' ) );
  }


  /**
   *  add_form_fields
   *
   *  Renders template field in add term form
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) taxonomy slug
   *  @return  N/A
   */

  public function add_form_fields( $taxonomy ) {
    ?>
    <div class="form-field term-parent-wrap">
      <label for="parent"><?= __( 'Custom Template', 'cptemplates' ) ?></label>
      <?php cptemplates_admin_custom_rules()->render(); ?>
      <p><?= __( 'Custom page template allows to overwrite the default layout for a term.', 'cptemplates' ) ?></p>
    </div>  
    <?php  
  }


  /**
   *  edit_form_fields
   *
   *  Renders template field in edit term form
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (object) term object
   *  @param (string) taxonomy slug
   *  @return  N/A
   */

  public function edit_form_fields( $term , $taxonomy ) {
    ?>
    <tr class="form-field term-parent-wrap">
      <th scope="row"><label for="cptemplate"><?= __( 'Custom Template', 'cptemplates' ) ?></label></th>
      <td>
        <?php cptemplates_admin_custom_rules()->render(); ?>
        <p class="description"><?= __( 'Custom page template allows to overwrite the default layout for the term.', 'cptemplates' ) ?></p>
      </td>
    </tr>
    <?php  
  }


  /**
   *  save_term_template
   *
   *  Saves template for the term when created or updated
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (int) term_id
   *  @param (int) tt_id
   *  @return  N/A
   */

  public function save_term_template( $term_id, $tt_id ) {
    cptemplates_admin_custom_rules()->save_posted_rules( $term_id, 'term' );
  }


  /**
   *  get_taxonomies
   *
   *  Retrieve taxonomies eligible for having a template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param  N/A
   *  @return  (array) taxonomies
   */

  public function get_taxonomies() {
    if ( isset( $this->taxonomies ) ) {
      return $this->taxonomies;
    }
    else {
      $taxonomy_objects = get_taxonomies( array( 'public' => true, 'show_ui' => true, 'publicly_queryable' => true ), 'objects' );

      $taxonomy_objects = array_filter( $taxonomy_objects, function( $item ) {
        return $item->rewrite;
      } );

      $this->taxonomies = array_keys( $taxonomy_objects );

      // Allow 3rd party plugins to customize this list
      $this->taxonomies = apply_filters( 'cptemplates/get_taxonomies', $this->taxonomies );

      return $this->taxonomies;
    }
  }


  /**
   *  enqueue_assets
   *
   *  Load required JS and CSS on term pages
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function enqueue_assets( $hook_suffix = '' ) {

    // Only on create new and edit term pages
    if ( in_array( $hook_suffix, array( 'term.php', 'edit-tags.php' ) ) ) {

      // Only for the supported taxonomies
      $taxonomy = isset( $_GET[ 'taxonomy' ] ) ? $_GET[ 'taxonomy' ] : '';
      if ( in_array( $taxonomy, $this->get_taxonomies() ) ) {

        $term_id = isset( $_GET[ 'tag_ID' ] ) ? (int)$_GET[ 'tag_ID' ] : 0;

        // Get saved rules
        $rules = cptemplates_rules()->get_custom_rules( $term_id, 'term', $optimized = false );

        // Enqueue
        cptemplates_admin_custom_rules()->enqueue_assets( $rules );
      }

    }

  }

}


/**
 *  cptemplates_admin_term
 *
 *  The main function responsible for returning cptemplates_admin_term object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_admin_term instance
 */

function cptemplates_admin_term() {
  return cptemplates_admin_term::instance();
}


// initialize
cptemplates_admin_term();


endif; // class_exists check

?>