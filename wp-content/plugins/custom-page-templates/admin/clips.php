<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_admin_clips' ) ) :

final class cptemplates_admin_clips {


  /**
   * Plugin instance
   *
   * @since 3.0.5
   * @var object $instance
   */
  protected static $instance;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_admin_clips
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 3.0.5
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    if ( cptemplates_is_visualcomposer_plugin_active() && cptemplates_get_option( 'enable_clips', true ) ) {
      add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_assets' ) );
    }
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 3.0.5
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_admin_clips instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  enqueue_assets
   *
   *  Load required JS and CSS for clips
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 3.0.5
   *
   *  @param N/A
   *  @return  N/A
   */

  public function enqueue_assets() {
    // Run only in Backend Editor where Visual Composer is allowed by role settings
    if ( ! cptemplates_vc_is_inline() && vc_check_post_type( get_post_type() ) && ! vc_is_frontend_editor() && ! wp_script_is( 'cptemplates-clips', 'enqueued' ) ) {
      wp_enqueue_script( 'cptemplates-clips' );
      wp_localize_script( 'cptemplates-clips', 'cptClips', array(
        'recent_clips_limit' => cptemplates_get_option( 'recent_clips_limit', 50 ),
        'i18n' => array(
          'Paste from clipboard' => __( 'Paste from clipboard', 'cptemplates' ),
          'Copy to clipboard' => __( 'Copy to clipboard', 'cptemplates' ),
          'Copied' => __( 'Copied', 'cptemplates' ),
          'Import' => __( 'Import', 'cptemplates' ),
          'Recent' => __( 'Recent', 'cptemplates' ),
          'Favorites' => __( 'Favorites', 'cptemplates' ),
          'Paste exported clip code here' => __( 'Paste exported clip code here', 'cptemplates' ),
          'Clips' => __( 'Clips', 'cptemplates' ),
          'Remove' => __( 'Remove', 'cptemplates' ),
          'Dismiss' => __( 'Dismiss', 'cptemplates' ),
          'Export' => __( 'Export', 'cptemplates' ),
          'Add to favorites' => __( 'Add to favorites', 'cptemplates' ),
          'Rename' => __( 'Rename', 'cptemplates' ),
          'Update' => __( 'Update', 'cptemplates' ),
          'Drag to reorder' => __( 'Drag to reorder', 'cptemplates' ),
          'There was an error exporting clip: ' => __( 'There was an error exporting clip: ', 'cptemplates' ),
          'Clip "{title}" has been successfully exported and copied to your clipboard. You can now import it into another place.' => __( 'Clip "{title}" has been successfully exported and copied to your clipboard. You can now import it into another place.', 'cptemplates' ),
          'Clip "{title}" has been exported. Copy the following export code below and import into another place' => __( 'Clip "{title}" has been exported. Copy the following export code below and import into another place', 'cptemplates' ),
          'Row can be only inserted after another Row.' => __( 'Row can be only inserted after another Row.', 'cptemplates' ),
          'Inner Row can not be pasted as root element.' => __( 'Inner Row can not be pasted as root element.', 'cptemplates' ),
          'Inner Row can be only inserted after another Inner Row or added to Column.' => __( 'Inner Row can be only inserted after another Inner Row or added to Column.', 'cptemplates' ),
          'Column can be only appended to Row or merged with another Column.' => __( 'Column can be only appended to Row or merged with another Column.', 'cptemplates' ),
          'Inner Column can be only appended to Inner Row or merged with another Column or Inner Column.' => __( 'Inner Column can be only appended to Inner Row or merged with another Column or Inner Column.', 'cptemplates' ),
          'Unknown error.' => __( 'Unknown error.', 'cptemplates' ),
          'Pasted shortcode is not accepted by this container.' => __( 'Pasted shortcode is not accepted by this container.', 'cptemplates' ),
          'Pasted shortcode does not allow to be pasted here.' => __( 'Pasted shortcode does not allow to be pasted here.', 'cptemplates' ),
          'Pasting to this container is not allowed.' => __( 'Pasting to this container is not allowed.', 'cptemplates' ),
          'Nested shortcodes are prohibited in WordPress or otherwise you may get unexpected result.' => __( 'Nested shortcodes are prohibited in WordPress or otherwise you may get unexpected result.', 'cptemplates' ),
          'Can not append to this element. Parent is not a container or undefined.' => __( 'Can not append to this element. Parent is not a container or undefined.', 'cptemplates' ),
          'Clip not found' => __( 'Clip not found', 'cptemplates' ),
          'Nothing to import' => __( 'Nothing to import', 'cptemplates' ),
          'Invalid clip code' => __( 'Invalid clip code', 'cptemplates' ),
          'Clip is invalid' => __( 'Clip is invalid', 'cptemplates' ),
          'Clip has been successfully imported!' => __( 'Clip has been successfully imported!', 'cptemplates' ),
          'Something went wrong. Clip can not be imported!' => __( 'Something went wrong. Clip can not be imported!', 'cptemplates' ),
          'This clip contains unrecognized shortcodes, which will be skipped: ' => __( 'This clip contains unrecognized shortcodes, which will be skipped: ', 'cptemplates' ),
          'This clip can not be used, because its main shortcode is unrecognized: ' => __( 'This clip can not be used, because its main shortcode is unrecognized: ', 'cptemplates' ),
        ),
      ) );
    }
  }

}


/**
 *  cptemplates_admin_clips
 *
 *  The main function responsible for returning cptemplates_admin_clips object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 3.0.5
 *
 *  @param N/A
 *  @return (object) cptemplates_admin_clips instance
 */

function cptemplates_admin_clips() {
  return cptemplates_admin_clips::instance();
}


// initialize
cptemplates_admin_clips();


endif; // class_exists check

?>