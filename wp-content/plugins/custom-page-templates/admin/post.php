<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_admin_post' ) ) :

final class cptemplates_admin_post {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_admin_post
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    // Init later to allow all post types to be registered
    add_action( 'init', array( $this, 'init' ), 20 );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_admin_post instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
    
    add_action( 'add_meta_boxes', array( $this, 'add_metaboxes' ) );

    // Setup hooks for supported post types
    foreach ( $this->get_post_types() as $post_type ) {
      add_action( "manage_{$post_type}_posts_columns", array( $this, 'posts_columns' ) );
      add_action( "manage_{$post_type}_posts_custom_column", array( $this, 'posts_custom_column' ), 10, 2 );
      add_action( "save_post_{$post_type}", array( $this, 'save_post' ), 10, 3 );
    }

    add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_assets' ) );
  }


  /**
   *  add_metaboxes
   *
   *  Adds template metabox
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param  N/A
   *  @return  N/A
   */

  public function add_metaboxes() {
    $post_types = $this->get_post_types();

    add_meta_box(
      'cptemplates-template', // ID
      __( 'Custom Template', 'cptemplates' ), // Title
      array( $this, 'metabox_custom_rules' ), // Callback
      $post_types, // Screen
      'normal', // Context
      'default' // Priority
    );

  }


  /**
   *  metabox_custom_rules
   *
   *  Renders template metabox
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param  N/A
   *  @return  N/A
   */

  public function metabox_custom_rules() {
    cptemplates_admin_custom_rules()->render();
  }


  /**
   *  posts_columns
   *
   *  Adds template column to post columns list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) posts_columns
   *  @return  (array) posts_columns
   */

  public function posts_columns( $posts_columns ) {
    $posts_columns[ 'cptemplate' ] = __( 'Template', 'cptemplates' );
    return $posts_columns;
  }


  /**
   *  posts_custom_column
   *
   *  Renders template column in posts list
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) column_name
   *  @param (int) post_id
   *  @return  N/A
   */

  public function posts_custom_column( $column_name, $post_id ) {
    if ( 'cptemplate' == $column_name ) {
      $templates = cptemplates_cptemplate()->get_templates();
      $rules = cptemplates_rules()->get_custom_rules( $post_id, 'post', $optimized = false );
      $post_templates = array_map( function( $rule ) use ( $templates ) {
        return $templates[ $rule[ 'template' ] ];
      }, $rules );
      echo implode( ', ', $post_templates );
    }
  }


  /**
   *  save_post
   *
   *  Update post template on post update
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (int) post_id
   *  @param (object) post
   *  @param (boolean) update
   *  @return  N/A
   */

  public function save_post( $post_id, $post, $update ) {
    if ( ! $update ) return; // This is to prevent blocking post creation with wp_insert_post from ajax post
    if ( empty( $_POST ) ) return; // Prevent function execution on post deletetion

    cptemplates_admin_custom_rules()->save_posted_rules( $post_id, 'post' );
  }


  /**
   *  get_post_types
   *
   *  Retrieve posts types eligible for having a template
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (array) post_types
   */

  public function get_post_types() {
    if ( isset( $this->post_types ) ) {
      return $this->post_types;
    }
    else {
      $post_type_objects = get_post_types( array( 'public' => true ), 'names' );
      $this->post_types = array_keys( $post_type_objects );

      // Some post types should not be included into the list
      // Allow 3rd party to adjust
      $exclude_post_types = apply_filters( 'cptemplates/exclude_post_types', array( 'cptemplate', 'vc_snippet' ) );
      $this->post_types = array_diff( $this->post_types, $exclude_post_types );

      // Some post types may be missing. Allow 3rd party plugins to customize this list
      $this->post_types = apply_filters( 'cptemplates/get_post_types', $this->post_types );

      return $this->post_types;
    }
  }


  /**
   *  enqueue_assets
   *
   *  Load required JS and CSS on post pages
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (string) $hook_suffix
   *  @return  N/A
   */

  public function enqueue_assets( $hook_suffix = '' ) {

    // Only on edit post page
    if ( in_array( $hook_suffix, array( 'post.php' ) ) ) { // , 'edit.php'
      global $post;

      // Only supported post types
      if ( in_array( $post->post_type, $this->get_post_types() ) ) {

        // Get saved rules
        $rules = cptemplates_rules()->get_custom_rules( $post->ID, 'post', $optimized = false );

        // Enqueue
        cptemplates_admin_custom_rules()->enqueue_assets( $rules );
      }

    }

  }

}


/**
 *  cptemplates_admin_post
 *
 *  The main function responsible for returning cptemplates_admin_post object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_admin_post instance
 */

function cptemplates_admin_post() {
  return cptemplates_admin_post::instance();
}


// initialize
cptemplates_admin_post();


endif; // class_exists check

?>