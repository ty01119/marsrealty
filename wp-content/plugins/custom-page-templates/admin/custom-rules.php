<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_admin_custom_rules' ) ) :

final class cptemplates_admin_custom_rules {

  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_admin_custom_rules
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_admin_custom_rules instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
  }


  /**
   *  render
   *
   *  Renders template metabox
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function render() {
    include cptemplates_get_path( 'admin/view-templates/select.php' );
    include cptemplates_get_path( 'admin/view-templates/number.php' );
    include cptemplates_get_path( 'admin/view-templates/norules.php' );
    include cptemplates_get_path( 'admin/view-templates/custom-rule.php' );
    $templates = $this->get_global_templates();
    ?>
    <div id="cptemplates-template-rules" class="cptemplates-rules">
      <?= do_action( 'cptemplates/admin_custom_rules/before_rules_list' ) ?>
      <div id="cptemplates-rules-list"></div>
      <?= do_action( 'cptemplates/admin_custom_rules/after_rules_list' ) ?>
      <p class="cptemplates-rules-add"><a href="#" class="button button-primary button-large btn-add-rule"><?= __( 'Add New Rule', 'cptemplates' ) ?></a></p>
      <p class="cptemplates-rules-global-templates">
        <?php if ( ! empty( $templates ) ) : ?>
          <?= __( 'The following global templates could be also applied to this page', 'cptemplates' ) ?>: <?= implode( ', ', $templates ) ?>.
        <?php else : ?>
          <?= __( 'No other global templates configured to be applied to this page', 'cptemplates' ) ?>.
        <?php endif; ?>
      </p>
    </div>
    <?php
  }


  /**
   *  get_global_templates
   *
   *  Retrieves a list of templates that could match the currently editing post or term
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  (array)
   */

  public function get_global_templates() {
    if ( ! function_exists( 'get_current_screen' ) || ! ( $screen = get_current_screen() ) ) {
      return array();
    }

    $admin_page = isset( $screen->base ) ? $screen->base : false;
    if ( ! $screen ) {
      return array();
    }

    $admin_page = $admin_page == 'edit-tags' ? 'term' : $admin_page;
    if ( ! in_array( $admin_page, array( 'post', 'term' ) ) ) {
      return array();
    }

    $request = $admin_page == 'post' ? 'post_type_' . $screen->post_type : 'taxonomy_' . $screen->taxonomy;

    $templates = cptemplates_template()->get_templates_by_request( $request );
    $templates = array_map( function( $template ) {
      if ( $template_post = get_post( $template->template_id ) ) {
        return sprintf( '<a href="%s" target="_blank">%s</a>', get_edit_post_link( $template->template_id ), $template_post->post_title );
      }
      else {
        return false;
      }

    }, $templates );
    $templates = array_filter( $templates );

    return $templates;
  }


  /**
   *  enqueue_assets
   *
   *  Load required JS and CSS
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (array) $rules
   *  @return  N/A
   */

  public function enqueue_assets( $rules = array() ) {
    $rules = is_array( $rules ) && ! empty( $rules ) ? $rules : array();

    $options = array(
      'status' => cptemplates_admin_rule_options()->get_status_options(),
      'templates' => cptemplates_admin_rule_options()->get_template_options(),
      'user' => cptemplates_admin_rule_options()->get_user_options(),
    );

    wp_enqueue_script( 'cptemplates-admin-rules' );
    wp_localize_script(
      'cptemplates-admin-rules',
      'cptemplatesAdminRules',
      array(
        'existingRules' => $rules,
        'options' => $options,
        'i18n' => cptemplates_admin()->get_i18n_strings(),
        'nonce' => wp_create_nonce( 'cptemplates-admin-rules' ),
      )
    );
  }


  /**
   *  save_rules
   *
   *  Saves posted rules for term or post on admin edit/create screens
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param (int) $object_id
   *  @param (string) $object_type
   *  @return  N/A
   */

  public function save_posted_rules( $object_id, $object_type ) {
    $object_id = (int)$object_id;
    $object_type = (string)$object_type;
    if ( ! $object_type || ! $object_id ) {
      return '';
    }

    // Get posted rules
    $rules = isset( $_POST[ 'cptemplates_rules' ] ) && is_array( $_POST[ 'cptemplates_rules' ] ) ? (array)$_POST[ 'cptemplates_rules' ] : array();

    // Pre-process posted rules
    $rules = cptemplates_admin_rules()->preprocess_posted_rules( $rules, 'custom' );

    // Allow 3rd parties to modify
    $rules = apply_filters( 'cptemplates/save_custom_rules', $rules, $object_id, $object_type );

    // Save rules
    update_metadata( $object_type, $object_id, 'cptemplate_custom_rules', $rules );


    // Optimize 
    $optimized_rules = array();
    foreach ( $rules as $rule ) {

      // No need to process disabled rules
      if ( ! $rule[ 'status' ] ) {
        continue;
      }

      // Compose rule object
      $pp_rule = new stdClass();
      $pp_rule->template_id = $rule[ 'template' ];

      // Remove few variables, and all the rest is conditions
      unset( $rule[ 'status' ] ); // We save just enabled rules
      unset( $rule[ 'priority' ] ); // Already sorted by priority
      unset( $rule[ 'template' ] ); // Template should is saved separately

      // Optimize conditions
      $pp_rule->conditions = cptemplates_admin_rules()->optimize_conditions( $rule );

      // Save to the list
      $optimized_rules []= $pp_rule;

    }

    // Save optimized rules in different meta
    update_metadata( $object_type, $object_id, 'cptemplate_custom_rules_optimized', $optimized_rules );


    // Notify 3rd parties
    do_action( 'cptemplates/saved_custom_rules', $rules, $optimized_rules, $object_id, $object_type );
  }  

}


/**
 *  cptemplates_admin_custom_rules
 *
 *  The main function responsible for returning cptemplates_admin_custom_rules object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_admin_custom_rules instance
 */

function cptemplates_admin_custom_rules() {
  return cptemplates_admin_custom_rules::instance();
}


// initialize
cptemplates_admin_custom_rules();


endif; // class_exists check

?>