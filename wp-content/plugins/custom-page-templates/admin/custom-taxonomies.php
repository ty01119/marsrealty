<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_admin_custom_taxonomies' ) ) :

final class cptemplates_admin_custom_taxonomies {


  /**
   * Plugin instance
   *
   *  @since 3.0.0
   *  @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   *  @since 3.0.0
   *  @var boolean $initialized
   */
  private $initialized = false;
  

  /**
   *  __construct
   *
   *  Initialize cptemplates_admin_custom_taxonomies
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  
    add_action( 'save_post_cpt_tax', array( $this, 'hook_pre_save_post' ), 10, 3 );
    add_action( 'trashed_post', array( $this, 'trashed_post' ) );
    add_action( 'untrashed_post', array( $this, 'untrashed_post' ) );
    add_action( 'delete_post', array( $this, 'delete_post' ) );

    add_filter( 'acf/load_field/key=field_5a774c50e4602', array( $this, 'load_post_types_options' ) );

    add_action( 'admin_notices', array( $this, 'admin_notices' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_admin_custom_taxonomies instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
    
    $this->register_post_type();
  }


  /**
   *  admin_notices
   *
   *  Displays post type update errors
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function admin_notices() {
    global $post;

    // Check if we're on cpt edit screen
    $current_screen = get_current_screen();
    if ( ! is_a( $current_screen, 'WP_Screen' ) ) return;
    if ( ! isset( $current_screen->base ) || ( 'post' !== $current_screen->base ) ) return;
    if ( ! isset( $current_screen->id ) || ( 'cpt_tax' !== $current_screen->id ) ) return;

    // Get errors if any
    $errors = get_post_meta( $post->ID, 'cpt_errors', true );
    if ( ! $errors ) return;

    // Display errors
    foreach ( $errors as $key => $error ) {
      cptemplates_display_admin_notice(
        $error,
        'error',
        true
      );
    }

    // Clean errors list
    delete_post_meta( $post->ID, 'cpt_errors' );
  }


  /**
   *  register_post_type
   *
   *  Registers post type for taxonomies setup
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function register_post_type() {
    $labels = array(
      'menu_name'             => __( 'Taxonomies', 'cptemplates' ),
      'name'                  => __( 'Custom Taxonomies', 'cptemplates' ),
      'singular_name'         => __( 'Custom Taxonomy', 'cptemplates' ),
      'all_items'             => __( 'Taxonomies', 'cptemplates' ),
      'add_new_item'          => __( 'Add New Taxonomy', 'cptemplates' ),
      'new_item'              => __( 'New Taxonomy', 'cptemplates' ),
      'edit_item'             => __( 'Edit Taxonomy', 'cptemplates' ),
      'update_item'           => __( 'Update Taxonomy', 'cptemplates' ),
      'view_item'             => __( 'View Taxonomy', 'cptemplates' ),
      'search_items'          => __( 'Search Taxonomies', 'cptemplates' ),
    );
    $args = array(
      'labels'                => $labels,
      'supports'              => array( 'title' ),
      'hierarchical'          => false,
      'public'                => false,
      'show_ui'               => true,
      'show_in_menu'          => 'cptemplates_general',
      'menu_icon'             => '',
      'menu_position'         => 10,
      'show_in_admin_bar'     => false,
      'show_in_nav_menus'     => false,
      'can_export'            => true,
      'has_archive'           => false,   
      'exclude_from_search'   => true,
      'publicly_queryable'    => false,
      'rewrite'               => false,
      'query_var'             => false,
      'capability_type'       => 'post',
    );

    register_post_type( 'cpt_tax', $args );  
  }


  /**
   *  hook_pre_save_post
   *
   *  This is an intermediary function that hooks to save_post only when cpt_tax post_type is being saved
   *  mostly for performance and memory usage optimization reason
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int) The post ID.
   *  @param $post (WP_Post) The post object.
   *  @param $update (boolean) Whether this is an existing post being updated or not.
   *  @return  N/A
   */

  public function hook_pre_save_post( $post_id, $post, $update ) {
    // Hook later to have ACF data saved
    add_action( 'save_post', array( $this, 'hook_save_post' ), 20, 3 );
  }


  /**
   *  hook_save_post
   *
   *  Retrieve created post types
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int) The post ID.
   *  @param $post (WP_Post) The post object.
   *  @param $update (boolean) Whether this is an existing post being updated or not.
   *  @return  N/A
   */

  public function hook_save_post( $post_id, $post, $update ) {
    if ( ! $update ) return; // This is to prevent blocking post creation with wp_insert_post from ajax post
    if ( empty( $_POST ) ) return; // Prevent function execution on post deletetion
    if ( 'cpt_tax' !== $post->post_type ) return; // Run for corresponding post type only
    if ( 'auto-draft' === $post->post_status ) return;

    $fields = get_fields( $post_id );

    $errors = array();
    $args = array();


    // Main Settings: Validate Post type
    $taxonomy_submitted = isset( $fields[ 'taxonomy' ] ) ? strval( $fields[ 'taxonomy' ] ) : '';
    $taxonomy = preg_replace( '/[^\w\d-_]/i', '_', $taxonomy_submitted );
    $taxonomy = substr( $taxonomy, 0, 20 );
    if ( $taxonomy != $taxonomy_submitted ) {
      $errors []= __( 'Taxonomy key is invalid', 'cptemplates' );
    }
    $args[ 'taxonomy' ] = $taxonomy;


    // Main Settings: Object type
    $args[ 'object_type' ] = isset( $fields[ 'object_type' ] ) && is_array( $fields[ 'object_type' ] ) ? $fields[ 'object_type' ] : array();

    // Main Settings: Hierarchical
    $args[ 'hierarchical' ] = isset( $fields[ 'hierarchical' ] ) ? !! $fields[ 'hierarchical' ] : false;

    // Main Settings: Description
    $args[ 'description' ] = isset( $fields[ 'description' ] ) && is_string( $fields[ 'description' ] ) ? !! $fields[ 'description' ] : '';


    // Accessibility Settings: Accessibility Options
    $accessibility_options = isset( $fields[ 'accessibility_options' ] ) && is_array( $fields[ 'accessibility_options' ] ) ? $fields[ 'accessibility_options' ] : array();
    $args[ 'public' ] = in_array( 'public', $accessibility_options );
    $args[ 'publicly_queryable' ] = in_array( 'publicly_queryable', $accessibility_options );
    $args[ 'show_ui' ] = in_array( 'show_ui', $accessibility_options );

    // Accessibility Settings: Capability
    $capabilities = isset( $fields[ 'capabilities' ] ) ? strtolower( trim( $fields[ 'capabilities' ] ) ) : 'default';
    if ( 'custom' === $capabilities ) {
      $args[ 'capabilities' ] = array(
        'manage_terms' => isset( $fields[ 'manage_terms' ] ) ? strtolower( trim( $fields[ 'manage_terms' ] ) ) : 'manage_categories',
        'edit_terms'   => isset( $fields[ 'edit_terms' ] ) ? strtolower( trim( $fields[ 'edit_terms' ] ) ) : 'manage_categories',
        'delete_terms' => isset( $fields[ 'delete_terms' ] ) ? strtolower( trim( $fields[ 'delete_terms' ] ) ) : 'manage_categories',
        'assign_terms' => isset( $fields[ 'assign_terms' ] ) ? strtolower( trim( $fields[ 'assign_terms' ] ) ) : 'edit_posts',
      );
    }


    // Visibility Settings: Menu Visibility
    $menu_visibility = isset( $fields[ 'menu_visibility' ] ) && is_array( $fields[ 'menu_visibility' ] ) ? $fields[ 'menu_visibility' ] : array();
    $args[ 'show_in_menu' ] = in_array( 'show_in_menu', $menu_visibility );
    $args[ 'show_in_nav_menus' ] = in_array( 'show_in_nav_menus', $menu_visibility );


    // Visibility Settings: Visibility in other areas
    $other_areas_visibility = isset( $fields[ 'other_areas_visibility' ] ) && is_array( $fields[ 'other_areas_visibility' ] ) ? $fields[ 'other_areas_visibility' ] : array();
    $args[ 'show_in_quick_edit' ] = in_array( 'show_in_quick_edit', $other_areas_visibility );
    $args[ 'show_admin_column' ] = in_array( 'show_admin_column', $other_areas_visibility );
    $args[ 'show_tagcloud' ] = in_array( 'show_tagcloud', $other_areas_visibility );


    // Frontend Settings: Rewrite
    $args[ 'rewrite' ] = isset( $fields[ 'rewrite' ] ) ? strval( $fields[ 'rewrite' ] ) : 'yes';
    if ( 'custom' === $args[ 'rewrite' ] ) {
      $rewrite_args = array();
      
      // Slug
      $rewrite_args[ 'slug' ] = isset( $fields[ 'custom_url_slug' ] ) ? sanitize_title_with_dashes( $fields[ 'custom_url_slug' ] ) : $post_type;
      
      // Rewrite options
      $rewrite_options = isset( $fields[ 'rewrite_options' ] ) && is_array( $fields[ 'rewrite_options' ] ) ? $fields[ 'rewrite_options' ] : array();
      $rewrite_args[ 'with_front' ] = in_array( 'with_front', $rewrite_options );
      $rewrite_args[ 'hierarchical' ] = in_array( 'hierarchical', $rewrite_options );

      $args[ 'rewrite' ] = $rewrite_args;
    }
    else {
      $args[ 'rewrite' ] = 'no' === $args[ 'rewrite' ] ? false : true;
    }

    // Frontend Settings: Query var
    $args[ 'query_var' ] = isset( $fields[ 'query_var' ] ) && trim( $fields[ 'query_var' ] ) ? trim( $fields[ 'query_var' ] ) : $taxonomy;


    // REST API Settings: Show in REST
    $args[ 'show_in_rest' ] = $fields[ 'show_in_rest' ] ? true : false;

    // REST API Settings: Rest Base
    if ( isset( $fields[ 'rest_base' ] ) && trim( $fields[ 'rest_base' ] ) ) {
      $args[ 'rest_base' ] = $fields[ 'rest_base' ];
    }

    // REST API Settings: Rest Controller Class
    if ( isset( $fields[ 'rest_controller_class' ] ) && trim( $fields[ 'rest_controller_class' ] ) ) {
      $args[ 'rest_controller_class' ] = $fields[ 'rest_controller_class' ];
    }
    

    // Label Settings
    $labels = array();
    $label_keys = array(
      'singular_name', 'search_items', 'popular_items', 'all_items', 'parent_item', 'parent_item_colon',
      'edit_item', 'view_item', 'update_item', 'add_new_item', 
      'new_item_name', 'separate_items_with_commas', 'add_or_remove_items', 'choose_from_most_used',
      'not_found', 'no_terms', 'items_list_navigation', 'items_list', 'most_used', 'back_to_items'
    );
    foreach ( $label_keys as $key ) {
      $label = isset( $fields[ $key ] ) ? trim( $fields[ $key ] ) : '';
      if ( $label ) {
        $labels[ $key ] = $label;
      }
    }
    $labels[ 'name' ] = $args[ 'label' ] = $post->post_title;
    $args[ 'labels' ] = $labels;


    // Prevent future status and notify user
    if ( 'future' === $post->post_status ) {
      $errors []= __( 'Taxonomy was not updated, because Future status is not supported. Use publish, draft or private', 'cptemplates' );
    }
    elseif ( 'pending' === $post->post_status ) {
      $errors []= __( 'Taxonomy was not updated, because Pending Review status is not supported. Use publish, draft or private', 'cptemplates' );
    }


    // Allow 3rd parties to validate and add own errors
    $errors = apply_filters( 'cptemplates/custom_taxonomies/admin/save_errors', $errors, $args, $post_id, $post );


    // Save custom post type if there are no errors and post status is publish
    if ( ! $errors ) {

      // Allow 3rd parties to adjust
      $args = apply_filters( 'cptemplates/custom_taxonomies/admin/save_args', $args, $post_id, $post );

      $this->update_taxonomy( $post_id, $post->post_status, $args );
    }


    // Save errors if any for notification on page reload
    if ( $errors ) {
      update_post_meta( $post_id, 'cpt_errors', $errors );
    }

  }


  /**
   *  update_taxonomy
   *
   *  Custom taxonomy args are saved in options table all in a single place
   *  for fast registration.
   *  This function allows updating or deleting custom taxonomy
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int)
   *  @param $args (array|null)
   *  @param $status (string)
   *  @return  N/A
   */

  private function update_taxonomy( $post_id, $status, $args = null ) {
    $taxonomies = cptemplates_custom_taxonomies()->get_taxonomies();

    if ( 'deleted' === $status ) {
      unset( $taxonomies[ $post_id ] );
    }
    else {

      // Args could be directly provided or automatically direved from the saved post types
      if ( ! is_array( $args ) ) {
        $args = isset( $taxonomies[ $post_id ] ) && is_array( $taxonomies[ $post_id ] ) ? $taxonomies[ $post_id ] : null;
      }

      // Do not update if args are invalid
      if ( ! $args ) {
        return;
      }

      $args[ 'status' ] = $status;
      $taxonomies[ $post_id ] = $args;

    }

    cptemplates_custom_taxonomies()->set_taxonomies( $taxonomies );
  }


  /**
   *  trashed_post
   *
   *  Change taxonomy status when moved to trash
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int)
   *
   *  @return  N/A
   */

  public function trashed_post( $post_id ) {
    // Apply for corresponding type only
    if ( 'cpt_tax' !== get_post_type( $post_id ) ) {
      return;
    }

    // Notify 3rd parties
    do_action( 'cptemplates/custom_taxonomies/admin/before_trash', $post_id );

    $this->update_taxonomy( $post_id, 'trash' );
  }


  /**
   *  untrashed_post
   *
   *  Restore taxonomy status when restored from trash
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int)
   *
   *  @return  N/A
   */

  public function untrashed_post( $post_id ) {
    // Apply for corresponding type only
    if ( 'cpt_tax' !== get_post_type( $post_id ) ) {
      return;
    }

    // Notify 3rd parties
    do_action( 'cptemplates/custom_taxonomies/admin/before_untrash', $post_id );

    $this->update_taxonomy( $post_id, get_post_status( $post_id ) );
  }


  /**
   *  delete_post
   *
   *  Finally, delete taxonomy settings when corresponding post has been removed
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $post_id (int)
   *
   *  @return  N/A
   */

  public function delete_post( $post_id ) {
    // Apply for corresponding type only
    if ( 'cpt_tax' !== get_post_type( $post_id ) ) {
      return;
    }

    // Notify 3rd parties
    do_action( 'cptemplates/custom_taxonomies/admin/before_delete', $post_id );

    $this->update_taxonomy( $post_id, 'deleted' );
  }


  /**
   *  load_post_types_options
   *
   *  Dynamically populates post types field's options with available post types
   *
   *  @type  function
   *  @date  03/02/18
   *  @since 3.0.0
   *
   *  @param $field (acf_field)
   *  @return  N/A
   */

  public function load_post_types_options( $field ) {
    $field[ 'choices' ] = array();

    $post_types = get_post_types( array(), 'objects' );
    foreach  ( $post_types as $post_type => $item ) {
      $field['choices'][ $post_type ] = sprintf( '%s (%s)', $item->name, $post_type );
    }

    return $field;
  }


}


/**
 *  cptemplates_admin_custom_taxonomies
 *
 *  The main function responsible for returning cptemplates_admin_custom_taxonomies object
 *
 *  @type  function
 *  @date  29/08/17
 *  @since 1.0.2
 *
 *  @param N/A
 *  @return (object) cptemplates_admin_custom_taxonomies instance
 */

function cptemplates_admin_custom_taxonomies() {
  return cptemplates_admin_custom_taxonomies::instance();
}


// initialize
cptemplates_admin_custom_taxonomies();


endif; // class_exists check

?>