<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'cptemplates_admin' ) ) :

final class cptemplates_admin {


  /**
   * Plugin instance
   *
   * @since 1.0.0
   * @var object $instance
   */
  protected static $instance;
  

  /**
   * Flag if instance has been initialized
   *
   * @since 1.0.0
   * @var boolean $initialized
   */
  private $initialized = false;

  
  /**
   *  __construct
   *
   *  Initialize cptemplates_admin
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  private function __construct() {
    add_action( 'init', array( $this, 'init' ) );
  }


  /**
   *  instance
   *
   *  Create or retrieve instance. Singleton pattern
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @static
   *
   *  @param N/A
   *  @return  (object) cptemplates_admin instance
   */

  public static function instance() {
    return self::$instance ? self::$instance : self::$instance = new self();
  }


  /**
   *  init
   *
   *  Initialize
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function init() {
    // Can be initialized just once
    if ( $this->initialized ) return;
    $this->initialized = true;
    
    add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_assets' ) );


    // Flush rewrite rules when needed
    if ( get_option( 'cpt_needs_rewrite_rules_update' ) ) {
      update_option( 'cpt_needs_rewrite_rules_update', false, true );
      flush_rewrite_rules();
    }
  }


  /**
   *  enqueue_assets
   *
   *  Enqueues required JS and CSS
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function enqueue_assets() {
    global $wp_scripts;

    // $major_version = isset( $wp_scripts->registered[ 'select2' ] ) ? (int) $wp_scripts->registered[ 'select2' ]->ver : false;
    $min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

    // v4
    // if ( ! $major_version || ( $major_version == 4 ) ) {
      $version = '4.0.3';
      $script = cptemplates_get_url( "assets/vendor/select2/4/js/select2.full{$min}.js" );
      $style = cptemplates_get_url( "assets/vendor/select2/4/css/select2{$min}.css" );
    // } 
    // // v3
    // else {
      // $version = '3.5.2';
      // $script = cptemplates_get_url( "assets/vendor/select2/3/select2{$min}.js" );
      // $style = cptemplates_get_url( "assets/vendor/select2/3/select2.css" );
    // }
    
    // Enqueue select2
    if ( cptemplates_get_option( 'load_select2_js', true ) && ! wp_script_is( 'select2', 'enqueued' ) ) {
      wp_enqueue_script( 'select2', $script, array( 'jquery' ), $version );
    }
    if ( cptemplates_get_option( 'load_select2_css', true ) && ! wp_style_is( 'select2', 'enqueued' ) ) {
      wp_enqueue_style('select2', $style, '', $version );
    }

    wp_enqueue_style( 'cptemplates-admin' );
  }


  /**
   *  get_i18n_strings
   *
   *  Retrieves i18n strings
   *
   *  @type  function
   *  @date  25/03/17
   *  @since 1.0.0
   *
   *  @param N/A
   *  @return  N/A
   */

  public function get_i18n_strings() {
    return array(
      'status_label' => __( 'Status', 'cptemplates' ),
      'status_help' => __( 'Activate or deactivate this rule.', 'cptemplates' ),
      'priority_label' => __( 'Priority', 'cptemplates' ),
      'priority_help' => __( 'Rule with a lower number will be processed first.', 'cptemplates' ),
      'priority_placeholder' => __( 'e.g. 10', 'cptemplates' ),
      'template_label' => __( 'Template', 'cptemplates' ),
      'template_help' => __( 'Pick a template you want to apply for this page.', 'cptemplates' ),
      'user_label' => __( 'User', 'cptemplates' ),
      'user_help' => __( 'Apply this template only if a user is one of the specified. Leave blank to apply for all users.', 'cptemplates' ),
      'request_label' => __( 'Request', 'cptemplates' ),
      'request_help' => __( 'The page to which this template will be applied.', 'cptemplates' ),
      'post_terms_label' => __( 'Post Terms', 'cptemplates' ),
      'post_terms_help' => __( 'Apply this template only if a post has one of the specified terms. Leave blank to apply this template regardless of the terms assigned to a post.', 'cptemplates' ),
      'post_formats_label' => __( 'Post Formats', 'cptemplates' ),
      'post_formats_help' => __( 'Apply this template only if a post format is one of the specified. Leave blank to apply for all post formats.', 'cptemplates' ),
    );
  }

}


/**
 *  cptemplates_admin
 *
 *  The main function responsible for returning cptemplates_admin object
 *
 *  @type  function
 *  @date  25/03/17
 *  @since 1.0.0
 *
 *  @param N/A
 *  @return (object) cptemplates_admin instance
 */

function cptemplates_admin() {
  return cptemplates_admin::instance();
}


// initialize
cptemplates_admin();


endif; // class_exists check

?>