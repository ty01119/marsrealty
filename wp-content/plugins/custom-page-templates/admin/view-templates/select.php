<script type="text/template" class="cptemplates-tpl" data-name="select">
  <% if( options && options.length ) { %>
    <div class="cptemplates-rule-setting cptemplates-rule-<%= prop %>">
      <div class="cptemplates-rule-label">
        <span class="cptemplates-rule-label-text"><%= label %></span>
        <p class="cptemplates-rule-option-description"><%= help %></p>
      </div>
      <div class="cptemplates-rule-input">
        <select name="cptemplates_rules[<%= index %>][<%= prop %>]<%= multiple ? '[]' : '' %>" data-prop="<%= prop %>" <%= multiple ? 'multiple' : '' %>>
          <% _( options ).each( function( item ) { %>
            <% if ( item.options ) { %>
              <optgroup label="<%= item.label %>">
                <% _( item.options ).each( function( option ) { %>
                  <option value="<%= option.value %>" <%= ( option.value == value ) || ( _.isArray( value ) && ( -1 != _.indexOf( value, option.value + '' ) ) ) ? 'selected' : '' %> ><%= option.label %></option>
                <% }); %>
              </optgroup>
            <% } else { %>
              <option value="<%= item.value %>" <%= item.value == value ? 'selected' : '' %> ><%= item.label %></option>
            <% } %>
          <% }); %>
        </select>
      </div>
    </div>
  <% } %>
</script>
