<script type="text/template" class="cptemplates-tpl" data-name="rule">
  <%= templates.select( { index: index, options: options.status, prop: 'status', value: model.status, multiple:false, label: i18n.status_label, help: i18n.status_help } ) %>

  <%= templates.number( { index: index, prop: 'priority', value: model.priority, label: i18n.priority_label, placeholder: i18n.priority_placeholder, help: i18n.priority_help } ) %>

  <%= templates.select( { index: index, options: options.request, prop: 'request', value: model.request, multiple:false, label: i18n.request_label, help: i18n.request_help } ) %>
  
  <%= templates.select( { index: index, options: options.post_terms, prop: 'post_terms', value: model.post_terms, multiple:true, label: i18n.post_terms_label, help: i18n.post_terms_help } ) %>
  
  <%= templates.select( { index: index, options: options.post_formats, prop: 'post_formats', value: model.post_formats, multiple:true, label: i18n.post_formats_label, help: i18n.post_formats_help } ) %>
  
  <%= templates.select( { index: index, options: options.user, prop: 'user', value: model.user, multiple:true, label: i18n.user_label, help: i18n.user_help } ) %>

  <div class="cptemplates-rule-setting cptemplates-rule-remove">
    <a href="#" class="button button-secondary button-large btn-remove-rule"><?= __( 'Remove This Rule', 'cptemplates' ) ?></a>
  </div>
</script>
