<script type="text/template" class="cptemplates-tpl" data-name="number">
  <div class="cptemplates-rule-setting cptemplates-rule-<%= prop %>">
    <div class="cptemplates-rule-label">
      <span class="cptemplates-rule-label-text"><%= label %></span>
      <p class="cptemplates-rule-option-description"><%= help %></p>
    </div>
    <div class="cptemplates-rule-input">
      <input type="number" name="cptemplates_rules[<%= index %>][<%= prop %>]" data-prop="<%= prop %>" value="<%= value ? value : 10 %>" min="0" max="999" placeholder="<%= placeholder %>">
    </div>
  </div>
</script>
