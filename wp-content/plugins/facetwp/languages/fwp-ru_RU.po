msgid ""
msgstr ""
"Project-Id-Version: FacetWP\n"
"POT-Creation-Date: 2018-08-08 09:59-0400\n"
"PO-Revision-Date: 2018-08-08 09:59-0400\n"
"Last-Translator: Matt Gibbs <hello@facetwp.com>\n"
"Language-Team: VBurlak <vburlak.ru@gmail.com>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.1.1\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: node_modules\n"

#: assets/js/dist/admin.min.js:70 assets/js/src/admin.js:67
#: includes/class-settings-admin.php:213
msgid "Results per row"
msgstr ""

#: assets/js/dist/admin.min.js:75 assets/js/src/admin.js:72
#: includes/class-settings-admin.php:214
msgid "Grid gap"
msgstr ""

#: assets/js/dist/admin.min.js:80 assets/js/src/admin.js:77
#: includes/class-settings-admin.php:215
msgid "Text style"
msgstr ""

#: assets/js/dist/admin.min.js:90 assets/js/src/admin.js:87
#: includes/class-settings-admin.php:216
msgid "Text color"
msgstr ""

#: assets/js/dist/admin.min.js:95 assets/js/src/admin.js:92
#: includes/class-settings-admin.php:217
msgid "Font size"
msgstr ""

#: assets/js/dist/admin.min.js:104 assets/js/src/admin.js:101
#: includes/class-settings-admin.php:218
msgid "Background color"
msgstr ""

#: assets/js/dist/admin.min.js:109 assets/js/src/admin.js:106
#: includes/class-settings-admin.php:219
msgid "Border"
msgstr ""

#: assets/js/dist/admin.min.js:125 assets/js/src/admin.js:122
#: includes/class-settings-admin.php:220
msgid "Border style"
msgstr ""

#: assets/js/dist/admin.min.js:127 assets/js/dist/admin.min.js:245
#: assets/js/dist/admin.min.js:270 assets/js/dist/admin.min.js:321
#: assets/js/src/admin.js:124 assets/js/src/admin.js:242
#: assets/js/src/admin.js:267 assets/js/src/admin.js:318
#: includes/class-settings-admin.php:31 includes/class-settings-admin.php:221
#: includes/facets/proximity.php:222
msgid "None"
msgstr "Ничего"

#: assets/js/dist/admin.min.js:128 assets/js/src/admin.js:125
#: includes/class-settings-admin.php:222
msgid "Solid"
msgstr ""

#: assets/js/dist/admin.min.js:129 assets/js/src/admin.js:126
#: includes/class-settings-admin.php:223
msgid "Dashed"
msgstr ""

#: assets/js/dist/admin.min.js:130 assets/js/src/admin.js:127
#: includes/class-settings-admin.php:224
msgid "Dotted"
msgstr ""

#: assets/js/dist/admin.min.js:131 assets/js/src/admin.js:128
#: includes/class-settings-admin.php:225
msgid "Double"
msgstr ""

#: assets/js/dist/admin.min.js:136 assets/js/src/admin.js:133
#: includes/class-settings-admin.php:226
msgid "Border color"
msgstr ""

#: assets/js/dist/admin.min.js:140 assets/js/src/admin.js:137
#: includes/class-settings-admin.php:227
msgid "Border width"
msgstr ""

#: assets/js/dist/admin.min.js:146 assets/js/src/admin.js:143
#: includes/class-settings-admin.php:228
msgid "Button text"
msgstr ""

#: assets/js/dist/admin.min.js:150 assets/js/src/admin.js:147
#: includes/class-settings-admin.php:229
msgid "Button text color"
msgstr ""

#: assets/js/dist/admin.min.js:154 assets/js/src/admin.js:151
msgid "Button color"
msgstr ""

#: assets/js/dist/admin.min.js:158 assets/js/src/admin.js:155
#: includes/class-settings-admin.php:230
msgid "Button padding"
msgstr ""

#: assets/js/dist/admin.min.js:169 assets/js/src/admin.js:166
#: includes/class-settings-admin.php:231
#, fuzzy
#| msgid "Separators"
msgid "Separator"
msgstr "Сепаратор"

#: assets/js/dist/admin.min.js:174 assets/js/src/admin.js:171
#: includes/class-settings-admin.php:232
#, fuzzy
#| msgid "Custom Fields"
msgid "Custom CSS"
msgstr "Произвольные поля"

#: assets/js/dist/admin.min.js:179 assets/js/src/admin.js:176
#: includes/class-settings-admin.php:233
msgid "Column widths"
msgstr ""

#: assets/js/dist/admin.min.js:184 assets/js/src/admin.js:181
#: includes/class-settings-admin.php:234 includes/class-settings-admin.php:280
msgid "Content"
msgstr ""

#: assets/js/dist/admin.min.js:188 assets/js/src/admin.js:185
#: includes/class-settings-admin.php:235
msgid "Image size"
msgstr ""

#: assets/js/dist/admin.min.js:203 assets/js/src/admin.js:200
#: includes/class-settings-admin.php:236
#, fuzzy
#| msgid "Custom Fields"
msgid "Author field"
msgstr "Произвольные поля"

#: assets/js/dist/admin.min.js:206 assets/js/src/admin.js:203
#: includes/class-settings-admin.php:237
#, fuzzy
#| msgid "Display Value"
msgid "Display name"
msgstr "Отображаемое Значение"

#: assets/js/dist/admin.min.js:207 assets/js/src/admin.js:204
#: includes/class-settings-admin.php:238
msgid "User login"
msgstr ""

#: assets/js/dist/admin.min.js:208 assets/js/src/admin.js:205
#: includes/class-settings-admin.php:239
msgid "User ID"
msgstr ""

#: assets/js/dist/admin.min.js:213 assets/js/src/admin.js:210
#: includes/class-settings-admin.php:240
#, fuzzy
#| msgid "Facet type"
msgid "Field type"
msgstr "Тип Фасета"

#: assets/js/dist/admin.min.js:223 assets/js/src/admin.js:220
#: includes/class-settings-admin.php:244
#, fuzzy
#| msgid "Display format"
msgid "Date format"
msgstr "Display Format"

#: assets/js/dist/admin.min.js:233 assets/js/src/admin.js:230
#: includes/class-settings-admin.php:245
#, fuzzy
#| msgid "The number format"
msgid "Input format"
msgstr "Числовой формат"

#: assets/js/dist/admin.min.js:243 assets/js/src/admin.js:240
#: includes/class-settings-admin.php:246
#, fuzzy
#| msgid "The number format"
msgid "Number format"
msgstr "Числовой формат"

#: assets/js/dist/admin.min.js:259 assets/js/src/admin.js:256
#: includes/class-settings-admin.php:247
msgid "Link"
msgstr ""

#: assets/js/dist/admin.min.js:268 assets/js/src/admin.js:265
#: includes/class-settings-admin.php:248
msgid "Link type"
msgstr ""

#: assets/js/dist/admin.min.js:271 assets/js/src/admin.js:268
#: includes/class-settings-admin.php:249
#, fuzzy
#| msgid "Posts"
msgid "Post URL"
msgstr "Записи"

#: assets/js/dist/admin.min.js:272 assets/js/dist/admin.min.js:323
#: assets/js/src/admin.js:269 assets/js/src/admin.js:320
#: includes/class-settings-admin.php:250
#, fuzzy
#| msgid "Custom Fields"
msgid "Custom URL"
msgstr "Произвольные поля"

#: assets/js/dist/admin.min.js:279 assets/js/src/admin.js:276
#: includes/class-settings-admin.php:252 includes/facets/slider.php:156
msgid "Prefix"
msgstr "Префикс"

#: assets/js/dist/admin.min.js:283 assets/js/src/admin.js:280
#: includes/class-settings-admin.php:253 includes/facets/slider.php:166
msgid "Suffix"
msgstr "Суффикс"

#: assets/js/dist/admin.min.js:288 assets/js/src/admin.js:285
#: includes/class-settings-admin.php:254
msgid "Hide item?"
msgstr ""

#: assets/js/dist/admin.min.js:293 assets/js/src/admin.js:290
#: includes/class-settings-admin.php:255
#, fuzzy
#| msgid "Loading"
msgid "Padding"
msgstr "Загрузка"

#: assets/js/dist/admin.min.js:305 assets/js/src/admin.js:302
#: includes/class-settings-admin.php:256
msgid "Unique name"
msgstr ""

#: assets/js/dist/admin.min.js:310 assets/js/src/admin.js:307
#: includes/class-settings-admin.php:257
msgid "CSS class"
msgstr ""

#: assets/js/dist/admin.min.js:316 assets/js/src/admin.js:313
#: includes/class-settings-admin.php:258
msgid "Button border"
msgstr ""

#: assets/js/dist/admin.min.js:322 assets/js/src/admin.js:319
#: includes/class-settings-admin.php:259
msgid "Term URL"
msgstr ""

#: assets/js/dist/admin.min.js:412 includes/class-settings-admin.php:277
#, fuzzy
#| msgid "Enter keywords"
msgid "Enter term slugs"
msgstr "Введите ключевые слова"

#: assets/js/dist/admin.min.js:412 includes/class-settings-admin.php:278
#, fuzzy
#| msgid "Enter keywords"
msgid "Enter values"
msgstr "Введите ключевые слова"

#: assets/js/dist/admin.min.js:616 assets/js/src/admin.js:847
#: includes/class-settings-admin.php:279
msgid "Layout"
msgstr ""

#: assets/js/dist/admin.min.js:647 assets/js/src/admin.js:876
#: includes/class-settings-admin.php:283
msgid "Row"
msgstr ""

#: assets/js/dist/admin.min.js:656 assets/js/src/admin.js:883
#: includes/class-settings-admin.php:284
msgid "Column"
msgstr ""

#: assets/js/dist/admin.min.js:1172 assets/js/src/admin.js:1600
#: includes/class-settings-admin.php:298
msgid "Saving"
msgstr "Сохраняется"

#: assets/js/dist/admin.min.js:1213 assets/js/dist/admin.min.js:1258
#: assets/js/src/admin.js:1636 assets/js/src/admin.js:1677
#: includes/class-settings-admin.php:299
msgid "Indexing"
msgstr "Index"

#: assets/js/dist/admin.min.js:1254 assets/js/src/admin.js:1673
#: includes/class-settings-admin.php:300
msgid "Indexing complete"
msgstr "Индексирование полностью"

#: assets/js/dist/admin.min.js:1316 assets/js/src/admin.js:1729
#: includes/class-settings-admin.php:303
msgid "Copied!"
msgstr ""

#: assets/js/dist/admin.min.js:1319 assets/js/src/admin.js:1732
#: includes/class-settings-admin.php:304
msgid "Press CTRL+C to copy"
msgstr ""

#: assets/js/dist/admin.min.js:1327 assets/js/src/admin.js:1740
#: includes/class-settings-admin.php:305
msgid "Activating"
msgstr "Активация"

#: assets/js/dist/admin.min.js:1384 assets/js/src/admin.js:1789
#: includes/class-settings-admin.php:307
msgid "Stop indexer"
msgstr ""

#: assets/js/dist/admin.min.js:1384 assets/js/src/admin.js:1789
#: includes/class-settings-admin.php:306
msgid "Re-index"
msgstr "Повторное индексирование"

#: assets/js/dist/admin.min.js:1400 assets/js/src/admin.js:1805
#: includes/class-settings-admin.php:308
msgid "Loading"
msgstr "Загрузка"

#: assets/js/dist/admin.min.js:1415 assets/js/src/admin.js:1820
#: includes/class-settings-admin.php:309
msgid "Importing"
msgstr "Импорт"

#: includes/class-ajax.php:224
#, fuzzy
msgid "FacetWP was unable to auto-detect the post listing"
msgstr "FacetWP не удалось автоматически обнаружить список пост"

#: includes/class-ajax.php:253
msgid "Settings saved"
msgstr "Настройки сохранены"

#: includes/class-ajax.php:260
msgid "Error: invalid JSON"
msgstr "Ошибка: недопустимый JSON"

#: includes/class-ajax.php:331
msgid "Done, please re-index"
msgstr ""

#: includes/class-ajax.php:430
msgid "Nothing to import"
msgstr "Нечего импортировать"

#: includes/class-ajax.php:466
msgid "Imported"
msgstr "Импортировано"

#: includes/class-ajax.php:469
msgid "Skipped"
msgstr "Пропущено"

#: includes/class-ajax.php:501
msgid "Error"
msgstr "Ошибка"

#: includes/class-display.php:162
msgid "No results found"
msgstr "Ничего не найдено!"

#: includes/class-helper.php:435 includes/class-settings-admin.php:265
msgid "Posts"
msgstr "Записи"

#: includes/class-helper.php:437 includes/class-settings-admin.php:268
msgid "Post Type"
msgstr "Тип Записи"

#: includes/class-helper.php:438 includes/class-settings-admin.php:269
msgid "Post Date"
msgstr "Дата Записи"

#: includes/class-helper.php:439 includes/class-settings-admin.php:270
msgid "Post Modified"
msgstr "Запись Изменена"

#: includes/class-helper.php:440 includes/class-settings-admin.php:266
msgid "Post Title"
msgstr "Название записи"

#: includes/class-helper.php:441
msgid "Post Author"
msgstr "Автор Записи"

#: includes/class-helper.php:446
msgid "Taxonomies"
msgstr "Таксономии"

#: includes/class-helper.php:451 includes/class-settings-admin.php:272
msgid "Custom Fields"
msgstr "Произвольные поля"

#: includes/class-init.php:138 templates/page-settings.php:92
msgid "Settings"
msgstr "Настройки"

#: includes/class-init.php:170
msgid "integration add-on"
msgstr "дополнение"

#: includes/class-init.php:171
#, fuzzy, php-format
msgid "To use FacetWP with %s, please install the %s, then re-index."
msgstr ""
"Чтобы использовать FacetWP с %s, пожалуйста установки %s, а затем повторное "
"индексирование."

#: includes/class-renderer.php:516 includes/class-renderer.php:633
msgid "of"
msgstr "из"

#: includes/class-renderer.php:550 includes/class-settings-admin.php:264
#: includes/facets/checkboxes.php:326 includes/facets/dropdown.php:141
#: includes/facets/fselect.php:312 includes/facets/hierarchy.php:187
#: includes/facets/radio.php:190
msgid "Sort by"
msgstr "Сортировать по"

#: includes/class-renderer.php:554
msgid "Title (A-Z)"
msgstr "Заголовок (А-Я)"

#: includes/class-renderer.php:561
msgid "Title (Z-A)"
msgstr "Заголовок (Я-А)"

#: includes/class-renderer.php:568
msgid "Date (Newest)"
msgstr "Дата (Сначала Новые)"

#: includes/class-renderer.php:575
msgid "Date (Oldest)"
msgstr "Дата (Сначала Старые)"

#: includes/class-renderer.php:632
msgid "Page"
msgstr "Страница"

#: includes/class-renderer.php:683
msgid "Per page"
msgstr "На страницу"

#: includes/class-settings-admin.php:14
msgid "General"
msgstr "Главные"

#: includes/class-settings-admin.php:17
msgid "License Key"
msgstr "Лицензионный ключ"

#: includes/class-settings-admin.php:21
msgid "Google Maps API Key"
msgstr "Google Maps API Key"

#: includes/class-settings-admin.php:25
msgid "Separators"
msgstr "Сепаратор"

#: includes/class-settings-admin.php:29
msgid "Loading Animation"
msgstr "Загрузка анимации"

#: includes/class-settings-admin.php:31
msgid "Fade"
msgstr "Увядать"

#: includes/class-settings-admin.php:31
msgid "Spin"
msgstr "Вращение"

#: includes/class-settings-admin.php:35
#, fuzzy
#| msgid "Prefix"
msgid "URL Prefix"
msgstr "Префикс"

#: includes/class-settings-admin.php:41
msgid "Debug Mode"
msgstr "Режим отладки"

#: includes/class-settings-admin.php:50
#: includes/integrations/woocommerce/woocommerce.php:48
msgid "WooCommerce"
msgstr "WooCommerce"

#: includes/class-settings-admin.php:53
#, fuzzy
msgid "Support product variations?"
msgstr "Варианты поддержки продукта?"

#: includes/class-settings-admin.php:54
msgid "Enable if your store uses variable products."
msgstr ""

#: includes/class-settings-admin.php:58
#, fuzzy
msgid "Include all products?"
msgstr "Включить все продукты?"

#: includes/class-settings-admin.php:59
msgid "Show facet choices for out-of-stock products?"
msgstr "Показать товары не в наличии?"

#: includes/class-settings-admin.php:65
msgid "Backup"
msgstr "Резервное копирование"

#: includes/class-settings-admin.php:68 includes/class-settings-admin.php:121
msgid "Export"
msgstr "Экспорт"

#: includes/class-settings-admin.php:72 includes/class-settings-admin.php:129
msgid "Import"
msgstr "Импорт"

#: includes/class-settings-admin.php:97
msgid "Activate"
msgstr "Активировать"

#: includes/class-settings-admin.php:103
#, fuzzy
#| msgid "Google Maps API Key"
msgid "Get an API key"
msgstr "Google Maps API Key"

#: includes/class-settings-admin.php:126
msgid "Paste the import code here"
msgstr "Вставьте код импорта здесь"

#: includes/class-settings-admin.php:127
msgid "Overwrite existing items?"
msgstr "Заменить существующие данные?"

#: includes/class-settings-admin.php:189
msgid "Not yet activated"
msgstr "Еще не активировано"

#: includes/class-settings-admin.php:195
msgid "License active"
msgstr "Лицензия активирована"

#: includes/class-settings-admin.php:196
msgid "expires"
msgstr "истекает"

#: includes/class-settings-admin.php:241
msgid "Text"
msgstr ""

#: includes/class-settings-admin.php:242 includes/facets/date_range.php:22
msgid "Date"
msgstr "Дата"

#: includes/class-settings-admin.php:243 includes/facets/number_range.php:22
#, fuzzy
#| msgid "Number Range"
msgid "Number"
msgstr "Выбор в диапазоне Чисел"

#: includes/class-settings-admin.php:251
msgid "Open in new tab?"
msgstr ""

#: includes/class-settings-admin.php:260
msgid "Fetch"
msgstr ""

#: includes/class-settings-admin.php:261
msgid "All post types"
msgstr "Все типы советов"

#: includes/class-settings-admin.php:262
msgid "and show"
msgstr ""

#: includes/class-settings-admin.php:263
#, fuzzy
#| msgid "Per page"
msgid "per page"
msgstr "На страницу"

#: includes/class-settings-admin.php:267
#, fuzzy
#| msgid "Post Date"
msgid "Post Name"
msgstr "Дата Записи"

#: includes/class-settings-admin.php:271
#, fuzzy
#| msgid "Term Order"
msgid "Menu Order"
msgstr "Срок заказа"

#: includes/class-settings-admin.php:273
#, fuzzy
#| msgid "No results"
msgid "Narrow results by"
msgstr "Ничего не найдено"

#: includes/class-settings-admin.php:274
msgid "Hit Enter"
msgstr ""

#: includes/class-settings-admin.php:275
msgid "Add sort"
msgstr ""

#: includes/class-settings-admin.php:276
msgid "Add filter"
msgstr ""

#: includes/class-settings-admin.php:281
msgid "Style"
msgstr ""

#: includes/class-settings-admin.php:282
msgid "Advanced"
msgstr ""

#: includes/class-settings-admin.php:285 includes/facets/autocomplete.php:61
#, fuzzy
#| msgid "Start typing..."
msgid "Start typing"
msgstr "Начните ввод"

#: includes/class-settings-admin.php:286
msgid "Label"
msgstr "Лейбл"

#: includes/class-settings-admin.php:287
msgid "Name"
msgstr "Имя"

#: includes/class-settings-admin.php:288
msgid "Facet type"
msgstr "Тип Фасета"

#: includes/class-settings-admin.php:289
msgid "Copy shortcode"
msgstr ""

#: includes/class-settings-admin.php:290
msgid "Data source"
msgstr "Источник данных"

#: includes/class-settings-admin.php:291
msgid "Switch to advanced mode"
msgstr ""

#: includes/class-settings-admin.php:292
msgid "Switch to visual mode"
msgstr ""

#: includes/class-settings-admin.php:293
#, fuzzy
#| msgid "Display Code"
msgid "Display"
msgstr "Отображаемый код"

#: includes/class-settings-admin.php:294
msgid "Query"
msgstr ""

#: includes/class-settings-admin.php:295
msgid "Help"
msgstr ""

#: includes/class-settings-admin.php:296
msgid "Display Code"
msgstr "Отображаемый код"

#: includes/class-settings-admin.php:297
msgid "Query Arguments"
msgstr "Аргументы Запроса"

#: includes/class-settings-admin.php:301
msgid "Looking"
msgstr ""

#: includes/class-settings-admin.php:302
msgid "Purging"
msgstr ""

#: includes/class-updater.php:107
msgid "Please activate or renew your license for automatic updates."
msgstr ""
"Просьба активировать или обновить лицензию для автоматического обновления."

#: includes/facets/autocomplete.php:10
msgid "Autocomplete"
msgstr "Автозаполнение"

#: includes/facets/autocomplete.php:64 includes/facets/number_range.php:31
msgid "Go"
msgstr ""

#: includes/facets/autocomplete.php:97
msgid "No results"
msgstr "Ничего не найдено"

#: includes/facets/autocomplete.php:157 includes/facets/search.php:75
msgid "Placeholder text"
msgstr "Рыба"

#: includes/facets/checkboxes.php:7
msgid "Checkboxes"
msgstr "Чекбоксы"

#: includes/facets/checkboxes.php:131
msgid "See {num} more"
msgstr "Смотрите {num}"

#: includes/facets/checkboxes.php:132 includes/facets/hierarchy.php:152
msgid "See less"
msgstr "показать меньше"

#: includes/facets/checkboxes.php:237 includes/facets/dropdown.php:127
#: includes/facets/fselect.php:227 includes/facets/radio.php:146
msgid "Parent term"
msgstr "Родительский элемент"

#: includes/facets/checkboxes.php:252 includes/facets/dropdown.php:153
#: includes/facets/fselect.php:253
msgid "Hierarchical"
msgstr "По иерархии"

#: includes/facets/checkboxes.php:255 includes/facets/dropdown.php:156
#: includes/facets/fselect.php:256
msgid "Is this a hierarchical taxonomy?"
msgstr "Это иерархической таксономии?"

#: includes/facets/checkboxes.php:267
#, fuzzy
msgid "Show expanded"
msgstr "Показать расширенные"

#: includes/facets/checkboxes.php:270
msgid "Should child terms be visible by default?"
msgstr "Следует ли видимым по умолчанию дочерних терминов?"

#: includes/facets/checkboxes.php:282 includes/facets/fselect.php:268
#: includes/facets/radio.php:161
msgid "Show ghosts"
msgstr "Показать призраков"

#: includes/facets/checkboxes.php:285 includes/facets/fselect.php:271
#: includes/facets/radio.php:164
msgid "Show choices that would return zero results?"
msgstr "Показать варианты, которые будут возвращать нулевой результат?"

#: includes/facets/checkboxes.php:297 includes/facets/fselect.php:283
#: includes/facets/radio.php:176
msgid "Preserve ghost order"
msgstr "Сохранять порядок призрак"

#: includes/facets/checkboxes.php:300 includes/facets/fselect.php:286
#: includes/facets/radio.php:179
msgid "Keep ghost choices in the same order?"
msgstr "Держать в том же порядке призрак выбор?"

#: includes/facets/checkboxes.php:312 includes/facets/fselect.php:298
msgid "Behavior"
msgstr "Поведение"

#: includes/facets/checkboxes.php:315 includes/facets/fselect.php:301
msgid "How should multiple selections affect the results?"
msgstr "Как множественный выбор должен влиять на результаты?"

#: includes/facets/checkboxes.php:320 includes/facets/fselect.php:306
msgid "Narrow the result set"
msgstr "Ограниченный набор результатов"

#: includes/facets/checkboxes.php:321 includes/facets/fselect.php:307
msgid "Widen the result set"
msgstr "Расширенный набор результатов"

#: includes/facets/checkboxes.php:329 includes/facets/dropdown.php:144
#: includes/facets/fselect.php:315 includes/facets/hierarchy.php:190
#: includes/facets/radio.php:193
msgid "Highest Count"
msgstr "Высокий игр"

#: includes/facets/checkboxes.php:330 includes/facets/dropdown.php:145
#: includes/facets/fselect.php:316 includes/facets/hierarchy.php:191
#: includes/facets/radio.php:194
msgid "Display Value"
msgstr "Отображаемое Значение"

#: includes/facets/checkboxes.php:331 includes/facets/dropdown.php:146
#: includes/facets/fselect.php:317 includes/facets/hierarchy.php:192
#: includes/facets/radio.php:195
msgid "Raw Value"
msgstr "Необработанное Значение"

#: includes/facets/checkboxes.php:332 includes/facets/dropdown.php:147
#: includes/facets/fselect.php:318 includes/facets/hierarchy.php:193
#: includes/facets/radio.php:196
msgid "Term Order"
msgstr "Срок заказа"

#: includes/facets/checkboxes.php:338 includes/facets/dropdown.php:168
#: includes/facets/fselect.php:324 includes/facets/hierarchy.php:199
#: includes/facets/radio.php:202
msgid "Count"
msgstr "Сколько выводить"

#: includes/facets/checkboxes.php:341 includes/facets/dropdown.php:171
#: includes/facets/fselect.php:327 includes/facets/hierarchy.php:202
#: includes/facets/radio.php:205
msgid "The maximum number of facet choices to show"
msgstr ""
"Максимальное количество выбранных вариантов, которые будут отображаться"

#: includes/facets/checkboxes.php:348
msgid "Soft Limit"
msgstr "Мягкий предел"

#: includes/facets/checkboxes.php:351
msgid "Show a toggle link after this many choices"
msgstr "Показать ссылку Переключение после этого много вариантов"

#: includes/facets/date_range.php:7
msgid "Date Range"
msgstr "Выбор в диапазоне Дат"

#: includes/facets/date_range.php:25 includes/facets/date_range.php:162
msgid "Start Date"
msgstr "Дата с"

#: includes/facets/date_range.php:28 includes/facets/date_range.php:163
msgid "End Date"
msgstr "Дата по"

#: includes/facets/date_range.php:113
msgid "Clear"
msgstr "Очистить"

#: includes/facets/date_range.php:131 includes/facets/number_range.php:112
#: includes/facets/slider.php:130
msgid "Other data source"
msgstr "Другой источник данных"

#: includes/facets/date_range.php:134 includes/facets/number_range.php:115
#: includes/facets/slider.php:133
msgid "Use a separate value for the upper limit?"
msgstr "Используйте отдельное значение для верхний предел?"

#: includes/facets/date_range.php:147 includes/facets/number_range.php:139
#: includes/facets/slider.php:146
msgid "Compare type"
msgstr "Сравнить тип"

#: includes/facets/date_range.php:150 includes/facets/number_range.php:142
#: includes/facets/slider.php:149
msgid "Basic"
msgstr "Основной"

#: includes/facets/date_range.php:151 includes/facets/number_range.php:143
msgid "Enclose"
msgstr "Заключать"

#: includes/facets/date_range.php:152 includes/facets/number_range.php:144
#: includes/facets/slider.php:150
msgid "Intersect"
msgstr "пересекаться"

#: includes/facets/date_range.php:157 includes/facets/number_range.php:128
msgid "Fields to show"
msgstr "Поля, чтобы показать"

#: includes/facets/date_range.php:160
msgid "Start + End Dates"
msgstr "Даты начала и окончания"

#: includes/facets/date_range.php:161
msgid "Exact Date"
msgstr "Определенная дата"

#: includes/facets/date_range.php:169
msgid "Display format"
msgstr "Display Format"

#: includes/facets/dropdown.php:7 includes/facets/proximity.php:220
msgid "Dropdown"
msgstr "Выпадающий список"

#: includes/facets/dropdown.php:59 includes/facets/dropdown.php:122
#: includes/facets/fselect.php:119 includes/facets/fselect.php:193
#: includes/facets/fselect.php:222 includes/facets/hierarchy.php:67
msgid "Any"
msgstr "Любой"

#: includes/facets/dropdown.php:113 includes/facets/fselect.php:220
msgid "Default label"
msgstr "По умолчанию метка"

#: includes/facets/fselect.php:7
msgid "fSelect"
msgstr "fSelect"

#: includes/facets/fselect.php:198
msgid "{n} selected"
msgstr "{n} выбран"

#: includes/facets/fselect.php:199 includes/facets/search.php:7
msgid "Search"
msgstr "Поиск"

#: includes/facets/fselect.php:242
msgid "Multi-select?"
msgstr "Множественный выбор?"

#: includes/facets/hierarchy.php:7
msgid "Hierarchy"
msgstr "Иерархия"

#: includes/facets/hierarchy.php:151
msgid "See more"
msgstr "Смотреть еще"

#: includes/facets/number_range.php:7
msgid "Number Range"
msgstr "Выбор в диапазоне Чисел"

#: includes/facets/number_range.php:25 includes/facets/number_range.php:133
msgid "Min"
msgstr "Мин"

#: includes/facets/number_range.php:28 includes/facets/number_range.php:134
msgid "Max"
msgstr "Макс"

#: includes/facets/number_range.php:131
msgid "Min + Max"
msgstr ""

#: includes/facets/number_range.php:132
#, fuzzy
#| msgid "Exact Date"
msgid "Exact"
msgstr "Определенная дата"

#: includes/facets/proximity.php:14
msgid "Proximity"
msgstr "Расстояние"

#: includes/facets/proximity.php:63
msgid "Enter location"
msgstr "Введите Местоположение"

#: includes/facets/proximity.php:177
msgid "Clear location"
msgstr "Очистить местоположение"

#: includes/facets/proximity.php:188
#, fuzzy
msgid "Longitude"
msgstr "Долгота (Longitude)"

#: includes/facets/proximity.php:191
msgid "(Optional) use a separate longitude field"
msgstr ""

#: includes/facets/proximity.php:205
msgid "Unit of measurement"
msgstr "Единица измерения"

#: includes/facets/proximity.php:209
msgid "Miles"
msgstr "мили"

#: includes/facets/proximity.php:210
msgid "Kilometers"
msgstr "Километров"

#: includes/facets/proximity.php:216
msgid "Radius UI"
msgstr ""

#: includes/facets/proximity.php:221 includes/facets/slider.php:7
msgid "Slider"
msgstr "Слайдер"

#: includes/facets/proximity.php:228
msgid "Radius options"
msgstr ""

#: includes/facets/proximity.php:242
#, fuzzy
#| msgid "Slider"
msgid "Slider range"
msgstr "Слайдер"

#: includes/facets/proximity.php:257
#, fuzzy
#| msgid "Default label"
msgid "Default radius"
msgstr "По умолчанию"

#: includes/facets/proximity.php:313
msgid "Distance"
msgstr "Расстояние"

#: includes/facets/radio.php:7
msgid "Radio"
msgstr "Радио"

#: includes/facets/rating.php:7
#, fuzzy
#| msgid "Start typing..."
msgid "Star Rating"
msgstr "Начните ввод"

#: includes/facets/rating.php:110
msgid "& up"
msgstr ""

#: includes/facets/rating.php:111
msgid "Undo"
msgstr ""

#: includes/facets/search.php:19
msgid "Enter keywords"
msgstr "Введите ключевые слова"

#: includes/facets/search.php:64
msgid "Search engine"
msgstr "Поисковик"

#: includes/facets/search.php:67
msgid "WP Default"
msgstr "WP по умолчанию"

#: includes/facets/search.php:80
#, fuzzy
msgid "Auto refresh"
msgstr "Автообновление"

#: includes/facets/search.php:83
msgid "Automatically refresh the results while typing?"
msgstr ""

#: includes/facets/slider.php:20
msgid "Reset"
msgstr "Сброс"

#: includes/facets/slider.php:159
msgid "Text that appears before each slider value"
msgstr "Текст, который появляется перед каждое значение ползунка"

#: includes/facets/slider.php:169
msgid "Text that appears after each slider value"
msgstr "Текст, который появляется после каждого значения ползунка"

#: includes/facets/slider.php:176
msgid "Format"
msgstr "Формат"

#: includes/facets/slider.php:179
msgid "The number format"
msgstr "Числовой формат"

#: includes/facets/slider.php:200
msgid "Step"
msgstr "Шаг"

#: includes/facets/slider.php:203
msgid "The amount of increase between intervals"
msgstr "Размер увеличения между интервалами"

#: includes/integrations/acf/acf.php:250
msgid "Yes"
msgstr "Да"

#: includes/integrations/acf/acf.php:250
msgid "No"
msgstr "Нет"

#: includes/integrations/searchwp/searchwp.php:105
msgid "Default"
msgstr "По умолчанию"

#: includes/integrations/woocommerce/woocommerce.php:50
msgid "Price"
msgstr "Цена"

#: includes/integrations/woocommerce/woocommerce.php:51
msgid "Sale Price"
msgstr "Выгодно!"

#: includes/integrations/woocommerce/woocommerce.php:52
msgid "Regular Price"
msgstr "Цена"

#: includes/integrations/woocommerce/woocommerce.php:53
msgid "Average Rating"
msgstr "Средний рейтинг"

#: includes/integrations/woocommerce/woocommerce.php:54
msgid "Stock Status"
msgstr "Есть ли в наличии"

#: includes/integrations/woocommerce/woocommerce.php:55
#: includes/integrations/woocommerce/woocommerce.php:411
msgid "On Sale"
msgstr "Распродажа"

#: includes/integrations/woocommerce/woocommerce.php:56
msgid "Product Type"
msgstr "Тип товара"

#: includes/integrations/woocommerce/woocommerce.php:403
msgid "In Stock"
msgstr "Есть в наличии"

#: includes/integrations/woocommerce/woocommerce.php:403
msgid "Out of Stock"
msgstr "Нет в наличии"

#: index.php:72
#, fuzzy, php-format
msgid ""
"FacetWP requires PHP %s or above. Please contact your host and request a PHP "
"upgrade."
msgstr ""
"FacetWP требует PHP %s или выше. Пожалуйста свяжитесь ваш хост и запросить "
"обновление PHP."

#: templates/page-settings.php:25
msgid "This facet type has no additional settings."
msgstr "Этот тип facet имеет никаких дополнительных настроек."

#: templates/page-settings.php:91
msgid "Basics"
msgstr "Основы"

#: templates/page-settings.php:93
msgid "Support"
msgstr "Поддержка"

#: templates/page-settings.php:102
msgid "Show indexer stats"
msgstr ""

#: templates/page-settings.php:103
#, fuzzy
#| msgid "All post types"
msgid "Show indexable post types"
msgstr "Все типы советов"

#: templates/page-settings.php:104
msgid "Purge the index table"
msgstr ""

#: templates/page-settings.php:109
#, fuzzy
#| msgid "Save Changes"
msgid "Save changes"
msgstr "Сохранить изменения"

#: templates/page-settings.php:127
msgid "Back"
msgstr "Назад"

#: templates/page-settings.php:134
msgid "Facets"
msgstr "Фасеты"

#: templates/page-settings.php:135 templates/page-settings.php:142
#, fuzzy
#| msgid "Add New"
msgid "Add new"
msgstr "Добавить местоположение"

#: templates/page-settings.php:141
msgid "Templates"
msgstr "Шаблоны"

#~ msgid "Start typing..."
#~ msgstr "Начните ввод"

#~ msgid "Off"
#~ msgstr "Выкл"

#~ msgid "On"
#~ msgstr "Вкл"

#~ msgid "Are you sure?"
#~ msgstr "Вы уверены?"

#~ msgid "Select some items"
#~ msgstr "Выберите некоторые элементы"

#~ msgid "Open query builder"
#~ msgstr "Открыть построитель"

#~ msgid "What goes here?"
#~ msgstr "То, что происходит здесь?"

#~ msgid "Query Builder"
#~ msgstr "Построитель Запросов"

#~ msgid "Which posts would you like to use for the listing?"
#~ msgstr "Какие записи вы хотели бы использовать для перечисления контента ?"

#~ msgid "Send to editor"
#~ msgstr "Отправить редактор"

#, fuzzy
#~| msgid "Min"
#~ msgid "min"
#~ msgstr "Мин"

#, fuzzy
#~| msgid "Max"
#~ msgid "max"
#~ msgstr "Макс"

#~ msgid "Update"
#~ msgstr "Обновить"

#~ msgid "OK"
#~ msgstr "Да"

#~ msgid "Use a separate value for the longitude?"
#~ msgstr "Используйте отдельное значение для долготы?"

#~ msgid "Welcome"
#~ msgstr "Добро пожаловать"

#~ msgid "Welcome to FacetWP"
#~ msgstr "Добро пожаловать на FacetWP"
