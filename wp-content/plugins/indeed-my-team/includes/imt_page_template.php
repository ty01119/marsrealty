<?php
/**
 * Template Name: Indeed My Team
 */
get_header();
?>
<?php

	$str = imt_return_infos_str_for_template(); //str is an array with all info, check functions.php
	global $post;
?>
<div id="main-content" class="main-content">
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<div class="imt_inside_page">
			<?php if(isset($str['photo'][0])){?>
				<div class="imt_item_img">
					<img src="<?php echo $str['photo'][0];?>"/>
				</div>
			<?php }?>
				<div class="imt_item_details">
					<div class="imt_name"><?php echo $str['name'];?></div>
					<div class="imt_job"><?php echo $str['job'];?></div>
					<?php if($str['email'] !='') { ?>
						<div class="imt_email">
							<i class="glyphiconinside-envelope"></i><a href="mailto:<?php echo $str['email'];?>"><?php echo $str['email'];?></a>
						</div>
					<?php } ?>

					<?php if($str['website'] !='') { ?>
						<div class="imt_website">
							<i class="glyphiconinside-globe"></i><a href="http://<?php echo $str['website'];?>" target="_blank"><?php echo $str['website'];?></a>
						</div>
					<?php } ?>
					<?php if($str['tel'] !='') { ?>
						<div class="imt_tel">
							<i class="iconinside-phone"></i><?php echo $str['tel'];?>
						</div>
					<?php } ?>
					<div class="imt_location"><?php echo $str['location'];?></div>
					<?php if(isset($str['skills'])){?>
						<div class="member-skills-wrapper"><?php echo $str['skills'];?></div>
					<?php }?>
					<div class="member-social">
					<?php if($str['social_icon']['fb'] !='') echo '<a href="'.$str['social_icon']['fb'].'" class="facebook"><i class="iconinside-facebook"></i></a>';?>
					<?php if($str['social_icon']['tw'] !='') echo '<a href="'.$str['social_icon']['tw'].'" class="twitter"><i class="iconinside-twitter-2"></i></a>';?>
					<?php if($str['social_icon']['ld'] !='') echo '<a href="'.$str['social_icon']['ld'].'" class="linkedin"><i class="iconinside-linkedin"></i></a>';?>
					<?php if($str['social_icon']['gp'] !='') echo '<a href="'.$str['social_icon']['gp'].'" class="gplus"><i class="iconinside-gplus"></i></a>';?>
					<?php if($str['social_icon']['ins'] !='') echo '<a href="'.$str['social_icon']['ins'].'" class="instagramm"><i class="iconinside-instagramm"></i></a>';?>
					</div>
				</div>
				<div class="imt_clear"></div>
					<div class="imt_description"><?php echo $str['description'];?></div>

				<?php
					#latest posts
					if(isset($str['author_id']) && $str['author_id']!=''){
							$args = array(
									'author' => $str['author_id'],
									'numberposts' => 10,
									'post_status' => 'publish',
									'order' => 'DESC'
							);

							query_posts($args);
							if (have_posts()){
								while (have_posts()){
									the_post();
									?>
									<article class="imt-article">
										<header class="entry-header">
											<h1 class="entry-title">
												<a href="<?php the_permalink();?>"><?php the_title();?></a>
											</h1>
										</header>
										<div class="entry-content"><?php the_content();?></div>
									</article>
									<?php
								}
							}
							wp_reset_postdata();
					}
					#end of latest posts
				?>
				</div>
				<?php
				$str['other_members'] = 1;
					$other_members = get_option('other_members');
					#latest posts
					if (!empty($other_members)){
						$team_members = getMembersByUser($post->ID);
					  	if (isset($team_members['members'])){  ?>
							<div class="imt_inside_team_members_wrapper">
								<div class="imt_inside_team_members_title">
								<?php foreach ($team_members['teams'] as $key=>$team){
									echo $team->name;
									if ($key < count($team_members['teams'])-1) echo ', ';
								}
								?></div>
								<div class="imt_inside_team_members_list">
									<link rel="stylesheet" href="<?php echo IMT_DIR_URL . '/themes/theme_1/style.css'?>" type="text/css" media="all">

									<style>
										@media only screen and (max-width: 479px) {
											#indeed_carousel_view_widget_4425 ul li{
												width: calc(100% - 1px) !important;
											}
										}

										@media only screen and (min-width: 480px) and (max-width: 767px){
											#indeed_carousel_view_widget_4425 ul li{
												width: calc(50% - 1px) !important;
											}
										}
									</style>
									<div class="theme_1 pag-theme1">
										<div class="ict_wrapp">
											<div class="ict_content_cl" id="indeed_carousel_view_widget_4425">
												<ul id="indeed_ul_4425_1" class="">
												<?php
												foreach ($team_members['members'] as $value){
													$post_data = imt_members_post_meta_array_clean($value);
													?>
												  <li style="width: calc(16.6666666667% - 1px)">
														<div class="team-member">
														<div class="member-img">
															<?php
																if (isset($post_data['photo'][0])){
																	?>
																	<a>
																		<img src="<?php echo $post_data['photo'][0];?>"/>
																	</a>
																	<?php
																}
															?>
															 <div class="member-name">
																<?php echo $post_data['name'];?>
																<span><?php echo $post_data['job'];?></span>
															 </div>
														</div>
														<div class="member-desc"><?php echo $post_data['description'];?></div>
														<?php
															if ($post_data['email'] != ''){
																?>
																<div class="member-email"><i class="glyphicont-envelope"></i> <a href="mailto:<?php echo $post_data['email'];?>"><?php echo $post_data['email'];?></a></div>
																<?php
															}

															if ($post_data['website'] != ''){
																?>
																<div class="member-web"><i class="glyphicont-globe"></i> <a href="http://<?php echo $post_data['website'];?>" target="_blank"><?php echo $post_data['website'];?></a></div>
																<?php
															}

															if ($post_data['tel'] != ''){
																?>
																<div class="member-phone"><i class="icont-phone"></i> <?php echo $post_data['tel'];?></div>
																<?php
															}
														?>

														<div class="member-location"><?php echo $post_data['location'];?></div>
														<div class="member-skills-wrapper">
															<?php if(isset($post_data['skills'])){?>
																<div class="member-skills-wrapper"><?php echo $post_data['skills'];?></div>
															<?php }?>
														</div>
														<div class="member-social">
														<?php if($post_data['social_icon']['fb'] !='') echo '<a href="'.$post_data['social_icon']['fb'].'" class="facebook"><i class="icont-facebook"></i></a>';?>
														<?php if($post_data['social_icon']['tw'] !='') echo '<a href="'.$post_data['social_icon']['tw'].'" class="twitter"><i class="icont-twitter-2"></i></a>';?>
														<?php if($post_data['social_icon']['ld'] !='') echo '<a href="'.$post_data['social_icon']['ld'].'" class="linkedin"><i class="icont-linkedin"></i></a>';?>
														<?php if($post_data['social_icon']['gp'] !='') echo '<a href="'.$post_data['social_icon']['gp'].'" class="gplus"><i class="icont-gplus"></i></a>';?>
														<?php if($post_data['social_icon']['ins'] !='') echo '<a href="'.$post_data['social_icon']['ins'].'" class="instagramm"><i class="icont-instagramm"></i></a>';?>
														</div>
														</div>
													</li>
													<?php }?>
													<div class="clear"></div>
													</ul>
												</div>
												<script>
													jQuery('.team-member').find('.fill').css('width','0%');

													jQuery(window).bind('load', function(){
													   jQuery('.team-member').each(function() {
														jQuery(this).find('.fill').each(function() {
																var k = 0, f = jQuery(this), p = f.attr('data-progress-animation'), w = f.width();
																if (w == 0) {
																	p = p.substring(0, p.length - 1);
																	var go = function() {
																		return k >= p || k>= 100 ?( false ) : ( k += 1, f.css('width', k + '%'), setTimeout(go, 1) )
																		};
																	go();
																}
															});
														});
													});

													</script>
												</div>
										</div>

								</div>
							</div>

					<?php
					  }
					}
					?>
		</div><!-- #content -->
	</div><!-- #primary -->
	<?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
