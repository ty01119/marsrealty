<div class="ict_wrap">
<div class="">
    <h1><?php _e('Search Shortcode Generator', 'imt');?></h1>
     <div class="ict_settings_wrapper">
        <div class="box-title">
            <h3><i class="icon-cogs"></i><?php _e('Settings', 'imt');?></h3>
            <div class="actions pointer">
			    <a onclick="jQuery('#the_shc_settings').slideToggle();" class="btn btn-mini content-slideUp">
                    <i class="icon-angle-down"></i>
                </a>
			</div>
            <div class="clear"></div>
        </div>
         <div id="the_shc_settings" class="ict_settings_wrapp">
         	<div class="imt-search-form-admin">
				<table>
		                <tbody>
		                <tr class="imt-table-header-search">
		                    <td>
		                        <b><?php _e('Show', 'imt');?></b>
		                    </td>
		                    <td>
		                        <b><?php _e('Option Name', 'imt');?></b>
		                    </td>
		                    <td>
		                        <b><?php _e('Label', 'imt');?></b>
		                    </td>
		                </tr>
		                <tr>
		                    <td class="imt-td-align-search">
		                        <input type="checkbox" id="search_name" onclick="imt_search_form();" checked="">
		                    </td>
		                    <td style="font-weight:bold;">
		                        <?php _e('Search Name', 'imt');?>
		                    </td>
		                    <td>
		                        <input type="text" value="Name" id="search_name_label" class="imtst_input" onkeyup="imt_search_form();">
		                    </td>
		                </tr>
		                <tr>
		                    <td class="imt-td-align-search">
		                        <input type="checkbox" id="search_cat" onclick="imt_search_form();" checked="">
		                    </td>
		                    <td style="font-weight:bold;">
		                        <?php _e('Search Team', 'imt');?>
		                    </td>
		                    <td>
		                        <input type="text" value="Team" id="search_cat_label" class="imtst_input" onkeyup="imt_search_form();">
		                    </td>
		                </tr>
		                <tr>
		                    <td class="imt-td-align-search">
		                    </td>
		                    <td style="font-weight:bold;">
		                        <?php _e('Search Button', 'imt');?>
		                    </td>
		                    <td>
		                        <input type="text" value="Search" id="search_bttn_label" class="imtst_input" onkeyup="imt_search_form();">
		                    </td>
		                </tr>	                	
		          </tbody>
	          </table> 
	          <div style="margin-top: 10px;">
	          	<b><?php _e('Template:', 'imt');?></b>
	          	<select id="search_template" onChange="imt_search_form();" style="width: 273px;">
	          		<option value="imt-search-form-1">Template 1</option>
	          		<option value="imt-search-form-2">Template 2</option>
	          	</select>
	          </div>        
	          <div style="margin-top: 10px;">
	          	<b><?php _e('Landing Page:', 'imt');?></b>
	          	<?php 
	          		$pages = get_pages();
	          	?>
	          	<select id="landing_page" onChange="imt_search_form();" style="width: 273px;">
	          		<?php 
	          			foreach ($pages as $page){
	          				?>
	          				<option value="<?php echo $page->ID;?>"><?php echo $page->post_title;?></option>
	          				<?php 
	          			}
	          		?>	          		
	          	</select>
	          </div> 	
         	</div>

             <div class="clear"></div>
        </div>
    </div>
</div>
    <div class="shortcode_wrapp">
        <div class="content_shortcode">
            <div>
                <span style="font-weight:bolder; color: #fff; font-style:italic; font-size:13px;">ShortCode : </span>
                <span class="the_shortcode">[imt-search-form]</span>
            </div>
            <div style="margin-top:10px;">
                <span style="font-weight:bolder; color: #fff; font-style:italic; font-size:13px;">PHP Code: </span>
                <span class="php_code">&lt;?php echo do_shortcode("[imt-search-form]");?&gt;</span>
            </div>
        </div>
    </div>
<div class="ict_preview_wrapp">
    <div class="box_title">
        <h2><i class="icon-eyes"></i><?php _e('Preview', 'imt');?></h2>
            <div class="actions_preview pointer">
			    <a onclick="jQuery('#preview').slideToggle();" class="btn btn-mini content-slideUp">
                    <i class="icon-angle-down"></i>
                </a>
			</div>
        <div class="clear"></div>
    </div>
    <div id="preview" class="ict_preview">
	</div>
</div>
<div style="clear:both;"></div>
</div>
<script>
	var imt_site_url = "<?php echo get_site_url();?>";
	var dir_url = "<?php echo IMT_DIR_URL;?>";
	jQuery(document).ready(function(){
		imt_search_form();
	});
</script>