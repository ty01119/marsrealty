<?php
$dir_path = plugin_dir_path (__FILE__);
$style="<style>".file_get_contents( $dir_path.'style.css')."
</style>";
$list_item_template = '
<div class="team-member">
<div class="rowt">
<div class="columnt-photo imt-table-column">
<div class="member-img">
 <a #POST_LINK#><img title="ICT_NAME" src="ICT_IMAGE" alt=""/></a>
 </div>
 </div>
  <div class="columnt-name imt-table-column">
 <div class="member-name">
  <a #POST_LINK#><span>ICT_NAME</span></a>
 </div>
</div>
<div class="columnt-job-title imt-table-column">
  <span>ICT_JOB</span>
</div>
<div class="columnt-description imt-table-column">
ICT_DESCRIPTION
</div>
ICT_EMAIL
ICT_PHONE
ICT_WEBSITE
ICT_LOCATION

<div class="columnt-skills imt-table-column">
<div class="member-skills-wrapper">
    ICT_SKILLS
</div>
</div>
<div class="columnt-social imt-table-column">
<div class="member-social">
  ICT_SOCIAL_MEDIA
</div>
</div>
<div class="imt-clear"></div>
</div>
</div>
';


$details_arr = array(
    'in_team_email' => '<div class="columnt-email imt-table-column"><i class="glyphicont-envelope"></i> <a href="mailto:#EMAIL#">#EMAIL#</a></div>',
    'in_team_telephone' => '<div class="columnt-number-phone imt-table-column"><i class="icont-phone"></i> #PHONE#</div>',
    'in_team_location' => '<div class="imt-table-column">#LOCATION#</div>',
    'in_team_website' => '<div class="imt-table-column"><i class="glyphicont-globe"></i> <a href="http://#WEBSITE#" target="_blank">#WEBSITE#</a></div>'
);
$socials_arr = array(
    'in_team_fb' => '<a href="FB" target="_blank" class="facebook"><i class="icont-facebook"></i></a>',
    'in_team_tw' => '<a href="TW" target="_blank" class="twitter"><i class="icont-twitter-2"></i></a>',
    'in_team_lin' => '<a href="LIN" target="_blank" class="linkedin"><i class="icont-linkedin"></i></a>',
    'in_team_gp' => '<a href="GP" target="_blank" class="gplus"><i class="icont-gplus"></i></a>',
	'in_team_ins' => '<a href="INS" target="_blank" class="instagramm"><i class="icont-instagramm"></i></a>'
);
?>
