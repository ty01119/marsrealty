<?php
/**
 * Team single post template.
 * 
 * @package The7
 * @since 1.0.0
 */

// File Security Check
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<article id="post-<?php the_ID() ?>" <?php post_class() ?>>

	<?php
	do_action( 'presscore_before_post_content' );

  echo "<pre>";

  $theMeta = get_post_meta(get_the_ID());
  echo $theMeta["_dt_teammate_options_position"][0];
  echo "\n";
  echo $theMeta["_dt_teammate_options_website"][0];
  echo "\n";
  echo $theMeta["_dt_teammate_options_mail"][0];
  echo "\n";
  echo $theMeta["_dt_teammate_options_facebook"][0];
  echo "\n";
  echo $theMeta["_dt_teammate_options_twitter"][0];
  echo "\n";
  echo $theMeta["_dt_teammate_options_google"][0];
  echo "\n";
  echo $theMeta["_dt_teammate_options_you-tube"][0];
  echo "\n";

  print_r($theMeta);

  echo "</pre>";

	the_content();

	do_action( 'presscore_after_post_content' );
	?>

</article>
