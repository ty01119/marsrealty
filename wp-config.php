<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mars');

/** MySQL database username */
define('DB_USER', 'mars');

/** MySQL database password */
define('DB_PASSWORD', 'gSjC43Yuy9ZVnk6a');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%;E&lSK*}x/1vH5<f-0g#Z=rc.i-nX~tGO;=6`ma&6[t2!zBq/pNqup+khV[)Oql');
define('SECURE_AUTH_KEY',  '6[ld0VMR#Y+o-_vyzEa0`6`zynl$W$3zZb% MycH&8Z(3$jk|-MwZ[}e+ Z|7y &');
define('LOGGED_IN_KEY',    'IYaf5,5Zy6Q[w:S8nkPMmBa1D!O]f0:*b-Ou7[;>! r*|}mp*}^#s25[#>qxf<(0');
define('NONCE_KEY',        'fhy;PI7J07$REMKZ5km#Id][X_@`MGT7yrJ55V!oznPG,RxMn369 v#gGe(Xz_VU');
define('AUTH_SALT',        '6mDPLAwMo=JrCcgNrPk)U%n:ep:yGnC$xQ`&bBbA/L6;:x|U0oY]0<fO KD5AT=8');
define('SECURE_AUTH_SALT', '-<50^kc>?&lJw7-ss>g]Z`l9mdi``I)T(fd@U~$G5AMx.Ear9VxAf2gR>gsHxJE`');
define('LOGGED_IN_SALT',   'cSwS>Sg]AYmZ3IpPDu.e#Vb!6PJJ<hLPotw6rgLHCL7`1bxib;#7YSyy=kCYiszC');
define('NONCE_SALT',       'JN^dC/*0sn6|z<fb=.J)EK75n!?cxSSl H1O$_6-C3{tH#s(B@Jr$4y8-(LhSH1v');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
